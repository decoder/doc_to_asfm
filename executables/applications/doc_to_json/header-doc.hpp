/**************************************************************************/
/*                                                                        */
/*  Copyright (C) 2019-2020                                               */
/*    CEA (Commissariat a l'Energie Atomique et aux Energies              */
/*         Alternatives)                                                  */
/*                                                                        */
/*  you can redistribute it and/or modify it under the terms of the GNU   */
/*  Lesser General Public License as published by the Free Software       */
/*  Foundation, version 2.1.                                              */
/*                                                                        */
/*  It is distributed in the hope that it will be useful,                 */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU Lesser General Public License for more details.                   */
/*                                                                        */
/*  See the GNU Lesser General Public License version 2.1                 */
/*  for more details (enclosed in the file LICENSE).                      */
/*                                                                        */
/**************************************************************************/

/////////////////////////////////
//
// Application : documentation to json converter
// Unit      : header for state-machine instrumentation
// File      : header_doc.hpp
// Description :
//   header file to read the documentation
//

#pragma once

#include "header-doc.h"

#include <string>

class XMLParserArguments;
class XMLParserStateStack {
  private:
#ifdef INSTR
  public:
#endif
   struct _XMLParserStateStack* _content;
   const struct _XMLParserStateStackFunctions* _functions;

   XMLParserStateStack(struct _XMLParserStateStack* content, const struct _XMLParserStateStackFunctions* functions)
      : _content(content), _functions(functions) {}

   friend void decoder_load_configuration(struct _DocumentConfiguration* config,
         const struct _DocumentConfigurationFunctions* config_functions,
         struct _XMLParserArguments* aarguments,
         const struct _XMLParserArgumentsFunctions* arguments_functions,
         struct _XMLParserStateStack* astate,
         const struct _XMLParserStateStackFunctions* state_functions);
   friend void decoder_load_document(struct _XMLParserArguments* aarguments,
         const struct _XMLParserArgumentsFunctions* arguments_functions,
         struct _XMLParserStateStack* astate,
         const struct _XMLParserStateStackFunctions* state_functions,
         struct _DocumentConfiguration* config,
         const struct _DocumentConfigurationFunctions* config_functions,
         struct _JSonWriter* writer_content, const struct _JSonWriterFunctions* writer_functions);
   friend enum _ReadResult decoder_load_configuration_instr(
         struct _DocumentConfiguration* config,
         const struct _DocumentConfigurationFunctions* config_functions,
         struct _XMLParserArguments* aarguments,
         const struct _XMLParserArgumentsFunctions* arguments_functions,
         struct _XMLParserStateStack* astate,
         const struct _XMLParserStateStackFunctions* state_functions);
   friend enum _ReadResult decoder_load_document_instr(struct _XMLParserArguments* aarguments,
         const struct _XMLParserArgumentsFunctions* arguments_functions,
         struct _XMLParserStateStack* astate,
         const struct _XMLParserStateStackFunctions* state_functions,
         struct _DocumentConfiguration* config,
         const struct _DocumentConfigurationFunctions* config_functions,
         struct _JSonWriter* writer_content, const struct _JSonWriterFunctions* writer_functions);

   bool doesContainAddress(void* pointer) const
      {  return (*_functions->does_contain_address_instr)(_content, pointer); }
   size_t convertAddressToRelative(void* pointer) const
      {  return (*_functions->convert_address_to_relative_instr)(_content, pointer); }
   void* convertRelativeToAddress(size_t shift) const
      {  return (*_functions->convert_relative_to_address_instr)(_content, shift); }

  public:
   void skipNode(XMLParserArguments& arguments);
   void skipNodeContent(XMLParserArguments& arguments);

   template <class Type>
   class TPureStackPointer;
   template <class Type>
   class TStackPointer {
     private:
      size_t ptPointer;
      bool fIsStack;
      friend class TPureStackPointer<Type>;

     public:
      TStackPointer() : ptPointer(0), fIsStack(false) {}
      TStackPointer(XMLParserStateStack& state, Type* pointer=nullptr)
         :  ptPointer(reinterpret_cast<size_t>(pointer)), fIsStack(false)
         {  if (state.doesContainAddress(pointer)) {
               ptPointer = state.convertAddressToRelative(pointer);
               fIsStack = true;
            }
         }
      TStackPointer(const TStackPointer<Type>& source) = default;
      TStackPointer(const TPureStackPointer<Type>& source)
         :  ptPointer(source.ptPointer), fIsStack(true) {}
      TStackPointer<Type>& operator=(const TStackPointer<Type>& source) = default;
      TStackPointer<Type>& operator=(const TPureStackPointer<Type>& source)
         {  ptPointer = source.ptPointer;
            fIsStack = true;
            return *this;
         }

      bool isStackPointer() const { return fIsStack; }
      Type& deref(XMLParserStateStack& state) const
         {  if (!fIsStack)
               return *reinterpret_cast<Type*>(ptPointer);
            else
               return *reinterpret_cast<Type*>(state.convertRelativeToAddress(ptPointer));
         }
      Type* value(XMLParserStateStack& state) const
         {  if (!fIsStack)
               return reinterpret_cast<Type*>(ptPointer);
            else
               return reinterpret_cast<Type*>(state.convertRelativeToAddress(ptPointer));
         }
   };

   template <class Type>
   class TPureStackPointer {
     private:
      size_t ptPointer;
      friend class TStackPointer<Type>;

     public:
      TPureStackPointer() : ptPointer(0) {}
      TPureStackPointer(XMLParserStateStack& state, Type* pointer)
         {  // assert(state.doesContainAddress(pointer));
            ptPointer = state.convertAddressToRelative(pointer);
         }
      TPureStackPointer(const TPureStackPointer<Type>& source) = default;
      TPureStackPointer<Type>& operator=(const TPureStackPointer<Type>& source) = default;

      Type& deref(XMLParserStateStack& state) const
         {  return *reinterpret_cast<Type*>(state.convertRelativeToAddress(ptPointer)); }
      Type* value(XMLParserStateStack& state) const
         {  return reinterpret_cast<Type*>(state.convertRelativeToAddress(ptPointer)); }
   };

   template <class Type> TStackPointer<Type> acquirePointer(Type* pointer)
      {  return TStackPointer<Type>(*this, pointer); }
   template <class Type> TStackPointer<Type> acquirePurePointer(Type* pointer)
      {  return TPureStackPointer<Type>(*this, pointer); }
};

class XMLParserArguments {
  private:
#ifdef INSTR
  public:
#endif
   struct _XMLParserArguments* _content;
   const struct _XMLParserArgumentsFunctions* _functions;

   friend class XMLParserStateStack;
   XMLParserArguments(struct _XMLParserArguments* content, const struct _XMLParserArgumentsFunctions* functions)
      :  _content(content), _functions(functions) {}

   friend void decoder_load_configuration(struct _DocumentConfiguration* config,
         const struct _DocumentConfigurationFunctions* config_functions,
         struct _XMLParserArguments* aarguments,
         const struct _XMLParserArgumentsFunctions* arguments_functions,
         struct _XMLParserStateStack* astate,
         const struct _XMLParserStateStackFunctions* state_functions);
   friend void decoder_load_document(struct _XMLParserArguments* aarguments,
         const struct _XMLParserArgumentsFunctions* arguments_functions,
         struct _XMLParserStateStack* astate,
         const struct _XMLParserStateStackFunctions* state_functions,
         struct _DocumentConfiguration* config,
         const struct _DocumentConfigurationFunctions* config_functions,
         struct _JSonWriter* writer_content, const struct _JSonWriterFunctions* writer_functions);
   friend enum _ReadResult decoder_load_configuration_instr(
         struct _DocumentConfiguration* config,
         const struct _DocumentConfigurationFunctions* config_functions,
         struct _XMLParserArguments* aarguments,
         const struct _XMLParserArgumentsFunctions* arguments_functions,
         struct _XMLParserStateStack* astate,
         const struct _XMLParserStateStackFunctions* state_functions);
   friend enum _ReadResult decoder_load_document_instr(struct _XMLParserArguments* aarguments,
         const struct _XMLParserArgumentsFunctions* arguments_functions,
         struct _XMLParserStateStack* astate,
         const struct _XMLParserStateStackFunctions* state_functions,
         struct _DocumentConfiguration* config,
         const struct _DocumentConfigurationFunctions* config_functions,
         struct _JSonWriter* writer_content, const struct _JSonWriterFunctions* writer_functions);

  public:
   bool hasEvent() const { return (*_functions->has_event)(_content); }
   bool isOpenStartElement() const { return (*_functions->is_open_start_element)(_content); }
   bool isCloseStartElement() const { return (*_functions->is_close_start_element)(_content); }
   bool isAttribute() const { return (*_functions->is_attribute)(_content); }
   bool isOpenAttribute() const { return (*_functions->is_open_attribute)(_content); }
   bool isAddAttribute() const { return (*_functions->is_add_attribute)(_content); }
   bool isEndElement() const { return (*_functions->is_end_element)(_content); }
   bool isCloseElement() const { return (*_functions->is_close_element)(_content); }
   bool isText() const { return (*_functions->is_text)(_content); }
   bool isExtendedText() const { return (*_functions->is_extended_text)(_content); }

   std::string value() const { return (*_functions->get_value)(_content); }
   std::string extendedValue() const { return (*_functions->get_extended_value)(_content); }
   std::string attribute() const { return (*_functions->get_attribute)(_content); }

   void setToNextToken() { return (*_functions->set_to_next_token)(_content); }
};

inline void
XMLParserStateStack::skipNode(XMLParserArguments& arguments)
   {  (*_functions->skip_node)(_content, arguments._content); }
   
inline void
XMLParserStateStack::skipNodeContent(XMLParserArguments& arguments)
   {  (*_functions->skip_node_content)(_content, arguments._content); }

#ifdef INSTR
class ExtStringBuilder;
#endif
class ExtString {
  private:
   const struct _ExtString* _content = nullptr;
#ifdef INSTR
   ExtString(const struct _ExtString* content) : _content(content) {}
   friend class ExtStringBuilder;
#endif

  public:
   ExtString() = default;
   ExtString(const ExtString&) = default;
   ExtString& operator=(const ExtString&) = default;
   ExtString& operator=(const char* source) { return *this; }
   ExtString& operator=(const std::string& source) { return *this; }

   bool isValid() const { return _content; }
   const struct _ExtString* scontent() const { return _content; }

   bool operator==(const ExtString& source) const { return true; }
   bool operator==(const char* source) const { return true; }
   bool operator==(const std::string& source) const { return true; }
   friend bool operator==(const char* first, const ExtString& second) { return true; }
   friend bool operator==(const std::string& first, const ExtString& second) { return true; }
};

#ifdef INSTR
class ExtStringBuilder {
  private:
   struct _DocumentConfiguration* _content;
   const struct _DocumentConfigurationFunctions* _functions;

  public:
   ExtStringBuilder(struct _DocumentConfiguration* content, const struct _DocumentConfigurationFunctions* functions)
      : _content(content), _functions(functions) {}

   ExtString operator=(const char* source) const
      {  return ExtString((*_functions->convert_string)(_content, source)); }
   ExtString operator=(const std::string& source) const
      {  return ExtString((*_functions->convert_string)(_content, source.c_str())); }
};
#endif

class Document;
class DocumentConfiguration {
  private:
#ifdef INSTR
   class RuleResult;
   DocumentConfiguration() : _content(nullptr), _functions(nullptr) {}
   DocumentConfiguration& operator=(const DocumentConfiguration&) = default;
  public:
#endif
   struct _DocumentConfiguration* _content;
   const struct _DocumentConfigurationFunctions* _functions;

  private:
   DocumentConfiguration(struct _DocumentConfiguration* content, const struct _DocumentConfigurationFunctions* functions)
      : _content(content), _functions(functions) {}
   void setKeyValue(const std::string& key, const std::string& value)
      {  (*_functions->set_key_value)(_content, key.c_str(), value.c_str()); }
   friend class Document;

   friend void decoder_load_configuration(struct _DocumentConfiguration* config,
         const struct _DocumentConfigurationFunctions* config_functions,
         struct _XMLParserArguments* aarguments,
         const struct _XMLParserArgumentsFunctions* arguments_functions,
         struct _XMLParserStateStack* astate,
         const struct _XMLParserStateStackFunctions* state_functions);
   friend void decoder_load_document(struct _XMLParserArguments* aarguments,
         const struct _XMLParserArgumentsFunctions* arguments_functions,
         struct _XMLParserStateStack* astate,
         const struct _XMLParserStateStackFunctions* state_functions,
         struct _DocumentConfiguration* config,
         const struct _DocumentConfigurationFunctions* config_functions,
         struct _JSonWriter* writer_content, const struct _JSonWriterFunctions* writer_functions);
   friend enum _ReadResult decoder_load_configuration_instr(
         struct _DocumentConfiguration* config,
         const struct _DocumentConfigurationFunctions* config_functions,
         struct _XMLParserArguments* aarguments,
         const struct _XMLParserArgumentsFunctions* arguments_functions,
         struct _XMLParserStateStack* astate,
         const struct _XMLParserStateStackFunctions* state_functions);
   friend enum _ReadResult decoder_load_document_instr(struct _XMLParserArguments* aarguments,
         const struct _XMLParserArgumentsFunctions* arguments_functions,
         struct _XMLParserStateStack* astate,
         const struct _XMLParserStateStackFunctions* state_functions,
         struct _DocumentConfiguration* config,
         const struct _DocumentConfigurationFunctions* config_functions,
         struct _JSonWriter* writer_content, const struct _JSonWriterFunctions* writer_functions);

#ifdef INSTR
  public:
   static void init_rule_object(void*);
   static void move_rule_object(void*, void*);
   static void copy_rule_object(void*, const void*);
   static void finalize_rule_object(void*);
   static size_t getRuleSize();
   static enum _ReadResult loadInstr(void* configuration, struct _XMLParserArguments* arguments,
         const struct _XMLParserArgumentsFunctions* arguments_functions,
         struct _XMLParserStateStack* state,
         const struct _XMLParserStateStackFunctions* state_functions);
#endif

  public:
   void load(XMLParserArguments& arguments, XMLParserStateStack& state);
   const char* getValue(const std::string& key)
      {  return (*_functions->get_value)(_content, key.c_str()); }
};

class JSonWriter {
#ifdef INSTR
  private:
   JSonWriter() : _content(nullptr), _functions(nullptr) {}
   JSonWriter& operator=(const JSonWriter&) = default;
#endif
  private:
   struct _JSonWriter* _content;
   const struct _JSonWriterFunctions* _functions;

   JSonWriter(struct _JSonWriter* content, const struct _JSonWriterFunctions* functions)
      :  _content(content), _functions(functions) {}
   friend class Document;

  public:
   void start() { (*_functions->start)(_content); }
   void addKey(const std::string& key) { (*_functions->add_key)(_content, key.c_str()); }
   void openArray() { (*_functions->open_array)(_content); }
   void closeArray() { (*_functions->close_array)(_content); }
   void openObject() { (*_functions->open_object)(_content); }
   void closeObject() { (*_functions->close_object)(_content); }
   void addString(const std::string& val) { (*_functions->add_string)(_content, val.c_str()); }
   void addFloat(double val) { (*_functions->add_float)(_content, val); }
   void addIValue(int64_t val) { (*_functions->add_ivalue)(_content, val); }
   void addUValue(uint64_t val) { (*_functions->add_uvalue)(_content, val); }
   void addNullValue() { (*_functions->add_null_value)(_content); }
   void end() { (*_functions->end)(_content); }
};

class Document {
  private:
#ifdef INSTR
   class RuleResult;
   friend class RuleResult;
   Document() {}
   Document& operator=(const Document&) = default;
   static Document& queryDocumentFromRule(RuleResult& rule);
#endif
  public:
   DocumentConfiguration configuration;
   JSonWriter jsonWriter;

  private:
   Document(struct _DocumentConfiguration* config_content,
         const struct _DocumentConfigurationFunctions* config_functions,
         struct _JSonWriter* writer_content, const struct _JSonWriterFunctions* writer_functions)
      :  configuration(config_content, config_functions),
         jsonWriter(writer_content, writer_functions) {}

   friend void decoder_load_configuration(struct _DocumentConfiguration* config,
         const struct _DocumentConfigurationFunctions* config_functions,
         struct _XMLParserArguments* aarguments,
         const struct _XMLParserArgumentsFunctions* arguments_functions,
         struct _XMLParserStateStack* astate,
         const struct _XMLParserStateStackFunctions* state_functions);
   friend void decoder_load_document(struct _XMLParserArguments* aarguments,
         const struct _XMLParserArgumentsFunctions* arguments_functions,
         struct _XMLParserStateStack* astate,
         const struct _XMLParserStateStackFunctions* state_functions,
         struct _DocumentConfiguration* config,
         const struct _DocumentConfigurationFunctions* config_functions,
         struct _JSonWriter* writer_content, const struct _JSonWriterFunctions* writer_functions);
   friend enum _ReadResult decoder_load_configuration_instr(
         struct _DocumentConfiguration* config,
         const struct _DocumentConfigurationFunctions* config_functions,
         struct _XMLParserArguments* aarguments,
         const struct _XMLParserArgumentsFunctions* arguments_functions,
         struct _XMLParserStateStack* astate,
         const struct _XMLParserStateStackFunctions* state_functions);
   friend enum _ReadResult decoder_load_document_instr(struct _XMLParserArguments* aarguments,
         const struct _XMLParserArgumentsFunctions* arguments_functions,
         struct _XMLParserStateStack* astate,
         const struct _XMLParserStateStackFunctions* state_functions,
         struct _DocumentConfiguration* config,
         const struct _DocumentConfigurationFunctions* config_functions,
         struct _JSonWriter* writer_content, const struct _JSonWriterFunctions* writer_functions);

#ifdef INSTR
  public:
   static void init_rule_object(void*);
   static void move_rule_object(void*, void*);
   static void copy_rule_object(void*, const void*);
   static void finalize_rule_object(void*);
   static size_t getRuleSize();
   static enum _ReadResult loadInstr(struct _XMLParserArguments* arguments, const struct _XMLParserArgumentsFunctions* arguments_functions, struct _XMLParserStateStack* state, const struct _XMLParserStateStackFunctions* state_functions);
#endif

  public:
   void load(XMLParserArguments& arguments, XMLParserStateStack& state);
};

