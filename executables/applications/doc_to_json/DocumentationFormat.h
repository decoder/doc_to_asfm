/**************************************************************************/
/*                                                                        */
/*  Copyright (C) 2019-2020                                               */
/*    CEA (Commissariat a l'Energie Atomique et aux Energies              */
/*         Alternatives)                                                  */
/*                                                                        */
/*  you can redistribute it and/or modify it under the terms of the GNU   */
/*  Lesser General Public License as published by the Free Software       */
/*  Foundation, version 2.1.                                              */
/*                                                                        */
/*  It is distributed in the hope that it will be useful,                 */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU Lesser General Public License for more details.                   */
/*                                                                        */
/*  See the GNU Lesser General Public License version 2.1                 */
/*  for more details (enclosed in the file LICENSE).                      */
/*                                                                        */
/**************************************************************************/

/////////////////////////////////
//
// Application : documentation to json converter
// Unit      : main program
// File      : DocumentationFormat.h
// Description :
//   Format of the documentation
//

#pragma once

#include "TString/SString.h"
#include "XML/XMLParser.h"
#include "Dll/dll.h"
#include "JSON/JSonParser.h"
#define DLL_EXPORTS
#include "header-doc.h"
#undef DLL_EXPORTS
#include <map>
#include <iostream>

class Documentation : public STG::Lexer::Base {
  public:
   struct ParserStateFunctions {
      static void set_object_instr(struct _XMLParserState* top_state, void* object);

      static struct _XMLParserStateFunctions functions;
   };
   struct ParserStateStackFunctions {
      static bool skip_node_instr(struct _XMLParserStateStack*, struct _XMLParserArguments* arguments, enum _ReadResult*, void** ruleResult);
      static bool skip_node_content_instr(struct _XMLParserStateStack*, struct _XMLParserArguments* arguments, enum _ReadResult*, void** ruleResult);
      static void* get_rule_result_instr(struct _XMLParserStateStack*);
      static int* get_point_instr(struct _XMLParserStateStack*);
      static bool does_contain_address_instr(struct _XMLParserStateStack*, void* pointer);
      static size_t convert_address_to_relative_instr(struct _XMLParserStateStack*, void* pointer);
      static void* convert_relative_to_address_instr(struct _XMLParserStateStack*, size_t shift);

      static struct _XMLParserStateStackFunctions functions;
   };
   struct ParserArgumentsFunctions {
      static bool has_event(const struct _XMLParserArguments*);
      static bool is_open_start_element(const struct _XMLParserArguments*);
      static bool is_close_start_element(const struct _XMLParserArguments*);
      static bool is_attribute(const struct _XMLParserArguments*);
      static bool is_open_attribute(const struct _XMLParserArguments*);
      static bool is_add_attribute(const struct _XMLParserArguments*);
      static bool is_end_element(const struct _XMLParserArguments*);
      static bool is_close_element(const struct _XMLParserArguments*);
      static bool is_text(const struct _XMLParserArguments*);
      static bool is_extended_text(const struct _XMLParserArguments*);
      static const char* get_value(const struct _XMLParserArguments*);
      static const char* get_extended_value(const struct _XMLParserArguments*);
      static const char* get_attribute(const struct _XMLParserArguments*);
      static void set_to_next_token(struct _XMLParserArguments*);
      static bool set_to_next_token_instr(struct _XMLParserArguments*, enum _ReadResult* result);
      static enum _ReadResult is_equal_value_instr(const struct _XMLParserArguments*, bool* result,
          const struct _ExtString* value);
      static enum _ReadResult is_equal_attribute_instr(const struct _XMLParserArguments*, bool* result,
          const struct _ExtString* value);
      static enum _ReadResult load_value_instr(const struct _XMLParserArguments*);
      static enum _ReadResult load_extended_value_instr(const struct _XMLParserArguments*);
      static enum _ReadResult is_equal_attribute_then_load_value_instr(const struct _XMLParserArguments*,
          bool* result, const struct _ExtString* attribute);
      static enum _ReadResult is_equal_attribute_then_load_extended_value_instr(const struct _XMLParserArguments*,
          bool* result, const struct _ExtString* attribute);
      static enum _ReadResult load_attribute_instr(const struct _XMLParserArguments*);
      static enum _ReadResult load_attribute_and_value_instr(const struct _XMLParserArguments*);
      static enum _ReadResult load_attribute_and_extended_value_instr(const struct _XMLParserArguments*);
      static struct _XMLParserState* shift_result_instr(struct _XMLParserArguments*,
          const struct _XMLParserArgumentsFunctions*, struct _XMLParserStateStack* state,
          const struct _XMLParserStateStackFunctions*, size_t size_result,
          enum _ReadResult (*read_function_instr)(void*, struct _XMLParserArguments*,
             const struct _XMLParserArgumentsFunctions*, struct _XMLParserStateStack*, const struct _XMLParserStateStackFunctions*),
          void (*init_object)(void*), void (*move_object)(void*, void*),
          void (*copy_object)(void*, const void*), void (*finalize_object)(void*), void** ruleResult,
          const struct _XMLParserStateFunctions** functions);
      static void reduce_state_instr(struct _XMLParserArguments*, struct _XMLParserStateStack* state, void** ruleResult);
      static bool parse_token_instr(struct _XMLParserArguments*, struct _XMLParserStateStack* state, enum _ReadResult* result);
      static int compare_value_instr(const struct _XMLParserArguments*, const struct _ExtString* text);
      static int compare_attribute_instr(const struct _XMLParserArguments*, const struct _ExtString* text);

      static struct _XMLParserArgumentsFunctions functions;
   };
   struct JSonWriter : public STG::JSon::BasicWriter {
      STG::DIOObject::OFStream out;
      STG::SubString buffer = STG::SString(5000);
      bool hasEnded = false;

      JSonWriter(const char* outputFilename) : out(outputFilename) {}
      ~JSonWriter()
         {  if (!hasEnded) {
               std::cerr << "bad generation of the output format!" << std::endl;
               out.writeall(buffer.asPersistent());
               out.flush();
            }
         }
   };
   struct JSonWriterFunctions {
      static void start(struct _JSonWriter*);
      static void add_key(struct _JSonWriter*, const char*);
      static void open_array(struct _JSonWriter*);
      static void close_array(struct _JSonWriter*);
      static void open_object(struct _JSonWriter*);
      static void close_object(struct _JSonWriter*);
      static void add_string(struct _JSonWriter*, const char*);
      static void add_float(struct _JSonWriter*, double);
      static void add_ivalue(struct _JSonWriter*, int64_t);
      static void add_uvalue(struct _JSonWriter*, uint64_t);
      static void add_null_value(struct _JSonWriter*);
      static void end(struct _JSonWriter*);

      static struct _JSonWriterFunctions functions;
   };

  public:
   struct Configuration : public STG::IOObject, public STG::Lexer::Base {
     private:
      std::map<std::string, std::string> mMapKeyValues;
      COL::TCopyCollection<COL::TArray<STG::SubString>> assConvertedStrings;

      static void set_key_value(struct _DocumentConfiguration*, const char* key, const char* value);
      static const char* get_value(struct _DocumentConfiguration*, const char* key);
      static const struct _ExtString* convert_string(struct _DocumentConfiguration*, const char* text);

     public:
      // #include "header-doc.inch"
      int (*load_rule)(struct _DocumentRuleFunctions* functions) = nullptr;
      enum _ReadResult (*load_configuration_instr)(
            struct _DocumentConfiguration* config,
            const struct _DocumentConfigurationFunctions* config_functions,
            struct _XMLParserArguments* aarguments,
            const struct _XMLParserArgumentsFunctions* arguments_functions,
            struct _XMLParserStateStack* astate,
            const struct _XMLParserStateStackFunctions* state_functions) = nullptr;
      static struct _DocumentConfigurationFunctions functions;

     public:
      static enum _ReadResult readXML(void* athisConfig, struct _XMLParserArguments* aarguments,
            const struct _XMLParserArgumentsFunctions* arguments_functions, struct _XMLParserStateStack* astate,
            const struct _XMLParserStateStackFunctions* state_functions);
      // ReadResult readXML(STG::XML::CommonParser::State& state, STG::XML::CommonParser::Arguments& arguments)
      //    {  AssumeCondition(load_configuration_instr)
      //       return (ReadResult)(*load_configuration_instr)(
      //          reinterpret_cast<struct _DocumentConfiguration*>(this), &functions,
      //          reinterpret_cast<struct _XMLParserArguments*>(&arguments), &ParserArgumentsFunctions::functions,
      //          reinterpret_cast<struct _XMLParserStateStack*>(&state), &ParserStateStackFunctions::functions);
      //    }
   };

  private:
   JSonWriter jwWriter;
   Configuration cConfiguration;
   DLL::Library dlParserLibrary;
   int (*load_rule)(struct _DocumentRuleFunctions* functions) = nullptr;
   enum _ReadResult (*load_document_instr)(
         struct _XMLParserArguments* aarguments,
         const struct _XMLParserArgumentsFunctions* arguments_functions,
         struct _XMLParserStateStack* astate,
         const struct _XMLParserStateStackFunctions* state_functions,
         struct _DocumentConfiguration* config,
         const struct _DocumentConfigurationFunctions* config_functions,
         struct _JSonWriter* writer_content, const struct _JSonWriterFunctions* writer_functions) = nullptr;

  public:
   Documentation(const char* outputFilename) : jwWriter(outputFilename) {}

   bool setParser(const char* libraryName);
   void setConfiguration(const char* filename);
   void initParser(STG::XML::CommonParser& parser);
   static enum _ReadResult readXML(void* athisDocument, struct _XMLParserArguments* aarguments,
         const struct _XMLParserArgumentsFunctions* arguments_functions, struct _XMLParserStateStack* astate,
         const struct _XMLParserStateStackFunctions* state_functions);
};
