/**************************************************************************/
/*                                                                        */
/*  Copyright (C) 2019                                                    */
/*    CEA (Commissariat a l'Energie Atomique et aux Energies              */
/*         Alternatives)                                                  */
/*                                                                        */
/*  you can redistribute it and/or modify it under the terms of the GNU   */
/*  Lesser General Public License as published by the Free Software       */
/*  Foundation, version 2.1.                                              */
/*                                                                        */
/*  It is distributed in the hope that it will be useful,                 */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU Lesser General Public License for more details.                   */
/*                                                                        */
/*  See the GNU Lesser General Public License version 2.1                 */
/*  for more details (enclosed in the file LICENSE).                      */
/*                                                                        */
/**************************************************************************/

/////////////////////////////////
//
// Application : documentation to json converter
// Unit      : main program
// File      : DocParser.cpp
// Description :
//   Implementation of the parser of word documentation
//

#include <iostream>
#include "DocxParser.h"

bool
DocxParser::setToDocument() {
   const char* contentFile = !fIsOpenDocument ? "word/document.xml" : "content.xml";
   int sizeFilename = strlen(contentFile);
   DDocx::FileHeader header;
   DDocx::EndOfCentralDirectory endOfCentralDirectory;
   DDocx::CentralDirectoryFileHeader centralDirectoryFileHeader;
   size_t posCentralDirectoryFileHeader = 0;
   int currentFileIndex = 0, centralDirectoryFileIndex = 0;
   header.read(ifFile);
   while (ifFile.good()) {
      if (!ifFile.good() || !header.isValid())
         return false;
      int fileNameLength = header.getFileNameLength();
      STG::SString filename;
      if (fileNameLength == sizeFilename) {
         ifFile.read(filename.asPersistent(), sizeFilename, true);
         ifFile.ignore(header.getExtraFieldLength());
         if (filename.compare(contentFile) == CREqual)
            return true;
      }
      else {
         ifFile.read(filename.asPersistent(), fileNameLength, true);
         // ifFile.ignore(fileNameLength);
         ifFile.ignore(header.getExtraFieldLength());
      };
      ++currentFileIndex;
      if ((header.getFlags() & (1U << 3)) && header.getCompressedSize() == 0) {
         size_t pos = ifFile.tellg();
         if (centralDirectoryFileIndex == 0) {
            if (!endOfCentralDirectory.isValid()) {
               ifFile.seekg(-22, std::ios_base::end);
               endOfCentralDirectory.read(ifFile);
               if (!endOfCentralDirectory.isValid()) {
                  ifFile.seekg(pos, std::ios_base::beg);
                  return false;
               }
            }
            posCentralDirectoryFileHeader = endOfCentralDirectory.getCentralDirectoryOffset();
            ++centralDirectoryFileIndex;
         }

         ifFile.seekg(posCentralDirectoryFileHeader, std::ios_base::beg);
         centralDirectoryFileHeader.read(ifFile);
         if (!centralDirectoryFileHeader.isValid()) {
            ifFile.seekg(pos, std::ios_base::beg);
            return false;
         }
         while (centralDirectoryFileIndex < currentFileIndex) {
            int centralFileNameLength = centralDirectoryFileHeader.getFileNameLength();
            ifFile.ignore(centralFileNameLength + centralDirectoryFileHeader.getExtraFieldLength()
                  + centralDirectoryFileHeader.getFileCommentLength());
            ++centralDirectoryFileIndex;
            centralDirectoryFileHeader.read(ifFile);
            if (!centralDirectoryFileHeader.isValid()) {
               ifFile.seekg(pos, std::ios_base::beg);
               return false;
            }
            posCentralDirectoryFileHeader = ifFile.tellg();
         }
         if (centralDirectoryFileHeader.getHeaderFileOffset() + header.getHeaderSize() != pos) {
            ifFile.seekg(pos, std::ios_base::beg);
            return false;
         }
         int centralFileNameLength = centralDirectoryFileHeader.getFileNameLength();
         ++centralDirectoryFileIndex;
         STG::SString centralFilename;
         ifFile.read(centralFilename.asPersistent(), centralFileNameLength, true);
         ifFile.ignore(centralDirectoryFileHeader.getExtraFieldLength()
               + centralDirectoryFileHeader.getFileCommentLength());
         posCentralDirectoryFileHeader = ifFile.tellg();
         if (fileNameLength != centralFileNameLength || filename.compare(centralFilename) != 0) {
            ifFile.seekg(pos, std::ios_base::beg);
            return false;
         }
         header.setCompressedSize(centralDirectoryFileHeader.getCompressedSize());
         header.setUncompressedSize(centralDirectoryFileHeader.getUncompressedSize());
         ifFile.seekg(pos, std::ios_base::beg);
      }
      ifFile.ignore(header.getCompressedSize());
      // static_assert(sizeof(unsigned int) == sizeof(uint32_t));
      header.clear();
      uint32_t dataDescriptorSignature = 0;
      ifFile.read(dataDescriptorSignature, true /* isRaw */);
      if (dataDescriptorSignature == 0x08074b50) {
         ifFile.ignore(UDataDescriptorLength - 4);
         header.read(ifFile);
      }
      else if (dataDescriptorSignature == 0x04034b50) {
         header.setSignature(dataDescriptorSignature);
         header.readAfterSignature(ifFile);
      }
      else
         return false;
   }
   return false;
}

class DocxParser::DecryptionState {
  private:
   enum EncryptedState { ESStreamInit, ESFillEncryptedBuffer, ESEmptyEncryptedBuffer, ESEndEncryptedBuffer };
   enum DecryptedState { DSFillDecryptedBuffer, DSEmptyDecryptedBuffer };
   z_stream zStream;
   STG::SubString sEncryptedBuffer;
   EncryptedState esEncryptedState;
   DecryptedState dsDecryptedState;

  public:
   DecryptionState(int bufferSize)
      :  sEncryptedBuffer(STG::SString(bufferSize)), esEncryptedState(ESStreamInit),
         dsDecryptedState(DSFillDecryptedBuffer)
      {  zStream.zalloc = Z_NULL;
         zStream.zfree = Z_NULL;
         zStream.opaque = Z_NULL;
         zStream.avail_in = 0;
         zStream.next_in = Z_NULL;
      }
   z_stream& getStream() { return zStream; }
   STG::SubString& getEncryptedBuffer() { return sEncryptedBuffer; }

   bool isStreamInit() const { return esEncryptedState == ESStreamInit; }
   bool isEmptyEncryptedBuffer() const { return esEncryptedState == ESEmptyEncryptedBuffer; }
   bool isFillEncryptedBuffer() const { return esEncryptedState == ESFillEncryptedBuffer; }
   bool isEndEncryptedBuffer() const { return esEncryptedState == ESEndEncryptedBuffer; }
   void advanceToFillEncryptedBuffer()
      {  AssumeCondition(esEncryptedState == ESStreamInit || esEncryptedState == ESEmptyEncryptedBuffer)
         esEncryptedState = ESFillEncryptedBuffer;
      }
   void advanceToEmptyEncryptedBuffer()
      {  AssumeCondition(esEncryptedState == ESFillEncryptedBuffer)
         esEncryptedState = ESEmptyEncryptedBuffer;
      }
   void advanceToEndEncryptedBuffer()
      {  AssumeCondition(esEncryptedState == ESFillEncryptedBuffer)
         esEncryptedState = ESEndEncryptedBuffer;
      }

   bool isFillDecryptedBuffer() const { return dsDecryptedState == DSFillDecryptedBuffer; }
   bool isEmptyDecryptedBuffer() const { return dsDecryptedState == DSEmptyDecryptedBuffer; }
   void advanceToEmptyDecryptedBuffer()
      {  AssumeCondition(dsDecryptedState == DSFillDecryptedBuffer)
         dsDecryptedState = DSEmptyDecryptedBuffer;
      }
   void advanceToFillDecryptedBuffer()
      {  AssumeCondition(dsDecryptedState == DSEmptyDecryptedBuffer)
         dsDecryptedState = DSFillDecryptedBuffer;
      }
};

int
DocxParser::inflateBuffer(DecryptionState& state, STG::SubString& decryptedBuffer, bool& isEnd) {
   if (state.isStreamInit()) {
      int result = inflateInit2(&state.getStream(), -MAX_WBITS);
      if (result != Z_OK)
         return result;
      state.advanceToFillEncryptedBuffer();
   }
   else if (!state.isFillDecryptedBuffer())
      state.advanceToFillDecryptedBuffer();

   if (state.getEncryptedBuffer().length() == 0) {
      if (state.isEmptyEncryptedBuffer()) {
         state.getEncryptedBuffer().seek(0,0);
         state.getEncryptedBuffer().clear();
         state.advanceToFillEncryptedBuffer();
      }
      if (state.isFillEncryptedBuffer()) {
         int readChars = ifFile.readsome(state.getEncryptedBuffer().asPersistent(),
               state.getEncryptedBuffer().getPlace());
         state.getStream().avail_in = readChars;
         if (readChars == 0) {
            isEnd = true;
            return Z_OK;
         }
         state.getStream().next_in = (unsigned char*) state.getEncryptedBuffer().getChunk().string;
         if (state.getEncryptedBuffer().getPlace() == 0)
            state.advanceToEmptyEncryptedBuffer();
         else
            state.advanceToEndEncryptedBuffer();
      }
   };

   if (state.isEmptyDecryptedBuffer())
      state.advanceToFillDecryptedBuffer();
   AssumeCondition(decryptedBuffer.getPlace() > 0)
   state.getStream().avail_out = decryptedBuffer.getPlace();
   decryptedBuffer.copy('\0', decryptedBuffer.getPlace());
   state.getStream().next_out = (unsigned char*) decryptedBuffer.getChunk().string;
   int result = inflate(&state.getStream(), Z_SYNC_FLUSH);
   AssumeCondition(result != Z_STREAM_ERROR)  /* state not clobbered */
   switch (result) {
      case Z_NEED_DICT:
         result = Z_DATA_ERROR;    /* and fall through */
         [[fallthrough]];
      case Z_DATA_ERROR:
      case Z_MEM_ERROR:
         decryptedBuffer.setLength(0);
         (void)inflateEnd(&state.getStream());
         return result;
   }
   decryptedBuffer.setLength(state.getStream().total_out);
   state.getEncryptedBuffer().advance(state.getStream().total_in);
   state.getStream().total_out = 0;
   state.getStream().total_in = 0;
   if (decryptedBuffer.getPlace() == 0)
      state.advanceToEmptyDecryptedBuffer();
   isEnd = state.isEndEncryptedBuffer() && result == Z_STREAM_END;
   return Z_OK;
}

void
DocxParser::parse() {
   STG::XML::CommonParser::ReadResult parseResult = STG::XML::CommonParser::RRNeedChars;
   if (!ifFile.good())
      return;
   static const int UBufferSize = 16384;
   STG::SubString decryptedBuffer = STG::SString(UBufferSize);
   // unsigned line = 1, column = 1;
   bool isEnd = false;
   DecryptionState decryptionState(UBufferSize);

   while ((ifFile.good() || decryptionState.getEncryptedBuffer().length() > 0)
         && !isEnd && (parseResult != STG::XML::CommonParser::RRFinished)) {
      if (decryptedBuffer.length() > 0) {
         STG::SubString text(decryptedBuffer);
         decryptedBuffer.seek(0,0);
         decryptedBuffer.copy(text).setToEnd();
         if (inflateBuffer(decryptionState, decryptedBuffer, isEnd) != Z_OK)
            xpParser.error(STG::SString("Docx decoding error"));
         decryptedBuffer.seek(0,0);
      }
      else {
         decryptedBuffer.seek(0,0);
         decryptedBuffer.setLength(0);
         if (inflateBuffer(decryptionState, decryptedBuffer, isEnd) != Z_OK)
            xpParser.error(STG::SString("Docx decoding error"));
      };
      // in.read(decryptedBuffer, decryptedBuffer.getPlace());
      if (decryptedBuffer.length() == 0) {
         xpParser.error(STG::SString("ill formed xml"));
         parseResult = STG::XML::CommonParser::RRFinished;
      }
      else
         parseResult = xpParser.parseChunk(decryptedBuffer /*, line, column */);
   };
   if (isEnd || parseResult == STG::XML::CommonParser::RRFinished)
      (void)inflateEnd(&decryptionState.getStream());
   if ((parseResult != STG::XML::CommonParser::RRFinished) /* && (xpParser.getState().getDocumentLevel() > 0) */)
      xpParser.error(STG::SString("Unexpected end of XML stream"));
   else
      xpParser.endDocument();
}
