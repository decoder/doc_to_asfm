/**************************************************************************/
/*                                                                        */
/*  Copyright (C) 2019-2021                                               */
/*    CEA (Commissariat a l'Energie Atomique et aux Energies              */
/*         Alternatives)                                                  */
/*                                                                        */
/*  you can redistribute it and/or modify it under the terms of the GNU   */
/*  Lesser General Public License as published by the Free Software       */
/*  Foundation, version 2.1.                                              */
/*                                                                        */
/*  It is distributed in the hope that it will be useful,                 */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU Lesser General Public License for more details.                   */
/*                                                                        */
/*  See the GNU Lesser General Public License version 2.1                 */
/*  for more details (enclosed in the file LICENSE).                      */
/*                                                                        */
/**************************************************************************/

/////////////////////////////////
//
// Application : code documentation to json converter
// Unit      : main program
// File      : CodeReader.cpp
// Description :
//   Implementation of the code documentation converter
//

#include "../header-doc.hpp"
#include <cassert>

void
DocumentConfiguration::load(XMLParserArguments& arguments, XMLParserStateStack& state) {
   enum TypeSection
      {  TSInvalid, TSApplication, TSUnit, TSClass, TSFieldsHeader, TSTypes, TSConstants,
         TSFields, TSInvariant, TSItem, TSMacros, TSMethods,
         TSStyle, TSStyleCode = TSStyle, TSStyleVariable, TSStyleLinkField, TSStyleLinkMethod,
         TSStyleLinkType, TSStyleLinkClass, TSStyleLinkMacro, TSStyleLinkConstant, TSTransform,
      } typeSection;
   while (!arguments.isOpenStartElement() || arguments.value() != "doc_to_json") {
      if (arguments.isOpenStartElement())
         state.skipNode(arguments);
      else if (arguments.isCloseElement()) {
         arguments.setToNextToken();
         return;
      }
      else
         arguments.setToNextToken();
   };
   // arguments.isOpenStartElement() && arguments.value() == "doc_to_json"
   arguments.setToNextToken();
   while (!arguments.isCloseStartElement())
      arguments.setToNextToken();
   arguments.setToNextToken();

   while (!arguments.isCloseElement()) {
      if (arguments.isOpenStartElement()) {
         if (arguments.value() == "application")
            typeSection = TSApplication;
         else if (arguments.value() == "unit")
            typeSection = TSUnit;
         else if (arguments.value() == "class")
            typeSection = TSClass;
         else if (arguments.value() == "fields_h")
            typeSection = TSFieldsHeader;
         else if (arguments.value() == "types")
            typeSection = TSTypes;
         else if (arguments.value() == "constants")
            typeSection = TSConstants;
         else if (arguments.value() == "fields")
            typeSection = TSFields;
         else if (arguments.value() == "invariant")
            typeSection = TSInvariant;
         else if (arguments.value() == "item")
            typeSection = TSItem;
         else if (arguments.value() == "macros")
            typeSection = TSMacros;
         else if (arguments.value() == "methods")
            typeSection = TSMethods;
         else if (arguments.value() == "style_code")
            typeSection = TSStyleCode;
         else if (arguments.value() == "style_variable")
            typeSection = TSStyleVariable;
         else if (arguments.value() == "style_link_field")
            typeSection = TSStyleLinkField;
         else if (arguments.value() == "style_link_method")
            typeSection = TSStyleLinkMethod;
         else if (arguments.value() == "style_link_type")
            typeSection = TSStyleLinkType;
         else if (arguments.value() == "style_link_class")
            typeSection = TSStyleLinkClass;
         else if (arguments.value() == "style_link_macro")
            typeSection = TSStyleLinkMacro;
         else if (arguments.value() == "style_link_constant")
            typeSection = TSStyleLinkConstant;
         else if (arguments.value() == "transform")
            typeSection = TSTransform;
         else
            typeSection = TSInvalid;
         arguments.setToNextToken();

         while (arguments.isAttribute()) {
            if (arguments.attribute() == "styleSection") {
               if (typeSection == TSApplication)
                  setKeyValue("applicationName", arguments.value());
               else if (typeSection == TSUnit)
                  setKeyValue("unitName", arguments.value());
               else if (typeSection == TSClass)
                  setKeyValue("className", arguments.value());
               else if (typeSection == TSFieldsHeader)
                  setKeyValue("fieldsHeaderName", arguments.value());
               else if (typeSection == TSTypes)
                  setKeyValue("typesName", arguments.value());
               else if (typeSection == TSConstants)
                  setKeyValue("constantsName", arguments.value());
               else if (typeSection == TSFields)
                  setKeyValue("fieldsName", arguments.value());
               else if (typeSection == TSInvariant)
                  setKeyValue("invariantName", arguments.value());
               else if (typeSection == TSItem)
                  setKeyValue("itemName", arguments.value());
               else if (typeSection == TSMacros)
                  setKeyValue("macrosName", arguments.value());
               else if (typeSection == TSMethods)
                  setKeyValue("methodsName", arguments.value());
               else if (typeSection == TSStyleCode)
                  setKeyValue("codeName", arguments.value());
               else if (typeSection == TSStyleVariable)
                  setKeyValue("variableName", arguments.value());
               else if (typeSection == TSStyleLinkField)
                  setKeyValue("linkFieldName", arguments.value());
               else if (typeSection == TSStyleLinkMethod)
                  setKeyValue("linkMethodName", arguments.value());
               else if (typeSection == TSStyleLinkType)
                  setKeyValue("linkTypeName", arguments.value());
               else if (typeSection == TSStyleLinkClass)
                  setKeyValue("linkClassName", arguments.value());
               else if (typeSection == TSStyleLinkMacro)
                  setKeyValue("linkMacroName", arguments.value());
               else if (typeSection == TSStyleLinkConstant)
                  setKeyValue("linkConstantName", arguments.value());
            }
            else if (arguments.attribute() == "removeSemiColon") {
               if (typeSection == TSTransform)
                  if (arguments.value() == "yes")
                     setKeyValue("doesRemoveSemiColon", "true");
            }
            else if (arguments.attribute() == "addMethodDoc") {
               if (typeSection == TSTransform)
                  if (arguments.value() == "yes")
                     setKeyValue("doesAddMethodDoc", "true");
            }
            arguments.setToNextToken();
         }
         while (!arguments.isCloseStartElement())
            arguments.setToNextToken();
         arguments.setToNextToken();

         while (!arguments.isCloseElement()) {
            if (arguments.isOpenStartElement())
               state.skipNode(arguments);
            else
               arguments.setToNextToken();
         };
      };
      arguments.setToNextToken();
   };
}

enum Header
   {  HUndefined, HLibrary, HUnit, HClass, HTypes, HConstants, HSectionField, HFields, HInvariant,
      HInvariantItem, HMacros, HMethods, HEnd
   };
enum HeaderContent { HCNone, HCArray, HCObject, HCArrayObject };
struct HeaderInfo
   {  Header parent;
      HeaderContent content;
      Header nextBrother;
      Header firstChild, lastChild;
      bool hasCode;
      const char* configurationStyle;
      const char* jsonKey;
   };

HeaderInfo infoHeader[HEnd] =
   {  {  HUndefined, HCNone, HUndefined, HLibrary, HLibrary, false, nullptr, nullptr } /* HUndefined */,
      {  HUndefined, HCObject, HLibrary, HUnit, HMethods, false, "applicationName", "application" } /* HLibrary */,
      {  HLibrary, HCArrayObject, HUnit, HClass, HMethods, false, "unitName", "units" } /* HUnit */,
      {  HUnit, HCArrayObject, HClass, HTypes, HMethods, false, "className", "classes" } /* HClass */,
      {  HClass, HCArrayObject, HConstants, HUndefined, HUndefined, false, "typesName", "types" } /* HTypes */,
      {  HClass, HCArrayObject, HSectionField, HUndefined, HUndefined, false, "constantsName", "constants" } /* HConstants */,
      {  HClass, HCNone, HMacros, HFields, HInvariantItem, false, "fieldsHeaderName", "fieldsHeader" } /* HSectionField */,
      {  HSectionField, HCArrayObject, HFields, HUndefined, HUndefined, false, "fieldsName", "fields" } /* HFields */,
      {  HSectionField, HCNone, HInvariant, HInvariantItem, HInvariantItem, false, "invariantName", "invariant" } /* HInvariant */,
      {  HInvariant, HCArray, HInvariantItem, HUndefined, HUndefined, true, "itemName", "item" } /* HInvariantItem */,
      {  HClass, HCArrayObject, HMethods, HUndefined, HUndefined, false, "macrosName", "macros" } /* HMacros */,
      {  HClass, HCArrayObject, HTypes, HUndefined, HUndefined, false, "methodsName", "methods" } /* HMethods */
   };

bool
closeToHeader(JSonWriter& jsonWriter, Header& currentHeader, Header header)
{  if (currentHeader < header)
      return false;
   while (currentHeader > header) {
      if (infoHeader[currentHeader].content & HCObject)
         jsonWriter.closeObject();
      if (infoHeader[currentHeader].content & HCArray)
         jsonWriter.closeArray();
      currentHeader = infoHeader[currentHeader].parent;
   };
   currentHeader = header;
   return true;
}

bool
closeAllHeaders(JSonWriter& jsonWriter, Header& currentHeader)
{  if (currentHeader == HUndefined)
      return false;
   while (currentHeader != HUndefined) {
      if (infoHeader[currentHeader].content & HCObject)
         jsonWriter.closeObject();
      if (infoHeader[currentHeader].content & HCArray)
         jsonWriter.closeArray();
      currentHeader = infoHeader[currentHeader].parent;
   };
   jsonWriter.end();
   currentHeader = HUndefined;
   return true;
}

enum TextStyle
   {  TSUndefined, TSCode, TSVariable, TSLinkField, TSLinkMethod, TSLinkType, TSLinkClass,
      TSLinkMacro, TSLinkConstant
   };

TextStyle getStyle(const std::string& styleName, DocumentConfiguration& configuration) {
   TextStyle result = TSUndefined;
   if (styleName == configuration.getValue("linkFieldName"))
      result = TSLinkField;
   else if (styleName == configuration.getValue("linkMethodName"))
      result = TSLinkMethod;
   else if (styleName == configuration.getValue("variableName"))
      result = TSVariable;
   else if (styleName == configuration.getValue("codeName"))
      result = TSCode;
   else if (styleName == configuration.getValue("linkTypeName"))
      result = TSLinkType;
   else if (styleName == configuration.getValue("linkClassName"))
      result = TSLinkClass;
   else if (styleName == configuration.getValue("linkMacroName"))
      result = TSLinkMacro;
   else if (styleName == configuration.getValue("linkConstantName"))
      result = TSLinkConstant;
   return result;
}

const char* getTextSymbol(const std::string& symbolValue) {
   const char* result = nullptr;
   if (symbolValue == "F0B3")
      result = ">=";
   if (symbolValue == "F0DB")
      result = "<=>";
   if (symbolValue == "F0B9")
      result = "!=";
   return result;
}

typedef ExtString ArrayOfStrings[HEnd];

void
readStyleParagraph(XMLParserArguments& arguments, XMLParserStateStack& state, Document& document,
      Header& headerType, Header& newHeaderType, bool& isInHeader, bool doesChangeStyle) {
   ArrayOfStrings style;
   if (!doesChangeStyle) {
      assert(arguments.isOpenStartElement()); 
      arguments.setToNextToken();

      // skip xml attributes for header "w:pPr"
      while (not arguments.isCloseStartElement())
         arguments.setToNextToken();
      arguments.setToNextToken();
   }

   newHeaderType = HUndefined; // = point() encoding
   while (doesChangeStyle || not arguments.isCloseElement()) { // w:pPr
      if (doesChangeStyle || arguments.isOpenStartElement()) {
         if (!doesChangeStyle) {
            if (arguments.value() != "w:pStyle") {
               state.skipNode(arguments);
               continue;
            }
            arguments.setToNextToken();
         }
         doesChangeStyle = false;
         while (arguments.isAttribute()) {
            if (arguments.attribute() == "w:val") {
               Header iterHeader = infoHeader[headerType].firstChild;
               if (iterHeader == HUndefined)
                  iterHeader = headerType;
               Header iterHeaderSave = iterHeader;
               bool hasFound = false;
               while (!hasFound && iterHeader != HUndefined) {
                  Header brotherHeader = iterHeader;
                  do {
                     style[brotherHeader] = document.configuration.getValue(
                              infoHeader[brotherHeader].configurationStyle);
                     if (arguments.value() == style[brotherHeader]) {
                        newHeaderType = brotherHeader;
                        isInHeader = infoHeader[brotherHeader].content != HCNone;
                        hasFound = true;
                     }
                     brotherHeader = infoHeader[brotherHeader].nextBrother;
                  } while (!hasFound && brotherHeader != iterHeader);
                  iterHeader = infoHeader[iterHeader].parent;
               };
               if (!hasFound) // unknown style => do not change
                  iterHeader = iterHeaderSave;
            };
            arguments.setToNextToken();
         }
         while (not arguments.isCloseStartElement())
            arguments.setToNextToken();
         arguments.setToNextToken();

         state.skipNodeContent(arguments);
         continue;
      } // end of w:pStyle
      arguments.setToNextToken();
   } // end of w:pPr
   // arguments.isCloseElement()
}

void
readContentParagraph(XMLParserArguments& arguments, XMLParserStateStack& state, Document& document,
      Header& headerType, Header& newHeaderType, std::string& headerTitle,
      bool& isContinueHeaderTitle, bool& isFirstInHeader) {
   assert(arguments.isOpenStartElement()); 
   arguments.setToNextToken();
   while (not arguments.isCloseStartElement())
      arguments.setToNextToken();
   TextStyle style = TSUndefined;
   arguments.setToNextToken();

   while (not arguments.isCloseElement()) { // w:r
      if (arguments.isOpenStartElement()) {
         bool doesSkip = arguments.value() != "w:t";
         bool hasCode = infoHeader[newHeaderType].hasCode;
         bool hasSym = doesSkip && arguments.value() == "w:sym";
         bool hasStyle = false;
         if (hasCode && doesSkip)
            doesSkip = !hasSym && !(hasStyle = arguments.value() == "w:rPr");
         if (doesSkip) {
            state.skipNode(arguments);
            continue;
         }
         if (hasCode) { // arguments.value() == "w:t" || arguments.value() == "w:sym" || arguments.value() == "w:rpr"
            if (hasStyle) {
               // style = loadStyle(arguments, state);
               arguments.setToNextToken();

               // skip xml attributes for header "w:rPr"
               while (not arguments.isCloseStartElement())
                  arguments.setToNextToken();
               arguments.setToNextToken();
               while (not arguments.isCloseElement()) { // w:rPr
                  if (arguments.isOpenStartElement()) {
                     if (arguments.value() != "w:rStyle") {
                        state.skipNode(arguments);
                        continue;
                     }
                     arguments.setToNextToken();

                     while (arguments.isAttribute()) {
                        if (arguments.attribute() == "w:val")
                           style = getStyle(arguments.value(), document.configuration);
                        arguments.setToNextToken();
                     }
                     while (not arguments.isCloseStartElement())
                        arguments.setToNextToken();
                     arguments.setToNextToken();

                     state.skipNodeContent(arguments);
                     continue;
                  } // end of w:rStyle
                  arguments.setToNextToken();
               } // end of w:rPr
               // arguments.isCloseElement()
               arguments.setToNextToken();
               continue;
            }
         };
         // infoHeader[newHeaderType].hasCode
         if (hasSym) {
            bool isSymbolFont = false;
            arguments.setToNextToken();
            while (not arguments.isCloseStartElement()) {
               if (arguments.attribute() == "w:font") {
                  if (arguments.value() == "Symbol")
                     isSymbolFont = true;
               }
               else if (arguments.attribute() == "w:char") {
                  const char* textSymbol = getTextSymbol(arguments.value());
                  if (textSymbol) {
                     if (isSymbolFont) {
                        if (headerType == infoHeader[newHeaderType].parent) {
                           document.jsonWriter.addKey(infoHeader[headerType].jsonKey);
                           if (infoHeader[newHeaderType].content & HCArray)
                              document.jsonWriter.openArray();
                           headerType = newHeaderType;
                           isFirstInHeader = true;
                        }
                        else if (!isContinueHeaderTitle) {
                           if (closeToHeader(document.jsonWriter, headerType, newHeaderType))
                              headerType = newHeaderType;
                           if (infoHeader[headerType].content & HCObject)
                              document.jsonWriter.closeObject();
                        }
                        isContinueHeaderTitle = true;
                        if (style != TSUndefined) {
                           if (headerTitle.size() == 0)
                              headerTitle = textSymbol;
                           else
                              headerTitle += textSymbol;
                        }
                     }
                  }
               }
               arguments.setToNextToken();
            }
            arguments.setToNextToken();
            while (not arguments.isCloseElement()) { // w:sym
               if (arguments.isOpenStartElement()) {
                  state.skipNode(arguments);
                  continue;
               }
               arguments.setToNextToken();
            }
            arguments.setToNextToken();
            continue;
         }
         arguments.setToNextToken();
         while (not arguments.isCloseStartElement())
            arguments.setToNextToken();
         arguments.setToNextToken();

         while (not arguments.isCloseElement()) { // w:t
            if (arguments.isExtendedText()) {
               Header parentHeader = infoHeader[headerType].parent;
               Header header = headerType;
               Header referenceHeader = newHeaderType;
               bool isStart = header == infoHeader[referenceHeader].parent,
                    isReopen = !isFirstInHeader && !isContinueHeaderTitle,
                    isAddText = infoHeader[referenceHeader].content != HCNone,
                    isRestart = header >= infoHeader[
                           infoHeader[referenceHeader].parent].firstChild
                       && header <= infoHeader[
                           infoHeader[referenceHeader].parent].lastChild
                       && header != referenceHeader
                       && !(header >= infoHeader[referenceHeader].firstChild
                            && header <= infoHeader[referenceHeader].lastChild);
               const char* textContent = infoHeader[referenceHeader].jsonKey;
               if (isStart || isRestart) {
                  if (isRestart) {
                     if (closeToHeader(document.jsonWriter, header, parentHeader))
                        headerType = parentHeader;
                  }
                  if (referenceHeader == HLibrary)
                     document.jsonWriter.start();
                  else {
                     if (isAddText) {
                        document.jsonWriter.addKey(textContent);
                        if (infoHeader[referenceHeader].content & HCArray)
                           document.jsonWriter.openArray();
                     }
                  };
                  headerType = newHeaderType;
                  isFirstInHeader = true;
               }
               else if (isReopen) {
                  if (closeToHeader(document.jsonWriter, header, referenceHeader))
                     headerType = referenceHeader;
                  if (infoHeader[referenceHeader].content & HCObject)
                     document.jsonWriter.closeObject();
               }
               isContinueHeaderTitle = true;
               if (isAddText) {
                  if (!infoHeader[referenceHeader].hasCode || style != TSUndefined) {
                     if (headerTitle.length() == 0)
                        headerTitle = arguments.extendedValue();
                     else
                        headerTitle += arguments.extendedValue();
                  }
               };
            }
            arguments.setToNextToken();
         } // end of w:t
      }
      arguments.setToNextToken();
   } // end of w:r
}

void
readParagraph(XMLParserArguments& arguments, XMLParserStateStack& state, Document& document,
      Header& headerType, Header& newHeaderType, bool& isInHeader, std::string& headerTitle,
      bool& isContinueHeaderTitle, bool& isFirstInHeader, bool& hasWrittenName,
      bool& isMethodDocumentation, bool doesChangeStyle) {
   if (!doesChangeStyle) {
      assert(arguments.isOpenStartElement() /* && arguments.value() == "w:p" */);
      arguments.setToNextToken();

      // skip xml attributes for "w:p"
      while (not arguments.isCloseStartElement())
         arguments.setToNextToken();
      arguments.setToNextToken();

      while (not arguments.isCloseElement() && not arguments.isOpenStartElement())
         arguments.setToNextToken();

      // arguments.isCloseElement() || arguments.isOpenStartElement()
      if (arguments.isOpenStartElement()) {
         while (arguments.value() != "w:pPr") {
            state.skipNode(arguments);
            while (not arguments.isOpenStartElement() && not arguments.isCloseElement())
               arguments.setToNextToken();
            if (arguments.isCloseElement())
               break;
         }
      }
   }
   if (doesChangeStyle || arguments.isOpenStartElement()) {
      readStyleParagraph(arguments, state, document, headerType, newHeaderType, isInHeader, 
            doesChangeStyle);
      while (not arguments.isCloseElement()) { // w:p
         if (arguments.isOpenStartElement()) {
            if (arguments.value() != "w:r" || newHeaderType == HUndefined) {
               state.skipNode(arguments);
               continue;
            }
            readContentParagraph(arguments, state, document, headerType, newHeaderType,
                  headerTitle, isContinueHeaderTitle, isFirstInHeader);
         }
         else
            arguments.setToNextToken();
      } // end of w:p
      if (isInHeader) {
         isInHeader = false;
         isFirstInHeader = false;
         if (infoHeader[headerType].content & HCObject) {
            document.jsonWriter.openObject();
            document.jsonWriter.addKey("name");
            if (std::string(document.configuration.getValue("doesRemoveSemiColon")) == "true")
               if (headerTitle[headerTitle.length()-1] == ';')
                  headerTitle.pop_back();
            document.jsonWriter.addString(headerTitle);
         }
         else
            document.jsonWriter.addString(headerTitle);
         headerTitle = std::string();
         isContinueHeaderTitle = false;
         hasWrittenName = true;
      }
   } // end of w:p
   if (headerType != HMethods || !hasWrittenName
         || std::string(document.configuration.getValue("doesAddMethodDoc")) != "true")
      hasWrittenName = false;
   else
      isMethodDocumentation = true;
}

bool
readMethodParagraph(XMLParserArguments& arguments, XMLParserStateStack& state, Document& document,
      Header& headerType, Header& newHeaderType, bool& isInHeader, std::string& headerTitle,
      bool& isContinueHeaderTitle, bool& isFirstInHeader, bool& hasWrittenName,
      bool& isMethodDocumentation) {
   // assert(arguments.isOpenStartElement() && arguments.value() == "w:p");
   arguments.setToNextToken();
   while (not arguments.isCloseStartElement())
      arguments.setToNextToken();
   arguments.setToNextToken();

   while (not arguments.isCloseElement()) { // w:p
      if (arguments.isOpenStartElement()) {
         bool isStyle = arguments.value() == "w:pPr";
         bool isText = !isStyle && arguments.value() == "w:r";
         if (isStyle) {
            arguments.setToNextToken();
            while (!arguments.isCloseStartElement())
               arguments.setToNextToken();
            arguments.setToNextToken();

            while (!arguments.isCloseElement()) { // w:pPr
               if (arguments.isOpenStartElement()) {
                  if (arguments.value() == "w:pStyle") {
                     hasWrittenName = false;
                     if (headerTitle.size() > 0) {
                        document.jsonWriter.addKey("doc");
                        headerTitle.pop_back();
                        document.jsonWriter.addString(headerTitle);
                     }
                     headerTitle = std::string();
                     isContinueHeaderTitle = false;
                     newHeaderType = HUndefined;
                     isMethodDocumentation = false;
                     return false;
                  }
                  else {
                     state.skipNode(arguments);
                     continue;
                  }
               };
               arguments.setToNextToken();
            } // end of w:pPr
         }
         else if (isText) {
            arguments.setToNextToken();
            while (!arguments.isCloseStartElement())
               arguments.setToNextToken();
            arguments.setToNextToken();

            while (!arguments.isCloseElement()) { // w:r
               if (arguments.isOpenStartElement()) {
                  if (arguments.value() != "w:t") {
                     state.skipNode(arguments);
                     continue;
                  }
                  arguments.setToNextToken();

                  while (!arguments.isCloseStartElement())
                     arguments.setToNextToken();
                  arguments.setToNextToken();

                  while (!arguments.isCloseElement()) { // w:t
                     if (arguments.isExtendedText()) {
                        isContinueHeaderTitle = true;
                        if (headerTitle.length() == 0)
                           headerTitle = arguments.extendedValue();
                        else
                           headerTitle += arguments.extendedValue();
                     }
                     arguments.setToNextToken();
                  } // end of w:t
               }
               arguments.setToNextToken();
            } // end of w:r
         }
         else {
            state.skipNode(arguments);
            continue;
         }
      };
      arguments.setToNextToken();
   } // end of w:p
   if (headerTitle.size() > 0)
      headerTitle += ' ';
   return true;
}

void
Document::load(XMLParserArguments& arguments, XMLParserStateStack& state) {
   Header headerType = HUndefined;
   bool isFirstInHeader = false;
   bool isInHeader = false;
   bool hasWrittenName = false;
   std::string headerTitle;
   bool isContinueHeaderTitle = false;

   while (not arguments.isOpenStartElement() && not arguments.isCloseElement())
      arguments.setToNextToken();
   if (arguments.isCloseElement()) {
      arguments.setToNextToken();
      return;
   }
   // arguments.isOpenStartElement()
   while (arguments.value() != "w:document") {
      state.skipNode(arguments);
      while (not arguments.isOpenStartElement() && not arguments.isCloseElement())
         arguments.setToNextToken();
      if (arguments.isCloseElement())
         return;
   }
   arguments.setToNextToken();

   // skip xml attributes for header "w:document"
   while (not arguments.isCloseStartElement())
      arguments.setToNextToken();
   arguments.setToNextToken();

   while (not arguments.isCloseElement()) {
      if (arguments.isOpenStartElement()) {
         if (arguments.value() != "w:body") {
            state.skipNode(arguments);
            continue;
         }
         arguments.setToNextToken();

         // skip xml attributes for header "w:body"
         while (not arguments.isCloseStartElement())
            arguments.setToNextToken();
         arguments.setToNextToken();

         while (not arguments.isCloseElement()) { // w:body
            bool isMethodDocumentation = false;
            Header newHeaderType = HUndefined;
            if (arguments.isOpenStartElement()) {
               if (arguments.value() != "w:p") {
                  state.skipNode(arguments);
                  continue;
               }
               readParagraph(arguments, state, *this, headerType, newHeaderType,
                     isInHeader, headerTitle, isContinueHeaderTitle, isFirstInHeader,
                     hasWrittenName, isMethodDocumentation, false /* doesChangeStyle */);
            }
            else
               arguments.setToNextToken();

            bool hasChangedStyle = false;
            while (isMethodDocumentation && hasWrittenName && !arguments.isCloseElement()) { // w:body
               if (arguments.isOpenStartElement()) {
                  if (arguments.value() != "w:p") {
                     state.skipNode(arguments);
                     continue;
                  }

                  hasChangedStyle = !readMethodParagraph(arguments, state, *this, headerType,
                        newHeaderType, isInHeader, headerTitle, isContinueHeaderTitle,
                        isFirstInHeader, hasWrittenName, isMethodDocumentation);
                  if (hasChangedStyle)
                     readParagraph(arguments, state, *this, headerType, newHeaderType, isInHeader,
                           headerTitle, isContinueHeaderTitle, isFirstInHeader, hasWrittenName,
                           isMethodDocumentation, true /* doesChangeStyle */);
               }
               else
                  arguments.setToNextToken();
            }
            if (!hasChangedStyle && hasWrittenName) {
               hasWrittenName = false;
               if (headerTitle.size() > 0) {
                  jsonWriter.addKey("doc");
                  headerTitle.pop_back();
                  jsonWriter.addString(headerTitle);
               }
               headerTitle = std::string();
               isContinueHeaderTitle = false;
            };
         } // end of w:body
      }
      arguments.setToNextToken();
   } // end of w:document
   closeAllHeaders(jsonWriter, headerType);
}

