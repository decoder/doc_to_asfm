/**************************************************************************/
/*                                                                        */
/*  Copyright (C) 2019                                                    */
/*    CEA (Commissariat a l'Energie Atomique et aux Energies              */
/*         Alternatives)                                                  */
/*                                                                        */
/*  you can redistribute it and/or modify it under the terms of the GNU   */
/*  Lesser General Public License as published by the Free Software       */
/*  Foundation, version 2.1.                                              */
/*                                                                        */
/*  It is distributed in the hope that it will be useful,                 */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU Lesser General Public License for more details.                   */
/*                                                                        */
/*  See the GNU Lesser General Public License version 2.1                 */
/*  for more details (enclosed in the file LICENSE).                      */
/*                                                                        */
/**************************************************************************/

/////////////////////////////////
//
// Application : code documentation to json converter
// Unit      : main program
// File      : CodeReader.cpp
// Description :
//   Implementation of the code documentation converter
//

#include "../header-doc.hpp"

struct DocumentConfiguration::RuleResult {
   enum TypeSection
      {  TSInvalid, TSApplication, TSUnit, TSClass, TSFieldsHeader, TSTypes, TSConstants,
         TSFields, TSInvariant, TSItem, TSMacros, TSMethods,
         TSStyle, TSStyleCode = TSStyle, TSStyleVariable, TSStyleLinkField, TSStyleLinkMethod,
         TSStyleLinkType, TSStyleLinkClass, TSStyleLinkMacro, TSStyleLinkConstant, TSTransform
      } typeSection;
};

void
DocumentConfiguration::init_rule_object(void* object)
   {  new (object) RuleResult(); }
void
DocumentConfiguration::move_rule_object(void* dst, void* src)
   {  *reinterpret_cast<RuleResult*>(dst) = std::move(*reinterpret_cast<RuleResult*>(src)); }
void
DocumentConfiguration::copy_rule_object(void* dst, const void* src)
   {  *reinterpret_cast<RuleResult*>(dst) = *reinterpret_cast<const RuleResult*>(src); }
void
DocumentConfiguration::finalize_rule_object(void* object)
   {  reinterpret_cast<RuleResult*>(object)->~RuleResult(); }

size_t
DocumentConfiguration::getRuleSize() { return sizeof(RuleResult); }

enum _ReadResult
DocumentConfiguration::loadInstr(void* aconfiguration, _XMLParserArguments* aarguments,
      const _XMLParserArgumentsFunctions* arguments_functions, _XMLParserStateStack* astate,
      const _XMLParserStateStackFunctions* state_functions) {
   DocumentConfiguration& configuration = *(DocumentConfiguration*) aconfiguration;
   XMLParserArguments arguments = XMLParserArguments(aarguments, arguments_functions);
   XMLParserStateStack state = XMLParserStateStack(astate, state_functions);
   enum Delimiters
      {  DLabel0, DLabel1, DLabel2, DLabel3, DLabel4, DLabel5, DLabel6,
         DLabel7, DLabel8
      };
   enum _ReadResult result = RRContinue;
   DocumentConfiguration::RuleResult* ruleResult = nullptr;
   static const struct _ExtString* string0 = nullptr;
   static const struct _ExtString* string1 = nullptr;
   static const struct _ExtString* string2 = nullptr;
   static const struct _ExtString* string3 = nullptr;
   static const struct _ExtString* string4 = nullptr;
   static const struct _ExtString* string5 = nullptr;
   static const struct _ExtString* string6 = nullptr;
   static const struct _ExtString* string7 = nullptr;
   static const struct _ExtString* string8 = nullptr;
   static const struct _ExtString* string9 = nullptr;
   static const struct _ExtString* string10 = nullptr;
   static const struct _ExtString* string11 = nullptr;
   static const struct _ExtString* string12 = nullptr;
   static const struct _ExtString* string13 = nullptr;
   static const struct _ExtString* string14 = nullptr;
   static const struct _ExtString* string15 = nullptr;
   static const struct _ExtString* string16 = nullptr;
   static const struct _ExtString* string17 = nullptr;
   static const struct _ExtString* string18 = nullptr;
   static const struct _ExtString* string19 = nullptr;
   static const struct _ExtString* string20 = nullptr;
   static const struct _ExtString* string21 = nullptr;
   static const struct _ExtString* string22 = nullptr;
   static const struct _ExtString* string23 = nullptr;
   static const struct _ExtString* string24 = nullptr;
   static const struct _ExtString* string25 = nullptr;
   bool doesContinue;

   switch (*(*state._functions->get_point_instr)(state._content)) {
      case DLabel0: goto LLabel0;
      case DLabel1: goto LLabel1;
      case DLabel2: goto LLabel2;
      case DLabel3: goto LLabel3;
      case DLabel4: goto LLabel4;
      case DLabel5: goto LLabel5;
      case DLabel6: goto LLabel6;
      case DLabel7: goto LLabel7;
      case DLabel8: goto LLabel8;
   };

LLabel0:
   doesContinue = !arguments.isOpenStartElement();
   if (!doesContinue) {
      if (!string0) string0 = (*configuration._functions->convert_string)(configuration._content, "doc_to_json");
      result = (*arguments._functions->is_equal_value_instr)(arguments._content, &doesContinue, string0);
      if (result == RRNeedChars) return RRNeedChars;
      doesContinue = !doesContinue;
   };
   while (doesContinue) {
      if (arguments.isOpenStartElement()) {
         *(*state._functions->get_point_instr)(state._content) = DLabel3;
         if (!(*state._functions->skip_node_instr)(state._content, arguments._content, &result, (void**) &ruleResult)) return result;
      }
      else if (arguments.isCloseElement()) {
         *(*state._functions->get_point_instr)(state._content) = DLabel1;
         if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
LLabel1:
         (*arguments._functions->reduce_state_instr)(arguments._content, state._content, (void**) &ruleResult);
         return RRFinished;
      }
      else {
         *(*state._functions->get_point_instr)(state._content) = DLabel2;
LLabel2:
         *(*state._functions->get_point_instr)(state._content) = DLabel0;
         if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
      }
      *(*state._functions->get_point_instr)(state._content) = DLabel3;
LLabel3:
      *(*state._functions->get_point_instr)(state._content) = DLabel0;
      doesContinue = !arguments.isOpenStartElement();
      if (doesContinue) {
         result = (*arguments._functions->is_equal_value_instr)(arguments._content, &doesContinue, string0);
         if (result == RRNeedChars) return RRNeedChars;
         doesContinue = !doesContinue;
      };
   }
   *(*state._functions->get_point_instr)(state._content) = DLabel4;
   if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;

LLabel4:
   while (!arguments.isCloseStartElement()) {
      *(*state._functions->get_point_instr)(state._content) = DLabel4;
      if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
   }
   *(*state._functions->get_point_instr)(state._content) = DLabel5;
   if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;

LLabel5:
   while (!arguments.isCloseElement()) {
      if (arguments.isOpenStartElement()) {
         if (!string1) string1 = (*configuration._functions->convert_string)(configuration._content, "application");
         if (!string2) string2 = (*configuration._functions->convert_string)(configuration._content, "unit");
         if (!string3) string3 = (*configuration._functions->convert_string)(configuration._content, "class");
         if (!string4) string4 = (*configuration._functions->convert_string)(configuration._content, "fields_h");
         if (!string5) string5 = (*configuration._functions->convert_string)(configuration._content, "types");
         if (!string6) string6 = (*configuration._functions->convert_string)(configuration._content, "constants");
         if (!string7) string7 = (*configuration._functions->convert_string)(configuration._content, "fields");
         if (!string8) string8 = (*configuration._functions->convert_string)(configuration._content, "invariant");
         if (!string9) string9 = (*configuration._functions->convert_string)(configuration._content, "item");
         if (!string10) string10 = (*configuration._functions->convert_string)(configuration._content, "macros");
         if (!string11) string11 = (*configuration._functions->convert_string)(configuration._content, "methods");
         if (!string12) string12 = (*configuration._functions->convert_string)(configuration._content, "style_code");
         if (!string13) string13 = (*configuration._functions->convert_string)(configuration._content, "style_variable");
         if (!string14) string14 = (*configuration._functions->convert_string)(configuration._content, "style_link_field");
         if (!string15) string15 = (*configuration._functions->convert_string)(configuration._content, "style_link_method");
         if (!string16) string16 = (*configuration._functions->convert_string)(configuration._content, "style_link_type");
         if (!string17) string17 = (*configuration._functions->convert_string)(configuration._content, "style_link_class");
         if (!string18) string18 = (*configuration._functions->convert_string)(configuration._content, "style_link_macro");
         if (!string19) string19 = (*configuration._functions->convert_string)(configuration._content, "style_link_constant");
         if (!string20) string20 = (*configuration._functions->convert_string)(configuration._content, "transform");
         result = (*arguments._functions->load_value_instr)(arguments._content);
         if (result == RRNeedChars) return RRNeedChars;
         if ((*arguments._functions->compare_value_instr)(arguments._content, string1) == 0) {
            if (!ruleResult) ruleResult = (RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
            ruleResult->typeSection = RuleResult::TSApplication;
         }
         else if ((*arguments._functions->compare_value_instr)(arguments._content, string2) == 0) {
            if (!ruleResult) ruleResult = (RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
            ruleResult->typeSection = RuleResult::TSUnit;
         }
         else if ((*arguments._functions->compare_value_instr)(arguments._content, string3) == 0) {
            if (!ruleResult) ruleResult = (RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
            ruleResult->typeSection = RuleResult::TSClass;
         }
         else if ((*arguments._functions->compare_value_instr)(arguments._content, string4) == 0) {
            if (!ruleResult) ruleResult = (RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
            ruleResult->typeSection = RuleResult::TSFieldsHeader;
         }
         else if ((*arguments._functions->compare_value_instr)(arguments._content, string5) == 0) {
            if (!ruleResult) ruleResult = (RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
            ruleResult->typeSection = RuleResult::TSTypes;
         }
         else if ((*arguments._functions->compare_value_instr)(arguments._content, string6) == 0) {
            if (!ruleResult) ruleResult = (RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
            ruleResult->typeSection = RuleResult::TSConstants;
         }
         else if ((*arguments._functions->compare_value_instr)(arguments._content, string7) == 0) {
            if (!ruleResult) ruleResult = (RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
            ruleResult->typeSection = RuleResult::TSFields;
         }
         else if ((*arguments._functions->compare_value_instr)(arguments._content, string8) == 0) {
            if (!ruleResult) ruleResult = (RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
            ruleResult->typeSection = RuleResult::TSInvariant;
         }
         else if ((*arguments._functions->compare_value_instr)(arguments._content, string9) == 0) {
            if (!ruleResult) ruleResult = (RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
            ruleResult->typeSection = RuleResult::TSItem;
         }
         else if ((*arguments._functions->compare_value_instr)(arguments._content, string10) == 0) {
            if (!ruleResult) ruleResult = (RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
            ruleResult->typeSection = RuleResult::TSMacros;
         }
         else if ((*arguments._functions->compare_value_instr)(arguments._content, string11) == 0) {
            if (!ruleResult) ruleResult = (RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
            ruleResult->typeSection = RuleResult::TSMethods;
         }
         else if ((*arguments._functions->compare_value_instr)(arguments._content, string12) == 0) {
            if (!ruleResult) ruleResult = (RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
            ruleResult->typeSection = RuleResult::TSStyleCode;
         }
         else if ((*arguments._functions->compare_value_instr)(arguments._content, string13) == 0) {
            if (!ruleResult) ruleResult = (RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
            ruleResult->typeSection = RuleResult::TSStyleVariable;
         }
         else if ((*arguments._functions->compare_value_instr)(arguments._content, string14) == 0) {
            if (!ruleResult) ruleResult = (RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
            ruleResult->typeSection = RuleResult::TSStyleLinkField;
         }
         else if ((*arguments._functions->compare_value_instr)(arguments._content, string15) == 0) {
            if (!ruleResult) ruleResult = (RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
            ruleResult->typeSection = RuleResult::TSStyleLinkMethod;
         }
         else if ((*arguments._functions->compare_value_instr)(arguments._content, string16) == 0) {
            if (!ruleResult) ruleResult = (RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
            ruleResult->typeSection = RuleResult::TSStyleLinkType;
         }
         else if ((*arguments._functions->compare_value_instr)(arguments._content, string17) == 0) {
            if (!ruleResult) ruleResult = (RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
            ruleResult->typeSection = RuleResult::TSStyleLinkClass;
         }
         else if ((*arguments._functions->compare_value_instr)(arguments._content, string18) == 0) {
            if (!ruleResult) ruleResult = (RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
            ruleResult->typeSection = RuleResult::TSStyleLinkMacro;
         }
         else if ((*arguments._functions->compare_value_instr)(arguments._content, string19) == 0) {
            if (!ruleResult) ruleResult = (RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
            ruleResult->typeSection = RuleResult::TSStyleLinkConstant;
         }
         else if ((*arguments._functions->compare_value_instr)(arguments._content, string20) == 0) {
            if (!ruleResult) ruleResult = (RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
            ruleResult->typeSection = RuleResult::TSTransform;
         }
         else {
            if (!ruleResult) ruleResult = (RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
            ruleResult->typeSection = RuleResult::TSInvalid;
         };
         *(*state._functions->get_point_instr)(state._content) = DLabel6;
         if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;

LLabel6:
         while (arguments.isAttribute()) {
            if (!string21) string21 = (*configuration._functions->convert_string)(configuration._content, "styleSection");
            if (!string22) string22 = (*configuration._functions->convert_string)(configuration._content, "removeSemiColon");
            if (!string23) string23 = (*configuration._functions->convert_string)(configuration._content, "addMethodDoc");
            result = (*arguments._functions->load_attribute_and_value_instr)(arguments._content);
            if (result == RRNeedChars) return RRNeedChars;
            if ((*arguments._functions->compare_attribute_instr)(arguments._content, string21) == 0) {
               if (!ruleResult) ruleResult = (RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
               if (ruleResult->typeSection == RuleResult::TSApplication)
                  (*configuration._functions->set_key_value)(configuration._content, "applicationName", (*arguments._functions->get_value)(arguments._content));
               else if (ruleResult->typeSection == RuleResult::TSUnit)
                  (*configuration._functions->set_key_value)(configuration._content, "unitName", (*arguments._functions->get_value)(arguments._content));
               else if (ruleResult->typeSection == RuleResult::TSClass)
                  (*configuration._functions->set_key_value)(configuration._content, "className", (*arguments._functions->get_value)(arguments._content));
               else if (ruleResult->typeSection == RuleResult::TSFieldsHeader)
                  (*configuration._functions->set_key_value)(configuration._content, "fieldsHeaderName", (*arguments._functions->get_value)(arguments._content));
               else if (ruleResult->typeSection == RuleResult::TSTypes)
                  (*configuration._functions->set_key_value)(configuration._content, "typesName", (*arguments._functions->get_value)(arguments._content));
               else if (ruleResult->typeSection == RuleResult::TSConstants)
                  (*configuration._functions->set_key_value)(configuration._content, "constantsName", (*arguments._functions->get_value)(arguments._content));
               else if (ruleResult->typeSection == RuleResult::TSFields)
                  (*configuration._functions->set_key_value)(configuration._content, "fieldsName", (*arguments._functions->get_value)(arguments._content));
               else if (ruleResult->typeSection == RuleResult::TSInvariant)
                  (*configuration._functions->set_key_value)(configuration._content, "invariantName", (*arguments._functions->get_value)(arguments._content));
               else if (ruleResult->typeSection == RuleResult::TSItem)
                  (*configuration._functions->set_key_value)(configuration._content, "itemName", (*arguments._functions->get_value)(arguments._content));
               else if (ruleResult->typeSection == RuleResult::TSMacros)
                  (*configuration._functions->set_key_value)(configuration._content, "macrosName", (*arguments._functions->get_value)(arguments._content));
               else if (ruleResult->typeSection == RuleResult::TSMethods)
                  (*configuration._functions->set_key_value)(configuration._content, "methodsName", (*arguments._functions->get_value)(arguments._content));
               else if (ruleResult->typeSection == RuleResult::TSStyleCode)
                  (*configuration._functions->set_key_value)(configuration._content, "codeName", (*arguments._functions->get_value)(arguments._content));
               else if (ruleResult->typeSection == RuleResult::TSStyleVariable)
                  (*configuration._functions->set_key_value)(configuration._content, "variableName", (*arguments._functions->get_value)(arguments._content));
               else if (ruleResult->typeSection == RuleResult::TSStyleLinkField)
                  (*configuration._functions->set_key_value)(configuration._content, "linkFieldName", (*arguments._functions->get_value)(arguments._content));
               else if (ruleResult->typeSection == RuleResult::TSStyleLinkMethod)
                  (*configuration._functions->set_key_value)(configuration._content, "linkMethodName", (*arguments._functions->get_value)(arguments._content));
               else if (ruleResult->typeSection == RuleResult::TSStyleLinkType)
                  (*configuration._functions->set_key_value)(configuration._content, "linkTypeName", (*arguments._functions->get_value)(arguments._content));
               else if (ruleResult->typeSection == RuleResult::TSStyleLinkClass)
                  (*configuration._functions->set_key_value)(configuration._content, "linkClassName", (*arguments._functions->get_value)(arguments._content));
               else if (ruleResult->typeSection == RuleResult::TSStyleLinkMacro)
                  (*configuration._functions->set_key_value)(configuration._content, "linkMacroName", (*arguments._functions->get_value)(arguments._content));
               else if (ruleResult->typeSection == RuleResult::TSStyleLinkConstant)
                  (*configuration._functions->set_key_value)(configuration._content, "linkConstantName", (*arguments._functions->get_value)(arguments._content));
            }
            else if ((*arguments._functions->compare_attribute_instr)(arguments._content, string22) == 0) {
               if (!ruleResult) ruleResult = (RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
               if (ruleResult->typeSection == RuleResult::TSTransform) {
                  if (!string24) string24 = (*configuration._functions->convert_string)(configuration._content, "yes");
                  if ((*arguments._functions->compare_value_instr)(arguments._content, string24) == 0)
                     (*configuration._functions->set_key_value)(configuration._content, "doesRemoveSemiColon", "true");
               }
            }
            else if ((*arguments._functions->compare_attribute_instr)(arguments._content, string23) == 0) {
               if (!ruleResult) ruleResult = (RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
               if (ruleResult->typeSection == RuleResult::TSTransform) {
                  if (!string25) string25 = (*configuration._functions->convert_string)(configuration._content, "yes");
                  if ((*arguments._functions->compare_value_instr)(arguments._content, string25) == 0)
                     (*configuration._functions->set_key_value)(configuration._content, "doesAddMethodDoc", "true");
               }
            }
            *(*state._functions->get_point_instr)(state._content) = DLabel6;
            if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
         }
         *(*state._functions->get_point_instr)(state._content) = DLabel7;
LLabel7:
         while (!arguments.isCloseStartElement()) {
            // *(*state._functions->get_point_instr)(state._content) = DLabel7;
            if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
         }
         *(*state._functions->get_point_instr)(state._content) = DLabel8;
         arguments.setToNextToken();

LLabel8:
         while (!arguments.isCloseElement()) {
            if (arguments.isOpenStartElement()) {
               *(*state._functions->get_point_instr)(state._content) = DLabel8;
               if (!(*state._functions->skip_node_instr)(state._content, arguments._content, &result, (void**) &ruleResult)) return result;
            }
            else {
               *(*state._functions->get_point_instr)(state._content) = DLabel8;
               if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
            }
         };
      };
      *(*state._functions->get_point_instr)(state._content) = DLabel5;
      if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
   };
   (*arguments._functions->reduce_state_instr)(arguments._content, state._content, (void**) &ruleResult);
   return RRFinished;
}

void
DocumentConfiguration::load(XMLParserArguments& arguments, XMLParserStateStack& state)
   {  loadInstr(this, arguments._content, arguments._functions, state._content, state._functions); }

enum Header
   {  HUndefined, HLibrary, HUnit, HClass, HTypes, HConstants, HSectionField, HFields, HInvariant,
      HInvariantItem, HMacros, HMethods
   };

bool
setToNewHeader(JSonWriter& jsonWriter, Header& currentHeader, Header header)
{  while (currentHeader >= header) {
      if (currentHeader >= HUnit) {
         if (currentHeader != HSectionField && currentHeader != HInvariant) {
            if (currentHeader != HInvariantItem)
               jsonWriter.closeObject();
            jsonWriter.closeArray();
         }
      }
      else
         jsonWriter.closeObject();
      if (currentHeader == HInvariantItem)
         currentHeader = HInvariant;
      else if (currentHeader == HFields || currentHeader == HInvariant)
         currentHeader = HSectionField;
      else if (currentHeader <= HClass)
         currentHeader = (Header) (currentHeader-1);
      else
         currentHeader = HClass;
   };
   if (header != HSectionField && header != HInvariant)
      jsonWriter.openObject();
   return true;
}

bool
closeToHeader(JSonWriter& jsonWriter, Header& currentHeader, Header header)
{  if (currentHeader < header)
      return false;
   while (currentHeader > header) {
      if (currentHeader >= HUnit) {
         if (currentHeader != HSectionField && currentHeader != HInvariant) {
            if (currentHeader != HInvariantItem)
               jsonWriter.closeObject();
            jsonWriter.closeArray();
         }
      }
      else
         jsonWriter.closeObject();
      if (currentHeader == HInvariantItem)
         currentHeader = HInvariant;
      else if (currentHeader == HFields || currentHeader == HInvariant)
         currentHeader = HSectionField;
      else if (currentHeader <= HClass)
         currentHeader = (Header) (currentHeader-1);
      else
         currentHeader = HClass;
   };
   currentHeader = header;
   return true;
}

bool
closeAllHeaders(JSonWriter& jsonWriter, Header& currentHeader)
{  if (currentHeader == HUndefined)
      return false;
   while (currentHeader != HUndefined) {
      if (currentHeader >= HUnit) {
         if (currentHeader != HSectionField && currentHeader != HInvariant) {
            if (currentHeader != HInvariantItem)
               jsonWriter.closeObject();
            jsonWriter.closeArray();
         }
      }
      else
         jsonWriter.closeObject();
      if (currentHeader == HInvariantItem)
         currentHeader = HInvariant;
      else if (currentHeader == HFields || currentHeader == HInvariant)
         currentHeader = HSectionField;
      else if (currentHeader <= HClass)
         currentHeader = (Header) (currentHeader-1);
      else
         currentHeader = HClass;
   };
   jsonWriter.end();
   currentHeader = HUndefined;
   return true;
}

enum TextStyle
   {  TSUndefined, TSCode, TSVariable, TSLinkField, TSLinkMethod, TSLinkType, TSLinkClass,
      TSLinkMacro, TSLinkConstant
   };

struct Document::RuleResult {
   Document document;
   Header headerType = HUndefined;
   bool isFirstInHeader = false;
   bool isInHeader = false;
   bool hasWrittenName = false;
   std::string headerTitle;
   bool isContinueHeaderTitle = false;
   Header newHeaderType_Label18_Label36 = HUndefined;
   bool isMethodDocumentation_Label11_Label45 = false;
   TextStyle style_Label25_Label35 = TSUndefined;
   bool isSymbolFont_Label32_Label35 = false;
};

void
Document::init_rule_object(void* object)
   {  new (object) RuleResult(); }
void
Document::move_rule_object(void* dst, void* src)
   {  *reinterpret_cast<RuleResult*>(dst) = std::move(*reinterpret_cast<RuleResult*>(src)); }
void
Document::copy_rule_object(void* dst, const void* src)
   {  *reinterpret_cast<RuleResult*>(dst) = *reinterpret_cast<const RuleResult*>(src); }
void
Document::finalize_rule_object(void* object)
   {  reinterpret_cast<RuleResult*>(object)->~RuleResult(); }

size_t
Document::getRuleSize() { return sizeof(RuleResult); }

enum _ReadResult
Document::loadInstr(_XMLParserArguments* aarguments,
      const _XMLParserArgumentsFunctions* arguments_functions, _XMLParserStateStack* astate,
      const _XMLParserStateStackFunctions* state_functions) {
   XMLParserArguments arguments = XMLParserArguments(aarguments, arguments_functions);
   XMLParserStateStack state = XMLParserStateStack(astate, state_functions);
   enum Delimiters
      {  DLabel0, DLabel1, DLabel2, DLabel3, DLabel4, DLabel5, DLabel6, DLabel7,
         DLabel8, DLabel9, DLabel10, DLabel11, DLabel12, DLabel13, DLabel14, DLabel15,
         DLabel16, DLabel17, DLabel18, DLabel19, DLabel20, DLabel21, DLabel22, DLabel23,
         DLabel24, DLabel25, DLabel26, DLabel27, DLabel28, DLabel29, DLabel30, DLabel31,
         DLabel32, DLabel33, DLabel34, DLabel35, DLabel36, DLabel37, DLabel38, DLabel39,
         DLabel40, DLabel41, DLabel42, DLabel43, DLabel44, DLabel45
      };
   enum _ReadResult result = RRContinue;
   Document::RuleResult* ruleResult = nullptr;
   static const struct _ExtString* string0 = nullptr;
   static const struct _ExtString* string1 = nullptr;
   static const struct _ExtString* string2 = nullptr;
   static const struct _ExtString* string3 = nullptr;
   static const struct _ExtString* string4 = nullptr;
   static const struct _ExtString* string5 = nullptr;
   static const struct _ExtString* string6 = nullptr;
   static const struct _ExtString* string7 = nullptr;
   static const struct _ExtString* string8 = nullptr;
   static const struct _ExtString* string9 = nullptr;
   static const struct _ExtString* string10 = nullptr;
   static const struct _ExtString* string11 = nullptr;
   static const struct _ExtString* string12 = nullptr;
   static const struct _ExtString* string13 = nullptr;
   static const struct _ExtString* string14 = nullptr;
   static const struct _ExtString* string15 = nullptr;
   static const struct _ExtString* string16 = nullptr;
   static const struct _ExtString* string17 = nullptr;
   static const struct _ExtString* string18 = nullptr;
   static const struct _ExtString* string19 = nullptr;
   static const struct _ExtString* string20 = nullptr;
   static const struct _ExtString* string21 = nullptr;
   static const struct _ExtString* string22 = nullptr;
   static const struct _ExtString* string23 = nullptr;
   static const struct _ExtString* string24 = nullptr;
   static const struct _ExtString* string25 = nullptr;
   static const struct _ExtString* string26 = nullptr;
   static const struct _ExtString* string27 = nullptr;
   static const struct _ExtString* string28 = nullptr;
   static const struct _ExtString* string29 = nullptr;
   static const struct _ExtString* string30 = nullptr;
   static const struct _ExtString* string31 = nullptr;
   static const struct _ExtString* string32 = nullptr;
   static const struct _ExtString* string33 = nullptr;
   static const struct _ExtString* string34 = nullptr;
   static const struct _ExtString* string35 = nullptr;
   static const struct _ExtString* string36 = nullptr;
   static const struct _ExtString* string37 = nullptr;
   static const struct _ExtString* string38 = nullptr;
   static const struct _ExtString* string39 = nullptr;
   static const struct _ExtString* string40 = nullptr;
   static const struct _ExtString* string41 = nullptr;
   bool doesContinue;

   switch (*(*state._functions->get_point_instr)(state._content)) {
      case DLabel0: goto LLabel0;
      case DLabel1: goto LLabel1;
      case DLabel2: goto LLabel2;
      case DLabel3: goto LLabel3;
      case DLabel4: goto LLabel4;
      case DLabel5: goto LLabel5;
      case DLabel6: goto LLabel6;
      case DLabel7: goto LLabel7;
      case DLabel8: goto LLabel8;
      case DLabel9: goto LLabel9;
      case DLabel10: goto LLabel10;
      case DLabel11: goto LLabel11;
      case DLabel12: goto LLabel12;
      case DLabel13: goto LLabel13;
      case DLabel14: goto LLabel14;
      case DLabel15: goto LLabel15;
      case DLabel16: goto LLabel16;
      case DLabel17: goto LLabel17;
      case DLabel18: goto LLabel18;
      case DLabel19: goto LLabel19;
      case DLabel20: goto LLabel20;
      case DLabel21: goto LLabel21;
      case DLabel22: goto LLabel22;
      case DLabel23: goto LLabel23;
      case DLabel24: goto LLabel24;
      case DLabel25: goto LLabel25;
      case DLabel26: goto LLabel26;
      case DLabel27: goto LLabel27;
      case DLabel28: goto LLabel28;
      case DLabel29: goto LLabel29;
      case DLabel30: goto LLabel30;
      case DLabel31: goto LLabel31;
      case DLabel32: goto LLabel32;
      case DLabel33: goto LLabel33;
      case DLabel34: goto LLabel34;
      case DLabel35: goto LLabel35;
      case DLabel36: goto LLabel36;
      case DLabel37: goto LLabel37;
      case DLabel38: goto LLabel38;
      case DLabel39: goto LLabel39;
      case DLabel40: goto LLabel40;
      case DLabel41: goto LLabel41;
      case DLabel42: goto LLabel42;
      case DLabel43: goto LLabel43;
      case DLabel44: goto LLabel44;
      case DLabel45: goto LLabel45;
   };

LLabel0:
   while (not arguments.isOpenStartElement() && not arguments.isCloseElement()) {
      // *(*state._functions->get_point_instr)(state._content) = DLabel0;
      if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
   }
   *(*state._functions->get_point_instr)(state._content) = DLabel1;
LLabel1:
   if (arguments.isCloseElement()) {
      *(*state._functions->get_point_instr)(state._content) = DLabel2;
      if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
LLabel2:
      (*arguments._functions->reduce_state_instr)(arguments._content, state._content, (void**) &ruleResult);
      return RRFinished;
   }
   *(*state._functions->get_point_instr)(state._content) = DLabel3;
LLabel3:
   // arguments.isOpenStartElement()
   if (!ruleResult) ruleResult = (Document::RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
   if (!string0) string0 = (*ruleResult->document.configuration._functions->convert_string)(ruleResult->document.configuration._content, "w:document");
   result = (*arguments._functions->is_equal_value_instr)(arguments._content, &doesContinue, string0);
   if (result == RRNeedChars) return RRNeedChars;
   doesContinue = !doesContinue;
   while (doesContinue) {
      *(*state._functions->get_point_instr)(state._content) = DLabel4;
      if (!(*state._functions->skip_node_instr)(state._content, arguments._content, &result, (void**) &ruleResult)) return result;
LLabel4:
      while (not arguments.isOpenStartElement() && not arguments.isCloseElement()) {
         // *(*state._functions->get_point_instr)(state._content) = DLabel4;
         if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
      }

      *(*state._functions->get_point_instr)(state._content) = DLabel5;
LLabel5:
      if (arguments.isCloseElement()) {
         *(*state._functions->get_point_instr)(state._content) = DLabel6;
         if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
LLabel6:
         (*arguments._functions->reduce_state_instr)(arguments._content, state._content, (void**) &ruleResult);
         return RRFinished;
      }
      *(*state._functions->get_point_instr)(state._content) = DLabel3;
      result = (*arguments._functions->is_equal_value_instr)(arguments._content, &doesContinue, string0);
      if (result == RRNeedChars) return RRNeedChars;
      doesContinue = !doesContinue;
   }
   *(*state._functions->get_point_instr)(state._content) = DLabel7;
   if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;

   // skip xml attributes for header "w:document"
LLabel7:
   while (not arguments.isCloseStartElement()) {
      // *(*state._functions->get_point_instr)(state._content) = DLabel7;
      if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
   }
   *(*state._functions->get_point_instr)(state._content) = DLabel8;
LLabel8:
   *(*state._functions->get_point_instr)(state._content) = DLabel9;
   if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;

LLabel9:
   while (not arguments.isCloseElement()) {
      if (arguments.isOpenStartElement()) {
         if (!ruleResult) ruleResult = (Document::RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
         if (!string1) string1 = (*ruleResult->document.configuration._functions->convert_string)(ruleResult->document.configuration._content, "w:body");
         result = (*arguments._functions->is_equal_value_instr)(arguments._content, &doesContinue, string1);
         if (result == RRNeedChars) return RRNeedChars;
         doesContinue = !doesContinue;
         if (doesContinue) {
            *(*state._functions->get_point_instr)(state._content) = DLabel9;
            if (!(*state._functions->skip_node_instr)(state._content, arguments._content, &result, (void**) &ruleResult)) return result;
            continue;
         }
         *(*state._functions->get_point_instr)(state._content) = DLabel10;
         if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
LLabel10:
         // skip xml attributes for header "w:body"
         while (not arguments.isCloseStartElement()) {
            // *(*state._functions->get_point_instr)(state._content) = DLabel10;
            if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
         }
         *(*state._functions->get_point_instr)(state._content) = DLabel11;
         if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
LLabel11:
         while (not arguments.isCloseElement()) { // w:body
            if (!ruleResult) ruleResult = (Document::RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
            ruleResult->isMethodDocumentation_Label11_Label45 = false;
            if (arguments.isOpenStartElement()) {
               if (!string2) string2 = (*ruleResult->document.configuration._functions->convert_string)(ruleResult->document.configuration._content, "w:p");
               result = (*arguments._functions->is_equal_value_instr)(arguments._content, &doesContinue, string2);
               if (result == RRNeedChars) return RRNeedChars;
               doesContinue = !doesContinue;
               if (doesContinue) {
                  *(*state._functions->get_point_instr)(state._content) = DLabel11;
                  if (!(*state._functions->skip_node_instr)(state._content, arguments._content, &result, (void**) &ruleResult)) return result;
                  continue;
               }
               *(*state._functions->get_point_instr)(state._content) = DLabel12;
               if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
LLabel12:
               // skip xml attributes for "w:p"
               while (not arguments.isCloseStartElement()) {
                  // *(*state._functions->get_point_instr)(state._content) = DLabel12;
                  if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
               }
               *(*state._functions->get_point_instr)(state._content) = DLabel13;
               if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
LLabel13:
               while (not arguments.isCloseElement() && not arguments.isOpenStartElement()) {
                  // *(*state._functions->get_point_instr)(state._content) = DLabel13;
                  if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
               }
               *(*state._functions->get_point_instr)(state._content) = DLabel14;

LLabel14:
               // arguments.isCloseElement() || arguments.isOpenStartElement()
               if (arguments.isOpenStartElement()) {
                  if (!ruleResult) ruleResult = (Document::RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
                  if (!string3) string3 = (*ruleResult->document.configuration._functions->convert_string)(ruleResult->document.configuration._content, "w:pPr");
                  result = (*arguments._functions->is_equal_value_instr)(arguments._content, &doesContinue, string3);
                  if (result == RRNeedChars) return RRNeedChars;
                  doesContinue = !doesContinue;
                  while (doesContinue) {
                     *(*state._functions->get_point_instr)(state._content) = DLabel15;
                     if (!(*state._functions->skip_node_instr)(state._content, arguments._content, &result, (void**) &ruleResult)) return result;
LLabel15:
                     while (not arguments.isOpenStartElement() && not arguments.isCloseElement()) {
                        // *(*state._functions->get_point_instr)(state._content) = DLabel15;
                        if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
                     }
                     if (arguments.isCloseElement()) {
                        *(*state._functions->get_point_instr)(state._content) = DLabel16;
                        break;
                     }
                     *(*state._functions->get_point_instr)(state._content) = DLabel14;
                     result = (*arguments._functions->is_equal_value_instr)(arguments._content, &doesContinue, string3);
                     if (result == RRNeedChars) return RRNeedChars;
                     doesContinue = !doesContinue;
                  }
               }
               *(*state._functions->get_point_instr)(state._content) = DLabel16;
LLabel16:
               if (arguments.isOpenStartElement()) {
                  *(*state._functions->get_point_instr)(state._content) = DLabel17;
                  if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
LLabel17:
                  // skip xml attributes for header "w:pPr"
                  while (not arguments.isCloseStartElement()) {
                     // *(*state._functions->get_point_instr)(state._content) = DLabel17;
                     if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
                  }
                  *(*state._functions->get_point_instr)(state._content) = DLabel18;
                  if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
                  if (!ruleResult) ruleResult = (Document::RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
                  ruleResult->newHeaderType_Label18_Label36 = HUndefined; // = point() encoding

LLabel18:
                  if (!ruleResult) ruleResult = (Document::RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
                  while (not arguments.isCloseElement()) { // w:pPr
                     if (arguments.isOpenStartElement()) {
                        if (!string4) string4 = (*ruleResult->document.configuration._functions->convert_string)(ruleResult->document.configuration._content, "w:pStyle");
                        result = (*arguments._functions->is_equal_value_instr)(arguments._content, &doesContinue, string4);
                        if (result == RRNeedChars) return RRNeedChars;
                        doesContinue = !doesContinue;
                        if (doesContinue) {
                           *(*state._functions->get_point_instr)(state._content) = DLabel18;
                           if (!(*state._functions->skip_node_instr)(state._content, arguments._content, &result, (void**) &ruleResult)) return result;
                           continue;
                        }
                        *(*state._functions->get_point_instr)(state._content) = DLabel19;
                        if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
LLabel19:
                        while (arguments.isAttribute()) {
                           if (!ruleResult) ruleResult = (Document::RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
                           if (!string5) string5 = (*ruleResult->document.configuration._functions->convert_string)(ruleResult->document.configuration._content, "w:val");
                           result = (*arguments._functions->is_equal_attribute_instr)(arguments._content, &doesContinue, string5);
                           if (result == RRNeedChars) return RRNeedChars;
                           if (doesContinue) {
                              if (!string6) string6 = (*ruleResult->document.configuration._functions->convert_string)(ruleResult->document.configuration._content, (*ruleResult->document.configuration._functions->get_value)(ruleResult->document.configuration._content, "applicationName"));
                              if (!string7) string7 = (*ruleResult->document.configuration._functions->convert_string)(ruleResult->document.configuration._content, (*ruleResult->document.configuration._functions->get_value)(ruleResult->document.configuration._content, "unitName"));
                              if (!string8) string8 = (*ruleResult->document.configuration._functions->convert_string)(ruleResult->document.configuration._content, (*ruleResult->document.configuration._functions->get_value)(ruleResult->document.configuration._content, "className"));
                              if (!string9) string9 = (*ruleResult->document.configuration._functions->convert_string)(ruleResult->document.configuration._content, (*ruleResult->document.configuration._functions->get_value)(ruleResult->document.configuration._content, "fieldsHeaderName"));
                              if (!string10) string10 = (*ruleResult->document.configuration._functions->convert_string)(ruleResult->document.configuration._content, (*ruleResult->document.configuration._functions->get_value)(ruleResult->document.configuration._content, "typesName"));
                              if (!string11) string11 = (*ruleResult->document.configuration._functions->convert_string)(ruleResult->document.configuration._content, (*ruleResult->document.configuration._functions->get_value)(ruleResult->document.configuration._content, "constantsName"));
                              if (!string12) string12 = (*ruleResult->document.configuration._functions->convert_string)(ruleResult->document.configuration._content, (*ruleResult->document.configuration._functions->get_value)(ruleResult->document.configuration._content, "fieldsName"));
                              if (!string13) string13 = (*ruleResult->document.configuration._functions->convert_string)(ruleResult->document.configuration._content, (*ruleResult->document.configuration._functions->get_value)(ruleResult->document.configuration._content, "invariantName"));
                              if (!string14) string14 = (*ruleResult->document.configuration._functions->convert_string)(ruleResult->document.configuration._content, (*ruleResult->document.configuration._functions->get_value)(ruleResult->document.configuration._content, "itemName"));
                              if (!string15) string15 = (*ruleResult->document.configuration._functions->convert_string)(ruleResult->document.configuration._content, (*ruleResult->document.configuration._functions->get_value)(ruleResult->document.configuration._content, "macrosName"));
                              if (!string16) string16 = (*ruleResult->document.configuration._functions->convert_string)(ruleResult->document.configuration._content, (*ruleResult->document.configuration._functions->get_value)(ruleResult->document.configuration._content, "methodsName"));
                              result = (*arguments._functions->load_value_instr)(arguments._content);
                              if (result == RRNeedChars) return RRNeedChars;
                              if (!ruleResult) ruleResult = (Document::RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
                              if ((*arguments._functions->compare_value_instr)(arguments._content, string6) == 0) {
                                 ruleResult->newHeaderType_Label18_Label36 = HLibrary;
                                 ruleResult->isInHeader = true;
                              }
                              else if ((*arguments._functions->compare_value_instr)(arguments._content, string7) == 0
                                    && ruleResult->headerType >= HUnit-1) {
                                 ruleResult->newHeaderType_Label18_Label36 = HUnit;
                                 ruleResult->isInHeader = true;
                              }
                              else if ((*arguments._functions->compare_value_instr)(arguments._content, string8) == 0
                                    && ruleResult->headerType >= HClass-1) {
                                 ruleResult->newHeaderType_Label18_Label36 = HClass;
                                 ruleResult->isInHeader = true;
                              }
                              else if ((*arguments._functions->compare_value_instr)(arguments._content, string9) == 0
                                    && ruleResult->headerType >= HClass) {
                                 ruleResult->newHeaderType_Label18_Label36 = HSectionField;
                                 ruleResult->isInHeader = false;
                              }
                              else if ((*arguments._functions->compare_value_instr)(arguments._content, string10) == 0
                                    && ruleResult->headerType >= HClass) {
                                 ruleResult->newHeaderType_Label18_Label36 = HTypes;
                                 ruleResult->isInHeader = true;
                              }
                              else if ((*arguments._functions->compare_value_instr)(arguments._content, string11) == 0
                                    && ruleResult->headerType >= HClass) {
                                 ruleResult->newHeaderType_Label18_Label36 = HConstants;
                                 ruleResult->isInHeader = true;
                              }
                              else if ((*arguments._functions->compare_value_instr)(arguments._content, string12) == 0
                                    && (ruleResult->headerType == HClass || ruleResult->headerType == HSectionField
                                          || ruleResult->headerType == HFields || ruleResult->headerType == HInvariant
                                          || ruleResult->headerType == HInvariantItem)) {
                                 ruleResult->newHeaderType_Label18_Label36 = HFields;
                                 ruleResult->isInHeader = true;
                              }
                              else if ((*arguments._functions->compare_value_instr)(arguments._content, string13) == 0
                                    && (ruleResult->headerType == HSectionField || ruleResult->headerType == HFields
                                          || ruleResult->headerType == HInvariant || ruleResult->headerType == HInvariantItem)) {
                                 ruleResult->newHeaderType_Label18_Label36 = HInvariant;
                                 ruleResult->isInHeader = false;
                              }
                              else if ((*arguments._functions->compare_value_instr)(arguments._content, string14) == 0
                                    && (ruleResult->headerType == HInvariant || ruleResult->headerType == HInvariantItem)) {
                                 ruleResult->newHeaderType_Label18_Label36 = HInvariantItem;
                                 ruleResult->isInHeader = true;
                              }
                              else if ((*arguments._functions->compare_value_instr)(arguments._content, string15) == 0
                                    && ruleResult->headerType >= HClass) {
                                 ruleResult->newHeaderType_Label18_Label36 = HMacros;
                                 ruleResult->isInHeader = true;
                              }
                              else if ((*arguments._functions->compare_value_instr)(arguments._content, string16) == 0
                                    && ruleResult->headerType >= HClass) {
                                 ruleResult->newHeaderType_Label18_Label36 = HMethods;
                                 ruleResult->isInHeader = true;
                              }
                           };
                           *(*state._functions->get_point_instr)(state._content) = DLabel19;
                           if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
                        }
                        *(*state._functions->get_point_instr)(state._content) = DLabel20;
LLabel20:
                        while (not arguments.isCloseStartElement()) {
                           // *(*state._functions->get_point_instr)(state._content) = DLabel20;
                           if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
                        }
                        *(*state._functions->get_point_instr)(state._content) = DLabel21;
                        if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
LLabel21:
                        *(*state._functions->get_point_instr)(state._content) = DLabel18;
                        if (!(*state._functions->skip_node_content_instr)(state._content, arguments._content, &result, (void**) &ruleResult)) return result;
                        continue;
                     } // end of w:pStyle
LLabel22:
                     *(*state._functions->get_point_instr)(state._content) = DLabel18;
                     if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
                  } // end of w:pPr
                  // arguments.isCloseElement()
                  *(*state._functions->get_point_instr)(state._content) = DLabel23;
                  if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;

LLabel23:
                  while (not arguments.isCloseElement()) { // w:p
                     if (arguments.isOpenStartElement()) {
                        if (!ruleResult) ruleResult = (Document::RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
                        if (!string17) string17 = (*ruleResult->document.configuration._functions->convert_string)(ruleResult->document.configuration._content, "w:r");
                        result = (*arguments._functions->is_equal_value_instr)(arguments._content, &doesContinue, string17);
                        if (result == RRNeedChars) return RRNeedChars;
                        doesContinue = !doesContinue || ruleResult->newHeaderType_Label18_Label36 == HUndefined;
                        if (doesContinue) {
                           *(*state._functions->get_point_instr)(state._content) = DLabel23;
                           if (!(*state._functions->skip_node_instr)(state._content, arguments._content, &result, (void**) &ruleResult)) return result;
                           continue;
                        }
                        *(*state._functions->get_point_instr)(state._content) = DLabel24;
                        if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
LLabel24:
                        while (not arguments.isCloseStartElement()) {
                           // *(*state._functions->get_point_instr)(state._content) = DLabel24;
                           if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
                        }
                        if (!ruleResult) ruleResult = (Document::RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
                        ruleResult->style_Label25_Label35 = TSUndefined;
                        *(*state._functions->get_point_instr)(state._content) = DLabel25;
                        if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
LLabel25:
                        while (not arguments.isCloseElement()) { // w:r
                           if (arguments.isOpenStartElement()) {
                              if (!ruleResult) ruleResult = (Document::RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
                              if (!string18) string18 = (*ruleResult->document.configuration._functions->convert_string)(ruleResult->document.configuration._content, "w:t");
                              if (!string19) string19 = (*ruleResult->document.configuration._functions->convert_string)(ruleResult->document.configuration._content, "w:sym");
                              result = (*arguments._functions->load_value_instr)(arguments._content);
                              if (result == RRNeedChars) return RRNeedChars;
                              if ((ruleResult->headerType == HInvariant && ruleResult->newHeaderType_Label18_Label36 == HInvariantItem)
                                    || ruleResult->headerType == HInvariantItem) {
                                 if (!string20) string20 = (*ruleResult->document.configuration._functions->convert_string)(ruleResult->document.configuration._content, "w:rPr");
                                 if ((*arguments._functions->compare_value_instr)(arguments._content, string18) != 0
                                       && (*arguments._functions->compare_value_instr)(arguments._content, string19) != 0
                                       && (*arguments._functions->compare_value_instr)(arguments._content, string20) != 0) {
                                    *(*state._functions->get_point_instr)(state._content) = DLabel25;
                                    if (!(*state._functions->skip_node_instr)(state._content, arguments._content, &result, (void**) &ruleResult)) return result;
                                    continue;
                                 }
                                 if ((*arguments._functions->compare_value_instr)(arguments._content, string20) == 0) {
                                    *(*state._functions->get_point_instr)(state._content) = DLabel26;
                                    if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
LLabel26:
                                    while (not arguments.isCloseStartElement()) {
                                       // *(*state._functions->get_point_instr)(state._content) = DLabel26;
                                       if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
                                    }
                                    *(*state._functions->get_point_instr)(state._content) = DLabel27;
                                    if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
LLabel27:
                                    while (not arguments.isCloseElement()) { // w:rPr
                                       if (arguments.isOpenStartElement()) {
                                          if (!string21) string21 = (*ruleResult->document.configuration._functions->convert_string)(ruleResult->document.configuration._content, "w:rStyle");
                                          result = (*arguments._functions->is_equal_value_instr)(arguments._content, &doesContinue, string21);
                                          if (result == RRNeedChars) return RRNeedChars;
                                          doesContinue = !doesContinue;
                                          if (doesContinue) {
                                             *(*state._functions->get_point_instr)(state._content) = DLabel27;
                                             if (!ruleResult) ruleResult = (Document::RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
                                             if (!(*state._functions->skip_node_instr)(state._content, arguments._content, &result, (void**) &ruleResult)) return result;
                                             continue;
                                          }
                                          *(*state._functions->get_point_instr)(state._content) = DLabel28;
                                          if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
LLabel28:
                                          while (arguments.isAttribute()) {
                                             if (!ruleResult) ruleResult = (Document::RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
                                             if (!string22) string22 = (*ruleResult->document.configuration._functions->convert_string)(ruleResult->document.configuration._content, "w:val");
                                             result = (*arguments._functions->is_equal_attribute_instr)(arguments._content, &doesContinue, string22);
                                             if (result == RRNeedChars) return RRNeedChars;
                                             if (doesContinue) {
                                                if (!string23) string23 = (*ruleResult->document.configuration._functions->convert_string)(ruleResult->document.configuration._content, (*ruleResult->document.configuration._functions->get_value)(ruleResult->document.configuration._content, "linkFieldName"));
                                                if (!string24) string24 = (*ruleResult->document.configuration._functions->convert_string)(ruleResult->document.configuration._content, (*ruleResult->document.configuration._functions->get_value)(ruleResult->document.configuration._content, "linkMethodName"));
                                                if (!string25) string25 = (*ruleResult->document.configuration._functions->convert_string)(ruleResult->document.configuration._content, (*ruleResult->document.configuration._functions->get_value)(ruleResult->document.configuration._content, "variableName"));
                                                if (!string26) string26 = (*ruleResult->document.configuration._functions->convert_string)(ruleResult->document.configuration._content, (*ruleResult->document.configuration._functions->get_value)(ruleResult->document.configuration._content, "codeName"));
                                                if (!string27) string27 = (*ruleResult->document.configuration._functions->convert_string)(ruleResult->document.configuration._content, (*ruleResult->document.configuration._functions->get_value)(ruleResult->document.configuration._content, "linkTypeName"));
                                                if (!string28) string28 = (*ruleResult->document.configuration._functions->convert_string)(ruleResult->document.configuration._content, (*ruleResult->document.configuration._functions->get_value)(ruleResult->document.configuration._content, "linkClassName"));
                                                if (!string29) string29 = (*ruleResult->document.configuration._functions->convert_string)(ruleResult->document.configuration._content, (*ruleResult->document.configuration._functions->get_value)(ruleResult->document.configuration._content, "linkMacroName"));
                                                if (!string30) string30 = (*ruleResult->document.configuration._functions->convert_string)(ruleResult->document.configuration._content, (*ruleResult->document.configuration._functions->get_value)(ruleResult->document.configuration._content, "linkConstantName"));
                                                result = (*arguments._functions->load_value_instr)(arguments._content);
                                                if (result == RRNeedChars) return RRNeedChars;
                                                if (!ruleResult) ruleResult = (Document::RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
                                                if ((*arguments._functions->compare_value_instr)(arguments._content, string23) == 0)
                                                   ruleResult->style_Label25_Label35 = TSLinkField;
                                                else if ((*arguments._functions->compare_value_instr)(arguments._content, string24) == 0)
                                                   ruleResult->style_Label25_Label35 = TSLinkMethod;
                                                else if ((*arguments._functions->compare_value_instr)(arguments._content, string25) == 0)
                                                   ruleResult->style_Label25_Label35 = TSVariable;
                                                else if ((*arguments._functions->compare_value_instr)(arguments._content, string26) == 0)
                                                   ruleResult->style_Label25_Label35 = TSCode;
                                                else if ((*arguments._functions->compare_value_instr)(arguments._content, string27) == 0)
                                                   ruleResult->style_Label25_Label35 = TSLinkType;
                                                else if ((*arguments._functions->compare_value_instr)(arguments._content, string28) == 0)
                                                   ruleResult->style_Label25_Label35 = TSLinkClass;
                                                else if ((*arguments._functions->compare_value_instr)(arguments._content, string29) == 0)
                                                   ruleResult->style_Label25_Label35 = TSLinkMacro;
                                                else if ((*arguments._functions->compare_value_instr)(arguments._content, string30) == 0)
                                                   ruleResult->style_Label25_Label35 = TSLinkConstant;
                                             };
                                             *(*state._functions->get_point_instr)(state._content) = DLabel28;
                                             if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
                                          }
                                          *(*state._functions->get_point_instr)(state._content) = DLabel29;
LLabel29:
                                          while (not arguments.isCloseStartElement()) {
                                             // *(*state._functions->get_point_instr)(state._content) = DLabel29;
                                             if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
                                          }
                                          *(*state._functions->get_point_instr)(state._content) = DLabel30;
                                          if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
LLabel30:
                                          *(*state._functions->get_point_instr)(state._content) = DLabel27;
                                          if (!ruleResult) ruleResult = (Document::RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
                                          if (!(*state._functions->skip_node_content_instr)(state._content, arguments._content, &result, (void**) &ruleResult)) return result;
                                          continue;
                                       } // end of w:rStyle
LLabel31:
                                       *(*state._functions->get_point_instr)(state._content) = DLabel27;
                                       if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
                                    } // end of w:rPr
                                    // arguments.isCloseElement()
                                    *(*state._functions->get_point_instr)(state._content) = DLabel25;
                                    if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
                                    continue;
                                 }
                              }
                              else {
                                 if ((*arguments._functions->compare_value_instr)(arguments._content, string18) != 0
                                       && (*arguments._functions->compare_value_instr)(arguments._content, string19) != 0) {
                                    *(*state._functions->get_point_instr)(state._content) = DLabel25;
                                    if (!ruleResult) ruleResult = (Document::RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
                                    if (!(*state._functions->skip_node_instr)(state._content, arguments._content, &result, (void**) &ruleResult)) return result;
                                    continue;
                                 }
                              }
                              if ((*arguments._functions->compare_value_instr)(arguments._content, string19) == 0) {
                                 if (!ruleResult) ruleResult = (Document::RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
                                 ruleResult->isSymbolFont_Label32_Label35 = false;
                                 *(*state._functions->get_point_instr)(state._content) = DLabel32;
                                 if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
LLabel32:
                                 while (not arguments.isCloseStartElement()) {
                                    if (!ruleResult) ruleResult = (Document::RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
                                    if (!string31) string31 = (*ruleResult->document.configuration._functions->convert_string)(ruleResult->document.configuration._content, "w:font");
                                    if (!string32) string32 = (*ruleResult->document.configuration._functions->convert_string)(ruleResult->document.configuration._content, "w:char");
                                    result = (*arguments._functions->load_attribute_and_value_instr)(arguments._content);
                                    if (result == RRNeedChars) return RRNeedChars;
                                    if ((*arguments._functions->compare_attribute_instr)(arguments._content, string31) == 0) {
                                       if (!string33) string33 = (*ruleResult->document.configuration._functions->convert_string)(ruleResult->document.configuration._content, "Symbol");
                                       if ((*arguments._functions->compare_value_instr)(arguments._content, string33) == 0) {
                                          if (!ruleResult) ruleResult = (Document::RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
                                          ruleResult->isSymbolFont_Label32_Label35 = true;
                                       }
                                    }
                                    else if ((*arguments._functions->compare_attribute_instr)(arguments._content, string32) == 0) {
                                       if (!string34) string34 = (*ruleResult->document.configuration._functions->convert_string)(ruleResult->document.configuration._content, "F0B3");
                                       if (!string35) string35 = (*ruleResult->document.configuration._functions->convert_string)(ruleResult->document.configuration._content, "F0DB");
                                       if (!string36) string36 = (*ruleResult->document.configuration._functions->convert_string)(ruleResult->document.configuration._content, "F0B9");
                                       const char* textSymbol = nullptr;
                                       if ((*arguments._functions->compare_value_instr)(arguments._content, string34) == 0)
                                          textSymbol = ">=";
                                       if ((*arguments._functions->compare_value_instr)(arguments._content, string35) == 0)
                                          textSymbol = "<=>";
                                       if ((*arguments._functions->compare_value_instr)(arguments._content, string36) == 0)
                                          textSymbol = "!=";
                                       if (textSymbol) {
                                          if (!ruleResult) ruleResult = (Document::RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
                                          if (ruleResult->isSymbolFont_Label32_Label35) {
                                             if (ruleResult->headerType == HInvariant) {
                                                ruleResult->document.jsonWriter.addKey("invariant");
                                                ruleResult->document.jsonWriter.openArray();
                                                ruleResult->headerType = ruleResult->newHeaderType_Label18_Label36;
                                                ruleResult->isFirstInHeader = true;
                                             }
                                             else if (!ruleResult->isContinueHeaderTitle) {
                                                if (closeToHeader(ruleResult->document.jsonWriter, ruleResult->headerType, ruleResult->newHeaderType_Label18_Label36))
                                                   ruleResult->headerType = ruleResult->newHeaderType_Label18_Label36;
                                                if (ruleResult->headerType != HInvariantItem)
                                                   ruleResult->document.jsonWriter.closeObject();
                                             }
                                             ruleResult->isContinueHeaderTitle = true;
                                             if (ruleResult->style_Label25_Label35 != TSUndefined) {
                                                if (ruleResult->headerTitle.size() == 0)
                                                   ruleResult->headerTitle = textSymbol;
                                                else
                                                   ruleResult->headerTitle += textSymbol;
                                             }
                                          }
                                       }
                                    }
                                    // *(*state._functions->get_point_instr)(state._content) = DLabel32;
                                    if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
                                 }
                                 *(*state._functions->get_point_instr)(state._content) = DLabel33;
                                 if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
LLabel33:
                                 while (not arguments.isCloseElement()) { // w:sym
                                    if (arguments.isOpenStartElement()) {
                                       *(*state._functions->get_point_instr)(state._content) = DLabel33;
                                       if (!ruleResult) ruleResult = (Document::RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
                                       if (!(*state._functions->skip_node_instr)(state._content, arguments._content, &result, (void**) &ruleResult)) return result;
                                       continue;
                                    }
                                    if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
                                 }
                                 *(*state._functions->get_point_instr)(state._content) = DLabel25;
                                 if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
                                 continue;
                              }

                              *(*state._functions->get_point_instr)(state._content) = DLabel34;
                              if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
LLabel34:
                              while (not arguments.isCloseStartElement()) {
                                 // *(*state._functions->get_point_instr)(state._content) = DLabel34;
                                 if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
                              }
                              *(*state._functions->get_point_instr)(state._content) = DLabel35;
                              if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
LLabel35:
                              while (not arguments.isCloseElement()) { // w:t
                                 if (arguments.isExtendedText()) {
                                    if (!ruleResult) ruleResult = (Document::RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
                                    Header parentHeader = HClass;
                                    Header header = ruleResult->headerType;
                                    Header referenceHeader = ruleResult->newHeaderType_Label18_Label36;
                                    bool isStart = false, isReopen = false, isAddText = false, isRestart = false;
                                    const char* textContent;
                                    if (referenceHeader == HLibrary) {
                                       textContent = "";
                                       isStart = header == HUndefined;
                                       isReopen = !(header == HLibrary && ruleResult->isFirstInHeader);
                                       isAddText = isStart || !isReopen;
                                    }
                                    else if (referenceHeader == HUnit) {
                                       textContent = "units";
                                       isStart = header == HLibrary;
                                       isRestart = (header > HLibrary && header != referenceHeader);
                                       isReopen = !ruleResult->isContinueHeaderTitle;
                                       parentHeader = HLibrary;
                                       isAddText = true;
                                    }
                                    else if (referenceHeader == HClass) {
                                       textContent = "classes";
                                       isStart = header == HUnit;
                                       isRestart = (header > HUnit && header != referenceHeader);
                                       isReopen = !ruleResult->isContinueHeaderTitle;
                                       parentHeader = HUnit;
                                       isAddText = true;
                                    }
                                    else if (referenceHeader == HSectionField) {
                                       isStart = header == HClass;
                                       isRestart = (header > HClass && header != referenceHeader);
                                    }
                                    else if (referenceHeader == HTypes) {
                                       textContent = "types";
                                       isStart = header == HClass;
                                       isRestart = (header > HClass && header != referenceHeader);
                                       isReopen = !ruleResult->isContinueHeaderTitle;
                                       isAddText = true;
                                    }
                                    else if (referenceHeader == HConstants) {
                                       textContent = "constants";
                                       isStart = header == HClass;
                                       isRestart = (header > HClass && header != referenceHeader);
                                       isReopen = !ruleResult->isContinueHeaderTitle;
                                       isAddText = true;
                                    }
                                    else if (referenceHeader == HFields) {
                                       textContent = "fields";
                                       isStart = header == HSectionField;
                                       isRestart = (header != HSectionField && header != HFields);
                                       isReopen = !ruleResult->isContinueHeaderTitle;
                                       parentHeader = HSectionField;
                                       isAddText = true;
                                    }
                                    else if (referenceHeader == HInvariant) {
                                       isStart = header == HSectionField;
                                       isRestart = header == HFields;
                                       parentHeader = HSectionField;
                                    }
                                    else if (referenceHeader == HInvariantItem) {
                                       textContent = "invariant";
                                       isStart = header == HInvariant;
                                       isRestart = (header != HInvariant && header != HInvariantItem);
                                       isReopen = !ruleResult->isContinueHeaderTitle;
                                       parentHeader = HInvariant;
                                       isAddText = true;
                                    }
                                    else if (referenceHeader == HMacros) {
                                       textContent = "macros";
                                       isStart = header == HClass;
                                       isRestart = (header > HClass && header != referenceHeader);
                                       isReopen = !ruleResult->isContinueHeaderTitle;
                                       isAddText = true;
                                    }
                                    else if (referenceHeader == HMethods) {
                                       textContent = "methods";
                                       isStart = header == HClass;
                                       isRestart = (header > HClass && header != referenceHeader);
                                       isReopen = !ruleResult->isContinueHeaderTitle;
                                       isAddText = true;
                                    }
                                    if (isStart || isRestart) {
                                       if (isRestart) {
                                          if (closeToHeader(ruleResult->document.jsonWriter, header, parentHeader))
                                             ruleResult->headerType = parentHeader;
                                       }
                                       if (referenceHeader == HLibrary)
                                          ruleResult->document.jsonWriter.start();
                                       else {
                                          if (isAddText) {
                                             ruleResult->document.jsonWriter.addKey(textContent);
                                             ruleResult->document.jsonWriter.openArray();
                                          }
                                       };
                                       ruleResult->headerType = ruleResult->newHeaderType_Label18_Label36;
                                       ruleResult->isFirstInHeader = true;
                                    }
                                    else if (isReopen) {
                                       if (closeToHeader(ruleResult->document.jsonWriter, header, referenceHeader))
                                          ruleResult->headerType = referenceHeader;
                                       if (referenceHeader != HLibrary && referenceHeader != HInvariantItem)
                                          ruleResult->document.jsonWriter.closeObject();
                                    }
                                    ruleResult->isContinueHeaderTitle = true;
                                    if (isAddText) {
                                       if (referenceHeader != HInvariantItem
                                             || ruleResult->style_Label25_Label35 != TSUndefined) {
                                          result = (*arguments._functions->load_extended_value_instr)(arguments._content);
                                          if (result == RRNeedChars) return RRNeedChars;
                                          if (ruleResult->headerTitle.size() == 0)
                                             ruleResult->headerTitle = arguments.value();
                                          else
                                             ruleResult->headerTitle += arguments.value();
                                       }
                                    };
                                 }
                                 *(*state._functions->get_point_instr)(state._content) = DLabel35;
                                 if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
                              } // end of w:t
                           }
                           *(*state._functions->get_point_instr)(state._content) = DLabel25;
                           if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
                        } // end of w:r
                     }
                     *(*state._functions->get_point_instr)(state._content) = DLabel23;
                     if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
                  } // end of w:p
                  *(*state._functions->get_point_instr)(state._content) = DLabel36;
LLabel36:
                  if (!ruleResult) ruleResult = (Document::RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
                  if (ruleResult->isInHeader) {
                     ruleResult->isInHeader = false;
                     ruleResult->isFirstInHeader = false;
                     if (ruleResult->headerType == HInvariantItem)
                        ruleResult->document.jsonWriter.addString(ruleResult->headerTitle);
                     else {
                        ruleResult->document.jsonWriter.openObject();
                        ruleResult->document.jsonWriter.addKey("name");
                        if (std::string(ruleResult->document.configuration.getValue("doesRemoveSemiColon")) == "true")
                           if (ruleResult->headerTitle[ruleResult->headerTitle.length()-1] == ';')
                              ruleResult->headerTitle.pop_back();
                        ruleResult->document.jsonWriter.addString(ruleResult->headerTitle);
                     }
                     ruleResult->headerTitle = std::string();
                     ruleResult->isContinueHeaderTitle = false;
                     ruleResult->hasWrittenName = true;
                  }
               } // end of w:p
               if (!ruleResult) ruleResult = (Document::RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
               if (ruleResult->headerType != HMethods || !ruleResult->hasWrittenName
                     || std::string(ruleResult->document.configuration.getValue("doesAddMethodDoc")) != "true")
                  ruleResult->hasWrittenName = false;
               else
                  ruleResult->isMethodDocumentation_Label11_Label45 = true;
            }
            *(*state._functions->get_point_instr)(state._content) = DLabel37;
            if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;

LLabel37:
            if (!ruleResult) ruleResult = (Document::RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
            while (ruleResult->isMethodDocumentation_Label11_Label45 && ruleResult->hasWrittenName && !arguments.isCloseElement()) { // w:body
               if (arguments.isOpenStartElement()) {
                  if (!string37) string37 = (*ruleResult->document.configuration._functions->convert_string)(ruleResult->document.configuration._content, "w:p");
                  result = (*arguments._functions->is_equal_value_instr)(arguments._content, &doesContinue, string37);
                  if (result == RRNeedChars) return RRNeedChars;
                  doesContinue = !doesContinue;
                  if (doesContinue) {
                     *(*state._functions->get_point_instr)(state._content) = DLabel37;
                     if (!(*state._functions->skip_node_instr)(state._content, arguments._content, &result, (void**) &ruleResult)) return result;
                     if (!ruleResult) ruleResult = (Document::RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
                     continue;
                  }
                  *(*state._functions->get_point_instr)(state._content) = DLabel38;
                  if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
LLabel38:
                  while (not arguments.isCloseStartElement()) {
                     // *(*state._functions->get_point_instr)(state._content) = DLabel38;
                     if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
                  }
                  *(*state._functions->get_point_instr)(state._content) = DLabel39;
                  if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;

LLabel39:
                  while (not arguments.isCloseElement()) { // w:p
                     if (arguments.isOpenStartElement()) {
                        if (!ruleResult) ruleResult = (Document::RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
                        if (!string38) string38 = (*ruleResult->document.configuration._functions->convert_string)(ruleResult->document.configuration._content, "w:pPr");
                        if (!string39) string39 = (*ruleResult->document.configuration._functions->convert_string)(ruleResult->document.configuration._content, "w:r");
                        result = (*arguments._functions->load_value_instr)(arguments._content);
                        if (result == RRNeedChars) return RRNeedChars;
                        if ((*arguments._functions->compare_value_instr)(arguments._content, string38) == 0) {
                           *(*state._functions->get_point_instr)(state._content) = DLabel40;
                           if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
LLabel40:
                           while (!arguments.isCloseStartElement()) {
                              // *(*state._functions->get_point_instr)(state._content) = DLabel40;
                              if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
                           }
                           *(*state._functions->get_point_instr)(state._content) = DLabel41;
                           if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
LLabel41:
                           while (!arguments.isCloseElement()) { // w:pPr
                              if (arguments.isOpenStartElement()) {
                                 if (!string40) string40 = (*ruleResult->document.configuration._functions->convert_string)(ruleResult->document.configuration._content, "w:pStyle");
                                 result = (*arguments._functions->is_equal_value_instr)(arguments._content, &doesContinue, string40);
                                 if (result == RRNeedChars) return RRNeedChars;
                                 if (doesContinue) {
                                    if (!ruleResult) ruleResult = (Document::RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
                                    ruleResult->hasWrittenName = false;
                                    if (ruleResult->headerTitle.size() > 0) {
                                       ruleResult->document.jsonWriter.addKey("doc");
                                       ruleResult->headerTitle.pop_back();
                                       ruleResult->document.jsonWriter.addString(ruleResult->headerTitle);
                                    }
                                    ruleResult->headerTitle = std::string();
                                    ruleResult->isContinueHeaderTitle = false;
                                    ruleResult->isMethodDocumentation_Label11_Label45 = false;
                                    ruleResult->newHeaderType_Label18_Label36 = HUndefined;
                                    *(*state._functions->get_point_instr)(state._content) = DLabel19;
                                    if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
                                    goto LLabel19;
                                 }
                                 else {
                                    *(*state._functions->get_point_instr)(state._content) = DLabel41;
                                    if (!(*state._functions->skip_node_instr)(state._content, arguments._content, &result, (void**) &ruleResult)) return result;
                                    continue;
                                 }
                              };
                              *(*state._functions->get_point_instr)(state._content) = DLabel41;
                              if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
                           } // end of w:pPr
                        }
                        else if ((*arguments._functions->compare_value_instr)(arguments._content, string39) == 0) {
                           *(*state._functions->get_point_instr)(state._content) = DLabel42;
                           if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
LLabel42:
                           while (!arguments.isCloseStartElement()) {
                              // *(*state._functions->get_point_instr)(state._content) = DLabel42;
                              if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
                           }
                           *(*state._functions->get_point_instr)(state._content) = DLabel43;
                           if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
LLabel43:
                           while (!arguments.isCloseElement()) { // w:r
                              if (arguments.isOpenStartElement()) {
                                 if (!ruleResult) ruleResult = (Document::RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
                                 if (!string41) string41 = (*ruleResult->document.configuration._functions->convert_string)(ruleResult->document.configuration._content, "w:t");
                                 result = (*arguments._functions->is_equal_value_instr)(arguments._content, &doesContinue, string41);
                                 if (result == RRNeedChars) return RRNeedChars;
                                 doesContinue = !doesContinue;
                                 if (doesContinue) {
                                    *(*state._functions->get_point_instr)(state._content) = DLabel43;
                                    if (!ruleResult) ruleResult = (Document::RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
                                    if (!(*state._functions->skip_node_instr)(state._content, arguments._content, &result, (void**) &ruleResult)) return result;
                                    continue;
                                 }
                                 *(*state._functions->get_point_instr)(state._content) = DLabel44;
                                 if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
LLabel44:
                                 while (!arguments.isCloseStartElement()) {
                                    // *(*state._functions->get_point_instr)(state._content) = DLabel44;
                                    if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
                                 }
                                 *(*state._functions->get_point_instr)(state._content) = DLabel45;
                                 if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
LLabel45:
                                 while (!arguments.isCloseElement()) { // w:t
                                    if (arguments.isExtendedText()) {
                                       if (!ruleResult) ruleResult = (Document::RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
                                       ruleResult->isContinueHeaderTitle = true;
                                       if (ruleResult->headerTitle.length() == 0) {
                                          result = (*arguments._functions->load_extended_value_instr)(arguments._content);
                                          if (result == RRNeedChars) return RRNeedChars;
                                          ruleResult->headerTitle = arguments.value();
                                       }
                                       else {
                                          result = (*arguments._functions->load_extended_value_instr)(arguments._content);
                                          if (result == RRNeedChars) return RRNeedChars;
                                          ruleResult->headerTitle += arguments.value();
                                       }
                                    }
                                    // *(*state._functions->get_point_instr)(state._content) = DLabel45;
                                    if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
                                 } // end of w:t
                              }
                              *(*state._functions->get_point_instr)(state._content) = DLabel43;
                              if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
                           } // end of w:r
                        }
                        else {
                           *(*state._functions->get_point_instr)(state._content) = DLabel39;
                           if (!ruleResult) ruleResult = (Document::RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
                           if (!(*state._functions->skip_node_instr)(state._content, arguments._content, &result, (void**) &ruleResult)) return result;
                           continue;
                        }
                     };
                     *(*state._functions->get_point_instr)(state._content) = DLabel39;
                     if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
                  } // end of w:p
                  if (!ruleResult) ruleResult = (Document::RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
                  if (ruleResult->headerTitle.size() > 0)
                     ruleResult->headerTitle += ' ';
               }
               *(*state._functions->get_point_instr)(state._content) = DLabel37;
               if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
               if (!ruleResult) ruleResult = (Document::RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
            }
            if (!ruleResult) ruleResult = (Document::RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
            if (ruleResult->hasWrittenName) {
               ruleResult->hasWrittenName = false;
               if (ruleResult->headerTitle.size() > 0) {
                  ruleResult->document.jsonWriter.addKey("doc");
                  ruleResult->headerTitle.pop_back();
                  ruleResult->document.jsonWriter.addString(ruleResult->headerTitle);
               }
               ruleResult->headerTitle = std::string();
               ruleResult->isContinueHeaderTitle = false;
            };
            *(*state._functions->get_point_instr)(state._content) = DLabel11;
         } // end of w:body
      }
      *(*state._functions->get_point_instr)(state._content) = DLabel9;
      if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
   } // end of w:document
   if (!ruleResult) ruleResult = (Document::RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
   closeAllHeaders(ruleResult->document.jsonWriter, ruleResult->headerType);
   (*arguments._functions->reduce_state_instr)(arguments._content, state._content, (void**) &ruleResult);
   return RRFinished;
}

Document&
Document::queryDocumentFromRule(RuleResult& rule) { return rule.document; }

void
Document::load(XMLParserArguments& arguments, XMLParserStateStack& state)
   {  loadInstr(arguments._content, arguments._functions, state._content, state._functions); }

