/**************************************************************************/
/*                                                                        */
/*  Copyright (C) 2019                                                    */
/*    CEA (Commissariat a l'Energie Atomique et aux Energies              */
/*         Alternatives)                                                  */
/*                                                                        */
/*  you can redistribute it and/or modify it under the terms of the GNU   */
/*  Lesser General Public License as published by the Free Software       */
/*  Foundation, version 2.1.                                              */
/*                                                                        */
/*  It is distributed in the hope that it will be useful,                 */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU Lesser General Public License for more details.                   */
/*                                                                        */
/*  See the GNU Lesser General Public License version 2.1                 */
/*  for more details (enclosed in the file LICENSE).                      */
/*                                                                        */
/**************************************************************************/

/////////////////////////////////
//
// Application : documentation to json converter
// Unit      : main program
// File      : DocToJson.cpp
// Description :
//   Main program to convert documentation into json format.
//

#include <iostream>
#include "DocxParser.h"
#include "DocumentationFormat.h"

class ProcessArgument {
  private:
   enum TypeInput { TIUndefined, TIHtml, TIDocx, TIOdt, TIOds };
   const char* szParserLibrary = nullptr;
   const char* szInputFile = nullptr;
   const char* szOutputFile = nullptr;
   bool fHasEchoedMessage = false;
   TypeInput tiInput = TIUndefined;
   DDocx::Algorithm aAlgorithm = DDocx::AOptimized;
   const char* szConfigFile = nullptr;

  public:
   ProcessArgument() = default;
   ProcessArgument(int argc, char** argv)
      {  int arg = argc-2;
         while (arg >= 0)
            fHasEchoedMessage = !process(argv + (argc - arg - 1), arg) && fHasEchoedMessage;
      }

   bool process(char** argument, int& currentArgument);
   void printUsage(std::ostream& out) const
      {  if (fHasEchoedMessage)
            return;
         out << "Usage of doc_to_json:\n"
             << "\tdoc_to_json samples/CodeReader.so [option]+ file.docx -o file.json\n"
             // << "\tdoc_to_json [option]+ file.html -o file.json\n"
             << "\n"
             << "where option can be:\n"
             << "\t-h \t\tto print help\n"
             << "\t-c config.xml \tto set pattern recognition\n"
             // << "\t-html \t\tto force html input format\n"
             << "\t-docx \t\tto force docx input format\n"
             << "\t-odt \t\tto force odt input format\n"
             << "\t-atom \t\tto work xml token by xml token\n";
             // << "\t-uns \t\tto use unstructured algorithm\n";
         out.flush();
      }

   bool isValid() const { return szParserLibrary && szInputFile && szOutputFile; }
   bool hasEchoedMessage() const { return fHasEchoedMessage; }

   class Extension {
     private:
      TypeInput tiTypeInput = TIUndefined;

     public:
      Extension() = default;
      Extension(TypeInput typeInput) : tiTypeInput(typeInput) {}
      Extension(const char* filename)
         {  STG::SString name(filename);
            auto extension = name.scanR('.');
            if (extension.isValid()) {
               extension->advance();
               if (extension->compareI("htm", 3) == CREqual)
                  tiTypeInput = TIHtml;
               else if (extension->compareI("docx") == CREqual)
                  tiTypeInput = TIDocx;
               else if (extension->compareI("odt") == CREqual)
                  tiTypeInput = TIOdt;
               else if (extension->compareI("ods") == CREqual)
                  tiTypeInput = TIOds;
            }
         }
      Extension(const Extension&) = default;

      bool isValid() const { return tiTypeInput != TIUndefined; }
      bool isHtml() const { return tiTypeInput == TIHtml; }
      bool isDocx() const { return tiTypeInput == TIDocx; }
      bool isOpenDocument() const { return tiTypeInput == TIOdt || tiTypeInput == TIOds; }
      operator bool() const { return  tiTypeInput != TIUndefined; }
   };
   friend class Extension;
   Extension getTypeInput() const
      {  if (!isValid())
            return Extension();
         if (tiInput != TIUndefined)
            return Extension(tiInput);
         return Extension(szInputFile);
      }
   const char* getInputFile() const { return szInputFile; }
   const char* getOutputFile() const { return szOutputFile; }
   const char* getParserLibrary() const { return szParserLibrary; }
   DDocx::Algorithm getAlgorithm() const { return aAlgorithm; }
   bool hasConfigFile() const { return szConfigFile; }
   const char* getConfigFile() const { return szConfigFile; }
};

bool
ProcessArgument::process(char** argument, int& currentArgument) {
   if (argument[0][0] == '-') {
      switch (argument[0][1]) {
         case 'o':
            if (currentArgument == 0 || szOutputFile) {
               printUsage(std::cout);
               return false;
            };
            currentArgument -= 2;
            szOutputFile = argument[1];
            return true;
         case 'd':
            if (strcmp(&argument[0][1], "docx") == 0)
               tiInput = TIDocx;
            else {
               printUsage(std::cout);
               fHasEchoedMessage = true;
            }
            --currentArgument;
            return true;
         case 'h':
            if (strcmp(&argument[0][1], "html") == 0 || strcmp(&argument[0][1], "htm") == 0)
               tiInput = TIHtml;
            else {
               printUsage(std::cout);
               fHasEchoedMessage = true;
            }
            --currentArgument;
            return true;
         case 'c':
            if (strcmp(&argument[0][1], "c") == 0 && currentArgument >= 1) {
               szConfigFile = argument[1];
               currentArgument -= 2;
            }
            else {
               printUsage(std::cout);
               fHasEchoedMessage = true;
               --currentArgument;
            }
            return true;
         case 'a':
            if (strcmp(&argument[0][1], "atom") == 0)
               aAlgorithm = DDocx::AAtomic;
            else {
               printUsage(std::cout);
               fHasEchoedMessage = true;
            }
            --currentArgument;
            return true;
         case 'u':
            if (strcmp(&argument[0][1], "uns") == 0)
               aAlgorithm = DDocx::AUnstructured;
            else {
               printUsage(std::cout);
               fHasEchoedMessage = true;
            }
            --currentArgument;
            return true;
         default:
            printUsage(std::cout);
            --currentArgument;
            return false;
      };
   }
   else {
      if (!szParserLibrary) {
         szParserLibrary = argument[0];
         --currentArgument;
      }
      else if (szInputFile) {
         printUsage(std::cout);
         --currentArgument;
         return false;
      }
      else {
         szInputFile = argument[0];
         --currentArgument;
      };
   };
   return true;
}

int main(int argc, char** argv) {
   ProcessArgument processArgument(argc, argv);
   if (argc == 1) {
      processArgument.printUsage(std::cout);
      return 0;
   };
   if (!processArgument.isValid()) {
      if (!processArgument.hasEchoedMessage())
         processArgument.printUsage(std::cout);
      return 0;
   };

   if (auto typeInput = processArgument.getTypeInput()) {
      if (typeInput.isHtml()) {
         std::cerr << "html not supported yet!\n" << std::endl;
         // HtmlParser parser(processArgument.getInputFile());
      }
      else if (typeInput.isDocx() || typeInput.isOpenDocument()) {
         DocxParser parser(processArgument.getInputFile(), typeInput.isOpenDocument(),
               processArgument.getAlgorithm());
         if (parser.setToDocument()) {
            Documentation documentation(processArgument.getOutputFile());
            if (documentation.setParser(processArgument.getParserLibrary())) {
               if (processArgument.hasConfigFile())
                  documentation.setConfiguration(processArgument.getConfigFile());
               documentation.initParser(parser.parser());
               // parser.setParseObject(documentation, (char*) nullptr);
               parser.parse();
            }
         }
         else
            std::cerr << "content not found in docx format!\n" << std::endl;
      }
      else {
         std::cerr << "unknown input file format!\n" << std::endl;
         if (!processArgument.hasEchoedMessage())
            processArgument.printUsage(std::cout);
      }
   }

   return 0;
}

