/**************************************************************************/
/*                                                                        */
/*  Copyright (C) 2019-2020                                               */
/*    CEA (Commissariat a l'Energie Atomique et aux Energies              */
/*         Alternatives)                                                  */
/*                                                                        */
/*  you can redistribute it and/or modify it under the terms of the GNU   */
/*  Lesser General Public License as published by the Free Software       */
/*  Foundation, version 2.1.                                              */
/*                                                                        */
/*  It is distributed in the hope that it will be useful,                 */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU Lesser General Public License for more details.                   */
/*                                                                        */
/*  See the GNU Lesser General Public License version 2.1                 */
/*  for more details (enclosed in the file LICENSE).                      */
/*                                                                        */
/**************************************************************************/

/////////////////////////////////
//
// Application : documentation to json converter
// Unit      : main program
// File      : DocumentationFormat.cpp
// Description :
//   Implementation of the documentation converter
//

#include "DocumentationFormat.h"
#include <dlfcn.h>

namespace Details {

class XMLParserState : public STG::Parser::TStateStack<STG::XML::CommonParser::Arguments>
      ::VirtualParseState {
  private:
   typedef STG::Parser::TStateStack<STG::XML::CommonParser::Arguments>::VirtualParseState inherited;
   typedef enum _ReadResult (*ReadFunction)(void*, struct _XMLParserArguments*, const struct _XMLParserArgumentsFunctions*,
         struct _XMLParserStateStack*, const struct _XMLParserStateStackFunctions*);
   size_t sizeResult;
   ReadFunction read_function;
   void (*init_result)(void*);
   void (*move_result)(void*, void*);
   void (*copy_result)(void*, const void*);
   void (*finalize_result)(void*);
   void* poObject;

  protected:
   virtual ComparisonResult _compare(const EnhancedObject& asource) const override
      {  ComparisonResult result = inherited::_compare(asource);
         const XMLParserState& source = (const XMLParserState&) asource;
         return (read_function != source.read_function) ? CRNonComparable : result;
      }
   virtual int getSize() const override { return sizeof(XMLParserState)+sizeResult; }
   virtual int getAlign() const override { return alignof(XMLParserState); }
   virtual void moveTo(void* dst) override
      {  new (dst) XMLParserState();
         *reinterpret_cast<XMLParserState*>(dst) = *this;
         (*init_result)((char*) dst+sizeof(XMLParserState));
         (*move_result)((char*) dst+sizeof(XMLParserState), (char*) this+sizeof(XMLParserState));
      }
   virtual void copyTo(void* dst) override
      {  new (dst) XMLParserState();
         *reinterpret_cast<XMLParserState*>(dst) = *this;
         (*init_result)((char*) dst+sizeof(XMLParserState));
         (*copy_result)((char*) dst+sizeof(XMLParserState), (const char*) this+sizeof(XMLParserState));
      }

  public:
   XMLParserState()
      :  sizeResult(0), read_function(nullptr), init_result(nullptr), move_result(nullptr),
         copy_result(nullptr), finalize_result(nullptr), poObject(nullptr)
      {}
   XMLParserState(const XMLParserState& source)
      :  inherited(source), sizeResult(source.sizeResult), read_function(source.read_function),
         init_result(source.init_result), move_result(source.move_result), copy_result(source.copy_result),
         finalize_result(source.finalize_result), poObject(source.poObject)
      {  if (sizeResult && init_result && copy_result) {
            (*init_result)((char*)this+sizeof(XMLParserState));
            (*copy_result)((char*)this+sizeof(XMLParserState), (const char*)&source+sizeof(XMLParserState));
         }
      }
   ~XMLParserState()
      {  (*finalize_result)((char*) this+sizeof(XMLParserState)); }
   XMLParserState& operator=(const XMLParserState& source)
      {  inherited::operator=(source);
         if (this == &source)
            return *this;
         if (sizeResult && finalize_result)
            (*finalize_result)((char*) this+sizeResult);
         sizeResult = source.sizeResult;
         read_function = source.read_function;
         init_result = source.init_result;
         move_result = source.move_result;
         copy_result = source.copy_result;
         finalize_result = source.finalize_result;
         poObject = source.poObject;
         if (sizeResult && init_result)
            (*init_result)((char*)this+sizeof(XMLParserState));
         if (sizeResult && copy_result)
            (*copy_result)((char*)this+sizeof(XMLParserState), (const char*)&source+sizeof(XMLParserState));
         return *this;
      }
   DefineCopy(XMLParserState)
   DDefineAssign(XMLParserState)

   void setReadMethod(ReadFunction readFunction) { read_function = readFunction; }
   void setResult(size_t asizeResult, void (*initResult)(void*), void (*moveResult)(void*, void*),
         void (*copyResult)(void*, const void*), void (*finalizeResult)(void*))
      {  AssumeCondition(!sizeResult)
         sizeResult = asizeResult;
         init_result = initResult;
         move_result = moveResult;
         copy_result = copyResult;
         finalize_result = finalizeResult;
         if (sizeResult && init_result)
            (*init_result)((char*)this+sizeof(XMLParserState));
      }
   void setObject(void* object) { poObject = object; }
   virtual void* getResultPlace() override
      {  AssumeCondition(sizeResult && init_result)
         return (char*) this+sizeof(XMLParserState);
      }
   virtual auto operator()(STG::Parser::TStateStack<STG::XML::CommonParser::Arguments>& stateStack,
         STG::XML::CommonParser::Arguments& args)
      -> STG::XML::CommonParser::Arguments::ResultAction override
      {  AssumeCondition(read_function)
         return (STG::XML::CommonParser::Arguments::ResultAction)
            (*read_function)(poObject,
                  reinterpret_cast<struct _XMLParserArguments*>(&args), &Documentation::ParserArgumentsFunctions::functions,
                  reinterpret_cast<struct _XMLParserStateStack*>(&stateStack), &Documentation::ParserStateStackFunctions::functions);
      }
};

} // end of namespace Details

void
Documentation::ParserStateFunctions::set_object_instr(struct _XMLParserState* top_state,
      void* object) {
   auto& topState = *reinterpret_cast<Details::XMLParserState*>(top_state);
   topState.setObject(object);
}

struct _XMLParserStateFunctions
Documentation::ParserStateFunctions::functions = { &set_object_instr };

bool
Documentation::ParserStateStackFunctions::skip_node_instr(struct _XMLParserStateStack* astate,
      struct _XMLParserArguments* aarguments, enum _ReadResult* result, void** ruleResult) {
   auto& state = *reinterpret_cast<STG::XML::CommonParser::State*>(astate);
   auto& arguments = *reinterpret_cast<STG::XML::CommonParser::Arguments*>(aarguments);
   if (!arguments.isOpenStartElement())
      return ::RRContinue;
   arguments.shift();
   auto& local = state.shiftResult((STG::XML::CommonParser::SkipNode*) nullptr,
         &STG::XML::CommonParser::SkipNode::skipInLoop, STG::XML::CommonParser::SkipNode());
   local.setObject(state.getSResult((STG::XML::CommonParser::SkipNode*) nullptr));
   if (ruleResult && *ruleResult)
      *ruleResult = nullptr;
   *result = ::RRContinue;
   if (!arguments.parseTokens(state, *reinterpret_cast<STG::Lexer::Base::ReadResult*>(result)))
      return false;
   return arguments.setToNextToken(*reinterpret_cast<STG::Lexer::Base::ReadResult*>(result));
}

bool
Documentation::ParserStateStackFunctions::skip_node_content_instr(struct _XMLParserStateStack* astate,
      struct _XMLParserArguments* aarguments, enum _ReadResult* result, void** ruleResult) {
   auto& state = *reinterpret_cast<STG::XML::CommonParser::State*>(astate);
   auto& arguments = *reinterpret_cast<STG::XML::CommonParser::Arguments*>(aarguments);
   arguments.shift();
   auto& local = state.shiftResult((STG::XML::CommonParser::SkipNode*) nullptr,
         &STG::XML::CommonParser::SkipNode::skipInLoop, STG::XML::CommonParser::SkipNode());
   local.setObject(state.getSResult((STG::XML::CommonParser::SkipNode*) nullptr));
   state.point() = 1;
   *result = ::RRContinue;
   if (ruleResult && *ruleResult)
      *ruleResult = nullptr;
   if (!arguments.parseTokens(state, *reinterpret_cast<STG::Lexer::Base::ReadResult*>(result)))
      return false;
   return arguments.setToNextToken(*reinterpret_cast<STG::Lexer::Base::ReadResult*>(result));
}

void*
Documentation::ParserStateStackFunctions::get_rule_result_instr(struct _XMLParserStateStack* astate) {
   auto& state = *reinterpret_cast<STG::XML::CommonParser::State*>(astate);
   return state.getResultPlace();
}

int*
Documentation::ParserStateStackFunctions::get_point_instr(struct _XMLParserStateStack* astate) {
   auto& state = *reinterpret_cast<STG::XML::CommonParser::State*>(astate);
   return &state.point();
}

bool
Documentation::ParserStateStackFunctions::does_contain_address_instr(struct _XMLParserStateStack* astate, void* pointer) {
   auto& state = *reinterpret_cast<STG::XML::CommonParser::State*>(astate);
   return state.doesContainAddress(pointer);
}

size_t
Documentation::ParserStateStackFunctions::convert_address_to_relative_instr(struct _XMLParserStateStack* astate, void* pointer) {
   auto& state = *reinterpret_cast<STG::XML::CommonParser::State*>(astate);
   return state.convertAddressToRelative(pointer);
}

void*
Documentation::ParserStateStackFunctions::convert_relative_to_address_instr(struct _XMLParserStateStack* astate, size_t shift) {
   auto& state = *reinterpret_cast<STG::XML::CommonParser::State*>(astate);
   return state.convertRelativeToAddress(shift);
}

struct _XMLParserStateStackFunctions
Documentation::ParserStateStackFunctions::functions =
   {  nullptr, nullptr, &skip_node_instr, &skip_node_content_instr, &get_rule_result_instr,
      &get_point_instr, &does_contain_address_instr, &convert_address_to_relative_instr,
      &convert_relative_to_address_instr
   };


bool
Documentation::ParserArgumentsFunctions::has_event(const struct _XMLParserArguments* aarguments) {
   const auto& arguments = *reinterpret_cast<const STG::XML::CommonParser::Arguments*>(aarguments);
   return arguments.hasEvent();
}

bool
Documentation::ParserArgumentsFunctions::is_open_start_element(const struct _XMLParserArguments* aarguments) {
   const auto& arguments = *reinterpret_cast<const STG::XML::CommonParser::Arguments*>(aarguments);
   return arguments.isOpenStartElement();
}

bool
Documentation::ParserArgumentsFunctions::is_close_start_element(const struct _XMLParserArguments* aarguments) {
   const auto& arguments = *reinterpret_cast<const STG::XML::CommonParser::Arguments*>(aarguments);
   return arguments.isCloseStartElement();
}

bool
Documentation::ParserArgumentsFunctions::is_attribute(const struct _XMLParserArguments* aarguments) {
   const auto& arguments = *reinterpret_cast<const STG::XML::CommonParser::Arguments*>(aarguments);
   return arguments.isAttribute();
}

bool
Documentation::ParserArgumentsFunctions::is_open_attribute(const struct _XMLParserArguments* aarguments) {
   const auto& arguments = *reinterpret_cast<const STG::XML::CommonParser::Arguments*>(aarguments);
   return arguments.isOpenAttribute();
}

bool
Documentation::ParserArgumentsFunctions::is_add_attribute(const struct _XMLParserArguments* aarguments) {
   const auto& arguments = *reinterpret_cast<const STG::XML::CommonParser::Arguments*>(aarguments);
   return arguments.isAddAttribute();
}

bool
Documentation::ParserArgumentsFunctions::is_end_element(const struct _XMLParserArguments* aarguments) {
   const auto& arguments = *reinterpret_cast<const STG::XML::CommonParser::Arguments*>(aarguments);
   return arguments.isEndElement();
}

bool
Documentation::ParserArgumentsFunctions::is_close_element(const struct _XMLParserArguments* aarguments) {
   const auto& arguments = *reinterpret_cast<const STG::XML::CommonParser::Arguments*>(aarguments);
   return arguments.isCloseElement();
}

bool
Documentation::ParserArgumentsFunctions::is_text(const struct _XMLParserArguments* aarguments) {
   const auto& arguments = *reinterpret_cast<const STG::XML::CommonParser::Arguments*>(aarguments);
   return arguments.isText();
}

bool
Documentation::ParserArgumentsFunctions::is_extended_text(const struct _XMLParserArguments* aarguments) {
   const auto& arguments = *reinterpret_cast<const STG::XML::CommonParser::Arguments*>(aarguments);
   return arguments.isExtendedText();
}

const char*
Documentation::ParserArgumentsFunctions::get_value(const struct _XMLParserArguments* aarguments) {
   auto& arguments = *reinterpret_cast<STG::XML::CommonParser::Arguments*>(const_cast<struct _XMLParserArguments*>(aarguments));
   const auto* result = arguments.value().getChunk().string;
   return result ? result : "";
}

const char*
Documentation::ParserArgumentsFunctions::get_extended_value(const struct _XMLParserArguments* aarguments) {
   auto& arguments = *reinterpret_cast<STG::XML::CommonParser::Arguments*>(const_cast<struct _XMLParserArguments*>(aarguments));
   const auto* result = arguments.value().getChunk().string;
   return result ? result : "";
}

const char*
Documentation::ParserArgumentsFunctions::get_attribute(const struct _XMLParserArguments* aarguments) {
   auto& arguments = *reinterpret_cast<STG::XML::CommonParser::Arguments*>(const_cast<struct _XMLParserArguments*>(aarguments));
   const auto* result = arguments.attribute().getChunk().string;
   return result ? result : "";
}

void
Documentation::ParserArgumentsFunctions::set_to_next_token(struct _XMLParserArguments* aarguments) {
   auto& arguments = *reinterpret_cast<STG::XML::CommonParser::Arguments*>(aarguments);
   STG::Lexer::Base::ReadResult result;
   arguments.setToNextToken(result);
}

bool
Documentation::ParserArgumentsFunctions::set_to_next_token_instr(struct _XMLParserArguments* aarguments,
      enum _ReadResult* result) {
   auto& arguments = *reinterpret_cast<STG::XML::CommonParser::Arguments*>(aarguments);
   return arguments.setToNextToken(*reinterpret_cast<STG::Lexer::Base::ReadResult*>(result));
}

enum _ReadResult
Documentation::ParserArgumentsFunctions::is_equal_value_instr(const struct _XMLParserArguments* aarguments,
      bool* result, const struct _ExtString* avalue) {
   auto& arguments = *reinterpret_cast<STG::XML::CommonParser::Arguments*>(const_cast<struct _XMLParserArguments*>(aarguments));
   const STG::SubString& value = *reinterpret_cast<const STG::SubString*>(avalue);
   STG::SubString oldValue = arguments.getValueCompare();
   if (!arguments.isContinuedToken())
      arguments.setValueCompare(value);
   if (arguments.assumeReaderValue() == STG::Lexer::Base::RRNeedChars) {
      *result = arguments.isCompareEqual();
      return ::RRNeedChars;
   }
   arguments.resetValueCompare(oldValue);
   *result = arguments.isCompareEqual();
   return ::RRContinue;
}

enum _ReadResult
Documentation::ParserArgumentsFunctions::is_equal_attribute_instr(const struct _XMLParserArguments* aarguments,
      bool* result, const struct _ExtString* aattribute) {
   auto& arguments = *reinterpret_cast<STG::XML::CommonParser::Arguments*>(const_cast<struct _XMLParserArguments*>(aarguments));
   const STG::SubString& attribute = *reinterpret_cast<const STG::SubString*>(aattribute);
   if (arguments.isOpenAttribute()) {
      STG::SubString oldValue = arguments.getValueCompare();
      if (!arguments.isContinuedToken())
         arguments.setAttributeCompare(attribute);
      if (arguments.assumeReaderAttribute() == STG::Lexer::Base::RRNeedChars) {
         *result = arguments.isCompareEqual();
         return ::RRNeedChars;
      }
      arguments.resetValueCompare(oldValue);
   }
   *result = arguments.isCompareEqual() && arguments.isAddAttribute();
   if (*result)
      if (arguments.convertReaderToValue() == RRNeedChars) return ::RRNeedChars;
   return ::RRContinue;
}

enum _ReadResult
Documentation::ParserArgumentsFunctions::load_value_instr(const struct _XMLParserArguments* aarguments) {
   auto& arguments = *reinterpret_cast<STG::XML::CommonParser::Arguments*>(const_cast<struct _XMLParserArguments*>(aarguments));
   return (enum _ReadResult) arguments.convertReaderToValue();
}

enum _ReadResult
Documentation::ParserArgumentsFunctions::load_extended_value_instr(const struct _XMLParserArguments* aarguments) {
   auto& arguments = *reinterpret_cast<STG::XML::CommonParser::Arguments*>(const_cast<struct _XMLParserArguments*>(aarguments));
   return (enum _ReadResult) arguments.convertExtendedReaderToValue();
}

enum _ReadResult
Documentation::ParserArgumentsFunctions::is_equal_attribute_then_load_value_instr(
      const struct _XMLParserArguments* aarguments, bool* result,
      const struct _ExtString* aattribute) {
   auto& arguments = *reinterpret_cast<STG::XML::CommonParser::Arguments*>(const_cast<struct _XMLParserArguments*>(aarguments));
   const STG::SubString& attribute = *reinterpret_cast<const STG::SubString*>(aattribute);
   if (arguments.isOpenAttribute()) {
      STG::SubString oldValue = arguments.getValueCompare();
      if (!arguments.isContinuedToken())
         arguments.setValueCompare(attribute);
      if (arguments.assumeReaderAttribute() == STG::Lexer::Base::RRNeedChars) {
         *result = arguments.isCompareEqual();
         return ::RRNeedChars;
      }
      arguments.resetValueCompare(oldValue);
      STG::Lexer::Base::ReadResult localResult = STG::Lexer::Base::RRContinue;
      if (!arguments.setToNextToken(localResult)) return ::RRNeedChars;
   }
   *result = arguments.isCompareEqual() && arguments.isAddAttribute();
   if (*result)
      if (arguments.convertReaderToValue() == RRNeedChars) return ::RRNeedChars;
   return ::RRContinue;
}

enum _ReadResult
Documentation::ParserArgumentsFunctions::is_equal_attribute_then_load_extended_value_instr(
      const struct _XMLParserArguments* aarguments, bool* result,
      const struct _ExtString* aattribute) {
   auto& arguments = *reinterpret_cast<STG::XML::CommonParser::Arguments*>(const_cast<struct _XMLParserArguments*>(aarguments));
   const STG::SubString& attribute = *reinterpret_cast<const STG::SubString*>(aattribute);
   if (arguments.isOpenAttribute()) {
      STG::SubString oldValue = arguments.getValueCompare();
      if (!arguments.isContinuedToken())
         arguments.setValueCompare(attribute);
      if (arguments.assumeReaderAttribute() == STG::Lexer::Base::RRNeedChars) {
         *result = arguments.isCompareEqual();
         return ::RRNeedChars;
      }
      arguments.resetValueCompare(oldValue);
      STG::Lexer::Base::ReadResult localResult = STG::Lexer::Base::RRContinue;
      if (!arguments.setToNextToken(localResult)) return ::RRNeedChars;
   }
   *result = arguments.isCompareEqual() && arguments.isAddAttribute();
   if (*result)
      if (arguments.convertExtendedReaderToValue() == RRNeedChars) return ::RRNeedChars;
   return ::RRContinue;
}

enum _ReadResult
Documentation::ParserArgumentsFunctions::load_attribute_instr(const struct _XMLParserArguments* aarguments) {
   auto& arguments = *reinterpret_cast<STG::XML::CommonParser::Arguments*>(const_cast<struct _XMLParserArguments*>(aarguments));
   return (enum _ReadResult) arguments.convertReaderToAttribute();
}

enum _ReadResult
Documentation::ParserArgumentsFunctions::load_attribute_and_value_instr(const struct _XMLParserArguments* aarguments) {
   auto& arguments = *reinterpret_cast<STG::XML::CommonParser::Arguments*>(const_cast<struct _XMLParserArguments*>(aarguments));
   if (arguments.isOpenAttribute()) {
      STG::Lexer::Base::ReadResult result = arguments.convertReaderToAttribute();
      if (result == STG::Lexer::Base::RRNeedChars) return (enum _ReadResult) result;
      if (!arguments.setToNextToken(result)) return ::RRNeedChars;
   }
   if (arguments.isAttribute())
      if (arguments.convertReaderToValue() == STG::Lexer::Base::RRNeedChars) return ::RRNeedChars;
   return ::RRContinue;
}

enum _ReadResult
Documentation::ParserArgumentsFunctions::load_attribute_and_extended_value_instr(const struct _XMLParserArguments* aarguments) {
   auto& arguments = *reinterpret_cast<STG::XML::CommonParser::Arguments*>(const_cast<struct _XMLParserArguments*>(aarguments));
   if (arguments.isOpenAttribute()) {
      STG::Lexer::Base::ReadResult result = arguments.convertReaderToAttribute();
      if (result == STG::Lexer::Base::RRNeedChars) return (enum _ReadResult) result;
      if (!arguments.setToNextToken(result)) return ::RRNeedChars;
   }
   if (arguments.isAttribute())
      if (arguments.convertExtendedReaderToValue() == STG::Lexer::Base::RRNeedChars) return ::RRNeedChars;
   return ::RRContinue;
}

struct _XMLParserState*
Documentation::ParserArgumentsFunctions::shift_result_instr(struct _XMLParserArguments* aarguments,
      const struct _XMLParserArgumentsFunctions* arguments_functions, struct _XMLParserStateStack* astate,
      const struct _XMLParserStateStackFunctions* state_functions, size_t size_result,
      enum _ReadResult (*read_function_instr)(void*, struct _XMLParserArguments*,
         const struct _XMLParserArgumentsFunctions*, struct _XMLParserStateStack*, const struct _XMLParserStateStackFunctions*),
      void (*init_object)(void*), void (*move_object)(void*, void*), void (*copy_object)(void*, const void*),
      void (*finalize_object)(void*), void** ruleResult, const struct _XMLParserStateFunctions** functions) {
   auto& arguments = *reinterpret_cast<STG::XML::CommonParser::Arguments*>(aarguments);
   arguments.shift();
   auto& state = *reinterpret_cast<STG::XML::CommonParser::State*>(astate);
   int previousSize = 0;
   void* dest = state.allocateResult(sizeof(Details::XMLParserState) + size_result, previousSize);
   auto* top_state = new (dest) Details::XMLParserState;
   state.setResultPreviousSize(previousSize);
   top_state->setReadMethod(read_function_instr);
   top_state->setResult(size_result, init_object, move_object, copy_object, finalize_object);
   if (ruleResult && *ruleResult)
      *ruleResult = nullptr;
   *functions = &Documentation::ParserStateFunctions::functions;
   return reinterpret_cast<struct _XMLParserState*>(top_state);
}

void
Documentation::ParserArgumentsFunctions::reduce_state_instr(struct _XMLParserArguments* aarguments,
      struct _XMLParserStateStack* astate, void** ruleResult) {
   auto& arguments = *reinterpret_cast<STG::XML::CommonParser::Arguments*>(aarguments);
   auto& state = *reinterpret_cast<STG::XML::CommonParser::State*>(astate);
   arguments.reduceState(state);
   if (ruleResult && *ruleResult)
      *ruleResult = nullptr;
}

bool
Documentation::ParserArgumentsFunctions::parse_token_instr(struct _XMLParserArguments* aarguments,
      struct _XMLParserStateStack* astate, enum _ReadResult* result) {
   auto& arguments = *reinterpret_cast<STG::XML::CommonParser::Arguments*>(aarguments);
   auto& state = *reinterpret_cast<STG::XML::CommonParser::State*>(astate);
   if (!arguments.parseTokens(state, *reinterpret_cast<STG::Lexer::Base::ReadResult*>(result)))
      return false;
   return arguments.setToNextToken(*reinterpret_cast<STG::Lexer::Base::ReadResult*>(result));
}

int
Documentation::ParserArgumentsFunctions::compare_value_instr(const struct _XMLParserArguments* aarguments,
      const struct _ExtString* atext) {
   auto& arguments = *reinterpret_cast<STG::XML::CommonParser::Arguments*>(const_cast<struct _XMLParserArguments*>(aarguments));
   const auto& text = *reinterpret_cast<const STG::SubString*>(atext);
   return arguments.value().compare(text);
}

int
Documentation::ParserArgumentsFunctions::compare_attribute_instr(const struct _XMLParserArguments* aarguments,
      const struct _ExtString* atext) {
   auto& arguments = *reinterpret_cast<STG::XML::CommonParser::Arguments*>(const_cast<struct _XMLParserArguments*>(aarguments));
   const auto& text = *reinterpret_cast<const STG::SubString*>(atext);
   return arguments.attribute().compare(text);
}


struct _XMLParserArgumentsFunctions
Documentation::ParserArgumentsFunctions::functions =
   {  &has_event, &is_open_start_element, &is_close_start_element, &is_attribute,
      &is_open_attribute, &is_add_attribute, &is_end_element, &is_close_element, &is_text,
      &is_extended_text, &get_value, &get_extended_value, &get_attribute, &set_to_next_token,
      &set_to_next_token_instr, &is_equal_value_instr, &is_equal_attribute_instr,
      &load_value_instr, &load_extended_value_instr, &is_equal_attribute_then_load_value_instr,
      &is_equal_attribute_then_load_extended_value_instr,
      &load_attribute_instr, &load_attribute_and_value_instr,
      &load_attribute_and_extended_value_instr,
      &shift_result_instr, &reduce_state_instr, &parse_token_instr,
      &compare_value_instr, &compare_attribute_instr,
   };


void
Documentation::JSonWriterFunctions::start(struct _JSonWriter* awriter) {
   JSonWriter& writer = *reinterpret_cast<JSonWriter*>(awriter);
   writer.istart();
}

void
Documentation::JSonWriterFunctions::add_key(struct _JSonWriter* awriter, const char* text) {
   JSonWriter& writer = *reinterpret_cast<JSonWriter*>(awriter);
   writer.iaddKey(STG::SString(text), writer.buffer, writer.out);
}

void
Documentation::JSonWriterFunctions::open_array(struct _JSonWriter* awriter) {
   JSonWriter& writer = *reinterpret_cast<JSonWriter*>(awriter);
   writer.iopenArray(writer.buffer, writer.out);
}

void
Documentation::JSonWriterFunctions::close_array(struct _JSonWriter* awriter) {
   JSonWriter& writer = *reinterpret_cast<JSonWriter*>(awriter);
   writer.icloseArray(writer.buffer, writer.out);
}

void
Documentation::JSonWriterFunctions::open_object(struct _JSonWriter* awriter) {
   JSonWriter& writer = *reinterpret_cast<JSonWriter*>(awriter);
   writer.iopenObject(writer.buffer, writer.out);
}

void
Documentation::JSonWriterFunctions::close_object(struct _JSonWriter* awriter) {
   JSonWriter& writer = *reinterpret_cast<JSonWriter*>(awriter);
   writer.icloseObject(writer.buffer, writer.out);
}

void
Documentation::JSonWriterFunctions::add_string(struct _JSonWriter* awriter, const char* text) {
   JSonWriter& writer = *reinterpret_cast<JSonWriter*>(awriter);
   writer.iaddString(STG::SString(text), writer.buffer, writer.out);
}

void
Documentation::JSonWriterFunctions::add_float(struct _JSonWriter* awriter, double val) {
   JSonWriter& writer = *reinterpret_cast<JSonWriter*>(awriter);
   writer.iaddFloat(val, writer.buffer, writer.out);
}

void
Documentation::JSonWriterFunctions::add_ivalue(struct _JSonWriter* awriter, int64_t val) {
   JSonWriter& writer = *reinterpret_cast<JSonWriter*>(awriter);
   writer.iaddLInt(val, writer.buffer, writer.out);
}

void
Documentation::JSonWriterFunctions::add_uvalue(struct _JSonWriter* awriter, uint64_t val) {
   JSonWriter& writer = *reinterpret_cast<JSonWriter*>(awriter);
   writer.iaddLUInt(val, writer.buffer, writer.out);
}

void
Documentation::JSonWriterFunctions::add_null_value(struct _JSonWriter* awriter) {
   JSonWriter& writer = *reinterpret_cast<JSonWriter*>(awriter);
   writer.iaddNull(writer.buffer, writer.out);
}

void
Documentation::JSonWriterFunctions::end(struct _JSonWriter* awriter) {
   JSonWriter& writer = *reinterpret_cast<JSonWriter*>(awriter);
   writer.hasEnded = true;
   writer.iend(writer.buffer, writer.out);
}

struct _JSonWriterFunctions
Documentation::JSonWriterFunctions::functions =
   {  &start, &add_key, &open_array, &close_array, &open_object, &close_object, &add_string,
      &add_float, &add_ivalue, &add_uvalue, &add_null_value, &end,
   };

void
Documentation::Configuration::set_key_value(struct _DocumentConfiguration* aconfiguration,
      const char* key, const char* value) {
   auto& configuration = *reinterpret_cast<Documentation::Configuration*>(aconfiguration);
   configuration.mMapKeyValues[key] = value;
}

const char*
Documentation::Configuration::get_value(struct _DocumentConfiguration* aconfiguration,
      const char* key) {
   auto& configuration = *reinterpret_cast<Documentation::Configuration*>(aconfiguration);
   return configuration.mMapKeyValues[key].c_str();
}

const struct _ExtString*
Documentation::Configuration::convert_string(struct _DocumentConfiguration* aconfiguration,
      const char* text) {
   auto& configuration = *reinterpret_cast<Documentation::Configuration*>(aconfiguration);
   configuration.assConvertedStrings.insertNewAtEnd(new STG::SString(text));
   return reinterpret_cast<struct _ExtString*>(&configuration.assConvertedStrings.getSLast());
}

struct _DocumentConfigurationFunctions
Documentation::Configuration::functions = { &set_key_value, &get_value, &convert_string };

enum _ReadResult
Documentation::Configuration::readXML(void* athisConfig, struct _XMLParserArguments* aarguments,
      const struct _XMLParserArgumentsFunctions* arguments_functions, struct _XMLParserStateStack* astate,
      const struct _XMLParserStateStackFunctions* state_functions) {
   auto* thisConfig = reinterpret_cast<Configuration*>(athisConfig);
   AssumeCondition(thisConfig->load_configuration_instr)
   return (*thisConfig->load_configuration_instr)(
      reinterpret_cast<struct _DocumentConfiguration*>(athisConfig), &Configuration::functions,
      aarguments, arguments_functions, astate, state_functions);
}

bool
Documentation::setParser(const char* libraryName) {
   dlParserLibrary.setFromFile(libraryName);
   bool result = dlParserLibrary.isOpen();
   if (!result) {
      std::cerr << "unable to load dynamic library " << libraryName << "!\n" << std::endl;
      std::cerr << "load error message: ";
#ifndef _WIN32
      std::cerr << (char*) dlerror();
#else
      std::cerr << (long unsigned int) GetLastError();
#endif
      std::cerr << std::endl;
   }
   try {
      dlParserLibrary.loadSymbol("decoder_load_configuration_instr", &cConfiguration.load_configuration_instr);
      dlParserLibrary.loadSymbol("decoder_load_rule_configuration", &cConfiguration.load_rule);
      dlParserLibrary.loadSymbol("decoder_load_document_instr", &load_document_instr);
      dlParserLibrary.loadSymbol("decoder_load_rule_document", &load_rule);
   } catch (STG::EReadError& error) {
      std::cerr << "symbol not loaded: " << error.getMessage() << "!" << std::endl;
      result = false;
   }
   return result;
}

void
Documentation::setConfiguration(const char* filename) {
   STG::XML::CommonParser localParse;
   localParse.setPartialToken();
   struct _DocumentRuleFunctions ruleFunctions{};
   int sizeResult = (*cConfiguration.load_rule)(&ruleFunctions);
   int previousSize = 0;
   void* stackPointer = localParse.state().allocateResult(sizeResult+sizeof(Details::XMLParserState), previousSize);
   auto* parseState = new (stackPointer) Details::XMLParserState();
   localParse.state().setResultPreviousSize(previousSize);
   parseState->setReadMethod(&Configuration::readXML);
   parseState->setResult(sizeResult, ruleFunctions.init_object, ruleFunctions.move_object,
         ruleFunctions.copy_object, ruleFunctions.finalize_object);
   parseState->setObject(&cConfiguration);
   // localParse.state().shift(cConfiguration, &Configuration::readXML, (int*) nullptr);
   STG::DIOObject::IFStream in(filename);
   try {
      localParse.parse(in);
   }
   catch (STG::EReadError&) {}
}

void
Documentation::initParser(STG::XML::CommonParser& parser) {
   struct _DocumentRuleFunctions ruleFunctions{};
   int sizeResult = (*load_rule)(&ruleFunctions);
   int previousSize = 0;
   void* stackPointer = parser.state().allocateResult(sizeResult+sizeof(Details::XMLParserState), previousSize);
   auto* parseState = new (stackPointer) Details::XMLParserState();
   parser.state().setResultPreviousSize(previousSize);
   parseState->setReadMethod(&Documentation::readXML);
   parseState->setResult(sizeResult, ruleFunctions.init_object, ruleFunctions.move_object,
         ruleFunctions.copy_object, ruleFunctions.finalize_object);
   parseState->setObject(this);
}

enum _ReadResult
Documentation::readXML(void* athisDocument, struct _XMLParserArguments* aarguments,
      const struct _XMLParserArgumentsFunctions* arguments_functions, struct _XMLParserStateStack* astate,
      const struct _XMLParserStateStackFunctions* state_functions) {
   auto* thisDocument = reinterpret_cast<Documentation*>(athisDocument);
   AssumeCondition(thisDocument->load_document_instr)
   return (*thisDocument->load_document_instr)(
      aarguments, arguments_functions, astate, state_functions,
      reinterpret_cast<struct _DocumentConfiguration*>(&thisDocument->cConfiguration),
         &Configuration::functions,
      reinterpret_cast<struct _JSonWriter*>(&thisDocument->jwWriter),
         &JSonWriterFunctions::functions);
}

