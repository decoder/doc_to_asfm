/**************************************************************************/
/*                                                                        */
/*  Copyright (C) 2019-2020                                               */
/*    CEA (Commissariat a l'Energie Atomique et aux Energies              */
/*         Alternatives)                                                  */
/*                                                                        */
/*  you can redistribute it and/or modify it under the terms of the GNU   */
/*  Lesser General Public License as published by the Free Software       */
/*  Foundation, version 2.1.                                              */
/*                                                                        */
/*  It is distributed in the hope that it will be useful,                 */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU Lesser General Public License for more details.                   */
/*                                                                        */
/*  See the GNU Lesser General Public License version 2.1                 */
/*  for more details (enclosed in the file LICENSE).                      */
/*                                                                        */
/**************************************************************************/

/////////////////////////////////
//
// Application : documentation to json converter
// Unit      : header for state-machine instrumentation
// File      : header_doc.hpp
// Description :
//   header file to read the documentation
//

#include "header-doc.hpp"

extern "C" {

void decoder_load_configuration(struct _DocumentConfiguration* config,
      const struct _DocumentConfigurationFunctions* config_functions,
      struct _XMLParserArguments* aarguments,
      const struct _XMLParserArgumentsFunctions* arguments_functions,
      struct _XMLParserStateStack* astate,
      const struct _XMLParserStateStackFunctions* state_functions)
{  DocumentConfiguration configuration(config, config_functions);
   XMLParserArguments arguments(aarguments, arguments_functions);
   XMLParserStateStack state(astate, state_functions);
   configuration.load(arguments, state);
}

void decoder_load_document(struct _XMLParserArguments* aarguments,
      const struct _XMLParserArgumentsFunctions* arguments_functions,
      struct _XMLParserStateStack* astate,
      const struct _XMLParserStateStackFunctions* state_functions,
      struct _DocumentConfiguration* config,
      const struct _DocumentConfigurationFunctions* config_functions,
      struct _JSonWriter* writer_content, const struct _JSonWriterFunctions* writer_functions)
{  Document document(config, config_functions, writer_content, writer_functions);
   XMLParserArguments arguments(aarguments, arguments_functions);
   XMLParserStateStack state(astate, state_functions);
   document.load(arguments, state);
}

DLL_API int decoder_load_rule_configuration(struct _DocumentRuleFunctions* functions)
{  
#ifdef INSTR
   functions->init_object = &DocumentConfiguration::init_rule_object;
   functions->move_object = &DocumentConfiguration::move_rule_object;
   functions->copy_object = &DocumentConfiguration::copy_rule_object;
   functions->finalize_object = &DocumentConfiguration::finalize_rule_object;
   return DocumentConfiguration::getRuleSize();
#else
   return 0;
#endif
}

DLL_API enum _ReadResult decoder_load_configuration_instr(
      struct _DocumentConfiguration* config,
      const struct _DocumentConfigurationFunctions* config_functions,
      struct _XMLParserArguments* aarguments,
      const struct _XMLParserArgumentsFunctions* arguments_functions,
      struct _XMLParserStateStack* astate,
      const struct _XMLParserStateStackFunctions* state_functions)
{  DocumentConfiguration configuration(config, config_functions);
#ifdef INSTR
   return (enum _ReadResult) DocumentConfiguration::loadInstr(&configuration, aarguments,
         arguments_functions, astate, state_functions);
#else
   configuration.load(arguments, state);
   return RRFinished;
#endif
}

DLL_API int decoder_load_rule_document(struct _DocumentRuleFunctions* functions)
{
#ifdef INSTR
   functions->init_object = &Document::init_rule_object;
   functions->move_object = &Document::move_rule_object;
   functions->copy_object = &Document::copy_rule_object;
   functions->finalize_object = &Document::finalize_rule_object;
   return Document::getRuleSize();
#else
   return 0;
#endif
}

DLL_API enum _ReadResult decoder_load_document_instr(struct _XMLParserArguments* aarguments,
      const struct _XMLParserArgumentsFunctions* arguments_functions,
      struct _XMLParserStateStack* astate,
      const struct _XMLParserStateStackFunctions* state_functions,
      struct _DocumentConfiguration* config,
      const struct _DocumentConfigurationFunctions* config_functions,
      struct _JSonWriter* writer_content, const struct _JSonWriterFunctions* writer_functions)
{  
#ifdef INSTR
   Document document(config, config_functions, writer_content, writer_functions);
   Document::queryDocumentFromRule(*(Document::RuleResult*) (*state_functions->get_rule_result_instr)(astate))
      = document;
   return (enum _ReadResult) Document::loadInstr(aarguments, arguments_functions,
         astate, state_functions);
#else
   Document document(config, config_functions, writer_content, writer_functions);
   document.load(arguments, state);
   return RRFinished;
#endif
}

} // end of extern "C"

