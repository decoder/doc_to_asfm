#include "StateMachineAction.h"
#include "clang/Basic/Version.h"
#include "clang/AST/AST.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/AST/ASTContext.h"
#include "clang/AST/Decl.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/ASTMatchers/ASTMatchFinder.h"
#include "clang/Lex/Lexer.h"
#include "clang/Tooling/Refactoring.h"
#include "llvm/ADT/SetVector.h"
#include <algorithm>
#include <string>
#include <sstream>
#include <cassert>

// [NOTES]
// a matcher matches exactly the root node!
// be aware of the visit ordering: eachOf = the first and then the second
//   forEachDescendant

namespace clang {

namespace ast_matchers {

#if CLANG_VERSION_MAJOR < 12

AST_POLYMORPHIC_MATCHER(isComparisonOperator,
                        AST_POLYMORPHIC_SUPPORTED_TYPES(BinaryOperator,
                                                        CXXOperatorCallExpr)) {
    switch (Node.getOperator()) {
    case OO_EqualEqual:
    case OO_ExclaimEqual:
    case OO_Greater:
    case OO_GreaterEqual:
    case OO_Less:
    case OO_LessEqual:
    case OO_Spaceship:
      return true;
    default:
      return false;
    }
}

#endif

} // end of namespace ast_matchers

namespace state_machine {

#if CLANG_VERSION_MAJOR < 12

#define CLANGASTTYPE clang::ast_type_traits

#else

#define CLANGASTTYPE clang

#endif
using namespace clang::ast_matchers;

bool
setToStartLine(const SourceManager& SM, SourceLocation& loc, std::string& indent) {
   bool result = false;
   const char* buffer = nullptr;
   std::pair<FileID, unsigned> LocInfo = SM.getDecomposedLoc(loc.isFileID() ? loc
         : SM.getSpellingLoc(loc));
   if (!LocInfo.first.isInvalid()) {
      bool isInvalid = false;
      StringRef Buffer = SM.getBufferData(LocInfo.first, &isInvalid);
      if (!isInvalid) {
         buffer = Buffer.data();
         unsigned offset = LocInfo.second;
         while (offset > 0 && isspace(buffer[offset-1]) && buffer[offset-1] != '\n') {
            indent.insert(0, 1, buffer[offset-1]);
            --offset;
         }
         loc = loc.getLocWithOffset(offset-LocInfo.second);
         result = true;
      }
   }
   return result;
}

bool
advanceToChar(const SourceManager& SM, SourceLocation& loc, const char* chars, int nbOfChars) {
   bool isInvalid = false;
   const char *bufferEnd = SM.getCharacterData(loc, &isInvalid);
   int shift = 0;
   if (!isInvalid) {
      bool hasFound;
      do {
         hasFound = !bufferEnd[shift];
         for (int i = 0; !hasFound && i < nbOfChars; ++i)
            if (bufferEnd[shift] == chars[i])
               hasFound = true;
         if (!hasFound)
            ++shift;
      } while (!hasFound);
   }
   if (shift > 0)
      loc = loc.getLocWithOffset(shift);
   return shift > 0;
}

// static void
// addReplacement(SourceRange Old, SourceRange New, const ASTContext &Context,
//       std::map<std::string, tooling::Replacements> &Replacements) {
//    StringRef NewText = Lexer::getSourceText(CharSourceRange::getTokenRange(New),
//          Context.getSourceManager(), Context.getLangOpts());
//    tooling::Replacement R(Context.getSourceManager(),
//          CharSourceRange::getTokenRange(Old), NewText, Context.getLangOpts());
//    consumeError(Replacements[std::string(R.getFilePath())].add(R));
//    Replacements[std::string(R.getFilePath())]
//          = Replacements[std::string(R.getFilePath())].merge({ R });
// }

class ReplacementContainer {
  private:
   SourceRange srSourceRange;
   std::map<size_t, std::pair<int, std::string> > mReplacements;

  public:
   ReplacementContainer(const SourceRange& sourceRange) : srSourceRange(sourceRange) {}

   void setReplace(const SourceRange& newRange, const std::string& replacement,
         const ASTContext& context)
      {  bool isInvalid = false;
         const auto& SM = context.getSourceManager();
         const char* buffer = SM.getCharacterData(srSourceRange.getBegin(), &isInvalid);
         if (isInvalid) return;
         size_t lengthBuffer = SM.getCharacterData(srSourceRange.getEnd(), &isInvalid) - buffer;
         if (isInvalid) return;
         size_t start = SM.getCharacterData(newRange.getBegin(), &isInvalid) - buffer;
         if (isInvalid) return;
         size_t length = SM.getCharacterData(newRange.getEnd(), &isInvalid) - buffer - start;
         if (isInvalid) return;
         assert(start+length <= lengthBuffer);
         auto iter = mReplacements.lower_bound(start);
         if (iter != mReplacements.end() && iter->first == start) {
            assert (iter->second.first == 0 || length == 0);
            if (iter->second.first == 0 && length == 0)
               iter->second.second += replacement;
            else if (iter->second.first == 0)
               iter->second.second += replacement;
            else
               iter->second.second.insert(0, replacement);
         }
         else {
            if (iter != mReplacements.end())
               assert(iter->first >= start+length);
            if (iter != mReplacements.begin()) {
               --iter;
               assert(iter->first+iter->second.first <= start);
               ++iter;
            }
            mReplacements.insert(iter, std::make_pair(start, std::make_pair(length, replacement)));
         }
      }
   void forceReplace(const SourceRange& newRange, const std::string* replacement,
         const ASTContext& context)
      {  bool isInvalid = false;
         const auto& SM = context.getSourceManager();
         const char* buffer = SM.getCharacterData(srSourceRange.getBegin(), &isInvalid);
         if (isInvalid) return;
         size_t lengthBuffer = SM.getCharacterData(srSourceRange.getEnd(), &isInvalid) - buffer;
         if (isInvalid) return;
         size_t start = SM.getCharacterData(newRange.getBegin(), &isInvalid) - buffer;
         if (isInvalid) return;
         size_t length = SM.getCharacterData(newRange.getEnd(), &isInvalid) - buffer - start + 1 /* for the last right paren */;
         if (isInvalid) return;
         assert(start+length <= lengthBuffer);
         {  auto iter = mReplacements.lower_bound(start);
            auto endIter = mReplacements.upper_bound(start+length);
            mReplacements.erase(iter, endIter);
         }
         mReplacements.insert(std::make_pair(start, std::make_pair(length, replacement ? *replacement : std::string())));
      }
   void apply(std::string& result)
      {  for (auto iter = mReplacements.rbegin(), iterEnd = mReplacements.rend();
               iter != iterEnd; ++iter) {
            if (iter->second.first > 0)
               result.erase(iter->first, iter->second.first);
            if (iter->second.second.length() > 0)
               result.insert(iter->first, iter->second.second);
         }
         mReplacements.clear();
      }
   void applyExcept(std::string& result, const SourceRange& sourceRange, const std::string* replaceText,
         const ASTContext& context)
      {  forceReplace(sourceRange, replaceText, context);
         apply(result);
      }
};

/* Definition of the block structure of the function to translate */

class CompoundRegister : public MatchFinder::MatchCallback {
  private:
   ASTContext& cContext;
   clang::BeforeThanCompare<SourceLocation> lessBetweenLocations;
   std::map<SourceLocation, std::pair<std::vector<const Stmt*>, int>,
      clang::BeforeThanCompare<SourceLocation> > compoundsByLocation;
   int uCurrentCompoundIdentifier = 0;

  public:
   CompoundRegister(const CompoundStmt& initialCompound, ASTContext& context)
      :  cContext(context),
         lessBetweenLocations(const_cast<SourceManager&>(context.getSourceManager())),
         compoundsByLocation(lessBetweenLocations)
      {  compoundsByLocation.insert(std::make_pair(initialCompound.getSourceRange().getBegin(),
               std::make_pair(std::vector<const Stmt*>(), 0)));
         compoundsByLocation.begin()->second.first.push_back(&initialCompound);
         compoundsByLocation.insert(std::make_pair(initialCompound.getSourceRange().getEnd(),
               std::make_pair(std::vector<const Stmt*>(), 0)));
      }

   const SourceLocation& getFirstLocation() const
      {  assert(!compoundsByLocation.empty());
         return compoundsByLocation.begin()->first;
      }
   SourceLocation getLastLocation() const
      {  assert(!compoundsByLocation.empty());
         return compoundsByLocation.begin()->second.first.front()->getEndLoc();
      }
   const std::vector<const Stmt*>& queryScope(const SourceLocation& loc) const
      {  auto iter = compoundsByLocation.upper_bound(loc);
         assert(iter != compoundsByLocation.begin());
         --iter;
         return iter->second.first;
      }
   int queryScopeIdentifier(const SourceLocation& loc) const
      {  auto iter = compoundsByLocation.upper_bound(loc);
         assert(iter != compoundsByLocation.begin());
         --iter;
         return iter->second.second;
      }
   virtual void run(const MatchFinder::MatchResult &Result);
};

void
CompoundRegister::run(const MatchFinder::MatchResult &Result) {
   const auto *stmt = Result.Nodes.getNodeAs<Stmt>("stmt");
   auto iter = compoundsByLocation.upper_bound(stmt->getSourceRange().getBegin());
   if (iter == compoundsByLocation.begin())
      return;
   --iter;
   if (!iter->second.first.empty()) {
      auto stackIter = iter->second.first.rbegin(), stackIterEnd = iter->second.first.rend();
      for (; stackIter != stackIterEnd
            && lessBetweenLocations((*stackIter)->getSourceRange().getEnd(),
                  stmt->getSourceRange().getBegin()); ++stackIter) {}
      if (stackIter == stackIterEnd)
         return;
      const auto *ifStmt = Result.Nodes.getNodeAs<IfStmt>("ifStmt");
      if (!ifStmt || !dyn_cast<IfStmt>(*stackIter)) {
         int oldScope = iter->second.second;
         int newScope = ++uCurrentCompoundIdentifier;
         compoundsByLocation.insert(iter, std::make_pair(stmt->getSourceRange().getBegin(),
               std::make_pair(std::vector<const Stmt*>(iter->second.first.begin(),
                     stackIter.base()), newScope)))
            ->second.first.push_back(stmt);
         compoundsByLocation.insert(iter, std::make_pair(stmt->getSourceRange().getEnd(),
               std::make_pair(std::vector<const Stmt*>(iter->second.first.begin(),
                     stackIter.base()), oldScope)));
      }
   };
}

void
retrieveStateAndArgumentNameFromParams(const FunctionDecl* function, StringRef& stateName,
      StringRef& argumentsName) {
   int paramsNumber = function->getNumParams();
   for (int paramIndex = 0; paramIndex < paramsNumber; ++paramIndex) {
      QualType paramType = function->getParamDecl(paramIndex)->getType();
      if (const auto* referenceType = dyn_cast<ReferenceType>(&*paramType)) {
         if (const auto* recordType = dyn_cast<RecordType>(&*referenceType->getPointeeType())) {
            if (recordType->getDecl()->getName() == "XMLParserArguments")
               argumentsName = function->getParamDecl(paramIndex)->getName();
            else if (recordType->getDecl()->getName() == "XMLParserStateStack")
               stateName = function->getParamDecl(paramIndex)->getName();
         }
      }
   }
}

/* Definition of the state machine structure of the function to translate */
/* Every function call having XMLParserArguments and XMLParserStateStack as
 *    arguments should be registered + setToNextToken + skipNode.
 * A RuleResult retrieval should be inserted just after if needed.
 */

std::string
getNameRulesResult(const FunctionDecl* functionDecl, bool isDeclared) {
   std::string result;
   bool hasPrefix = true;
   if (functionDecl->getParent()
         && functionDecl->getParent() != functionDecl->getLexicalParent()) {
      if (auto* methodDecl = llvm::dyn_cast<CXXMethodDecl>(functionDecl)) {
         hasPrefix = false;
         const CXXRecordDecl* cxxparent = methodDecl->getParent();
         const NamespaceDecl* moduleparent = nullptr;
         std::vector<std::string> prefix;
         while (cxxparent) {
            prefix.push_back(cxxparent->getName().str());
            prefix.back().append(isDeclared ? "::" : "_");
            if (cxxparent->getParent()) {
               auto* newparent = dyn_cast<CXXRecordDecl>(cxxparent->getParent());
               if (!newparent)
                  moduleparent = dyn_cast<NamespaceDecl>(cxxparent->getParent());
               cxxparent = newparent;
            }
            else
               cxxparent = nullptr;
         }
         while (moduleparent && moduleparent != functionDecl->getLexicalParent()) {
            prefix.push_back(moduleparent->getName().str());
            prefix.back().append("::");
            if (moduleparent->getParent())
               moduleparent = dyn_cast<NamespaceDecl>(moduleparent->getParent());
            else
               moduleparent = nullptr;
         }
         for (auto iter = prefix.rbegin(), iterEnd = prefix.rend(); iter != iterEnd; ++iter)
            result += *iter;
      }
   }
   if (hasPrefix)
      (result += functionDecl->getName()) += "_";
   result += "RuleResult";
   return result;
}

class StateMachineBuilder : public MatchFinder::MatchCallback {
  private:
   std::map<std::string, tooling::Replacements> &replacements;
   ASTContext& cContext;
   const FunctionDecl* pfFunctionDecl;
   bool fIsDeclared;
   int uStatesNumber = 0;
   CompoundRegister& crCompoundStructure;
   clang::BeforeThanCompare<SourceLocation> lessBetweenLocations;
   struct StateInfo {
      int state = 0;
      bool isCompoundContext = false;
      bool doesNeedLocalStorage = false;
      enum Context { CUndefined, CPhiNode, CSetToNextToken, CShiftCall } context = CUndefined;
      bool* additionalLocalStorage = nullptr;
      std::string callText; // context == CShiftCall
   };
   std::map<SourceLocation, StateInfo,
      clang::BeforeThanCompare<SourceLocation> > muStateByLocation;
   std::map<const CallExpr*, int> mCallsCache;
   class MultipleExpressionReplacement {
     private:
      std::map<SourceLocation, SourceLocation, clang::BeforeThanCompare<SourceLocation> > msrModifiedRanges;
      std::map<SourceLocation, tooling::Replacement, clang::BeforeThanCompare<SourceLocation> > mrSubExpressionReplacements;

     public:
      MultipleExpressionReplacement(const clang::BeforeThanCompare<SourceLocation>& lessBetweenLocations)
         :  msrModifiedRanges(lessBetweenLocations), mrSubExpressionReplacements(lessBetweenLocations) {}
      void addOuterModifiedRange(const SourceRange& sourceRange)
         {  auto iter = msrModifiedRanges.lower_bound(sourceRange.getBegin());
            bool hasInserted = false;
            if (iter != msrModifiedRanges.end()) {
               if (msrModifiedRanges.key_comp()(iter->first, sourceRange.getEnd()))
                  hasInserted = true;
               if (msrModifiedRanges.key_comp()(iter->second, sourceRange.getEnd())) {
                  iter->second = sourceRange.getEnd();
                  auto nextIter = iter;
                  ++nextIter;
                  while (nextIter != msrModifiedRanges.end()
                        && msrModifiedRanges.key_comp()(iter->first, sourceRange.getEnd())) {
                     msrModifiedRanges.erase(nextIter);
                     nextIter = iter;
                     ++nextIter;
                  }
                  if (msrModifiedRanges.key_comp()(sourceRange.getBegin(), iter->first)) {
                     auto previousIter = iter;
                     --previousIter;
                     auto endLoc = iter->second;
                     msrModifiedRanges.erase(iter);
                     ++previousIter;
                     iter = msrModifiedRanges.insert(previousIter,
                           std::make_pair(sourceRange.getBegin(), endLoc));
                  }
               }
            }
            if (iter != msrModifiedRanges.begin() && (iter == msrModifiedRanges.end() ||
                  msrModifiedRanges.key_comp()(sourceRange.getBegin(), iter->first))) {
               auto previousIter = iter;
               --previousIter;
               if (msrModifiedRanges.key_comp()(sourceRange.getBegin(), previousIter->second)) {
                  if (hasInserted) {
                     previousIter->second = iter->second;
                     msrModifiedRanges.erase(iter);
                  }
                  else 
                     previousIter->second = sourceRange.getEnd();
               }
               else
                  msrModifiedRanges.insert(std::make_pair(sourceRange.getBegin(), sourceRange.getEnd()));
            }
            else
               msrModifiedRanges.insert(std::make_pair(sourceRange.getBegin(), sourceRange.getEnd()));

            auto iterSubExpr = mrSubExpressionReplacements.lower_bound(sourceRange.getBegin());
            while (iterSubExpr != mrSubExpressionReplacements.end()
                  && mrSubExpressionReplacements.key_comp()(iterSubExpr->first, sourceRange.getEnd())) {
               auto eraseIter = iterSubExpr;
               ++iterSubExpr;
               mrSubExpressionReplacements.erase(eraseIter);
               --iterSubExpr;
            }
         }
      bool hasOuterModifiedRange(const SourceRange& sourceRange) const
         {  auto iter = msrModifiedRanges.lower_bound(sourceRange.getBegin());
            if (iter != msrModifiedRanges.end()) {
               if (msrModifiedRanges.key_comp()(iter->first, sourceRange.getEnd()))
                  return true;
            }
            if (iter != msrModifiedRanges.begin() && (iter == msrModifiedRanges.end() ||
                  msrModifiedRanges.key_comp()(sourceRange.getBegin(), iter->first))) {
               auto previousIter = iter;
               --previousIter;
               if (msrModifiedRanges.key_comp()(sourceRange.getBegin(), previousIter->second))
                  return true;
            }
            return false;
         }
      void addInnerReplacement(const SourceLocation& loc, const tooling::Replacement& replacement)
         {  SourceLocation endLoc = loc.getLocWithOffset(replacement.getLength());
            if (!hasOuterModifiedRange(SourceRange(loc, endLoc))) {
               auto iter = mrSubExpressionReplacements.lower_bound(loc);
               if (iter != mrSubExpressionReplacements.end()
                     && !mrSubExpressionReplacements.key_comp()(loc, iter->first)) {
                  assert(iter->second.getLength() == 0
                        && replacement.getLength() == 0);
                  std::string newText;
                  newText += iter->second.getReplacementText();
                  newText += replacement.getReplacementText();
                  iter->second = tooling::Replacement(iter->second.getFilePath(),
                        iter->second.getOffset(), iter->second.getLength(), newText.c_str());
               }
               mrSubExpressionReplacements.insert(std::make_pair(loc, replacement));
            }
         }
      void applyInnerReplacements(std::map<std::string, tooling::Replacements>& replacements)
         {  for (const auto& subExpressionReplacement : mrSubExpressionReplacements)
               consumeError(replacements[std::string(subExpressionReplacement.second.getFilePath())]
                     .add(subExpressionReplacement.second));
         }
   };
   MultipleExpressionReplacement merMultipleExpressionReplacement;

  public:
   struct InsertionsAtStatements {
     private:
      enum TypeInsertion { TIBlock, TIResultNeedChars, TISetLabel, TIConstString, TIConstStringEatNext };
      std::map<SourceLocation, std::pair<std::string, int>, clang::BeforeThanCompare<SourceLocation> > mcContent;

     public:
      InsertionsAtStatements(const clang::BeforeThanCompare<SourceLocation>& compareLoc)
         :  mcContent(compareLoc) {}

      bool hasInsertionAt(const SourceLocation& loc) const { return mcContent.find(loc) != mcContent.end(); }
      void addResultNeedChars(const SourceLocation& loc, const std::string& text)
         {  auto iter = mcContent.lower_bound(loc);
            if (iter != mcContent.end() && !mcContent.key_comp()(loc, iter->first)) {
               if (!(iter->second.second & (1U << TIResultNeedChars))) {
                  size_t index = 0;
                  if (iter->second.second & (1U << TIBlock)) {
                     while (index < iter->second.first.size() && isspace(iter->second.first[index]))
                        ++index;
                     if (index >= iter->second.first.size() || iter->second.first[index] != '{')
                        return;
                     ++index;
                     while (index < iter->second.first.size() && isspace(iter->second.first[index]))
                        ++index;
                     if (index >= iter->second.first.size())
                        return;
                  }
                  iter->second.first.insert(index, text);
                  iter->second.second |= (1U << TIResultNeedChars);
               }
            }
            else
               mcContent.insert(iter, std::make_pair(loc, std::make_pair(text, (1U << TIResultNeedChars))));
         }
      void addSetLabel(const SourceLocation& loc, const std::string& text)
         {  auto iter = mcContent.lower_bound(loc);
            if (iter != mcContent.end() && !mcContent.key_comp()(loc, iter->first)) {
               if (!(iter->second.second & (1U << TISetLabel))) {
                  size_t index = 0;
                  if (iter->second.second & (1U << TIBlock)) {
                     while (index < iter->second.first.size() && isspace(iter->second.first[index]))
                        ++index;
                     if (index >= iter->second.first.size() || iter->second.first[index] != '{')
                        return;
                     ++index;
                     while (index < iter->second.first.size() && isspace(iter->second.first[index]))
                        ++index;
                     if (index >= iter->second.first.size())
                        return;
                  }
                  iter->second.first.insert(index, text);
                  iter->second.second |= (1U << TISetLabel);
               }
            }
            else
               mcContent.insert(iter, std::make_pair(loc, std::make_pair(text, (1U << TISetLabel))));
         }
      void addBlock(const SourceLocation& loc, const std::string& textPre,
            const SourceLocation& endLoc, const std::string& textPost)
         {  auto iter = mcContent.lower_bound(loc);
            if (iter != mcContent.end() && !mcContent.key_comp()(loc, iter->first)) {
               if (!(iter->second.second & (1U << TIBlock))) {
                  iter->second.first.insert(0, textPre);
                  auto iterEnd = mcContent.lower_bound(endLoc);
                  if (iterEnd != mcContent.end() && !mcContent.key_comp()(endLoc, iterEnd->first)) {
                     iterEnd->second.first += textPost;
                     iterEnd->second.second |= (1U << TIBlock);
                  }
                  else
                     mcContent.insert(iterEnd, std::make_pair(endLoc, std::make_pair(textPost,
                                 (1U << TIBlock))));
                  iter->second.second |= (1U << TIBlock);
               }
            }
            else {
               mcContent.insert(iter, std::make_pair(loc, std::make_pair(textPre, (1U << TIBlock))));
               mcContent.insert(iter, std::make_pair(loc, std::make_pair(textPost, (1U << TIBlock))));
            }
         }
      void addConstString(const SourceLocation& loc, const std::string& text,
            bool doesEnterBlockBefore, bool doesEatNext = false, bool isBefore = false)
         {  auto iter = mcContent.lower_bound(loc);
            if (iter != mcContent.end() && !mcContent.key_comp()(loc, iter->first)) {
               if (iter->second.second & (1U << TIConstStringEatNext))
                  return;
               if (doesEnterBlockBefore && (iter->second.second & (1U << TIBlock))) {
                  size_t index = iter->second.first.length();
                  while (index > 0 && isspace(iter->second.first[index-1]))
                     --index;
                  if (index <= 0 || iter->second.first[index-1] != '}')
                     return;
                  --index;
                  while (index > 0 && isspace(iter->second.first[index-1]))
                     --index;
                  iter->second.first.insert(index, text);
               }
               else if (!isBefore)
                  iter->second.first += text;
               else
                  iter->second.first.insert(0, text);
               iter->second.second |= !doesEatNext ? (1U << TIConstString) : (3U << TIConstString);
            }
            else
               mcContent.insert(iter, std::make_pair(loc, std::make_pair(text,
                           !doesEatNext ? (1U << TIConstString) : (3U << TIConstString))));
         }
      void printAll(std::map<std::string, tooling::Replacements> &replacements, ASTContext& context)
         {  for (const auto& content : mcContent) {
               tooling::Replacement insert(context.getSourceManager(),
                  CharSourceRange::getCharRange(SourceRange(content.first, content.first)),
                     StringRef(content.second.first.c_str()), context.getLangOpts());
               consumeError(replacements[std::string(insert.getFilePath())].add(insert));
            }
         }
   };
  private:
   InsertionsAtStatements iasInsertionsAtStatements;

  public:
   StateMachineBuilder(std::map<std::string, tooling::Replacements>& Replacements,
         ASTContext& context, const FunctionDecl* functionDecl, bool isDeclared,
         CompoundRegister& compoundStructure)
      :  replacements(Replacements), cContext(context), pfFunctionDecl(functionDecl),
         fIsDeclared(isDeclared), crCompoundStructure(compoundStructure),
         lessBetweenLocations(const_cast<SourceManager&>(context.getSourceManager())),
         muStateByLocation(lessBetweenLocations), merMultipleExpressionReplacement(lessBetweenLocations),
         iasInsertionsAtStatements(lessBetweenLocations)
      {  muStateByLocation.insert(std::make_pair(crCompoundStructure.getFirstLocation(), StateInfo())); }

   const std::vector<const Stmt*>& queryScope(const SourceLocation& loc) const
      {  return crCompoundStructure.queryScope(loc); }
   int getStatesNumber() const { return uStatesNumber; }
   int queryState(const SourceLocation& loc) const
      {  auto previousSetNextTokenIter = muStateByLocation.upper_bound(loc);
         if (previousSetNextTokenIter == muStateByLocation.begin())
            return 0;
         --previousSetNextTokenIter;
         return previousSetNextTokenIter->second.state;
      }
   StateInfo* getStateLocalStorage(const SourceLocation& loc)
      {  auto previousSetNextTokenIter = muStateByLocation.upper_bound(loc);
         if (previousSetNextTokenIter == muStateByLocation.begin())
            return nullptr;
         --previousSetNextTokenIter;
         return &previousSetNextTokenIter->second;
      }
   SourceLocation queryEndInstruction(const SourceLocation& loc)
      {  bool isInvalid = false;
         const char *bufferEnd = cContext.getSourceManager().getCharacterData(loc, &isInvalid);
         int shift = 0;
         if (!isInvalid) {
            if (bufferEnd[shift] != '}') {
               while (bufferEnd[shift] && bufferEnd[shift] != ';')
                  ++shift;
            }
            ++shift;
         }
         return loc.getLocWithOffset(shift);
      }
   void addLocalStorage(const SourceLocation& loc)
      {  auto previousSetNextTokenIter = muStateByLocation.upper_bound(loc);
         if (previousSetNextTokenIter == muStateByLocation.begin())
            return;
         --previousSetNextTokenIter;
         previousSetNextTokenIter->second.doesNeedLocalStorage = true;
         if (previousSetNextTokenIter->second.additionalLocalStorage)
            *previousSetNextTokenIter->second.additionalLocalStorage = true;
      }
   bool hasLocalStorage(const SourceLocation& loc) const
      {  auto previousSetNextTokenIter = muStateByLocation.upper_bound(loc);
         if (previousSetNextTokenIter == muStateByLocation.begin())
            return true;
         --previousSetNextTokenIter;
         return previousSetNextTokenIter->second.doesNeedLocalStorage;
      }
   bool hasFirstLocalAccess() const
      {  return !muStateByLocation.empty() && muStateByLocation.begin()->second.doesNeedLocalStorage; }
   void addTextOfLabels()
      {  bool isFirst = true;
         for (const auto& state : muStateByLocation) {
            if (isFirst) { // skip LLabel0 soon inserted
               isFirst = false;
               continue;
            }
            StringRef stateName, argumentsName;
            retrieveStateAndArgumentNameFromParams(pfFunctionDecl, stateName, argumentsName);
            std::ostringstream setPointText, labelText;
            StringRef indent = Lexer::getIndentationForLine(state.first, cContext.getSourceManager());
            setPointText << "*(*" << stateName.str() << "._functions->get_point_instr)("
               << stateName.str() << "._content) = DLabel" << state.second.state << ";\n"
               << indent.str();
            if (state.second.context >= StateInfo::CSetToNextToken)
               labelText << "\nLLabel" << state.second.state << ":";
            else
               labelText << "LLabel" << state.second.state << ":\n" << indent.str();
            bool isInvalid = false;
            const char *buffer = cContext.getSourceManager().getCharacterData(
                  state.first, &isInvalid);
            if (isInvalid)
               continue;
            int shift = 0;
            if (state.second.context >= StateInfo::CSetToNextToken) {
               while (buffer[++shift] && buffer[shift] != ';') {}
               while (buffer[++shift] && buffer[shift] != '\n') {}
            }
            if (state.second.doesNeedLocalStorage || state.second.callText != "") {
               labelText << "if (!ruleResult) ruleResult = ("
                  << getNameRulesResult(pfFunctionDecl, fIsDeclared)
                  << "*) (*" << stateName.str() << "._functions->get_rule_result_instr)("
                  << stateName.str() << "._content);\n"
                  << indent.str();
               if (state.second.callText != "")
                  labelText << state.second.callText << '\n' << indent.str();
            }
            else if (state.second.isCompoundContext)
               labelText << ';';
            else {
               int nextCloseBlock = shift;
               while (buffer[++nextCloseBlock] && isspace(buffer[nextCloseBlock])) {}
               if (buffer[nextCloseBlock] == '}')
                  labelText << ';';
            }
            if (state.second.isCompoundContext)
               setPointText << "   ";
            insertionsAtStatements().addConstString(state.first, setPointText.str(), false /* doesEnterBlockBefore */);
            if (state.second.isCompoundContext) {
               std::string preText, postText;
               (preText += "{  ");
               ((postText += "\n") += indent) += "}";
               SourceLocation locEnd = queryEndInstruction(state.first);
               insertionsAtStatements().addBlock(state.first, preText, locEnd, postText);
            }
            SourceLocation insertionLoc = state.first.getLocWithOffset(shift);
            insertionsAtStatements().addConstString(insertionLoc, labelText.str(),
                  state.second.isCompoundContext /* doesEnterBlockBefore */,
                  false /* doesEatNext */, true /* isBefore */);
            // tooling::Replacement insertionLabel(cContext.getSourceManager(),
            //    CharSourceRange::getCharRange(SourceRange(insertionLoc, insertionLoc)),
            //       StringRef(labelText.str().c_str()),
            //       cContext.getLangOpts());
            // consumeError(replacements[std::string(insertionLabel.getFilePath())].add(insertionLabel));
         }
      }
   const SourceLocation& getFirstLocation() const
      {  return crCompoundStructure.getFirstLocation(); }
   SourceLocation getLastLocation() const
      {  return crCompoundStructure.getLastLocation(); }
   const clang::BeforeThanCompare<SourceLocation>& getLessBetweenLocations() const { return lessBetweenLocations; }
   InsertionsAtStatements& insertionsAtStatements() { return iasInsertionsAtStatements; }
   CompoundRegister& compoundStructure() { return crCompoundStructure; }
   std::map<const CallExpr*, int>& callsCache() { return mCallsCache; }
   void addOuterModifiedRange(const SourceRange& sourceRange)
      {  merMultipleExpressionReplacement.addOuterModifiedRange(sourceRange); }
   bool hasOuterModifiedRange(const SourceRange& sourceRange) const
      {  return merMultipleExpressionReplacement.hasOuterModifiedRange(sourceRange); }
   void addInnerReplacement(const SourceLocation& loc, const tooling::Replacement& replacement)
      {  merMultipleExpressionReplacement.addInnerReplacement(loc, replacement); }
   void applyInnerReplacements(std::map<std::string, tooling::Replacements>& replacements)
      {  merMultipleExpressionReplacement.applyInnerReplacements(replacements); }
   void addStateAt(const SourceLocation& loc, const Stmt* parentStmt, std::string* callText)
      {  muStateByLocation.insert(std::make_pair(loc,
               StateInfo {(int) ++uStatesNumber, !dyn_cast<CompoundStmt>(parentStmt) /* isCompoundContext */,
               false /* doesNeedLocalStorage */,
               !callText ? StateInfo::CSetToNextToken : StateInfo::CShiftCall,
               nullptr /* additionalLocalStorage */, callText /* callText */ ? *callText : std::string()}));
      }
   void addPhiNodesAt(const SourceLocation& loc, const StringRef& stateName, const StringRef& argumentsName)
      {  // set the ends of the label = phi nodes
         const std::vector<const Stmt*>& thisScope = crCompoundStructure.queryScope(loc);
         for (auto iterScope = thisScope.begin(), iterEndScope = thisScope.end(); iterScope != iterEndScope; ++iterScope) {
            const auto* scopeStmt = *iterScope;
            if (!dyn_cast<CompoundStmt>(scopeStmt)) {
               SourceLocation locBegin = scopeStmt->getSourceRange().getBegin();
               auto iter = muStateByLocation.upper_bound(locBegin);
               if (iter == muStateByLocation.begin())
                  continue;
               --iter;
               if (lessBetweenLocations(iter->first, locBegin)) { // not inserted location iter->first != locBegin
                  if (!dyn_cast<IfStmt>(scopeStmt)) {
                     iter = muStateByLocation.insert(iter, std::make_pair(locBegin,
                        StateInfo {
                           (int) ++uStatesNumber, false /* isCompoundContext */,
                           false /* doesNeedLocalStorage */, StateInfo::CPhiNode /* context */,
                           nullptr /* additionalLocalStorage */, "" /* callText */ }));
                     std::ostringstream labelText;
                     SourceLocation locEnd = scopeStmt->getSourceRange().getEnd();
                     bool isInvalid = false;
                     const char *buffer = cContext.getSourceManager().getCharacterData(
                           locEnd, &isInvalid);
                     int index=0;
                     if (!isInvalid && buffer[index] != '\n' && buffer[index] != '}' && buffer[index] != ';') {
                        locEnd = locEnd.getLocWithOffset(1);
                        ++index;
                     }
                     if (!isInvalid && buffer[index] == ';') {
                        locEnd = locEnd.getLocWithOffset(1);
                        labelText << "\n" << Lexer::getIndentationForLine(locEnd, cContext.getSourceManager()).str()
                           << "*(*" << stateName.str() << "._functions->get_point_instr)("
                           << stateName.str() << "._content) = DLabel" << uStatesNumber << ";";
                     }
                     else
                        labelText << "*(*" << stateName.str() << "._functions->get_point_instr)("
                              << stateName.str() << "._content) = DLabel" << uStatesNumber << ";" << "\n"
                              << Lexer::getIndentationForLine(scopeStmt->getSourceRange().getEnd(),
                                    cContext.getSourceManager()).str();
                     insertionsAtStatements().addSetLabel(locEnd, labelText.str());
                     StateInfo* endInfo = getStateLocalStorage(scopeStmt->getEndLoc());
                     iter->second.additionalLocalStorage = &endInfo->doesNeedLocalStorage;
                     endInfo->additionalLocalStorage = &iter->second.doesNeedLocalStorage;
                  }
               }
               // SourceLocation locEnd = queryEndInstruction(scopeStmt->getSourceRange().getEnd());
               // auto iterEnd = muStateByLocation.lower_bound(locEnd);
               // if (iterEnd != muStateByLocation.end()
               //       && lessBetweenLocations(locEnd, iterEnd->first)) {
               //    auto nextIterScope = iterScope;
               //    const Stmt* parentStmt = nullptr;
               //    if (++nextIterScope != iterEndScope)
               //       parentStmt = *nextIterScope;
               //    muStateByLocation.insert(std::make_pair(locEnd,
               //          StateInfo {(int) ++uStatesNumber,
               //             parentStmt && !dyn_cast<CompoundStmt>(parentStmt) /* isCompoundContext */,
               //             false /* doesNeedLocalStorage */, CPhiNode /* context */, nullptr, ""}));
               // }
            }
         }
      }

   virtual void run(const MatchFinder::MatchResult &Result);
};

void
StateMachineBuilder::run(const MatchFinder::MatchResult &Result) {
   const auto *calleeExpr = Result.Nodes.getNodeAs<CXXMemberCallExpr>("setNextToken");
   const Stmt *parentStmt = cContext.getParents(*calleeExpr)[0].get<Stmt>();
   while (parentStmt && dyn_cast<Expr>(parentStmt))
      parentStmt = cContext.getParents(*parentStmt)[0].get<Stmt>();

   addStateAt(calleeExpr->getSourceRange().getBegin(), parentStmt, nullptr /* callText */);
   StringRef stateName, argumentsName;
   retrieveStateAndArgumentNameFromParams(pfFunctionDecl, stateName, argumentsName);
   // if (!(*arguments._functions->set_to_next_token_instr)(arguments._content, &result)) return result;
   {  
      std::string replaceText;
      ((((replaceText += "if (!(*") += argumentsName) += "._functions->set_to_next_token_instr)(")
         += argumentsName) += "._content, &result)) return result";
      tooling::Replacement replace(cContext.getSourceManager(),
         CharSourceRange::getTokenRange(calleeExpr->getSourceRange()),
            StringRef(replaceText.c_str()), cContext.getLangOpts());
      consumeError(replacements[std::string(replace.getFilePath())].add(replace));
   }
   addPhiNodesAt(calleeExpr->getSourceRange().getBegin(), stateName, argumentsName);
}

class SkipNodeBuilder : public MatchFinder::MatchCallback {
  private:
   std::map<std::string, tooling::Replacements> &replacements;
   ASTContext& cContext;
   StateMachineBuilder& smbStateMachine;

  public:
   SkipNodeBuilder(std::map<std::string, tooling::Replacements>& Replacements,
         ASTContext& context, StateMachineBuilder& stateMachine)
      :  replacements(Replacements), cContext(context), smbStateMachine(stateMachine) {}
   virtual void run(const MatchFinder::MatchResult &Result);
};

void
SkipNodeBuilder::run(const MatchFinder::MatchResult &Result) {
   const auto *calleeExpr = Result.Nodes.getNodeAs<CXXMemberCallExpr>("skipNode");

   const Stmt *parentStmt = cContext.getParents(*calleeExpr)[0].get<Stmt>();
   while (parentStmt && dyn_cast<Expr>(parentStmt))
      parentStmt = cContext.getParents(*parentStmt)[0].get<Stmt>();
   smbStateMachine.addStateAt(calleeExpr->getBeginLoc(), parentStmt, nullptr /* callText */);

   bool isContent = calleeExpr->getMethodDecl()->getName() == "skipNodeContent";
   // if (!(*state._functions->skip_node_instr)(state._content, arguments._content, &result, (void**) &ruleResult)) return result;
   {  
      StringRef state = Lexer::getSourceText(CharSourceRange::getTokenRange(
            calleeExpr->getImplicitObjectArgument()->getSourceRange()), cContext.getSourceManager(), cContext.getLangOpts());
      StringRef argument = Lexer::getSourceText(CharSourceRange::getTokenRange(
            calleeExpr->getArg(0)->getSourceRange()), cContext.getSourceManager(), cContext.getLangOpts());
      std::string replaceText;
      ((((((((replaceText += "if (!(*") += state) += "._functions->skip_node_")
         += (isContent ? "content_" : "")) += "instr)(")
         += state) += "._content, ")
         += argument) += "._content, &result, (void**) &ruleResult)) return result";
      tooling::Replacement replace(cContext.getSourceManager(),
         CharSourceRange::getTokenRange(calleeExpr->getSourceRange()),
            StringRef(replaceText.c_str()), cContext.getLangOpts());
      consumeError(replacements[std::string(replace.getFilePath())].add(replace));
   }
}

/* Definition of the translation of the access to the local variables */

class DeclarationRefMover : public MatchFinder::MatchCallback {
  private:
   std::map<std::string, tooling::Replacements>* replacements = nullptr;
   ReplacementContainer* subExprContainer = nullptr;
   ASTContext& cContext;
   StateMachineBuilder& smbStateMachine;
   const FunctionDecl* pfFunctionDecl;
   bool fIsDeclared;
   bool fIsTopLevelArgument = false;
   bool fDoesRemoveReference = false;

   void addLocalStorageFrom(const Expr* expr);

  public:
   DeclarationRefMover(std::map<std::string, tooling::Replacements>& Replacements,
         ASTContext& context, StateMachineBuilder& currentState, const FunctionDecl* functionDecl,
         bool isDeclared)
      :  replacements(&Replacements), cContext(context), smbStateMachine(currentState),
         pfFunctionDecl(functionDecl), fIsDeclared(isDeclared) {}
   DeclarationRefMover(ReplacementContainer& subExprContainer, ASTContext& context,
         StateMachineBuilder& currentState, const FunctionDecl* functionDecl, bool isDeclared)
      :  subExprContainer(&subExprContainer), cContext(context), smbStateMachine(currentState),
         pfFunctionDecl(functionDecl), fIsDeclared(isDeclared) {}

   StateMachineBuilder::InsertionsAtStatements& insertionsAtStatements() { return smbStateMachine.insertionsAtStatements(); }
   void setTopLevelArgument() { fIsTopLevelArgument = true; }
   bool doesRemoveReference() const { return fDoesRemoveReference; }
   virtual void run(const MatchFinder::MatchResult &Result);
};

void
DeclarationRefMover::addLocalStorageFrom(const Expr* expr) {
   const Stmt *topLevelExpr = expr;
   const Stmt *parentStmt = cContext.getParents(*expr)[0].get<Stmt>();
   while (parentStmt && dyn_cast<Expr>(parentStmt)) {
      topLevelExpr = static_cast<const Expr*>(parentStmt);
      parentStmt = cContext.getParents(*parentStmt)[0].get<Stmt>();
   }
   if (parentStmt && dyn_cast<IfStmt>(parentStmt)) {
      if (static_cast<const IfStmt*>(parentStmt)->getCond() == topLevelExpr) {
         topLevelExpr = parentStmt;
         parentStmt = cContext.getParents(*parentStmt)[0].get<Stmt>();
      }
   }
   else if (parentStmt && dyn_cast<WhileStmt>(parentStmt)) {
      const auto* whileStmt = static_cast<const WhileStmt*>(parentStmt);
      if (whileStmt->getCond() == topLevelExpr) {
         topLevelExpr = parentStmt;
         parentStmt = cContext.getParents(*parentStmt)[0].get<Stmt>();
         smbStateMachine.addLocalStorage(topLevelExpr->getBeginLoc());
         smbStateMachine.addLocalStorage(whileStmt->getBody()->getEndLoc());
      }
   }
   else if (parentStmt && dyn_cast<DoStmt>(parentStmt)) {
      const auto* doStmt = static_cast<const DoStmt*>(parentStmt);
      if (doStmt->getCond() == topLevelExpr) {
         topLevelExpr = parentStmt;
         parentStmt = cContext.getParents(*parentStmt)[0].get<Stmt>();
         smbStateMachine.addLocalStorage(doStmt->getBody()->getEndLoc());
      }
   }
   else if (parentStmt && dyn_cast<ForStmt>(parentStmt)) {
      const auto* forStmt = static_cast<const ForStmt*>(parentStmt);
      if (forStmt->getInit() == topLevelExpr) {
         topLevelExpr = parentStmt;
         parentStmt = cContext.getParents(*parentStmt)[0].get<Stmt>();
      }
      else if (forStmt->getCond() == topLevelExpr) {
         topLevelExpr = parentStmt;
         parentStmt = cContext.getParents(*parentStmt)[0].get<Stmt>();
         smbStateMachine.addLocalStorage(topLevelExpr->getBeginLoc());
         smbStateMachine.addLocalStorage(forStmt->getBody()->getEndLoc());
      }
      else if (forStmt->getInc() == topLevelExpr) {
         topLevelExpr = parentStmt;
         parentStmt = cContext.getParents(*parentStmt)[0].get<Stmt>();
         smbStateMachine.addLocalStorage(forStmt->getBody()->getEndLoc());
      }
   }
   else if (parentStmt && dyn_cast<CXXForRangeStmt>(parentStmt)) {
      const auto* forStmt = static_cast<const CXXForRangeStmt*>(parentStmt);
      if (forStmt->getInit() == forStmt->getRangeInit()) {
         topLevelExpr = parentStmt;
         parentStmt = cContext.getParents(*parentStmt)[0].get<Stmt>();
      }
   }
   if (parentStmt)
      smbStateMachine.addLocalStorage(topLevelExpr->getBeginLoc());
}

void
DeclarationRefMover::run(const MatchFinder::MatchResult &Result) {
   const auto *declRef = Result.Nodes.getNodeAs<DeclRefExpr>("declRef");
   const auto *thisRef = Result.Nodes.getNodeAs<CXXThisExpr>("this");
   StringRef stateGarbage;
   const StringRef* stateName = nullptr;
   if (thisRef) {
      const auto* methodDecl = llvm::dyn_cast<CXXMethodDecl>(pfFunctionDecl);
      const bool isDeclared = pfFunctionDecl->getName() == "load"
         && methodDecl && (methodDecl->getParent()->getName() == "DocumentConfiguration"
            || methodDecl->getParent()->getName() == "Document");
      bool isConfiguration = isDeclared &&  methodDecl && (methodDecl->getParent()->getName() == "DocumentConfiguration");
      std::string result;
      if (!fIsTopLevelArgument) {
         if (isDeclared) {
            if (isConfiguration)
               result = "&configuration";
            else
               result = "&ruleResult->document";
         }
         else
            result = "&object";
      }
      else {
         if (!stateName) {
            StringRef arguments;
            retrieveStateAndArgumentNameFromParams(pfFunctionDecl, stateGarbage, arguments);
            stateName = &stateGarbage;
         }
         if (isDeclared) {
            if (isConfiguration)
               (result = "state") += ".acquirePurePointer(&configuration)";
            else
               (result = "state") += ".acquirePurePointer(&ruleResult->document)";
         }
         else
            (result = "state") += ".acquirePointer(&object)";
         fDoesRemoveReference = true;
      }
      SourceLocation endLoc = thisRef->getEndLoc();
      bool isInvalid = false;
      const char *buffer = cContext.getSourceManager().getCharacterData(thisRef->getBeginLoc(), &isInvalid);
      bool isInsertion = false;
      if (!isInvalid) {
         if (strncmp(buffer, "this", 4) == 0 && !isalnum(buffer[4]))
            endLoc = endLoc.getLocWithOffset(4);
         else {
            result.insert(0, 1, '(');
            result += ")->";
            isInsertion = true;
         }
      }
      if (replacements) {
         tooling::Replacement replace(cContext.getSourceManager(),
            CharSourceRange::getCharRange(thisRef->getBeginLoc(), endLoc), result.c_str(),
               cContext.getLangOpts());
         if (!isInsertion)
            smbStateMachine.addInnerReplacement(thisRef->getBeginLoc(), replace);
         else
            smbStateMachine.insertionsAtStatements().addConstString(thisRef->getBeginLoc(), result, false /* doesEnterBlockBefore */);
         // consumeError((*replacements)[std::string(replace.getFilePath())].add(replace));
      }
      else if (subExprContainer)
         subExprContainer->setReplace(SourceRange(thisRef->getBeginLoc(), endLoc),
                  result, cContext);
      return;
   }
   if (const auto* paramDecl = dyn_cast<ParmVarDecl>(declRef->getDecl())) {
      if (const auto* referenceType = dyn_cast<ReferenceType>(&*paramDecl->getType())) {
         if (const auto* recordType = dyn_cast<RecordType>(&*referenceType->getPointeeType())) {
            if (recordType->getDecl()->getName() == "XMLParserArguments")
               return;
            else if (recordType->getDecl()->getName() == "XMLParserStateStack")
               return;
         }
      }
      int paramNumber = 0;
      bool hasRuleResult = false;
      for (unsigned paramIndex = 0; paramIndex < pfFunctionDecl->getNumParams(); ++paramIndex) {
         const ParmVarDecl* param = pfFunctionDecl->getParamDecl(paramIndex);
         if (param == paramDecl) {
            if (!hasRuleResult) {
               addLocalStorageFrom(declRef);
               hasRuleResult = true;
            }
            // smbStateMachine.addLocalStorage(declRef->getBeginLoc());
            bool isPointerParam = param->getType()->isPointerType();
            bool isReferenceParam = param->getType()->isReferenceType();
            std::ostringstream newParam;
            newParam << "ruleResult->_param" << paramNumber;
            if (!stateName && (isReferenceParam || isPointerParam)) {
               StringRef arguments;
               retrieveStateAndArgumentNameFromParams(pfFunctionDecl, stateGarbage, arguments);
               stateName = &stateGarbage;
            }
            if (!fIsTopLevelArgument) {
               if (isReferenceParam)
                  newParam << ".deref(" << stateName->str() << ')';
               else if (isPointerParam)
                  newParam << ".value(" << stateName->str() << ')';
            }
            else if (isReferenceParam)
               fDoesRemoveReference = true;
            if (replacements) {
               tooling::Replacement replace(cContext.getSourceManager(),
                  CharSourceRange::getTokenRange(declRef->getSourceRange()), newParam.str().c_str(),
                     cContext.getLangOpts());
               smbStateMachine.addInnerReplacement(declRef->getBeginLoc(), replace);
               // consumeError((*replacements)[std::string(replace.getFilePath())].add(replace));
            }
            else if (subExprContainer) {
               SourceLocation endLoc = declRef->getEndLoc();
               bool isInvalid = false;
               const char *buffer = cContext.getSourceManager().getCharacterData(endLoc, &isInvalid);
               if (!isInvalid) {
                  int shift = 0;
                  while (isalnum(buffer[shift]) || buffer[shift] == '_')
                     ++shift;
                  endLoc = endLoc.getLocWithOffset(shift);
               }
               subExprContainer->setReplace(SourceRange(declRef->getBeginLoc(), endLoc),
                        newParam.str(), cContext);
            }
         }
         if (const auto* referenceType = dyn_cast<ReferenceType>(&*param->getType())) {
            if (const auto* recordType = dyn_cast<RecordType>(&*referenceType->getPointeeType())) {
               if (recordType->getDecl()->getName() == "XMLParserArguments")
                  continue;
               else if (recordType->getDecl()->getName() == "XMLParserStateStack")
                  continue;
            }
         }
         ++paramNumber;
      }
   }
   else if (const auto* varDecl = dyn_cast<VarDecl>(declRef->getDecl())) {
      if (varDecl->hasLocalStorage()) {
         if (int scopeIdentifier = smbStateMachine.compoundStructure()
               .queryScopeIdentifier(varDecl->getBeginLoc())) {
            std::ostringstream extension;
            extension << '_' << scopeIdentifier;
            SourceLocation endLoc = declRef->getEndLoc();
            bool isInvalid = false;
            const char *buffer = cContext.getSourceManager().getCharacterData(endLoc, &isInvalid);
            if (!isInvalid) {
               int shift = 0;
               while (isalnum(buffer[shift]) || buffer[shift] == '_')
                  ++shift;
               endLoc = endLoc.getLocWithOffset(shift);
            }
            if (replacements) {
               tooling::Replacement insert(cContext.getSourceManager(),
                  CharSourceRange::getCharRange(SourceRange(endLoc, endLoc)),
                     StringRef(extension.str().c_str()), cContext.getLangOpts());
               smbStateMachine.addInnerReplacement(declRef->getEndLoc().getLocWithOffset(-1), insert);
               // consumeError((*replacements)[std::string(insert.getFilePath())].add(insert));
            }
            else if (subExprContainer)
               subExprContainer->setReplace(SourceRange(endLoc, endLoc), extension.str(), cContext);
         }
         addLocalStorageFrom(declRef);
         if (replacements) {
            if (insertionsAtStatements().hasInsertionAt(declRef->getBeginLoc()))
               insertionsAtStatements().addConstString(declRef->getBeginLoc(), "ruleResult->", false /* doesEnterBlockBefore */);
            else {
               tooling::Replacement insert(cContext.getSourceManager(),
                  CharSourceRange::getCharRange(SourceRange(declRef->getBeginLoc(), declRef->getBeginLoc())),
                     "ruleResult->", cContext.getLangOpts());
               smbStateMachine.addInnerReplacement(declRef->getBeginLoc(), insert);
            }
         }
         else if (subExprContainer) {
            if (!fIsTopLevelArgument)
               subExprContainer->setReplace(SourceRange(declRef->getBeginLoc(), declRef->getBeginLoc()),
                     "ruleResult->", cContext);
            else {
               SourceLocation beginLoc = declRef->getBeginLoc(), endLoc = declRef->getEndLoc();
               bool isInvalid = false;
               const char *buffer = cContext.getSourceManager().getCharacterData(endLoc, &isInvalid);
               if (!isInvalid) {
                  int shift = 0;
                  while (isalnum(buffer[shift]) || buffer[shift] == '_')
                     ++shift;
                  endLoc = endLoc.getLocWithOffset(shift);
               }
               if (!stateName) {
                  StringRef arguments;
                  retrieveStateAndArgumentNameFromParams(pfFunctionDecl, stateGarbage, arguments);
                  stateName = &stateGarbage;
               }
               std::string preAccess = stateName->str();
               preAccess += ".acquirePurePointer(&ruleResult->";
               subExprContainer->setReplace(SourceRange(beginLoc, beginLoc),
                        preAccess, cContext);
               subExprContainer->setReplace(SourceRange(endLoc, endLoc), ")", cContext);
               fDoesRemoveReference = true;
            }
         }
      }
   }
   else if (auto* enumConstant = dyn_cast<EnumConstantDecl>(declRef->getDecl())) {
      if (smbStateMachine.getLessBetweenLocations()(
               smbStateMachine.getFirstLocation(),
               enumConstant->getSourceRange().getBegin())
            && smbStateMachine.getLessBetweenLocations()(
               enumConstant->getSourceRange().getEnd(),
               smbStateMachine.getLastLocation())) {
         const auto *parentStmt = Result.Nodes.getNodeAs<Stmt>("parentStmt");
         if (parentStmt)
            return;
         const auto& loc = declRef->getBeginLoc();
         std::string nameRulesResult = getNameRulesResult(pfFunctionDecl, fIsDeclared);
         nameRulesResult += "::";
         if (replacements) {
            tooling::Replacement insert(cContext.getSourceManager(),
               CharSourceRange::getCharRange(SourceRange(loc, loc)),
                  StringRef(nameRulesResult), cContext.getLangOpts());
            smbStateMachine.addInnerReplacement(loc, insert);
            // consumeError((*replacements)[std::string(insert.getFilePath())].add(insert));
         }
         else if (subExprContainer)
            subExprContainer->setReplace(SourceRange(loc, loc), nameRulesResult, cContext);
      }
   }
}

class ContinuePropagator : public MatchFinder::MatchCallback {
  private:
   ASTContext& cContext;
   StateMachineBuilder& smbStateMachine;

  public:
   ContinuePropagator(ASTContext& context, StateMachineBuilder& stateMachine)
      :  cContext(context), smbStateMachine(stateMachine) {}
   virtual void run(const MatchFinder::MatchResult &Result)
      {  const ContinueStmt* continueStmt = Result.Nodes.getNodeAs<ContinueStmt>("continue");
         const Stmt *parentStmt = cContext.getParents(*continueStmt)[0].get<Stmt>();
         while (parentStmt) {
            if (dyn_cast<WhileStmt>(parentStmt)) {
               const auto* whileStmt = static_cast<const WhileStmt*>(parentStmt);
               if (smbStateMachine.hasLocalStorage(whileStmt->getBeginLoc()))
                  smbStateMachine.addLocalStorage(continueStmt->getBeginLoc());
               return;
            }
            else if (dyn_cast<DoStmt>(parentStmt)) {
               const auto* doStmt = static_cast<const DoStmt*>(parentStmt);
               if (smbStateMachine.hasLocalStorage(doStmt->getBody()->getEndLoc()))
                  smbStateMachine.addLocalStorage(continueStmt->getBeginLoc());
               return;
            }
            else if (parentStmt && dyn_cast<ForStmt>(parentStmt)) {
               const auto* forStmt = static_cast<const ForStmt*>(parentStmt);
               if (smbStateMachine.hasLocalStorage(forStmt->getBody()->getEndLoc()))
                  smbStateMachine.addLocalStorage(continueStmt->getBeginLoc());
            };
            parentStmt = cContext.getParents(*parentStmt)[0].get<Stmt>();
         }
      }
};

class ShiftCallBuilder : public MatchFinder::MatchCallback {
  private:
   std::map<std::string, tooling::Replacements>* replacements = nullptr;
   ReplacementContainer* subExprContainer = nullptr;
   std::string& sRuleContainer;
   StateMachineBuilder& smbStateMachine;
   ASTContext& cContext;
   const FunctionDecl* pfFunction;
   int uCallNumbers = 0;

   std::string getModifiedText(const Expr* expr, bool isDeclared, bool* doesRemoveReference,
         const Expr* exceptSubExp=nullptr, const std::string* replaceExceptText=nullptr) const;

  public:
   ShiftCallBuilder(std::map<std::string, tooling::Replacements>& Replacements,
         std::string& ruleContainer, ASTContext& context, StateMachineBuilder& stateMachine,
         const FunctionDecl* function)
      :  replacements(&Replacements), sRuleContainer(ruleContainer), smbStateMachine(stateMachine),
         cContext(context), pfFunction(function) {}
   ShiftCallBuilder(ReplacementContainer& subExprContainer, std::string& ruleContainer,
         ASTContext& context, StateMachineBuilder& stateMachine, const FunctionDecl* function)
      :  subExprContainer(&subExprContainer), sRuleContainer(ruleContainer),
         smbStateMachine(stateMachine), cContext(context), pfFunction(function) {}
   virtual void run(const MatchFinder::MatchResult &Result);

   std::string getResultType(const QualType& resultType, const SourceRange& typeSourceRange,
         bool& isReferenceResult, const std::string& varResult);
   std::string getParameterType(const QualType& parameterType, const SourceRange& parameterSourceRange,
         const StringRef& parameterName, bool& isReferenceParameter, const std::string& varResult);
};

std::string
ShiftCallBuilder::getModifiedText(const Expr* topExpr, bool isDeclared, bool* doesRemoveReference,
      const Expr* exceptSubExp, const std::string* replaceExceptText) const {
   SourceRange sourceRangeString = topExpr->getSourceRange();
   std::string result;
   result += Lexer::getSourceText(CharSourceRange::getTokenRange(sourceRangeString),
         cContext.getSourceManager(), cContext.getLangOpts());
   ReplacementContainer subexprReplacements(SourceRange(sourceRangeString.getBegin(),
         sourceRangeString.getBegin().getLocWithOffset(result.length())));
   DeclarationRefMover declRefMoverOptim(subexprReplacements, cContext, smbStateMachine,
         pfFunction, isDeclared);
   declRefMoverOptim.setTopLevelArgument();
   DeclarationRefMover declRefMover(subexprReplacements, cContext, smbStateMachine,
         pfFunction, isDeclared);
   ShiftCallBuilder shiftCallBuilder(subexprReplacements, sRuleContainer, cContext,
         smbStateMachine, pfFunction);
   MatchFinder finder;
   if (!exceptSubExp) {
      finder.addMatcher(traverse(CLANGASTTYPE::TK_IgnoreUnlessSpelledInSource,
            findAll(callExpr(
               hasAnyArgument(declRefExpr(to(parmVarDecl(hasType(
                  references(recordDecl(matchesName("XMLParserStateStack")))))))),
               hasAnyArgument(declRefExpr(to(parmVarDecl(hasType(
                  references(recordDecl(matchesName("XMLParserArguments"))))))))).bind("shiftCall"))),
            &shiftCallBuilder);
   }
   finder.addMatcher(traverse(CLANGASTTYPE::TK_IgnoreUnlessSpelledInSource,
         expr(anyOf(declRefExpr(to(eachOf(varDecl(hasLocalStorage()),
                  enumConstantDecl()))).bind("declRef"), cxxThisExpr().bind("this")))),
         &declRefMoverOptim);
   finder.addMatcher(traverse(CLANGASTTYPE::TK_IgnoreUnlessSpelledInSource,
         expr(forEachDescendant(expr(anyOf(declRefExpr(to(eachOf(varDecl(hasLocalStorage()),
                  enumConstantDecl()))).bind("declRef"), cxxThisExpr().bind("this")))))),
         &declRefMover);
   finder.match(*topExpr, cContext);
   if (exceptSubExp)
      subexprReplacements.applyExcept(result, exceptSubExp->getSourceRange(), replaceExceptText,
            cContext);
   else
      subexprReplacements.apply(result);
   if (doesRemoveReference)
      *doesRemoveReference = declRefMoverOptim.doesRemoveReference();
   return result;
}
         
void
ShiftCallBuilder::run(const MatchFinder::MatchResult &Result) {
   const auto *calleeExpr = Result.Nodes.getNodeAs<CallExpr>("shiftCall");
   const Stmt *parentStmt = cContext.getParents(*calleeExpr)[0].get<Stmt>();
   const Expr* topExpr = calleeExpr;
   while (parentStmt && dyn_cast<Expr>(parentStmt)) {
      topExpr = static_cast<const Expr*>(parentStmt);
      parentStmt = cContext.getParents(*parentStmt)[0].get<Stmt>();
   }

   // ... ruleResult->_result_call1;
   // {  struct _XMLParserStateFunctions* _local_state_functions = nullptr;
   //    if (!ruleResult) ruleResult = (RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
   //    struct RuleResult _ruleResult;
   //    _ruleResult. ... = ...
   //    _ruleResult._result = state.acquirePurePointer(&ruleResult->_result_call1);
   //    struct _XMLParserState* _local_state = (*arguments._functions->shift_result_instr)
   //       (arguments._content, arguments._functions, state._content, state._functions, sizeof(RuleResult), &read_function,
   //       &init_rule, &move_rule, &copy_rule, &finalize_rule, (void**) &ruleResult, &_local_state_functions);
   //    (*_local_state_functions->set_object_instr)(_local_state, this);
   //    *((struct RuleResult*) (*state._functions->get_rule_result_instr)(state._content)) = _ruleResult;
   //    if (!(*arguments._functions->parse_token_instr)(arguments._content, state._content, &result)) return result;
   //    if (!ruleResult) ruleResult = (RuleResult*) (*state._functions->get_rule_result_instr)(state._content);
   //    ... = ruleResult->_result_call1;
   // }
   {  StringRef state, arguments;
      retrieveStateAndArgumentNameFromParams(pfFunction, state, arguments);
      if (state.size() == 0 || arguments.size() == 0)
         return;
      if (!calleeExpr->getDirectCallee()) {
         // [TODO] error
         return;
      }
      const auto* methodDecl = llvm::dyn_cast<CXXMethodDecl>(pfFunction);
      const bool isDeclared = pfFunction->getName() == "load"
         && methodDecl && (methodDecl->getParent()->getName() == "DocumentConfiguration"
            || methodDecl->getParent()->getName() == "Document");
      std::string parentNameRulesResult = getNameRulesResult(pfFunction, isDeclared);
      std::string nameRulesResult = getNameRulesResult(calleeExpr->getDirectCallee(), false /* isDeclared */);
      StringRef indent = Lexer::getIndentationForLine(topExpr->getBeginLoc(), cContext.getSourceManager());
      bool isReferenceResult = false;
      std::ostringstream varRes;
      int val;
      if (replacements) {
         val = ++uCallNumbers;
         smbStateMachine.callsCache()[calleeExpr] = val;
      }
      else
         val = smbStateMachine.callsCache()[calleeExpr];
      varRes << "_result_call" << val;
      std::string nameReadFunction = calleeExpr->getDirectCallee()->getNameAsString() + "Instr";
      std::string typeResult = getResultType(calleeExpr->getDirectCallee()->getReturnType(),
            calleeExpr->getDirectCallee()->getReturnTypeSourceRange(), isReferenceResult, varRes.str());
      if (typeResult.length() > 0)
         (((sRuleContainer += indent) += typeResult) += ";") += "\n";
      std::string callText;
      ((((((((((((callText
         += "{  const struct _XMLParserStateFunctions* _local_state_functions = nullptr;\n")
         += indent)
         += "   if (!ruleResult) ruleResult = (") += parentNameRulesResult) += "*) (*") += state)
               += "._functions->get_rule_result_instr)(") += state) += "._content);\n")
         += indent)
         += "   struct ") += nameRulesResult) += " _ruleResult;\n";
      int argumentNumber = 0;
      if (auto* memberCallExpr = dyn_cast<CXXMemberCallExpr>(calleeExpr)) {
         bool isReference = false;
         if (auto* memberExpr = dyn_cast<MemberExpr>(calleeExpr->getCallee()))
            isReference = !memberExpr->isArrow();
         Expr* argument = memberCallExpr->getImplicitObjectArgument();
         StringRef textArgument = Lexer::getSourceText(CharSourceRange::getTokenRange(argument->getSourceRange()),
               cContext.getSourceManager(), cContext.getLangOpts());
         std::ostringstream num;
         num << argumentNumber;
         (((((callText += indent) += "   (*_local_state_functions->set_object_instr)"
               "(_local_state, ")) += (isReference ? "&" : "")) += textArgument) += ");\n";
         ((((((callText += indent) += "   ruleResult->_param") += num.str()) += " = ")
            += (isReference ? "&" : "")) += textArgument) += ";\n";
         ++argumentNumber;
      }
      bool isConfiguration = isDeclared &&  methodDecl && (methodDecl->getParent()->getName() == "DocumentConfiguration");
      ((((callText += indent) += "   _ruleResult._configuration") += " = ")
         += (!isDeclared ? "ruleResult->_configuration"
            : (isConfiguration ? "configuration._content" : "ruleResult->document.configuration._content"))) += ";\n";
      ((((callText += indent) += "   _ruleResult._configuration_functions") += " = ")
         += (!isDeclared ? "ruleResult->_configuration_functions"
            : (isConfiguration ? "configuration._functions" : "ruleResult->document.configuration._functions"))) += ";\n";
      for (unsigned paramIndex = 0, argumentIndex = 0;
            paramIndex < calleeExpr->getDirectCallee()->getNumParams();
            ++paramIndex, ++argumentIndex) {
         const ParmVarDecl* param = calleeExpr->getDirectCallee()->getParamDecl(paramIndex);
         bool isReference = false, isPointer = false;
         if (const auto* referenceType = dyn_cast<ReferenceType>(&*param->getType())) {
            isReference = true;
            if (const auto* recordType = dyn_cast<RecordType>(&*referenceType->getPointeeType())) {
               if (recordType->getDecl()->getName() == "XMLParserArguments"
                     || recordType->getDecl()->getName() == "XMLParserStateStack") {
                  continue;
               }
            }
         }
         else if (dyn_cast<PointerType>(&*param->getType()))
            isPointer = true;

         bool doesRemoveReference = false;
         std::string textArgument = getModifiedText(calleeExpr->getArg(argumentIndex), isDeclared,
               &doesRemoveReference);
         std::ostringstream num;
         num << argumentNumber;
         if (isReference || isPointer) {
            if (!doesRemoveReference) {
               std::string preAccess;
               (preAccess += state) += ".acquirePointer(";
               if (isReference)
                  preAccess += "&";
               textArgument.insert(0, preAccess);
               textArgument.append(")");
            };
         }
         (((((callText += indent) += "   _ruleResult._param") += num.str()) += " = ")
            += textArgument) += ";\n";
         ++argumentNumber;
      }
      std::string textResult;
      if (typeResult.length() > 0) {
         (((((callText += indent) += "   _ruleResult._result = ") += state)
            += ".acquirePurePointer(&ruleResult->") += varRes.str()) += ");\n";
         std::string result = "ruleResult->";
         result += varRes.str();
         textResult = getModifiedText(topExpr, isDeclared, nullptr, calleeExpr, &result);
         textResult += ";";
      }
      (((((((((((((((((((((((((((callText
         += indent)
         += "   struct _XMLParserState* _local_state = (*") += arguments)
               += "._functions->shift_result_instr)\n")
         += indent)
         += "     (") += arguments) += "._content, ") += arguments) += "._functions, ")
               += state) += "._content, ") += state) += "._functions, sizeof(")
               += nameRulesResult) +="), &") += nameReadFunction) += ",\n")
         += indent)
         += "        &init_") += nameRulesResult) += ", &move_") += nameRulesResult)
               += ", &copy_") += nameRulesResult) += ", &finalize_") += nameRulesResult)
               += ", (void**) &ruleResult, &_local_state_functions);\n";
      (((((((callText += indent) += "   *((struct ")
         += nameRulesResult) += "*) (*") += state) += "._functions->get_rule_result_instr)(")
         += state) += "._content)) = _ruleResult;\n";
      (((((((callText += indent) += "   if (!(*") += arguments) += "._functions->parse_token_instr)(")
            += arguments) += "._content, ") += state) += "._content, &result)) return result;\n";

      (callText += indent) += "}"; // += "\n" += indent;

      SourceLocation start = topExpr->getBeginLoc(), end = topExpr->getEndLoc();
      // if (dyn_cast<CompoundStmt>(parentStmt)) {
      //    std::string indentString = indent.str();
      //    setToStartLine(cContext.getSourceManager(), start, indentString);
      //    advanceToChar(cContext.getSourceManager(), end, ";", 1);
      //    callText.insert(0, indentString);
      // }
      smbStateMachine.addStateAt(start.getLocWithOffset(-1), parentStmt, &textResult);
      smbStateMachine.addPhiNodesAt(start, state, arguments);

      if (replacements) {
         if (!dyn_cast<CompoundStmt>(parentStmt))
            smbStateMachine.insertionsAtStatements().addConstString(
                  parentStmt->getEndLoc().getLocWithOffset(1), callText, true /* doesEnterBlockBefore */);
         else
            smbStateMachine.insertionsAtStatements().addConstString(start,
                  callText, false /* doesEnterBlockBefore */, true /* doesEatNext */);
         tooling::Replacement remove(cContext.getSourceManager(),
            CharSourceRange::getTokenRange(SourceRange(start, end)),
               StringRef(""), cContext.getLangOpts());
         consumeError((*replacements)[std::string(remove.getFilePath())].add(remove));
      }
      else if (subExprContainer)
         subExprContainer->setReplace(topExpr->getSourceRange(), "", cContext);
   }
}

std::string
ShiftCallBuilder::getResultType(const QualType& resultType, const SourceRange& typeSourceRange,
      bool& isReferenceResult, const std::string& varResult) {
   std::string result;
   result += Lexer::getSourceText(CharSourceRange::getTokenRange(typeSourceRange),
         cContext.getSourceManager(), cContext.getLangOpts());
   if (resultType->isReferenceType()) {
      size_t index = result.length()-1;
      while (index > 0 && result[index] != '&')
         --index;
      if (result[index] == '&') {
         result[index] = '*';
         isReferenceResult = true;
      }
   }
   else if (resultType->isVoidType())
      return "";
   else if (resultType->isFunctionType()) {
      int rightParenNumber = 0;
      size_t index = result.length()-1;
      while (index > 0 && result[index] != ')')
         --index;
      if (index <= 0) {
         (result += " ") += varResult;
         return result;
      }
      ++rightParenNumber;
      --index;
      while (rightParenNumber > 0) {
         while (index > 0 && result[index] != ')' && result[index] != '(')
            --index;
         if (result[index] == ')')
            ++rightParenNumber;
         else if (result[index] == '(')
            --rightParenNumber;
         if (index == 0) {
            (result += " ") += varResult;
            return result;
         }
         --index;
      }
      while (index > 0 && result[index] != ')')
         --index;
      if (index <= 0) {
         (result += " ") += varResult;
         return result;
      }
      result.insert(index, varResult);
      return result;
   }
   (result += " ") += varResult;
   return result;
}

std::string
ShiftCallBuilder::getParameterType(const QualType& parameterType, const SourceRange& parameterSourceRange,
      const StringRef& parameterName, bool& isReferenceParameter, const std::string& varResult) {
   std::string result;
   result += Lexer::getSourceText(CharSourceRange::getTokenRange(parameterSourceRange),
         cContext.getSourceManager(), cContext.getLangOpts());
   size_t posParameter = result.rfind(parameterName.str());
   if (posParameter == std::string::npos)
      return "";
   result.replace(posParameter, parameterName.size(), varResult);
   if (parameterType->isFunctionType()) {
      int rightParenNumber = 0;
      size_t index = posParameter-1;
      while (index > 0 && result[index] != ')')
         --index;
      if (index <= 0) {
         (result += " ") += varResult;
         return result;
      }
      ++rightParenNumber;
      --index;
      while (rightParenNumber > 0) {
         while (index > 0 && result[index] != ')' && result[index] != '(')
            --index;
         if (result[index] == ')')
            ++rightParenNumber;
         else if (result[index] == '(')
            --rightParenNumber;
         if (index == 0) {
            (result += " ") += varResult;
            return result;
         }
         --index;
      }
      while (index > 0 && result[index] != ')')
         --index;
      if (index <= 0) {
         (result += " ") += varResult;
         return result;
      }
      result.insert(index, varResult);
      return result;
   }
   else if (parameterType->isReferenceType()) {
      size_t index = posParameter-1;
      while (index > 0 && result[index] != '&')
         --index;
      if (result[index] == '&') {
         // result[index] = '*';
         result.erase(index, 1);
         result.insert(index, ">");
         isReferenceParameter = true;
         result.insert(0, "XMLParserStateStack::TStackPointer<");
      }
   }
   else if (parameterType->isPointerType()) {
      size_t index = posParameter-1;
      while (index > 0 && result[index] != '*')
         --index;
      if (result[index] == '*') {
         result.erase(index, 1);
         result.insert(index, ">");
         isReferenceParameter = true;
         result.insert(0, "XMLParserStateStack::TStackPointer<");
      }
   }
   return result;
}

class ExtStringPropagator : public MatchFinder::MatchCallback {
  private:
   std::map<std::string, tooling::Replacements>& replacements;
   std::string& sRuleContainer;
   ASTContext& cContext;
   StateMachineBuilder& smbStateMachine;
   const FunctionDecl* pfFunctionDecl;
   bool fIsDeclared;

  public:
   ExtStringPropagator(std::map<std::string, tooling::Replacements>& Replacements,
         std::string& ruleContainer, ASTContext& context, StateMachineBuilder& stateMachine,
         const FunctionDecl* functionDecl, bool isDeclared)
      :  replacements(Replacements), sRuleContainer(ruleContainer), cContext(context),
         smbStateMachine(stateMachine), pfFunctionDecl(functionDecl), fIsDeclared(isDeclared) {}
   virtual void run(const MatchFinder::MatchResult &Result)
      {  const CallExpr* extStringMethod = Result.Nodes.getNodeAs<CallExpr>("ext_string");
         const Stmt *parentStmt = extStringMethod;
         while (parentStmt && dyn_cast<Expr>(parentStmt))
            parentStmt = cContext.getParents(*parentStmt)[0].get<Stmt>();
         if (const auto* operatorCall = dyn_cast<CXXOperatorCallExpr>(extStringMethod)) {
            if (operatorCall->getOperator() == OO_Equal) {
               SourceRange sourceRange = operatorCall->getArg(0)->getSourceRange();
               std::string textIf;
               std::string indent;
               SourceLocation insertionLoc = sourceRange.getBegin();
               setToStartLine(cContext.getSourceManager(), insertionLoc, indent);
               textIf += Lexer::getSourceText(CharSourceRange::getTokenRange(sourceRange),
                     cContext.getSourceManager(), cContext.getLangOpts());

               ReplacementContainer subexprReplacements(sourceRange);
               DeclarationRefMover declRefMover(subexprReplacements, cContext, smbStateMachine,
                     pfFunctionDecl, fIsDeclared);
               ShiftCallBuilder shiftCallBuilder(subexprReplacements, sRuleContainer, cContext,
                     smbStateMachine, pfFunctionDecl);
               MatchFinder finder;
               finder.addMatcher(traverse(CLANGASTTYPE::TK_IgnoreUnlessSpelledInSource,
                     findAll(callExpr(
                        hasAnyArgument(declRefExpr(to(parmVarDecl(hasType(
                           references(recordDecl(matchesName("XMLParserStateStack")))))))),
                        hasAnyArgument(declRefExpr(to(parmVarDecl(hasType(
                           references(recordDecl(matchesName("XMLParserArguments"))))))))).bind("shiftCall"))),
                     &shiftCallBuilder);
               finder.addMatcher(traverse(CLANGASTTYPE::TK_IgnoreUnlessSpelledInSource,
                     findAll(expr(anyOf(declRefExpr(to(eachOf(varDecl(hasLocalStorage()),
                              enumConstantDecl()))).bind("declRef"), cxxThisExpr().bind("this"))))),
                     &declRefMover);
               finder.match(*operatorCall->getArg(0), cContext);
               subexprReplacements.apply(textIf);
               textIf.insert(0, "if (!(");
               textIf.insert(0, indent);
               textIf.append(").isValid())\n   ");
               tooling::Replacement insertion(cContext.getSourceManager(),
                  CharSourceRange::getTokenRange(insertionLoc, insertionLoc),
                     StringRef(textIf.c_str()), cContext.getLangOpts());
               consumeError(replacements[std::string(insertion.getFilePath())].add(insertion));

               std::string textConversion;
               auto* methodDecl = llvm::dyn_cast<CXXMethodDecl>(pfFunctionDecl);
               bool isConfiguration = fIsDeclared &&  methodDecl && (methodDecl->getParent()->getName() == "DocumentConfiguration");
               if (!fIsDeclared)
                  textConversion = "= ExtStringBuilder(ruleResult->_configuration, ruleResult->_configuration_functions) =";
               else if (isConfiguration)
                  textConversion = "= ExtStringBuilder(configuration._content, configuration._functions) =";
               else
                  textConversion = "= ExtStringBuilder(ruleResult->document.configuration._content, ruleResult->document.configuration._functions) =";
               SourceRange argSourceRange = operatorCall->getOperatorLoc();
               tooling::Replacement replaceOperator(cContext.getSourceManager(),
                  CharSourceRange::getTokenRange(argSourceRange),
                     StringRef(textConversion.c_str()), cContext.getLangOpts());
               consumeError(replacements[std::string(replaceOperator.getFilePath())].add(replaceOperator));
            }
         }
      }
};

/* Definition of the translation of return instructions */

class ReturnNodeBuilder : public MatchFinder::MatchCallback {
  private:
   std::map<std::string, tooling::Replacements> &replacements;
   ASTContext& cContext;
   StateMachineBuilder& smbStateMachine;
   const FunctionDecl* pfFunction;

  public:
   ReturnNodeBuilder(std::map<std::string, tooling::Replacements>& Replacements,
         ASTContext& context, StateMachineBuilder& stateMachine, const FunctionDecl* function)
      :  replacements(Replacements), cContext(context), smbStateMachine(stateMachine),
         pfFunction(function) {}
   virtual void run(const MatchFinder::MatchResult &Result);
};

void
ReturnNodeBuilder::run(const MatchFinder::MatchResult &Result) {
   const auto *returnStmt = Result.Nodes.getNodeAs<ReturnStmt>("returnNode");
   const Stmt *parentStmt = cContext.getParents(*returnStmt)[0].get<Stmt>();
   while (parentStmt && dyn_cast<Expr>(parentStmt))
      parentStmt = cContext.getParents(*parentStmt)[0].get<Stmt>();
   {  
      std::string textPre, textPost;
      if (!dyn_cast<CompoundStmt>(parentStmt)) {
         textPre = "{  ";
         textPost = "}\n";
      }
      StringRef indent = Lexer::getIndentationForLine(returnStmt->getBeginLoc(), cContext.getSourceManager());
      StringRef state, argument;
      retrieveStateAndArgumentNameFromParams(pfFunction, state, argument);
      if (returnStmt->getRetValue()) {
         smbStateMachine.addLocalStorage(returnStmt->getBeginLoc());
         StringRef result = Lexer::getSourceText(CharSourceRange::getTokenRange(
               returnStmt->getRetValue()->getSourceRange()), cContext.getSourceManager(), cContext.getLangOpts());
         (((((textPre += "ruleResult->_result.deref(" ) += state) += ") = ") += result)
             += ";\n") += indent;
      }
      // (*arguments._functions->reduce_state_instr)(arguments._content, state._content, (void**) &ruleResult)
      ((((((((textPre += "(*") += argument) += "._functions->reduce_state_instr)(")
         += argument) += "._content, ") += state) += "._content, (void**) &ruleResult);\n")
         += indent) += "return ";
      bool isMain = false;
      if (const auto* method = dyn_cast<CXXMethodDecl>(pfFunction)) {
         if (method->getParent()->getName() == "DocumentConfiguration"
               && method->getName() == "load")
            isMain = true;
         else if (method->getParent()->getName() == "Document"
               && method->getName() == "load")
            isMain = true;
      }
      textPre += (isMain ? "RRFinished" : "RRHasToken");
      if (textPost.length() > 0)
         ((textPre += ";\n") += indent) += textPost;
      tooling::Replacement replace(cContext.getSourceManager(),
         CharSourceRange::getTokenRange(returnStmt->getSourceRange()),
            StringRef(textPre.c_str()), cContext.getLangOpts());
      consumeError(replacements[std::string(replace.getFilePath())].add(replace));
   }
}

/* Definition of the translation of the comparison with const char* strings */

class ConstStringBuilder : public MatchFinder::MatchCallback {
  private:
   std::map<std::string, tooling::Replacements> &replacements;
   std::string& sRuleContainer;
   ASTContext& cContext;
   int uStringsNumber = 0;
   int uPreviousState = 0;
   StateMachineBuilder& smbStateMachine;
   const FunctionDecl* pfFunctionDecl;
   bool fHasAccessToValueCompare = false;
   bool fHasAccessToAttributeCompare = false;
   struct LazyReplacementText {
      int stringNumber = 0;
      SourceRange sourceRange;
      std::string optimizedReplacement;
      std::string secureReplacement;
   };
   LazyReplacementText lrtReplacementTextValue, lrtReplacementTextAttribute;
   std::set<int> msfAccessToValueByState;

   struct FoundMatcher : public MatchFinder::MatchCallback {
      bool hasFound = false;
      virtual void run(const MatchFinder::MatchResult &Result)
         {  hasFound = true; }
   };

  public:
   ConstStringBuilder(std::map<std::string, tooling::Replacements>& Replacements,
         std::string& ruleContainer, ASTContext& context, StateMachineBuilder& currentState,
         const FunctionDecl* functionDecl)
      :  replacements(Replacements), sRuleContainer(ruleContainer), cContext(context),
         smbStateMachine(currentState), pfFunctionDecl(functionDecl) {}
   ~ConstStringBuilder() { closeReplacements(); }

   int getStringsNumber() const { return uStringsNumber; }
   StateMachineBuilder::InsertionsAtStatements& insertionsAtStatements() { return smbStateMachine.insertionsAtStatements(); }
   virtual void run(const MatchFinder::MatchResult &Result);
   bool hasPreviousAccessToValueCompare(bool isValue, int newState) const
      {  return msfAccessToValueByState.find(newState) != msfAccessToValueByState.end(); }
   bool hasPreviousAccessToValueCompare(bool isValue) const
      {  return isValue ? fHasAccessToValueCompare : fHasAccessToAttributeCompare; }
   bool doesRestrictInsertReplacement(bool isValue)
      {  bool result = hasPreviousAccessToValueCompare(isValue);
         if (result) {
            auto& replacementText = isValue ? lrtReplacementTextValue : lrtReplacementTextAttribute;
            if (replacementText.sourceRange.isValid()) {
               tooling::Replacement replace(cContext.getSourceManager(),
                  CharSourceRange::getTokenRange(replacementText.sourceRange),
                     StringRef(replacementText.secureReplacement.c_str()), cContext.getLangOpts());
               consumeError(replacements[std::string(replace.getFilePath())].add(replace));
               replacementText = LazyReplacementText {};
            }
         }
         else {
            (isValue ? fHasAccessToValueCompare : fHasAccessToAttributeCompare) = true;
            msfAccessToValueByState.insert(uPreviousState); // attribute requires value
         }
         return result;
      }

   bool insertReplacement(const SourceRange& sourceRange, const std::string& optimizedChange,
         const std::string& secureChange, int firstSecureReplacementChar,
         int lastSecureReplacementChar, bool isValue)
      {  auto& replacementText = isValue ? lrtReplacementTextValue : lrtReplacementTextAttribute;
         bool result = true;
         if (hasPreviousAccessToValueCompare(isValue)) {
            if (replacementText.sourceRange.isValid()) {
               tooling::Replacement replace(cContext.getSourceManager(),
                  CharSourceRange::getTokenRange(replacementText.sourceRange),
                     StringRef(replacementText.secureReplacement.c_str()), cContext.getLangOpts());
               consumeError(replacements[std::string(replace.getFilePath())].add(replace));
               replacementText = LazyReplacementText {};
            }
            tooling::Replacement newReplace(cContext.getSourceManager(),
               CharSourceRange::getTokenRange(sourceRange),
                  StringRef(std::string(secureChange, firstSecureReplacementChar,
                        lastSecureReplacementChar-firstSecureReplacementChar+1).c_str()),
                  cContext.getLangOpts());
            consumeError(replacements[std::string(newReplace.getFilePath())].add(newReplace));
            result = false;
         }
         else {
            replacementText.stringNumber = uStringsNumber;
            replacementText.sourceRange = sourceRange;
            replacementText.optimizedReplacement = optimizedChange;
            replacementText.secureReplacement = secureChange;
            (isValue ? fHasAccessToValueCompare : fHasAccessToAttributeCompare) = true;
            msfAccessToValueByState.insert(uPreviousState); // attribute requires value
         }
         return result;
      }
   void closeReplacements()
      {  if (fHasAccessToValueCompare && lrtReplacementTextValue.sourceRange.isValid()) {
            tooling::Replacement replace(cContext.getSourceManager(),
               CharSourceRange::getTokenRange(lrtReplacementTextValue.sourceRange),
                  StringRef(lrtReplacementTextValue.optimizedReplacement.c_str()), cContext.getLangOpts());
            consumeError(replacements[std::string(replace.getFilePath())].add(replace));
            lrtReplacementTextValue = LazyReplacementText {};
         }
         if (fHasAccessToAttributeCompare && lrtReplacementTextAttribute.sourceRange.isValid()) {
            tooling::Replacement replace(cContext.getSourceManager(),
               CharSourceRange::getTokenRange(lrtReplacementTextAttribute.sourceRange),
                  StringRef(lrtReplacementTextAttribute.optimizedReplacement.c_str()), cContext.getLangOpts());
            consumeError(replacements[std::string(replace.getFilePath())].add(replace));
            lrtReplacementTextAttribute = LazyReplacementText {};
         }
         fHasAccessToValueCompare = fHasAccessToAttributeCompare = false;
      }
};

void
ConstStringBuilder::run(const MatchFinder::MatchResult &Result) {
   const auto *calleeExpr = Result.Nodes.getNodeAs<CXXOperatorCallExpr>("valueOrAttributeComparison");
   const Stmt *parentStmt = cContext.getParents(*calleeExpr)[0].get<Stmt>();
   const Decl *parentDecl = nullptr;
   const Stmt* oldParentStmt = calleeExpr;
   while (parentStmt && dyn_cast<Expr>(parentStmt)) {
      oldParentStmt = parentStmt;
      parentStmt = cContext.getParents(*parentStmt)[0].get<Stmt>();
   }
   if (!parentStmt) {
      parentDecl = cContext.getParents(*oldParentStmt)[0].get<Decl>();
      if (!parentDecl)
         return;
   }

   // const auto *parentStmt = Result.Nodes.getNodeAs<Stmt>("parentStmt");
   int newState = smbStateMachine.queryState(calleeExpr->getSourceRange().getBegin());
   if (uPreviousState != newState) {
      closeReplacements();
      uPreviousState = newState;
   }

   std::ostringstream textString;
   SourceLocation insertLoc = parentStmt ? parentStmt->getSourceRange().getBegin()
      : parentDecl->getBeginLoc();
   StringRef indent;
   if (parentStmt && dyn_cast<CompoundStmt>(parentStmt)) {
      insertLoc = insertLoc.getLocWithOffset(2); // [TODO] should be 1
      indent = Lexer::getIndentationForLine(insertLoc, cContext.getSourceManager());
      textString << "\n" << indent.str();
   }
   else
      indent = Lexer::getIndentationForLine(insertLoc, cContext.getSourceManager());

   smbStateMachine.addLocalStorage(insertLoc);

   SourceRange sourceRangeString;
   bool isConstChar = false, isExtString = false;
   bool isFirstArgumentValueOrAttribute = false;
   int sourceIndex = 0;
   // find which argument has .value() or .attribute()
   if (calleeExpr->getArg(0)->getType()->isPointerType()
         && static_cast<const PointerType&>(*calleeExpr->getArg(0)->getType())
               .getPointeeType()->isCharType()) {
      sourceRangeString = calleeExpr->getArg(0)->getSourceRange();
      sourceIndex = 0;
      isConstChar = true;
   }
   else if (calleeExpr->getArg(1)->getType()->isPointerType()
         && static_cast<const PointerType&>(*calleeExpr->getArg(1)->getType())
               .getPointeeType()->isCharType()) {
      sourceRangeString = calleeExpr->getArg(1)->getSourceRange();
      sourceIndex = 1;
      isConstChar = true;
      isFirstArgumentValueOrAttribute = true;
   }
   else if (calleeExpr->getArg(0)->getType()->isRecordType()
         && calleeExpr->getArg(0)->getType()->getAsRecordDecl()->getName() == "ExtString") {
      sourceRangeString = calleeExpr->getArg(0)->getSourceRange();
      sourceIndex = 0;
      isExtString = true;
   }
   else if (calleeExpr->getArg(1)->getType()->isRecordType()
         && calleeExpr->getArg(1)->getType()->getAsRecordDecl()->getName() == "ExtString") {
      sourceRangeString = calleeExpr->getArg(1)->getSourceRange();
      sourceIndex = 1;
      isExtString = true;
      isFirstArgumentValueOrAttribute = true;
   }
   else {
      MatchFinder argumentFinder;
      FoundMatcher foundMatcher;
      argumentFinder.addMatcher(traverse(CLANGASTTYPE::TK_IgnoreUnlessSpelledInSource,
         expr(anyOf(unless(whileStmt()), cxxMemberCallExpr(on(declRefExpr(to(parmVarDecl(hasType(
                  references(recordDecl(matchesName("XMLParserArguments")))))))),
               callee(cxxMethodDecl(anyOf(hasName("value"), hasName("extendedValue"),
                        hasName("attribute")))))))),
         &foundMatcher);
      argumentFinder.match(*calleeExpr->getArg(0), cContext);
      if (foundMatcher.hasFound) {
         isFirstArgumentValueOrAttribute = true;
         sourceRangeString = calleeExpr->getArg(1)->getSourceRange();
         sourceIndex = 1;
      }
      else {
         sourceRangeString = calleeExpr->getArg(0)->getSourceRange();
         sourceIndex = 0;
      }
   }

   const auto* methodDecl = llvm::dyn_cast<CXXMethodDecl>(pfFunctionDecl);
   const bool isDeclared = pfFunctionDecl->getName() == "load"
      && methodDecl && (methodDecl->getParent()->getName() == "DocumentConfiguration"
         || methodDecl->getParent()->getName() == "Document");
   if (!isExtString) {
      textString << "if (!string" << uStringsNumber << ") string" << uStringsNumber << " = (*";
      bool isConfiguration = isDeclared &&  methodDecl && (methodDecl->getParent()->getName() == "DocumentConfiguration");
      if (!isDeclared)
         textString << "ruleResult->_configuration_functions->convert_string)(ruleResult->_configuration, ";
      else if (isConfiguration)
         textString << "configuration._functions->convert_string)(configuration._content, ";
      else
         textString << "ruleResult->document.configuration._functions->convert_string)(ruleResult->document.configuration._content, ";
      {  const auto& scope = smbStateMachine.queryScope(insertLoc);
         if (!scope.empty() && !dyn_cast<CompoundStmt>(scope.back()))
            insertLoc = scope.back()->getSourceRange().getBegin();
      }
   }
   if (sourceRangeString.isValid()) {
      // insert string initialization
      std::string textExpr = Lexer::getSourceText(CharSourceRange::getTokenRange(sourceRangeString),
            cContext.getSourceManager(), cContext.getLangOpts()).str();
      ReplacementContainer subexprReplacements(SourceRange(sourceRangeString.getBegin(),
               sourceRangeString.getBegin().getLocWithOffset(textExpr.length())));
      DeclarationRefMover declRefMover(subexprReplacements, cContext, smbStateMachine,
            pfFunctionDecl, isDeclared);
      ShiftCallBuilder shiftCallBuilder(subexprReplacements, sRuleContainer, cContext,
            smbStateMachine, pfFunctionDecl);
      MatchFinder finder;
      finder.addMatcher(traverse(CLANGASTTYPE::TK_IgnoreUnlessSpelledInSource,
            findAll(callExpr(
               hasAnyArgument(declRefExpr(to(parmVarDecl(hasType(
                  references(recordDecl(matchesName("XMLParserStateStack")))))))),
               hasAnyArgument(declRefExpr(to(parmVarDecl(hasType(
                  references(recordDecl(matchesName("XMLParserArguments"))))))))).bind("shiftCall"))),
            &shiftCallBuilder);
      // finder.addMatcher(traverse(CLANGASTTYPE::TK_IgnoreUnlessSpelledInSource,
      //       findAll(cxxOperatorCallExpr(isComparisonOperator(),
      //             hasAnyArgument(cxxMemberCallExpr(
      //                on(declRefExpr(to(parmVarDecl(hasType(
      //                   references(recordDecl(matchesName("XMLParserArguments")))))))),
      //                callee(cxxMethodDecl(anyOf(hasName("value"), hasName("extendedValue"),
      //                      hasName("attribute"))))))).bind("valueOrAttributeComparison"))),
      //       &constStringBuilder);
      finder.addMatcher(traverse(CLANGASTTYPE::TK_IgnoreUnlessSpelledInSource,
            findAll(expr(anyOf(declRefExpr(to(eachOf(varDecl(hasLocalStorage()),
                     enumConstantDecl()))).bind("declRef"), cxxThisExpr().bind("this"))))),
            &declRefMover);
      // finder.addMatcher(traverse(CLANGASTTYPE::TK_IgnoreUnlessSpelledInSource,
      //       findAll(cxxMemberCallExpr(
      //          on(declRefExpr(to(parmVarDecl(hasType(
      //             references(recordDecl(matchesName("XMLParserArguments")))))))),
      //          callee(cxxMethodDecl(anyOf(hasName("value"), hasName("extendedValue"))))).bind("value"))),
      //       &argumentValueAccessor);
      finder.match(*calleeExpr->getArg(sourceIndex), cContext);
      subexprReplacements.apply(textExpr);
      if (!isExtString) {
         if (isConstChar)
            textString << textExpr;
         else
            textString << '(' << textExpr << ')' << ".c_str()";
         textString << ");\n" << indent.str();
         insertionsAtStatements().addConstString(insertLoc, textString.str(), false /* doesEnterBlockBefore */);
         // tooling::Replacement insert(cContext.getSourceManager(),
         //    CharSourceRange::getCharRange(SourceRange(insertLoc, insertLoc)),
         //       StringRef(textString.str().c_str()), cContext.getLangOpts());
         // consumeError(replacements[std::string(insert.getFilePath())].add(insert));
      }

      // change comparisontext
      StringRef firstOperand = Lexer::getSourceText(CharSourceRange::getTokenRange(
               calleeExpr->getArg(0)->getSourceRange()), cContext.getSourceManager(), cContext.getLangOpts());
      StringRef secondOperand = Lexer::getSourceText(CharSourceRange::getTokenRange(
               calleeExpr->getArg(1)->getSourceRange()), cContext.getSourceManager(), cContext.getLangOpts());
      std::ostringstream newStringText;
      if (!isExtString)
         newStringText << "string" << uStringsNumber;
      else
         newStringText << '(' << textExpr << ").scontent()";
      std::string argumentAccess = std::string(isFirstArgumentValueOrAttribute ? firstOperand : secondOperand);
      bool isValue = false, isExtendedValue = false, isAttribute = false;
      size_t valueLength = strlen("value()"), extendedValueLength = strlen("extendedValue()"),
             attributeLength = strlen("attribute()");
      if (argumentAccess.length() > valueLength
            && std::string(argumentAccess, argumentAccess.length()-valueLength, valueLength) == "value()") {
         isValue = true;
         argumentAccess = std::string(argumentAccess, 0, argumentAccess.length()-valueLength);
      }
      else if (argumentAccess.length() > extendedValueLength
            && std::string(argumentAccess, argumentAccess.length()-extendedValueLength, extendedValueLength) == "extendedValue()") {
         isValue = true;
         argumentAccess = std::string(argumentAccess, 0, argumentAccess.length()-extendedValueLength);
      }
      else if (argumentAccess.length() > attributeLength
            && std::string(argumentAccess, argumentAccess.length()-attributeLength, attributeLength) == "attribute()") {
         isAttribute = true;
         argumentAccess = std::string(argumentAccess, 0, argumentAccess.length()-attributeLength);
      }
      if (!isValue && !isExtendedValue && !isAttribute)
         return;
      int compareResult;
      switch (calleeExpr->getOperator()) {
         case OO_EqualEqual: compareResult = 1; break;
         case OO_ExclaimEqual: compareResult = 6; break;
         case OO_Greater: compareResult = 4; break;
         case OO_GreaterEqual: compareResult = 5; break;
         case OO_Less: compareResult = 2; break;
         case OO_LessEqual: compareResult = 3; break;
         case OO_Spaceship: compareResult = 7; break;
         default: compareResult = 0; break;
      }
      if (!compareResult) {
         tooling::Replacement replace(cContext.getSourceManager(),
            CharSourceRange::getTokenRange(sourceRangeString), StringRef(newStringText.str().c_str()),
               cContext.getLangOpts());
         consumeError(replacements[std::string(replace.getFilePath())].add(replace));
         if (!isExtString)
            ++uStringsNumber;
         return;
      }
      // (((result = (*arguments._functions->load_value_instr)(arguments._content)) != RRNeedChars)
      //    && ((*arguments._functions->compare_value_instr)(arguments._content, string0) != 0))
      std::string textAlternateComparison;
      // (((result = (*arguments._functions->is_equal_value_instr)(arguments._content,
      //       &doesContinue, string0)) != RRNeedChars) || !doesContinue)
      std::string textComparison = "(((result = (*";
      (textComparison += argumentAccess) += "_functions->";
      bool hasOptimization = false;
      if (isAttribute) {
         textComparison += "load_attribute_and_value_instr";
         doesRestrictInsertReplacement(true /* isValue */);
      }
      else if (isExtendedValue)
         textComparison += "load_extended_value_instr";
      else if (compareResult == 1 || compareResult == 6) {
         hasOptimization = !isExtString && !hasPreviousAccessToValueCompare(false /* isValue */);
         if (hasOptimization) {
            textAlternateComparison = textComparison;
            textComparison += "is_equal_value_instr";
            textAlternateComparison += "load_value_instr";
         }
         else
            textComparison += "load_value_instr";
      }
      else
         textComparison += "load_value_instr";
      ((textComparison += ")(") += argumentAccess) += "_content";
      int firstSureReplacementChar = 0, lastSureReplacementChar = 0;
      if (hasOptimization) {
         ((textAlternateComparison += ")(") += argumentAccess) += "_content";
         ((textComparison += ", &doesContinue, ") += newStringText.str())
            += ")) == RRNeedChars) || ";
         if (compareResult == 6)
            textComparison += "!";
         textComparison += "doesContinue)";
         ((textAlternateComparison += ")) != RRNeedChars)\n") += indent.str()) += "   && ";
         firstSureReplacementChar = textAlternateComparison.length();
         ((((((textAlternateComparison += "((*") += argumentAccess) += "_functions->compare_value_instr)(")
            += argumentAccess) += "_content, ") += newStringText.str()) += ") ";
         if (compareResult == 1)
            textAlternateComparison += "==";
         else
            textAlternateComparison += "!=";
         textAlternateComparison += " 0))";
         lastSureReplacementChar = textAlternateComparison.length()-2;
      }
      else {
         ((textComparison += ")) != RRNeedChars)\n") += indent.str()) += "   && ";
         firstSureReplacementChar = textComparison.length();
         ((((((textComparison += "((*") += argumentAccess)
            += (isValue ? "_functions->compare_value_instr)(" : "_functions->compare_attribute_instr)("))
            += argumentAccess) += "_content, ") += newStringText.str()) += ") ";
         if (compareResult == 6)
            textComparison += "!=";
         else {
            if (compareResult & 2)
               textComparison += "<";
            if (compareResult & 1)
               textComparison += "=";
            if (compareResult == 1)
               textComparison += "=";
            if (compareResult & 4)
               textComparison += ">";
         }
         textComparison += " 0))";
         lastSureReplacementChar = textComparison.length()-2;
      };
      bool doesUpdateNeedChars = !hasPreviousAccessToValueCompare(isValue);
      smbStateMachine.addOuterModifiedRange(calleeExpr->getSourceRange());
      if (hasOptimization)
         insertReplacement(calleeExpr->getSourceRange(), textComparison, textAlternateComparison,
               firstSureReplacementChar, lastSureReplacementChar, isValue);
      else {
         if (doesRestrictInsertReplacement(isValue))
            textComparison = std::string(textComparison, firstSureReplacementChar,
                  lastSureReplacementChar-firstSureReplacementChar+1);
         // add block
         tooling::Replacement replace(cContext.getSourceManager(),
            CharSourceRange::getTokenRange(calleeExpr->getSourceRange()),
               StringRef(textComparison.c_str()), cContext.getLangOpts());
         consumeError(replacements[std::string(replace.getFilePath())].add(replace));
      }
      if (doesUpdateNeedChars) {
         std::string insertionText = "\n";
         (insertionText += indent) += "if (result == RRNeedChars) return RRNeedChars;";
         std::string blockInsertionText = "if (result == RRNeedChars) return RRNeedChars;\n";
         blockInsertionText += indent;
         auto locInsertion = parentStmt ? parentStmt->getSourceRange().getEnd()
            : parentDecl->getEndLoc();
         locInsertion = Lexer::getLocForEndOfToken(locInsertion, 0, cContext.getSourceManager(), cContext.getLangOpts());
         locInsertion = locInsertion.getLocWithOffset(1); // for ';'
         // tooling::Replacement insertion(cContext.getSourceManager(),
         //    CharSourceRange::getTokenRange(SourceRange(locInsertion, locInsertion)),
         //       StringRef(insertionText.c_str()), cContext.getLangOpts());
         // consumeError(replacements[std::string(insertion.getFilePath())].add(insertion));
         if (auto* parentIf = parentStmt ? dyn_cast<IfStmt>(parentStmt) : nullptr) {
            insertionText.insert(1, "   ");
            blockInsertionText.insert(blockInsertionText.length()-1, "   ");
            bool hasElseManaged = false;
            if (auto* compoundStmt = dyn_cast<CompoundStmt>(parentIf->getThen()))
               insertionsAtStatements().addResultNeedChars(compoundStmt->getSourceRange()
                     .getBegin().getLocWithOffset(1), insertionText);
            else {
               insertionsAtStatements().addResultNeedChars(parentIf->getThen()
                     ->getSourceRange().getBegin(), blockInsertionText);
               std::string preText = "\n", postText = "\n";
               (preText += indent) += "{  ";
               (postText += indent) += "}";
               SourceLocation locEnd = smbStateMachine.queryEndInstruction(
                     parentIf->getThen()->getEndLoc());
               if (!parentIf->getElse()) {
                  insertionText.erase(1, 3);
                  postText += insertionText;
                  hasElseManaged = true;
               }
               insertionsAtStatements().addBlock(parentIf->getThen()
                     ->getSourceRange().getBegin(), preText, locEnd, postText);
            }
            if (parentIf->getElse()) {
               if (auto* compoundStmt = dyn_cast<CompoundStmt>(parentIf->getElse()))
                  insertionsAtStatements().addResultNeedChars(compoundStmt->getSourceRange()
                        .getBegin().getLocWithOffset(1), insertionText);
               else {
                  insertionsAtStatements().addResultNeedChars(parentIf->getElse()
                        ->getSourceRange().getBegin(), blockInsertionText);
                  std::string preText = "\n", postText = "\n";
                  (preText += indent) += "{  ";
                  (postText += indent) += "}";
                  SourceLocation locEnd = smbStateMachine.queryEndInstruction(
                        parentIf->getElse()->getEndLoc());
                  insertionsAtStatements().addBlock(parentIf->getElse()
                        ->getSourceRange().getBegin(), preText, locEnd, postText);
               }
            }
            else if (!hasElseManaged) {
               // [TODO] to complete if parent is an if
               insertionText.erase(1, 3);
               insertionsAtStatements().addResultNeedChars(parentIf->getEndLoc()
                     .getLocWithOffset(1), insertionText);
            }
         }
         else if (parentStmt && (dyn_cast<WhileStmt>(parentStmt) || dyn_cast<ForStmt>(parentStmt)
               || dyn_cast<CXXForRangeStmt>(parentStmt))) {
            const Stmt* body = nullptr;
            if (auto* parentWhile = dyn_cast<WhileStmt>(parentStmt))
               body = parentWhile->getBody();
            else if (auto* parentFor = dyn_cast<ForStmt>(parentStmt))
               body = parentFor->getBody();
            else if (auto* parentFor = dyn_cast<CXXForRangeStmt>(parentStmt))
               body = parentFor->getBody();
            insertionsAtStatements().addResultNeedChars(locInsertion, insertionText);
            insertionText.insert(1, "   ");
            blockInsertionText.insert(blockInsertionText.length()-1, "   ");
            if (auto* compoundStmt = dyn_cast<CompoundStmt>(body))
               insertionsAtStatements().addResultNeedChars(compoundStmt->getSourceRange()
                     .getBegin().getLocWithOffset(1), insertionText);
            else {
               insertionsAtStatements().addResultNeedChars(body->getSourceRange().getBegin(),
                     blockInsertionText);
               std::string preText = "\n", postText = "\n";
               (preText += indent) += "{  ";
               (postText += indent) += "}";
               SourceLocation locEnd = smbStateMachine.queryEndInstruction(
                     body->getSourceRange().getEnd());
               insertionsAtStatements().addBlock(body->getSourceRange().getBegin(), preText,
                     locEnd, postText);
            }
         }
         else {
            if (!parentStmt || parentStmt != pfFunctionDecl->getBody())
               insertionsAtStatements().addResultNeedChars(locInsertion, insertionText);
         }
      }
   }
   // const auto& SM = *Result.SourceManager;
   // const auto& Loc = parentStmt->getSourceRange().getBegin();
   // llvm::outs() << SM.getFilename(Loc) << ":"
   //              << SM.getSpellingLineNumber(Loc) << ":"
   //              << SM.getSpellingColumnNumber(Loc) << "\n";
   if (!isExtString)
      ++uStringsNumber;
}

/* Definition of the translation of set/get methods on XMLParserArguments */

class ArgumentValueAccessor : public MatchFinder::MatchCallback {
  private:
   std::map<std::string, tooling::Replacements> &replacements;
   ASTContext& cContext;
   StateMachineBuilder& smbStateMachine;
   ConstStringBuilder& csbConstStringBuilder;
   int uPreviousState = 0;
   bool fHasAccessToValue = false;

  public:
   ArgumentValueAccessor(std::map<std::string, tooling::Replacements>& Replacements,
         ASTContext& context, StateMachineBuilder& stateMachine,
         ConstStringBuilder& constStringBuilder)
      :  replacements(Replacements), cContext(context), smbStateMachine(stateMachine),
         csbConstStringBuilder(constStringBuilder) {}
   virtual void run(const MatchFinder::MatchResult &Result);
};

void
ArgumentValueAccessor::run(const MatchFinder::MatchResult &Result) {
   const auto *calleeExpr = Result.Nodes.getNodeAs<CXXMemberCallExpr>("value");

   // (*arguments._functions->get_value)(arguments._content)
   if (!smbStateMachine.hasOuterModifiedRange(calleeExpr->getSourceRange())) {  
      std::string nameCallee = calleeExpr->getDirectCallee()->getNameAsString();
      bool isValue = nameCallee == "value", isExtendedValue = nameCallee == "extendedValue";
      if (!isValue && !isExtendedValue)
         return;

      int newState = smbStateMachine.queryState(calleeExpr->getSourceRange().getBegin());
      if (uPreviousState != newState) {
         fHasAccessToValue = false;
         uPreviousState = newState;
      }
      if (csbConstStringBuilder.hasPreviousAccessToValueCompare(true /* isValue */, newState))
         fHasAccessToValue = true;
      StringRef argument = Lexer::getSourceText(CharSourceRange::getTokenRange(
            calleeExpr->getImplicitObjectArgument()->getSourceRange()), cContext.getSourceManager(), cContext.getLangOpts());
      if (!fHasAccessToValue) {
         SourceLocation locInsertion;
         const Stmt *parentStmt = cContext.getParents(*calleeExpr)[0].get<Stmt>();
         const Stmt *oldParentStmt = parentStmt;
         while (parentStmt && dyn_cast<Expr>(parentStmt)) {
            oldParentStmt = parentStmt;
            parentStmt = cContext.getParents(*parentStmt)[0].get<Stmt>();
         }
         if (!parentStmt) {
            if (const Decl* decl = cContext.getParents(*oldParentStmt)[0].get<Decl>())
               locInsertion = decl->getBeginLoc();
         }
         else {
            if (parentStmt && dyn_cast<IfStmt>(parentStmt))
               oldParentStmt = parentStmt;
            locInsertion = oldParentStmt->getBeginLoc();
         }
         std::string indent;
         setToStartLine(cContext.getSourceManager(), locInsertion, indent);
         if (locInsertion.isValid()) {
            fHasAccessToValue = true;
            std::string insertText = indent;
            ((((((insertText += "if ((result = (*") += argument) += "._functions->")
               += (isValue ? "load_value_instr" : "load_extended_value_instr"))
               += ")(") += argument) += "._content)) == RRNeedChars) return RRNeedChars;\n";
            tooling::Replacement insert(cContext.getSourceManager(),
               CharSourceRange::getTokenRange(locInsertion, locInsertion),
                  StringRef(insertText.c_str()), cContext.getLangOpts());
            consumeError(replacements[std::string(insert.getFilePath())].add(insert));
         }
      }

      std::string replaceText;
      ((((replaceText += "(*") += argument) += "._functions->get_value)(")
         += argument) += "._content)";
      tooling::Replacement replace(cContext.getSourceManager(),
         CharSourceRange::getTokenRange(calleeExpr->getSourceRange()),
            StringRef(replaceText.c_str()), cContext.getLangOpts());
      consumeError(replacements[std::string(replace.getFilePath())].add(replace));
   }
}

class ConfigurationSetKeyValueReplacer : public MatchFinder::MatchCallback {
  private:
   std::map<std::string, tooling::Replacements> &replacements;
   ASTContext& cContext;

  public:
   ConfigurationSetKeyValueReplacer(std::map<std::string, tooling::Replacements>& Replacements,
         ASTContext& context)
      :  replacements(Replacements), cContext(context) {}
   virtual void run(const MatchFinder::MatchResult &Result);
};

void
ConfigurationSetKeyValueReplacer::run(const MatchFinder::MatchResult &Result) {
   const auto *calleeExpr = Result.Nodes.getNodeAs<CXXMemberCallExpr>("setKeyValue");
   // (*_functions->set_key_value)(_content, ...);
   {  
      // StringRef argument = Lexer::getSourceText(CharSourceRange::getTokenRange(
      //       calleeExpr->getImplicitObjectArgument()->getSourceRange()), cContext.getSourceManager(), cContext.getLangOpts());
      tooling::Replacement replace(cContext.getSourceManager(),
         CharSourceRange::getTokenRange(calleeExpr->getCallee()->getSourceRange()),
            StringRef("_functions->set_key_value"), cContext.getLangOpts());
      consumeError(replacements[std::string(replace.getFilePath())].add(replace));
      SourceLocation locEnd = calleeExpr->getCallee()->getEndLoc();
      bool isInvalid = false;
      const char *buffer = cContext.getSourceManager().getCharacterData(
            locEnd, &isInvalid);
      int index=0;
      while (buffer[index] && (isalnum(buffer[index]) || buffer[index] == '_'))
         ++index;
      if (!isInvalid && buffer[index] != '(') {
         while (isspace(buffer[++index])) {}
         if (buffer[index] != '(') // Lexer::AdvanceToTokenCharacter
            return;
      }
      else if (isInvalid)
         return;
      locEnd = locEnd.getLocWithOffset(index+1);
      tooling::Replacement insert(cContext.getSourceManager(),
         CharSourceRange::getCharRange(locEnd, locEnd),
            StringRef("configuration._content, "), cContext.getLangOpts());
      consumeError(replacements[std::string(insert.getFilePath())].add(insert));
   }
}

/* Definition of the RuleResult data structure */

class VariableMover : public MatchFinder::MatchCallback {
  private:
   std::map<std::string, tooling::Replacements> &replacements;
   std::string& sResult;
   ASTContext& cContext;
   StateMachineBuilder& smbStateMachine;
   CompoundRegister& crCompoundStructure;

  public:
   VariableMover(std::map<std::string, tooling::Replacements>& Replacements,
         std::string& result, ASTContext& context, StateMachineBuilder& stateMachine,
         CompoundRegister& compoundStructure)
      :  replacements(Replacements), sResult(result), cContext(context),
         smbStateMachine(stateMachine), crCompoundStructure(compoundStructure) {}

   virtual void run(const MatchFinder::MatchResult &Result);
};

void
VariableMover::run(const MatchFinder::MatchResult &Result) {
   const auto *declStmt = Result.Nodes.getNodeAs<DeclStmt>("declStmt");
   bool hasInit = false;
   for (auto iter = declStmt->decl_begin(), iterEnd = declStmt->decl_end();
         !hasInit && iter != iterEnd; ++iter)
      if (auto* varDecl = dyn_cast<VarDecl>(*iter))
         hasInit = varDecl->hasInit();

   std::ostringstream extension;
   int scopeIdentifier = crCompoundStructure.queryScopeIdentifier(declStmt->getBeginLoc());
   if (scopeIdentifier > 0)
      extension << "_" << scopeIdentifier;
   SourceLocation locDeclStart = declStmt->getBeginLoc();
   std::string indent;
   const auto& SM = cContext.getSourceManager();
   setToStartLine(SM, locDeclStart, indent);

   if (hasInit) {
      smbStateMachine.addLocalStorage(declStmt->getBeginLoc());
      SourceLocation locStart = declStmt->getBeginLoc();
      SourceLocation locEnd;
      bool isFirst = true;
      for (auto iter = declStmt->decl_begin(), iterEnd = declStmt->decl_end();
            iter != iterEnd; ++iter) {
         if (auto* varDecl = dyn_cast<VarDecl>(*iter)) {
            if (varDecl->hasInit()) {
               const Expr* init = varDecl->getInit();
               if (init->getStmtClass() != Stmt::CXXConstructExprClass) {
                  locEnd = init->getBeginLoc().getLocWithOffset(-1);
                  std::ostringstream replaceText;
                  if (!isFirst)
                     replaceText << ";\n" << indent;
                  replaceText << "ruleResult->" << varDecl->getName().str() << extension.str()
                     << " = ";
                  tooling::Replacement replace(SM, CharSourceRange::getCharRange(locStart, locEnd),
                        replaceText.str().c_str(), cContext.getLangOpts());
                  consumeError(replacements[std::string(replace.getFilePath())].add(replace));
                  locStart = init->getEndLoc();
                  bool isInvalid = false;
                  const char* buffer = SM.getCharacterData(locStart, &isInvalid);
                  int endInit = 0;
                  if (!isInvalid) {
                     if (isalnum(buffer[endInit])) {
                        do {
                           ++endInit;
                        } while (isalnum(buffer[endInit]));
                     }
                     else
                        endInit = 1;
                  }
                  locStart = locStart.getLocWithOffset(endInit);
                  isFirst = false;
               }
            }
         }
      }
      tooling::Replacement remove(SM, CharSourceRange::getTokenRange(locStart,
            declStmt->getEndLoc().getLocWithOffset(-1)), "", cContext.getLangOpts());
      consumeError(replacements[std::string(remove.getFilePath())].add(remove));
      locStart = locDeclStart;
      for (auto iter = declStmt->decl_begin(), iterEnd = declStmt->decl_end();
            iter != iterEnd; ++iter) {
         SourceLocation currentLoc = (*iter)->getLocation();
         sResult += Lexer::getSourceText(CharSourceRange::getCharRange(locStart, currentLoc),
               SM, cContext.getLangOpts());
         bool isInvalid = false;
         const char *buffer = SM.getCharacterData(currentLoc, &isInvalid);
         size_t index = 0;
         if (!isInvalid) {
            while (isalnum(buffer[index]) || buffer[index] == '_')
               ++index;
            buffer += index;
         }

         sResult += Lexer::getSourceText(CharSourceRange::getCharRange(currentLoc,
               currentLoc.getLocWithOffset(index)), SM, cContext.getLangOpts());
         locStart = currentLoc.getLocWithOffset(index);
         if (auto* varDecl = dyn_cast<VarDecl>(*iter)) {
            if (scopeIdentifier > 0)
               sResult += extension.str();
            if (varDecl->hasInit()) {
               const Expr* init = varDecl->getInit();
               if (init->getStmtClass() != Stmt::CXXConstructExprClass && !isInvalid) { // [TODO]
                  size_t bufferLength = SM.getCharacterData(init->getBeginLoc(), &isInvalid)
                     - buffer;
                  for (size_t i = 0; i < bufferLength; ++i)
                     if (buffer[i] == '=') {
                        bufferLength = i;
                        break;
                     }
                  sResult += Lexer::getSourceText(CharSourceRange::getCharRange(locStart,
                     locStart.getLocWithOffset(bufferLength-1)), SM, cContext.getLangOpts());
               }
               locStart = Lexer::getLocForEndOfToken(init->getEndLoc(), 0,
                     SM, cContext.getLangOpts());
            }
         }
      }
      (sResult += Lexer::getSourceText(CharSourceRange::getCharRange(locStart, locEnd),
            SM, cContext.getLangOpts())) += ";\n";
   }
   else {
      SourceLocation locStart = locDeclStart;
      SourceLocation locEnd = declStmt->getSourceRange().getEnd().getLocWithOffset(1);
      bool isInvalid = false;
      const char *bufferEnd = SM.getCharacterData(locEnd, &isInvalid);
      int shift = 0;
      if (!isInvalid) {
         while (isspace(bufferEnd[shift]) && bufferEnd[shift] != '\n')
            ++shift;
         if (bufferEnd[shift] == '\n')
            ++shift;
      }
      if (shift > 0)
         locEnd = locEnd.getLocWithOffset(shift);
      tooling::Replacement removeText(SM, CharSourceRange::getCharRange(locStart, locEnd),"",
            cContext.getLangOpts());
      consumeError(replacements[std::string(removeText.getFilePath())].add(removeText));

      StringRef textVar = Lexer::getSourceText(CharSourceRange::getCharRange(locStart, locEnd),
            SM, cContext.getLangOpts());
      size_t posStart = sResult.length();
      sResult += textVar;
      if (scopeIdentifier > 0) {
         int newShift = 0;
         for (auto iter = declStmt->decl_begin(), iterEnd = declStmt->decl_end();
                  iter != iterEnd; ++iter) {
            const Decl* decl = *iter;
            if (auto* varDecl = dyn_cast<VarDecl>(decl)) {
               bool Invalid = false;

               int shift = SM.getCharacterData(varDecl->getLocation(), &Invalid)
                  - SM.getCharacterData(locStart, &Invalid);
               if (!isInvalid) {
                  while (isalnum(sResult[posStart+shift+newShift])
                        || sResult[posStart+shift+newShift] == '_')
                     ++shift;
               }
               sResult.insert(posStart+shift+newShift, extension.str());
               newShift += extension.str().length();
            }
         }
      }
   }
}

/* Definition of the translations of each function having XMLParserArguments and
 *    XMLParserStateStack as arguments.
 */

class FunctionBodyTranslator : public MatchFinder::MatchCallback {
  private:
   std::map<std::string, tooling::Replacements> &replacements;
   ASTContext& context;

  public:
   FunctionBodyTranslator(std::map<std::string, tooling::Replacements>& Replacements,
         ASTContext& Context)
      :  replacements(Replacements), context(Context) {}
   virtual void run(const MatchFinder::MatchResult &Result);
};

void
FunctionBodyTranslator::run(const MatchFinder::MatchResult &Result) {
   const auto *functionDecl = Result.Nodes.getNodeAs<FunctionDecl>("functionDecl");
   const auto* methodDecl = llvm::dyn_cast<CXXMethodDecl>(functionDecl);
   const bool isDeclared = functionDecl->getName() == "load"
      && methodDecl && (methodDecl->getParent()->getName() == "DocumentConfiguration"
            || methodDecl->getParent()->getName() == "Document");
   bool isConfiguration = isDeclared &&  methodDecl && (methodDecl->getParent()->getName() == "DocumentConfiguration");

   // change the name of the function
   std::string newName = functionDecl->getNameAsString();
   int oldNameLength = newName.length();
   newName += "Instr";
   tooling::Replacement replace(context.getSourceManager(),
      CharSourceRange::getTokenRange(SourceRange(functionDecl->getLocation(),
            functionDecl->getLocation().getLocWithOffset(oldNameLength-1))), newName,
         context.getLangOpts());
   consumeError(replacements[std::string(replace.getFilePath())].add(replace));
   int paramsNumber = functionDecl->getNumParams();
   std::string argumentsText, stateText;
   SourceRange firstArg, secondArg;
   for (int paramIndex = 0; paramIndex < paramsNumber; ++paramIndex) {
      QualType paramType = functionDecl->getParamDecl(paramIndex)->getType();
      if (const auto* referenceType = dyn_cast<ReferenceType>(&*paramType)) {
         if (const auto* recordType = dyn_cast<RecordType>(&*referenceType->getPointeeType())) {
            bool isArguments = recordType->getDecl()->getName() == "XMLParserArguments";
            bool isState = !isArguments && recordType->getDecl()->getName() == "XMLParserStateStack";
            if (isArguments || isState) {
               const ParmVarDecl* param = functionDecl->getParamDecl(paramIndex);
               std::string& result = isArguments ? argumentsText : stateText;
               result += Lexer::getSourceText(CharSourceRange::getTokenRange(param->getSourceRange()),
                     context.getSourceManager(), context.getLangOpts());
               size_t posParameter = result.rfind(param->getNameAsString());
               if (posParameter == std::string::npos)
                  continue;
               result.insert(posParameter, 1, '_');
               size_t index = posParameter-1;
               while (index > 0 && result[index] != '&')
                  --index;
               if (result[index] == '&')
                  result[index] = '*';
               posParameter = result.find(isArguments ? "XMLParserArguments" : "XMLParserStateStack");
               if (posParameter != std::string::npos)
                  result.insert(posParameter, 1, '_');
               if (!firstArg.isValid())
                  firstArg = param->getSourceRange();
               else
                  secondArg = param->getSourceRange();
               continue;
            }
         }
      }
      SourceLocation locStart = functionDecl->getParamDecl(paramIndex)->getBeginLoc();
      SourceLocation locEnd = functionDecl->getParamDecl(paramIndex)->getEndLoc();
      bool isInvalid = false;
      bool hasFoundComma = false;
      const char* buffer = nullptr;
      const auto& SM = context.getSourceManager();
      std::pair<FileID, unsigned> LocInfo = SM.getDecomposedLoc(locStart.isFileID() ? locStart
            : SM.getSpellingLoc(locStart));
      if (!LocInfo.first.isInvalid()) {
         StringRef Buffer = SM.getBufferData(LocInfo.first, &isInvalid);
         if (!isInvalid) {
            buffer = Buffer.data();
            unsigned offset = LocInfo.second;
            while (offset > 0 && isspace(buffer[offset-1]) && buffer[offset-1] != ','
                  && buffer[offset-1] != '(')
               --offset;
            if (buffer[offset-1] == ',')
               locStart = locStart.getLocWithOffset(offset-1-LocInfo.second);
         }
      }
      if (!hasFoundComma) {
         const char *buffer = context.getSourceManager().getCharacterData(
               locEnd, &isInvalid);
         if (!isInvalid) {
            int index = 0;
            while (buffer[index] && buffer[index] != ',' && buffer[index] != ')') ++index;
            if (buffer[index] == ',')
               locEnd = locEnd.getLocWithOffset(index);
         }
      }
      tooling::Replacement remove(context.getSourceManager(),
         CharSourceRange::getTokenRange(SourceRange(locStart, locEnd)), "",
            context.getLangOpts());
      consumeError(replacements[std::string(remove.getFilePath())].add(remove));
   }
   if (paramsNumber > 0) {
      SourceLocation locObject = functionDecl->getParamDecl(0)->getBeginLoc().getLocWithOffset(-1);
      bool isInvalid = false;
      const char *buffer = context.getSourceManager().getCharacterData(locObject, &isInvalid);
      std::string text;
      text.append(1, buffer[0]);
      if (!isDeclared || isConfiguration)
         text += "void* _object, ";
      argumentsText += ", const struct _XMLParserArgumentsFunctions* _arguments_functions";
      stateText += ", const struct _XMLParserStateStackFunctions* _state_functions";
      tooling::Replacement insert(context.getSourceManager(),
         CharSourceRange::getTokenRange(SourceRange(locObject, locObject)), text.c_str(),
            context.getLangOpts());
      consumeError(replacements[std::string(insert.getFilePath())].add(insert));
      if (firstArg.isValid()) {
         tooling::Replacement replaceFirst(context.getSourceManager(),
            CharSourceRange::getTokenRange(firstArg), argumentsText.c_str(),
               context.getLangOpts());
         consumeError(replacements[std::string(replaceFirst.getFilePath())].add(replaceFirst));
      }
      if (secondArg.isValid()) {
         tooling::Replacement replaceSecond(context.getSourceManager(),
            CharSourceRange::getTokenRange(secondArg), stateText.c_str(),
               context.getLangOpts());
         consumeError(replacements[std::string(replaceSecond.getFilePath())].add(replaceSecond));
      }
   }

   // move the return type in the struct RuleResult.
   auto preLocation = functionDecl->getBeginLoc(),
        postLocation = functionDecl->getEndLoc().getLocWithOffset(1);
   std::string varDeclarations;
   if (!functionDecl->getReturnType()->isVoidType()) {
      varDeclarations = "   ";
      StringRef returnTypeText = Lexer::getSourceText(CharSourceRange::getTokenRange(
            functionDecl->getReturnTypeSourceRange()), context.getSourceManager(),
            context.getLangOpts());
      ((varDeclarations += "XMLParserStateStack::TStackPointer<") += returnTypeText) += "> _result;\n";
   }
   if (!isDeclared) {
      varDeclarations += "   struct _DocumentConfiguration* _configuration;\n";
      varDeclarations += "   const struct _DocumentConfigurationFunctions* _configuration_functions;\n";
   }
   tooling::Replacement replaceReturnType(context.getSourceManager(),
         CharSourceRange::getTokenRange(functionDecl->getReturnTypeSourceRange()),
         "enum _ReadResult", context.getLangOpts());
   consumeError(replacements[std::string(replaceReturnType.getFilePath())].add(replaceReturnType));

   // move the local variables in the struct RuleResult
   if (!dyn_cast<CompoundStmt>(functionDecl->getBody()))
      return;
   CompoundRegister compoundRegister(*static_cast<const CompoundStmt*>(functionDecl->getBody()),
         context);
   StateMachineBuilder stateMachineBuilder(replacements, context, functionDecl, isDeclared,
         compoundRegister);
   SkipNodeBuilder skipNodeBuilder(replacements, context, stateMachineBuilder);
   ShiftCallBuilder shiftCallBuilder(replacements, varDeclarations, context, stateMachineBuilder,
         functionDecl);
   ReturnNodeBuilder returnNodeBuilder(replacements, context, stateMachineBuilder, functionDecl);
   ConstStringBuilder constStringBuilder(replacements, varDeclarations, context,
         stateMachineBuilder, functionDecl);
   VariableMover varMover(replacements, varDeclarations, context, stateMachineBuilder,
         compoundRegister);
   ArgumentValueAccessor argumentValueAccessor(replacements, context, stateMachineBuilder, constStringBuilder);
   ConfigurationSetKeyValueReplacer configurationSetKeyValueReplacer(replacements, context);
   DeclarationRefMover declarationRefMover(replacements, context, stateMachineBuilder,
         functionDecl, isDeclared);
   ContinuePropagator continuePropagator(context, stateMachineBuilder);
   ExtStringPropagator extStringPropagator(replacements, varDeclarations, context,
         stateMachineBuilder, functionDecl, isDeclared);
   MatchFinder varDeclarationFinder;
   varDeclarationFinder.addMatcher(traverse(CLANGASTTYPE::TK_IgnoreUnlessSpelledInSource,
         stmt(forEachDescendant(stmt(anyOf(
               ifStmt().bind("ifStmt"),
               whileStmt().bind("whileStmt"),
               doStmt().bind("doStmt"),
               forStmt().bind("forStmt"),
               cxxForRangeStmt().bind("forRangeStmt"),
               compoundStmt().bind("compoundStmt"))).bind("stmt")))),
         &compoundRegister);
   varDeclarationFinder.addMatcher(traverse(CLANGASTTYPE::TK_IgnoreUnlessSpelledInSource,
         stmt(forEachDescendant(cxxMemberCallExpr(
            on(declRefExpr(to(parmVarDecl(hasType(
               references(recordDecl(matchesName("XMLParserArguments")))))))),
            callee(cxxMethodDecl(hasName("setToNextToken")))).bind("setNextToken")))),
         &stateMachineBuilder);
   varDeclarationFinder.addMatcher(traverse(CLANGASTTYPE::TK_IgnoreUnlessSpelledInSource,
         stmt(forEachDescendant(cxxMemberCallExpr(
            on(declRefExpr(to(parmVarDecl(hasType(
               references(recordDecl(matchesName("XMLParserStateStack")))))))),
            callee(cxxMethodDecl(anyOf(hasName("skipNode"),
                     hasName("skipNodeContent"))))).bind("skipNode")))),
         &skipNodeBuilder);
   varDeclarationFinder.addMatcher(traverse(CLANGASTTYPE::TK_IgnoreUnlessSpelledInSource,
         stmt(forEachDescendant(callExpr(
            hasAnyArgument(declRefExpr(to(parmVarDecl(hasType(
               references(recordDecl(matchesName("XMLParserStateStack")))))))),
            hasAnyArgument(declRefExpr(to(parmVarDecl(hasType(
               references(recordDecl(matchesName("XMLParserArguments"))))))))).bind("shiftCall")))),
         &shiftCallBuilder);
   varDeclarationFinder.addMatcher(traverse(CLANGASTTYPE::TK_IgnoreUnlessSpelledInSource,
         stmt(forEachDescendant(returnStmt().bind("returnNode")))),
         &returnNodeBuilder);
   {  auto hasValueOrAttributeMatcher = cxxMemberCallExpr(
         on(declRefExpr(to(parmVarDecl(hasType(
            references(recordDecl(matchesName("XMLParserArguments")))))))),
         callee(cxxMethodDecl(anyOf(hasName("value"), hasName("extendedValue"),
                  hasName("attribute")))));
      varDeclarationFinder.addMatcher(traverse(CLANGASTTYPE::TK_IgnoreUnlessSpelledInSource,
            stmt(forEachDescendant(cxxOperatorCallExpr(isComparisonOperator(),
                  hasAnyArgument(anyOf(hasDescendant(hasValueOrAttributeMatcher),
                        hasValueOrAttributeMatcher))).bind("valueOrAttributeComparison")))),
            &constStringBuilder);
   }
   varDeclarationFinder.addMatcher(traverse(CLANGASTTYPE::TK_IgnoreUnlessSpelledInSource,
         stmt(forEachDescendant(declStmt().bind("declStmt")))), &varMover);
   varDeclarationFinder.addMatcher(traverse(CLANGASTTYPE::TK_IgnoreUnlessSpelledInSource,
         stmt(forEachDescendant(expr(anyOf(declRefExpr(to(eachOf(varDecl(hasLocalStorage()),
                  enumConstantDecl()))).bind("declRef"), cxxThisExpr().bind("this")))))),
         &declarationRefMover);
   varDeclarationFinder.addMatcher(traverse(CLANGASTTYPE::TK_IgnoreUnlessSpelledInSource,
         stmt(forEachDescendant(continueStmt().bind("continue")))),
         &continuePropagator);
   varDeclarationFinder.addMatcher(traverse(CLANGASTTYPE::TK_IgnoreUnlessSpelledInSource,
         stmt(forEachDescendant(cxxMemberCallExpr(
            on(declRefExpr(to(parmVarDecl(hasType(
               references(recordDecl(matchesName("XMLParserArguments")))))))),
            callee(cxxMethodDecl(anyOf(hasName("value"), hasName("extendedValue"))))).bind("value")))),
         &argumentValueAccessor);
   varDeclarationFinder.addMatcher(traverse(CLANGASTTYPE::TK_IgnoreUnlessSpelledInSource,
         stmt(forEachDescendant(stmt(callExpr(
            callee(ast_matchers::functionDecl(hasParent(recordDecl(hasName("ExtString")))))).bind("ext_string"))))),
         &extStringPropagator);
   // varDeclarationFinder.addMatcher(traverse(CLANGASTTYPE::TK_IgnoreUnlessSpelledInSource,
   //       stmt(forEachDescendant(stmt(cxxMemberCallExpr(
   //          callee(memberExpr(member(cxxMethodDecl(hasParent(recordDecl(matchesName("ExtString")))))).bind("ext_string"))))))),
   //       &extStringPropagator);
   if (const auto* method = dyn_cast<CXXMethodDecl>(functionDecl)) {
      if (method->getParent()->getName() == "DocumentConfiguration"
            && method->getName() == "load")
      varDeclarationFinder.addMatcher(traverse(CLANGASTTYPE::TK_IgnoreUnlessSpelledInSource,
            stmt(forEachDescendant(cxxMemberCallExpr(
               callee(cxxMethodDecl(hasName("setKeyValue")))).bind("setKeyValue")))),
            &configurationSetKeyValueReplacer);
   }
   varDeclarationFinder.match(*functionDecl->getBody(), context);
   stateMachineBuilder.addTextOfLabels();
   stateMachineBuilder.insertionsAtStatements().printAll(replacements, context);
   constStringBuilder.closeReplacements();
   stateMachineBuilder.applyInnerReplacements(replacements);
   std::string nameRulesResult;
   if (!functionDecl->getReturnType()->isVoidType() || functionDecl->getNumParams() > 2
         || varDeclarations.length() > 0) {
      // create the struct RuleResult = "stack frame"
      std::string structDef = "struct ";
      nameRulesResult = getNameRulesResult(functionDecl, isDeclared);
      structDef += nameRulesResult;
      structDef += " {\n";
      if (isDeclared && !isConfiguration)
         structDef += "   Document document;\n";

      // if (!functionDecl->getReturnType()->isVoidType()) {
      //    bool isReferenceResult = false;
      //    std::string typeResult = shiftCallBuilder.getResultType(functionDecl->getReturnType(),
      //          functionDecl->getReturnTypeSourceRange(), isReferenceResult, "_result_call");
      //    ((structDef += "   ") += typeResult) += ";\n";
      // }
      int paramNumber = 0;
      for (unsigned paramIndex = 0; paramIndex < functionDecl->getNumParams(); ++paramIndex) {
         const ParmVarDecl* param = functionDecl->getParamDecl(paramIndex);
         if (const auto* referenceType = dyn_cast<ReferenceType>(&*param->getType())) {
            if (const auto* recordType = dyn_cast<RecordType>(&*referenceType->getPointeeType())) {
               if (recordType->getDecl()->getName() == "XMLParserArguments")
                  continue;
               else if (recordType->getDecl()->getName() == "XMLParserStateStack")
                  continue;
            }
         }

         bool isReferenceParam = false;
         std::ostringstream textIndex;
         textIndex << "_param" << paramNumber++;
         std::string typeParam = shiftCallBuilder.getParameterType(param->getType(),
               param->getSourceRange(), param->getName(), isReferenceParam, textIndex.str());
         ((structDef += "   ") += typeParam) += ";\n";
      }
      varDeclarations.insert(0, structDef);
      varDeclarations.append("};\n\n");

      // add additional declarations after struct RuleResult
      if (isDeclared) {
         ((varDeclarations += "void\n") += methodDecl->getParent()->getName())
            += "::init_rule_object(void* object)\n"
               "   {  new (object) RuleResult(); };\n";
         ((varDeclarations += "void\n") += methodDecl->getParent()->getName())
            += "::move_rule_object(void* dst, void* src)\n"
               "   {  *reinterpret_cast<RuleResult*>(dst) = std::move(*reinterpret_cast<RuleResult*>(src)); }\n";
         ((varDeclarations += "void\n") += methodDecl->getParent()->getName())
            += "::copy_rule_object(void* dst, const void* src)\n"
               "   {  *reinterpret_cast<RuleResult*>(dst) = *reinterpret_cast<const RuleResult*>(src); }\n";
         ((varDeclarations += "void\n") += methodDecl->getParent()->getName())
            += "::finalize_rule_object(void* object)\n"
               "   {  reinterpret_cast<RuleResult*>(object)->~RuleResult(); }\n";
         ((varDeclarations += "\nsize_t\n") += methodDecl->getParent()->getName())
            += "::getRuleSize() { return sizeof(RuleResult); }\n\n";
      }
      else {
         (((((varDeclarations += "void\n")
            += "init_") += nameRulesResult) += "(void* object)\n"
               "   {  new (object) ") += nameRulesResult) += "(); };\n";
         (((((((varDeclarations += "void\n")
            += "move_") += nameRulesResult) += "(void* dst, void* src)\n"
               "   {  *reinterpret_cast<") += nameRulesResult)
               += "*>(dst) = std::move(*reinterpret_cast<") += nameRulesResult) += "*>(src)); }\n";
         (((((((varDeclarations += "void\n")
            += "copy_") += nameRulesResult) += "(void* dst, const void* src)\n"
               "   {  *reinterpret_cast<") += nameRulesResult)
               += "*>(dst) = *reinterpret_cast<const ") += nameRulesResult) += "*>(src); }\n";
         (((((((varDeclarations += "void\n")
            += "finalize_") += nameRulesResult) += "(void* object)\n"
               "   {  reinterpret_cast<") += nameRulesResult) += "*>(object)->~")
               += nameRulesResult) += "(); }\n\n";
      }
      tooling::Replacement insertText(context.getSourceManager(),
            CharSourceRange::getCharRange(SourceRange(preLocation, preLocation)),
            varDeclarations, context.getLangOpts());
      consumeError(replacements[std::string(insertText.getFilePath())].add(insertText));
   }

   // add function preliminary
   StringRef state, argument;
   retrieveStateAndArgumentNameFromParams(functionDecl, state, argument);
   std::string preliminaries = "\n";
   if (isDeclared) {
      if (isConfiguration)
         preliminaries += "   DocumentConfiguration& configuration = *(DocumentConfiguration*) _object;\n";
      // else
      //    preliminaries += "   Document& document = *(Document*) _object;\n";
   }

   ((((preliminaries += "   XMLParserArguments ") += argument) += "(_") += argument) += ", _arguments_functions);\n";
   ((((preliminaries += "   XMLParserStateStack ") += state) += "(_") += state) += ", _state_functions);\n";
   int states = stateMachineBuilder.getStatesNumber();
   if (states > 0) {
      preliminaries += "   enum Delimiters {\n";
      int len = preliminaries.length();
      preliminaries.append("     ");
      for (int index = 0; index <= states; ++index) {
         std::ostringstream labelText;
         labelText << " DLabel" << index;
         if (index < states)
            labelText << ',';
         if (preliminaries.length()+labelText.str().length() - len >= 80) {
            len = preliminaries.length()+1; // end of line
            preliminaries.append("\n     ");
         }
         preliminaries.append(labelText.str());
      };
      preliminaries.append("\n   };\n");
      preliminaries.append("   enum _ReadResult result = RRContinue;\n");
      if (nameRulesResult.length() > 0)
         preliminaries.append("   ").append(nameRulesResult).append("* ruleResult = nullptr;\n");
      int stringsNumber = constStringBuilder.getStringsNumber();
      for (int index = 0; index < stringsNumber; ++index) {
         std::ostringstream stringText;
         stringText << "   static const struct _ExtString* string" << index << " = nullptr;\n";
         preliminaries.append(stringText.str());
      }

      preliminaries.append("   bool doesContinue;\n\n");
      ((((preliminaries += "   switch (*(*") += state) += "._functions->get_point_instr)(")
         += state) += "._content)) {\n";
      for (int index = 0; index <= states; ++index) {
         std::ostringstream labelText;
         labelText << "      case DLabel" << index << ": goto LLabel" << index << ";\n";
         preliminaries.append(labelText.str());
      };
      preliminaries.append("   };\n\n"
            "LLabel0:");
      if (stateMachineBuilder.hasFirstLocalAccess())
         ((((((preliminaries += "\n   if (!ruleResult) ruleResult = (")
            += getNameRulesResult(functionDecl, isDeclared))
            += "*) (*") += state) += "._functions->get_rule_result_instr)(") += state)
            += "._content);";

      const CompoundStmt* instructions = dyn_cast<CompoundStmt>(functionDecl->getBody());
      if (instructions) {
         SourceLocation loc = instructions->getSourceRange().getBegin().getLocWithOffset(1);
         tooling::Replacement insertText(context.getSourceManager(),
               CharSourceRange::getCharRange(SourceRange(loc, loc)),
               preliminaries, context.getLangOpts());
         consumeError(replacements[std::string(insertText.getFilePath())].add(insertText));
      }
   };
   if (functionDecl->getReturnType()->isVoidType()) {
      std::string textReturn;
      // (*arguments._functions->reduce_state_instr)(arguments._content, state._content, (void**) &ruleResult)
      (((((((textReturn += "(*") += argument) += "._functions->reduce_state_instr)(")
         += argument) += "._content, ") += state) += "._content, (void**) &ruleResult);\n")
         += "   return ";
      bool isMain = false;
      if (const auto* method = dyn_cast<CXXMethodDecl>(functionDecl)) {
         if (method->getParent()->getName() == "DocumentConfiguration"
               && method->getName() == "load")
            isMain = true;
         else if (method->getParent()->getName() == "Document"
               && method->getName() == "load")
            isMain = true;
      }
      textReturn += (isMain ? "RRFinished;\n" : "RRHasToken;\n");
      SourceLocation endLoc = functionDecl->getBody()->getEndLoc();
      tooling::Replacement insertText(context.getSourceManager(),
            CharSourceRange::getCharRange(SourceRange(endLoc, endLoc)),
            textReturn.c_str(), context.getLangOpts());
      consumeError(replacements[std::string(insertText.getFilePath())].add(insertText));
   }

   if (isDeclared) {
      // add additional definitions after the function
      std::string postDeclaration = "\nvoid\n";
      (((postDeclaration += methodDecl->getParent()->getName())
         += "::load(XMLParserArguments& arguments, XMLParserStateStack& state)\n"
            "   {  loadInstr(") += (!isConfiguration ? "" : "this, ")) += "arguments._content, arguments._functions, state._content, state._functions); }\n\n";
      if (!isConfiguration)
         postDeclaration += "Document&\n"
            "Document::queryDocumentFromRule(RuleResult& rule) { return rule.document; }\n\n";
      tooling::Replacement insertText(context.getSourceManager(),
            CharSourceRange::getCharRange(SourceRange(postLocation, postLocation)),
            postDeclaration, context.getLangOpts());
      consumeError(replacements[std::string(insertText.getFilePath())].add(insertText));
   }
}

static void
translateMethodWithArguments(ASTContext &Context,
      std::map<std::string, tooling::Replacements> &Replacements) {
   FunctionBodyTranslator functionBodyTranslator(Replacements, Context);
   MatchFinder finder;
   finder.addMatcher(traverse(CLANGASTTYPE::TK_IgnoreUnlessSpelledInSource,
            functionDecl(isDefinition(),
               hasAnyParameter(hasType(references(recordDecl(matchesName("XMLParserArguments"))))),
               hasAnyParameter(hasType(references(recordDecl(matchesName("XMLParserStateStack")))))).
            bind("functionDecl")),
         &functionBodyTranslator);
   finder.matchAST(Context);
}

namespace {

class StateMachineConsumer : public ASTConsumer {
   std::map<std::string, tooling::Replacements> &Replacements;

  public:
   StateMachineConsumer(std::map<std::string, tooling::Replacements> &Replacements)
      :  Replacements(Replacements) {}
   StateMachineConsumer(const StateMachineConsumer &) = delete;
   StateMachineConsumer &operator=(const StateMachineConsumer &) = delete;

   void HandleTranslationUnit(ASTContext &Context) override {
      translateMethodWithArguments(Context, Replacements);
   }
};

} // end anonymous namespace

std::unique_ptr<ASTConsumer> StateMachineAction::newASTConsumer() {
   return std::make_unique<StateMachineConsumer>(Replacements);
}

#undef CLANGASTTYPE

} // namespace state_machine
} // namespace clang
