#pragma once

#include "clang/Tooling/Refactoring.h"

namespace clang {
class ASTConsumer;

namespace state_machine {

class StateMachineAction {
  private:
   std::map<std::string, tooling::Replacements> &Replacements;

public:
   StateMachineAction(std::map<std::string, tooling::Replacements> &Replacements)
      :  Replacements(Replacements) {}

   StateMachineAction(const StateMachineAction &) = delete;
   StateMachineAction &operator=(const StateMachineAction &) = delete;

   std::unique_ptr<ASTConsumer> newASTConsumer();
};

} // namespace state_machine
} // namespace clang

