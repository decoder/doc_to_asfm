#pragma once

#include "clang/AST/AST.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Sema/Sema.h"

namespace clang {

class CompilerInstance;

class ParseXMLInstrumentVisitor : public RecursiveASTVisitor<ParseXMLInstrumentVisitor> {
  typedef RecursiveASTVisitor<ParseXMLInstrumentVisitor> Parent;
  CompilerInstance &ciInstance;
  FunctionDecl* fdVisitedLoadFunction = nullptr;
  RecordDecl* ruleResultDecl = nullptr;
  const Type* ptReadResultType = nullptr;

  bool isSourceCode(const SourceRange& sourceRange) const;
  Expr* buildFunctionName(const char* functionName, const SourceLocation& loc, NamedDecl*& declFn);
  bool hasReplacementExpr(const Expr* expr, Expr*& newExpr);

  void setReadResultType();
  const Type* getReadResultType() const
    { if (!ptReadResultType)
        const_cast<ParseXMLInstrumentVisitor*>(this)->setReadResultType();
      return ptReadResultType;
    }
public:
  ParseXMLInstrumentVisitor(CompilerInstance& instance)
    : ciInstance(instance) {}

  // bool shouldTraversePostOrder() const { return true; }
  bool VisitCXXMethodDecl(CXXMethodDecl *FD);
  bool TraverseCXXMethodDecl(CXXMethodDecl *FD);
  bool TraverseCXXConversionDecl(CXXConversionDecl *FD) { return true; }
  bool TraverseCXXConstructorDecl(CXXConstructorDecl *FD) { return true; }
  bool TraverseCXXDestructorDecl(CXXDestructorDecl *FD) { return true; }

  bool VisitExpr(Expr* expr);
  bool VisitParenExpr(ParenExpr* expr);
  bool VisitBinaryOperator(BinaryOperator *E);
  bool VisitUnaryOperator(UnaryOperator *E);
  bool VisitUnaryExprOrTypeTraitExpr(UnaryExprOrTypeTraitExpr* E);
  bool TraverseCallExpr(CallExpr *E);
  bool VisitCallExpr(CallExpr *E);
  bool VisitCompoundLiteralExpr(CompoundLiteralExpr* E);
  bool VisitCastExpr(CastExpr *E);
  bool VisitBinaryConditionalOperator(BinaryConditionalOperator* E);
  bool VisitShuffleVectorExpr(ShuffleVectorExpr *E); // SSE ?
  bool VisitConvertVectorExpr(ConvertVectorExpr* E);
  bool VisitVAArgExpr(VAArgExpr* E);
  bool VisitInitListExpr(InitListExpr* E);
  bool VisitDesignatedInitExpr(DesignatedInitExpr* E);
  bool VisitDesignatedInitUpdateExpr(DesignatedInitUpdateExpr* E);
  bool VisitArrayInitLoopExpr(ArrayInitLoopExpr* E);
  bool VisitParenListExpr(ParenListExpr* E);
  bool VisitGenericSelectionExpr(GenericSelectionExpr* E);
  bool VisitExtVectorElementExpr(ExtVectorElementExpr* E);
  bool VisitAsTypeExpr(AsTypeExpr* E);
  bool VisitOffsetOfExpr(OffsetOfExpr* E);
  bool VisitPseudoObjectExpr(PseudoObjectExpr* E);

  bool VisitDeclRefExpr(DeclRefExpr* E);
  bool VisitArraySubscriptExpr(ArraySubscriptExpr* E);
  bool VisitChooseExpr(ChooseExpr* E);
  bool VisitCaseStmt(CaseStmt* stmt);
  bool VisitDefaultStmt(DefaultStmt* stmt);
  bool VisitIfStmt(IfStmt* stmt);
  bool VisitSwitchStmt(SwitchStmt* stmt);
  bool VisitWhileStmt(WhileStmt* stmt);
  bool VisitDoStmt(DoStmt* stmt);
  bool VisitForStmt(ForStmt* stmt);
  bool VisitReturnStmt(ReturnStmt* stmt);
  bool VisitCompoundStmt(CompoundStmt* stmt);
};

} // end of namespace clang

