//===- ParseXMLInstrument.cpp -----------------------------------------------------------===//

#include "ParseXMLInstrumentVisitor.h"
#include "clang/Frontend/FrontendPluginRegistry.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/Frontend/CompilerInstance.h"
#include "llvm/Support/raw_ostream.h"

using namespace clang;

namespace {

class ParseXMLInstrumentConsumer : public ASTConsumer {
  CompilerInstance &Instance;
  ParseXMLInstrumentVisitor cvVisitor;

public:
  ParseXMLInstrumentConsumer(CompilerInstance &Instance)
      : Instance(Instance), cvVisitor(Instance) {}

  bool HandleTopLevelDecl(DeclGroupRef DG) override
    { for (DeclGroupRef::iterator i = DG.begin(), e = DG.end(); i != e; ++i) {
        Decl *D = *i;
        cvVisitor.TraverseDecl(D); // recursively visit each AST node in Decl "D"
      }
      return true;
    }

  void HandleTranslationUnit(ASTContext& context) override
    { // called after HandleTopLevelDecl
    }
  void CompleteTentativeDefinition(VarDecl *D) override
    { //
    }

  ParseXMLInstrumentVisitor& instrumentVisitor() { return cvVisitor; }
};

class ParseXMLInstrumentAfterConsumer : public ASTConsumer {
  CompilerInstance &Instance;
  ParseXMLInstrumentVisitor& cvVisitor;

public:
  ParseXMLInstrumentAfterConsumer(CompilerInstance &Instance, ParseXMLInstrumentConsumer& instrumentConsumer)
      : Instance(Instance), cvVisitor(instrumentConsumer.instrumentVisitor()) {}

  bool HandleTopLevelDecl(DeclGroupRef DG) override
     {
       return true;
     }
};

class ParseXMLInstrumentAction : public PluginASTAction {
protected:
  std::unique_ptr<ASTConsumer> CreateASTConsumer(CompilerInstance &CI,
                                                 llvm::StringRef) override {
    std::unique_ptr<ASTConsumer> result = std::make_unique<ParseXMLInstrumentConsumer>(CI);
    createdConsumer = &(ParseXMLInstrumentConsumer&) *result;
    return result;
  }

  bool ParseArgs(const CompilerInstance &CI,
                 const std::vector<std::string> &args) override {
    if (!args.empty() && args[0] == "help")
      PrintHelp(llvm::errs());

    return true;
  }
  PluginASTAction::ActionType getActionType() override {
     return AddBeforeMainAction;
  }
  void PrintHelp(llvm::raw_ostream& ros) {
    ros << "Help for ParseXMLInstrument plugin goes here\n";
  }
public:
  static ParseXMLInstrumentConsumer* createdConsumer;
};

ParseXMLInstrumentConsumer*
ParseXMLInstrumentAction::createdConsumer = nullptr;

class ParseXMLInstrumentAfterAction : public PluginASTAction {
protected:
  std::unique_ptr<ASTConsumer> CreateASTConsumer(CompilerInstance &CI,
                                                 llvm::StringRef) override {
    assert(ParseXMLInstrumentAction::createdConsumer);
    return std::make_unique<ParseXMLInstrumentAfterConsumer>(CI, *ParseXMLInstrumentAction::createdConsumer);
  }

  bool ParseArgs(const CompilerInstance &CI,
                 const std::vector<std::string> &args) override { return true; }
  PluginASTAction::ActionType getActionType() override {
     return AddAfterMainAction;
  }
};

}

static FrontendPluginRegistry::Add<ParseXMLInstrumentAction>
X("parse_xml_instrument", "instrument for xml parsing");

static FrontendPluginRegistry::Add<ParseXMLInstrumentAfterAction>
AfterX("parse_xml_instrument", "after parsing instrumentation");

