#include "ParseXMLInstrumentVisitor.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Sema/Scope.h"
#include "clang/Sema/Lookup.h"
#include "clang/Lex/Lexer.h"
#include "clang/Basic/Version.h"
#include <iostream>

namespace clang {

void
ParseXMLInstrumentVisitor::setReadResultType() {
   auto& context = ciInstance.getASTContext();
   auto& sema = ciInstance.getSema();
   IdentifierInfo* info = &context.Idents.get("_ReadResult");
   const auto& sourceManager = context.getSourceManager();
   LookupResult R(sema, info,
         sourceManager.getLocForStartOfFile(sourceManager.getMainFileID()),
         Sema::LookupTagName);
   const DeclContext* outerScope = context.getTranslationUnitDecl();
   if (sema.LookupQualifiedName(R, const_cast<DeclContext*>(outerScope))) {
      if (R.getResultKind() == LookupResult::Found) {
         assert(llvm::dyn_cast<EnumDecl>(R.getFoundDecl()));
         ptReadResultType = ((const EnumDecl&) *R.getFoundDecl()).getTypeForDecl();
      };
   };
   assert(ptReadResultType);
}

bool
ParseXMLInstrumentVisitor::isSourceCode(const SourceRange& sourceRange) const {
   auto& context = ciInstance.getASTContext();
   return sourceRange.getBegin().isValid()
         && !context.getSourceManager().isInSystemHeader(sourceRange.getBegin());
}

Expr*
ParseXMLInstrumentVisitor::buildFunctionName(const char* functionName, const SourceLocation& loc,
      NamedDecl*& declFn) {
   auto& context = ciInstance.getASTContext();
   auto& sema = ciInstance.getSema();
   IdentifierInfo* info = &context.Idents.get(functionName);
   const auto& sourceManager = context.getSourceManager();
   LookupResult R(sema, info,
         sourceManager.getLocForStartOfFile(sourceManager.getMainFileID()),
         Sema::LookupOrdinaryName);
   const DeclContext* outerScope = context.getTranslationUnitDecl();
   if (sema.LookupQualifiedName(R, const_cast<DeclContext*>(outerScope))) {
      if (R.getResultKind() == LookupResult::Found) {
         assert(llvm::dyn_cast<FunctionDecl>(R.getFoundDecl()));
         FunctionDecl* result = static_cast<FunctionDecl*>(R.getFoundDecl());
         declFn = result;
         return sema.BuildDeclRefExpr(result, result->getType(), VK_RValue, loc);
      };
   };
   return nullptr;
}

bool
ParseXMLInstrumentVisitor::hasReplacementExpr(const Expr* expr, Expr*& newExpr) {
   return false;
}

bool
ParseXMLInstrumentVisitor::TraverseCXXMethodDecl(CXXMethodDecl *FD) {
   if (!isSourceCode(FD->getSourceRange()))
      return true;

   bool isLoadArgument = false, isLoadState = false;
   {
      for (ParmVarDecl* param : FD->parameters()) {
         if (const auto* type = llvm::dyn_cast<ReferenceType>(param->getType().getTypePtr())) {
            if (const auto* recordType = llvm::dyn_cast<RecordType>(type->getPointeeType().getTypePtr())) {
               if (!isLoadArgument)
                  isLoadArgument = recordType->getDecl()->getName() == "XMLParserArguments";
               if (!isLoadState)
                  isLoadState = recordType->getDecl()->getName() == "XMLParserStateStack";
            }
         }
      };
   };
   if (isLoadArgument != isLoadState)
      std::cout << "inconsistent declaration of " << FD->getNameInfo().getName().getAsString() << std::endl;

   bool result = true;
   if (isLoadArgument && isLoadState) {
      result = Parent::TraverseCXXMethodDecl(FD);
      ruleResultDecl = nullptr;
   }
   return result;
}

bool
ParseXMLInstrumentVisitor::VisitCXXMethodDecl(CXXMethodDecl *FD) {
   if (!FD->hasBody())
      return true;
   auto& context = ciInstance.getASTContext();
   if (IdentifierInfo* info = FD->getDeclName().getAsIdentifierInfo()) {
      std::string name = FD->getDeclName().getAsString();
      if (name.size() > 5 && name.substr(name.size()-5, 5) == "Instr")
         return true;
      IdentifierInfo* instrInfo = &context.Idents.get(name + "Instr");
      if (!instrInfo) {
         std::cout << "impossible to find " << name << "Instr" << std::endl;
         return true;
      }
      FD->setDeclName(DeclarationName(instrInfo));
   }
   IdentifierInfo* info = FD->getNameInfo().getName().getAsIdentifierInfo();
   std::cout << "instrumentation of " << info->getName().str() << std::endl;
   if (FD->getReturnType()->getTypeClass() == Type::Builtin
         && static_cast<const clang::BuiltinType&>(*FD->getReturnType()).getKind()
               == BuiltinType::Void) {
      assert(FD->getType()->getTypeClass() == Type::FunctionProto);
      FD->setType(ciInstance.getASTContext().getFunctionType(
            QualType(getReadResultType(), FD->getReturnType().getLocalFastQualifiers()),
            ((const FunctionProtoType&) *FD->getType()).getParamTypes(),
            ((const FunctionProtoType&) *FD->getType()).getExtProtoInfo()));
   }
   else {
      assert(false); // unimplemented
   }
   DeclContext* outerScope = const_cast<DeclContext*>((const DeclContext*)
         context.getTranslationUnitDecl());
   info = &context.Idents.get("RuleResult");
   ruleResultDecl = RecordDecl::Create(context, TTK_Struct, outerScope,
         FD->getSourceRange().getBegin(), FD->getSourceRange().getBegin(), info);
   ruleResultDecl->setDeclContext(FD->getDeclContext());
   FD->getDeclContext()->addDecl(ruleResultDecl);
   if (CompoundStmt* stmts = dyn_cast<CompoundStmt>(FD->getBody())) {
      std::vector<Stmt*> vecBody;
      vecBody.reserve(stmts->size()+4);
      {  // enum Delimiters;
         IdentifierInfo* delimitersInfo = &context.Idents.get("Delimiters");
         EnumDecl* delimiters = EnumDecl::Create(context, FD,
               stmts->getSourceRange().getBegin(),
               stmts->getSourceRange().getBegin(), delimitersInfo, nullptr,
               true /* IsScoped */, false /* IsScopedUsingClassTag */, false /* IsFixed */);
         // DeclGroup* declDelimiters = DeclGroup::Create(context, &delimiters, 1);
         vecBody.push_back(new (context) DeclStmt(DeclGroupRef(delimiters), stmts->getSourceRange().getBegin(),
                  stmts->getSourceRange().getBegin()));
      }

      QualType readResultConstType = QualType(getReadResultType(), 0U).withConst();
      {  // enum _ReadResult result = RRContinue;
         IdentifierInfo* resultInfo = &context.Idents.get("result");
         QualType resultType = QualType(getReadResultType(), 0U);
         VarDecl* resultDecl = VarDecl::Create(context, FD,
               stmts->getSourceRange().getBegin(),
               stmts->getSourceRange().getBegin(), resultInfo, resultType,
               context.getTrivialTypeSourceInfo(resultType, stmts->getSourceRange().getBegin()),
               SC_None);
         EnumConstantDecl* continueReadResult = nullptr;
         {  auto& sema = ciInstance.getSema();
            IdentifierInfo* info = &context.Idents.get("RRContinue");
            const auto& sourceManager = context.getSourceManager();
            LookupResult R(sema, info,
                  sourceManager.getLocForStartOfFile(sourceManager.getMainFileID()),
                  Sema::LookupOrdinaryName);
            const DeclContext* outerScope = context.getTranslationUnitDecl();
            if (sema.LookupQualifiedName(R, const_cast<DeclContext*>(outerScope))) {
               assert(R.getResultKind() == LookupResult::Found);
               assert(llvm::dyn_cast<EnumConstantDecl>(R.getFoundDecl()));
               continueReadResult = &(EnumConstantDecl&) *R.getFoundDecl();
            };
         }
         auto* declRefExpr = new (context) DeclRefExpr(context, continueReadResult,
               false /* RefersToEnclosingVariableOrCapture */,
               readResultConstType, VK_RValue, stmts->getSourceRange().getBegin());
         declRefExpr->setType(QualType(getReadResultType(), 0U));
         resultDecl->setInit(declRefExpr);
         vecBody.push_back(new (context) DeclStmt(DeclGroupRef(resultDecl), stmts->getSourceRange().getBegin(),
                  stmts->getSourceRange().getBegin()));
      }

      {  // RuleResult* ruleResult = nullptr;
         IdentifierInfo* ruleResultInfo = &context.Idents.get("ruleResult");
         QualType pointerType = context.getPointerType(context.getTypeDeclType(ruleResultDecl));
         VarDecl* ruleResult = VarDecl::Create(context, FD,
               stmts->getSourceRange().getBegin(),
               stmts->getSourceRange().getBegin(), ruleResultInfo, pointerType,
               context.getTrivialTypeSourceInfo(pointerType, stmts->getSourceRange().getBegin()),
               SC_None);
         auto* nullPtrExpr = new (context) CXXNullPtrLiteralExpr(context.NullPtrTy, stmts->getSourceRange().getBegin());
         auto* implicitCastExpr = new (context) ImplicitCastExpr(ImplicitCastExpr::OnStack,
               pointerType, CK_NullToPointer, nullPtrExpr, VK_RValue);
         implicitCastExpr->setType(pointerType);
         ruleResult->setInit(implicitCastExpr);
         vecBody.push_back(new (context) DeclStmt(DeclGroupRef(ruleResult), stmts->getSourceRange().getBegin(),
                  stmts->getSourceRange().getBegin()));
      }

      {  // bool doesContinue;
         IdentifierInfo* continueInfo = &context.Idents.get("doesContinue");
         VarDecl* boolDecl = VarDecl::Create(context, FD,
               stmts->getSourceRange().getBegin(),
               stmts->getSourceRange().getBegin(), continueInfo, context.BoolTy,
               context.getTrivialTypeSourceInfo(context.BoolTy, stmts->getSourceRange().getBegin()),
               SC_None);
         vecBody.push_back(new (context) DeclStmt(DeclGroupRef(boolDecl), stmts->getSourceRange().getBegin(),
                  stmts->getSourceRange().getBegin()));
      }

      {  // switch (*(*state._functions->get_point_instr)(state._content)) {};
         const RecordDecl* xmlHiddenStateStack = nullptr;
         const RecordDecl* xmlStateStack = nullptr;
         const RecordDecl* xmlStateStackFunctions = nullptr;
         FieldDecl* functionFieldInStateStack = nullptr;
         FieldDecl* contentFieldInStateStack = nullptr;
         FieldDecl* getPointInstrField = nullptr;
         {  auto& sema = ciInstance.getSema();
            IdentifierInfo* info = &context.Idents.get("_XMLParserStateStack");
            const auto& sourceManager = context.getSourceManager();
            LookupResult R(sema, info,
                  sourceManager.getLocForStartOfFile(sourceManager.getMainFileID()),
                  Sema::LookupTagName);
            const DeclContext* outerScope = context.getTranslationUnitDecl();
            assert(sema.LookupQualifiedName(R, const_cast<DeclContext*>(outerScope)));
            assert(R.getResultKind() == LookupResult::Found);
            assert(llvm::dyn_cast<RecordDecl>(R.getFoundDecl()));
            xmlHiddenStateStack = &(const RecordDecl&) *R.getFoundDecl();

            info = &context.Idents.get("XMLParserStateStack");
            LookupResult R1(sema, info,
                  sourceManager.getLocForStartOfFile(sourceManager.getMainFileID()),
                  Sema::LookupTagName);
            assert(sema.LookupQualifiedName(R1, const_cast<DeclContext*>(outerScope)));
            assert(R1.getResultKind() == LookupResult::Found);
            assert(llvm::dyn_cast<RecordDecl>(R1.getFoundDecl()));
            xmlStateStack = &(const RecordDecl&) *R1.getFoundDecl();
            for (auto* field : xmlStateStack->fields()) {
               if (field->getName() == "_functions") {
                  functionFieldInStateStack = field;
                  if (contentFieldInStateStack)
                     break;
                  else
                     continue;
               }
               if (field->getName() == "_content") {
                  contentFieldInStateStack = field;
                  if (functionFieldInStateStack)
                     break;
                  else
                     continue;
               }
            }
            assert(functionFieldInStateStack && contentFieldInStateStack);

            info = &context.Idents.get("_XMLParserStateStackFunctions");
            LookupResult R2(sema, info,
                  sourceManager.getLocForStartOfFile(sourceManager.getMainFileID()),
                  Sema::LookupTagName);
            assert(sema.LookupQualifiedName(R2, const_cast<DeclContext*>(outerScope)));
            assert(R2.getResultKind() == LookupResult::Found);
            assert(llvm::dyn_cast<RecordDecl>(R2.getFoundDecl()));
            xmlStateStackFunctions = &(const RecordDecl&) *R2.getFoundDecl();
            for (auto* field : xmlStateStackFunctions->fields())
               if (field->getName() == "get_point_instr") {
                  getPointInstrField = field;
                  break;
               }
            assert(getPointInstrField);
         }
         QualType argumentType = context.getPointerType(context.getRecordType(xmlHiddenStateStack));
         auto functionType = context.getFunctionType(context.getPointerType(context.IntTy),
               ArrayRef<QualType>(&argumentType, 1), FunctionProtoType::ExtProtoInfo());
         auto* stateDotFunctions = MemberExpr::CreateImplicit(context, new (context)
               DeclRefExpr(context, FD->getParamDecl(1), false /* RefersToEnclosingVariableOrCapture */,
                  context.getRecordType(xmlStateStack),
                  VK_LValue, stmts->getSourceRange().getBegin()),
               false /* IsArrow */, functionFieldInStateStack,
               context.getPointerType(context.getRecordType(xmlStateStackFunctions)),
               VK_LValue, OK_Ordinary);
         auto* getPointIntrFunction = MemberExpr::CreateImplicit(context, new (context)
               ImplicitCastExpr(ImplicitCastExpr::OnStack,
                  context.getPointerType(context.getRecordType(xmlStateStackFunctions)),
                  CK_LValueToRValue, stateDotFunctions, VK_RValue),
               true /* IsArrow */, getPointInstrField,
               context.getPointerType(functionType), VK_LValue, OK_Ordinary);
         auto* callExpr = new (context) ImplicitCastExpr(ImplicitCastExpr::OnStack,
               context.getPointerType(functionType), CK_FunctionToPointerDecay, new (context)
               ParenExpr(stmts->getSourceRange().getBegin(), stmts->getSourceRange().getBegin(),
                  new (context) UnaryOperator(new (context) ImplicitCastExpr(ImplicitCastExpr::OnStack,
                        context.getPointerType(functionType), CK_LValueToRValue, getPointIntrFunction, VK_RValue),
                     UO_Deref, functionType,
                     VK_LValue, OK_Ordinary, stmts->getSourceRange().getBegin(),
                     false /* CanOverflow */)),
               VK_RValue);
         auto* stateDotContent = MemberExpr::CreateImplicit(context, new (context)
               DeclRefExpr(context, FD->getParamDecl(1), false /* RefersToEnclosingVariableOrCapture */,
                  context.getRecordType(xmlStateStack),
                  VK_LValue, stmts->getSourceRange().getBegin()),
               false /* IsArrow */, contentFieldInStateStack,
               context.getPointerType(context.getRecordType(xmlHiddenStateStack)),
               VK_LValue, OK_Ordinary);
         Expr* argState = new (context) ImplicitCastExpr(ImplicitCastExpr::OnStack,
                  context.getPointerType(context.getRecordType(xmlHiddenStateStack)),
                  CK_LValueToRValue, stateDotContent, VK_RValue);
         auto* switchExpr = new (context) ImplicitCastExpr(ImplicitCastExpr::OnStack,
               context.IntTy, CK_LValueToRValue, new (context)
               UnaryOperator(CallExpr::Create(context,
                     callExpr, ArrayRef<Expr*>(&argState, 1),
                     context.getPointerType(context.IntTy), VK_RValue,
                     stmts->getSourceRange().getBegin(), 1),
                  UO_Deref, context.IntTy, VK_LValue, OK_Ordinary,
                  stmts->getSourceRange().getBegin(), false /* CanOverflow */),
               VK_RValue);
         auto* switchStmt = SwitchStmt::Create(context, nullptr, nullptr, switchExpr);
         switchStmt->setBody(CompoundStmt::Create(context, ArrayRef<Stmt *>(),
                  stmts->getSourceRange().getBegin(), stmts->getSourceRange().getBegin()));
         vecBody.push_back(switchStmt);
      }

      for (Stmt* stmt : stmts->body())
         vecBody.push_back(stmt);

      {  EnumConstantDecl* finishedReadResult = nullptr;
         {
            auto& sema = ciInstance.getSema();
            IdentifierInfo* info = &context.Idents.get("RRFinished");
            const auto& sourceManager = context.getSourceManager();
            LookupResult R(sema, info,
                  sourceManager.getLocForStartOfFile(sourceManager.getMainFileID()),
                  Sema::LookupOrdinaryName);
            const DeclContext* outerScope = context.getTranslationUnitDecl();
            if (sema.LookupQualifiedName(R, const_cast<DeclContext*>(outerScope))) {
               assert(R.getResultKind() == LookupResult::Found);
               assert(llvm::dyn_cast<EnumConstantDecl>(R.getFoundDecl()));
               finishedReadResult = &(EnumConstantDecl&) *R.getFoundDecl();
            };
         }
         auto* declRefExpr = new (context) DeclRefExpr(context, finishedReadResult,
               false /* RefersToEnclosingVariableOrCapture */,
               readResultConstType, VK_RValue, stmts->getSourceRange().getBegin());
         declRefExpr->setType(QualType(getReadResultType(), 0U));
         vecBody.push_back(ReturnStmt::Create(context, stmts->getSourceRange().getBegin(),
               declRefExpr, nullptr));
      }
      auto* newBody = CompoundStmt::Create(context, ArrayRef<Stmt*>(vecBody),
            FD->getBody()->getBeginLoc(), FD->getBody()->getEndLoc());
      FD->setBody(newBody);
   }
   return true;
}

bool
ParseXMLInstrumentVisitor::VisitExpr(Expr* expr) {
   return true;
}

bool
ParseXMLInstrumentVisitor::VisitParenExpr(ParenExpr* expr) {
   if (isSourceCode(expr->getSourceRange())) {
      Expr* newExpr = nullptr;
      if (hasReplacementExpr(expr->getSubExpr(), newExpr))
         expr->setSubExpr(newExpr);
   };
   return true;
}

bool
ParseXMLInstrumentVisitor::VisitBinaryOperator(BinaryOperator *E) {
   if (isSourceCode(E->getSourceRange())) {
      Expr* newLHS = nullptr, *newRHS = nullptr;
      if (hasReplacementExpr(E->getLHS(), newLHS))
         E->setLHS(newLHS);
      if (hasReplacementExpr(E->getRHS(), newRHS))
         E->setRHS(newRHS);
   };
   return true;
}

bool
ParseXMLInstrumentVisitor::VisitUnaryOperator(UnaryOperator *E) {
   if (isSourceCode(E->getSourceRange())) {
      Expr* newExpr = nullptr;
      if (hasReplacementExpr(E->getSubExpr(), newExpr))
         E->setSubExpr(newExpr);
   };
   return true;
}

bool
ParseXMLInstrumentVisitor::VisitUnaryExprOrTypeTraitExpr(UnaryExprOrTypeTraitExpr* E) {
   if (isSourceCode(E->getSourceRange())) {
      if (!E->isArgumentType()) {
         Expr* newExpr = nullptr;
         if (hasReplacementExpr(E->getArgumentExpr(), newExpr))
            E->setArgument(newExpr);
      };
   };
   return true;
}

bool
ParseXMLInstrumentVisitor::VisitCallExpr(CallExpr *E) {
   if (isSourceCode(E->getSourceRange())) {
      if (const FunctionDecl* fnDecl = E->getDirectCallee()) {
         if (IdentifierInfo* info = fnDecl->getNameInfo().getName().getAsIdentifierInfo()) {
            if (info->getName() != "Float_instrument_print_oldfloat" && info->getName() != "Float_instrument_print_olddouble") {
               Expr* newExpr = nullptr;
               int count = E->getNumArgs();
               for (int argIndex = 0; argIndex < count; ++argIndex)
                  if (hasReplacementExpr(E->getArg(argIndex), newExpr)) {
                     if (newExpr->isLValue())
                        newExpr = ImplicitCastExpr::Create(ciInstance.getASTContext(),
                              newExpr->getType(), CK_LValueToRValue, newExpr, nullptr, VK_RValue);
                     E->setArg(argIndex, newExpr);
                  };
            }
         };
      };
      // Decl* fnDecl = E->getCalleeDecl();
   };
   return true;
}

bool
ParseXMLInstrumentVisitor::TraverseCallExpr(CallExpr *E) {
   if (isSourceCode(E->getSourceRange())) {
      bool doesTraverse = true;
      if (const FunctionDecl* fnDecl = E->getDirectCallee()) {
         if (IdentifierInfo* info = fnDecl->getNameInfo().getName().getAsIdentifierInfo()) {
            if (strncmp(info->getName().data(), "Float_instrument_", 6) == 0) {
               if (info->getName() == "Float_instrument_create_interval_float" || info->getName() == "Float_instrument_create_interval_double")
                  doesTraverse = false;
               else if (info->getName() == "Float_instrument_create_interval_float_with_error" || info->getName() == "Float_instrument_create_interval_double_with_error")
                  doesTraverse = false;
            };
         };
      };
      if (doesTraverse)
         Parent::TraverseCallExpr(E);
   };
   return true;
}

bool
ParseXMLInstrumentVisitor::VisitCompoundLiteralExpr(CompoundLiteralExpr* E) {
   if (isSourceCode(E->getSourceRange())) {
      Expr* newExpr = nullptr;
      if (hasReplacementExpr(E->getInitializer(), newExpr))
         E->setInitializer(newExpr);
   };
   return true;
}

bool
ParseXMLInstrumentVisitor::VisitCastExpr(CastExpr *E) {
   if (isSourceCode(E->getSourceRange())) {
      Expr* newExpr = nullptr;
      if (hasReplacementExpr(E->getSubExpr(), newExpr))
         E->setSubExpr(newExpr);
   };
   return true;
}

bool
ParseXMLInstrumentVisitor::VisitBinaryConditionalOperator(BinaryConditionalOperator* E) {
   if (isSourceCode(E->getSourceRange())) {
      for (Stmt*& current : E->children()) {
         Expr* newExpr = nullptr;
         if (hasReplacementExpr(static_cast<Expr*>(current), newExpr))
            current = newExpr;
      };
   };
   return true;
}

bool
ParseXMLInstrumentVisitor::VisitShuffleVectorExpr(ShuffleVectorExpr *E) {
   if (isSourceCode(E->getSourceRange())) {
      for (Stmt*& current : E->children()) {
         Expr* newExpr = nullptr;
         if (hasReplacementExpr(static_cast<Expr*>(current), newExpr))
            current = newExpr;
      };
   };
   return true;
}

bool
ParseXMLInstrumentVisitor::VisitConvertVectorExpr(ConvertVectorExpr* E) {
   if (isSourceCode(E->getSourceRange())) {
      for (Stmt*& current : E->children()) {
         Expr* newExpr = nullptr;
         if (hasReplacementExpr(static_cast<Expr*>(current), newExpr))
            current = newExpr;
      };
   };
   return true;
}

bool
ParseXMLInstrumentVisitor::VisitVAArgExpr(VAArgExpr* E) {
   if (isSourceCode(E->getSourceRange())) {
      Expr* newExpr = nullptr;
      if (hasReplacementExpr(E->getSubExpr(), newExpr))
         E->setSubExpr(newExpr);
   };
   return true;
}

bool
ParseXMLInstrumentVisitor::VisitInitListExpr(InitListExpr* E) {
   if (isSourceCode(E->getSourceRange())) {
      int count = E->getNumInits();
      for (int index = 0; index < count; ++index) {
         Expr* newExpr = nullptr;
         if (hasReplacementExpr(E->getInit(index), newExpr))
            E->setInit(index, newExpr);
      };
   };
   return true;
}

bool
ParseXMLInstrumentVisitor::VisitDesignatedInitExpr(DesignatedInitExpr* E) {
   if (isSourceCode(E->getSourceRange())) {
      for (Stmt*& current : E->children()) {
         Expr* newExpr = nullptr;
         if (hasReplacementExpr(static_cast<Expr*>(current), newExpr))
            current = newExpr;
      };
   };
   return true;
}

bool
ParseXMLInstrumentVisitor::VisitDesignatedInitUpdateExpr(DesignatedInitUpdateExpr* E) {
   if (isSourceCode(E->getSourceRange())) {
      for (Stmt*& current : E->children()) {
         Expr* newExpr = nullptr;
         if (hasReplacementExpr(static_cast<Expr*>(current), newExpr))
            current = newExpr;
      };
   };
   return true;
}

bool
ParseXMLInstrumentVisitor::VisitArrayInitLoopExpr(ArrayInitLoopExpr* E) {
   if (isSourceCode(E->getSourceRange())) {
      for (Stmt*& current : E->children()) {
         Expr* newExpr = nullptr;
         if (hasReplacementExpr(static_cast<Expr*>(current), newExpr))
            current = newExpr;
      };
   };
   return true;
}

bool
ParseXMLInstrumentVisitor::VisitParenListExpr(ParenListExpr* E) {
   if (isSourceCode(E->getSourceRange())) {
      for (Stmt*& current : E->children()) {
         Expr* newExpr = nullptr;
         if (hasReplacementExpr(static_cast<Expr*>(current), newExpr))
            current = newExpr;
      };
   };
   return true;
}

bool
ParseXMLInstrumentVisitor::VisitGenericSelectionExpr(GenericSelectionExpr* E) {
   if (isSourceCode(E->getSourceRange())) {
      for (Stmt*& current : E->children()) {
         Expr* newExpr = nullptr;
         if (hasReplacementExpr(static_cast<Expr*>(current), newExpr))
            current = newExpr;
      };
   };
   return true;
}

bool
ParseXMLInstrumentVisitor::VisitExtVectorElementExpr(ExtVectorElementExpr* E) {
   if (isSourceCode(E->getSourceRange())) {
      for (Stmt*& current : E->children()) {
         Expr* newExpr = nullptr;
         if (hasReplacementExpr(static_cast<Expr*>(current), newExpr))
            current = newExpr;
      };
   };
   return true;
}

bool
ParseXMLInstrumentVisitor::VisitAsTypeExpr(AsTypeExpr* E) {
   if (isSourceCode(E->getSourceRange())) {
      for (Stmt*& current : E->children()) {
         Expr* newExpr = nullptr;
         if (hasReplacementExpr(static_cast<Expr*>(current), newExpr))
            current = newExpr;
      };
   };
   return true;
}

bool
ParseXMLInstrumentVisitor::VisitOffsetOfExpr(OffsetOfExpr* E) {
   if (isSourceCode(E->getSourceRange())) {
      unsigned countSubExpr = E->getNumExpressions();
      for (unsigned index = 0; index < countSubExpr; ++index) {
         Expr* newExpr = nullptr;
         if (hasReplacementExpr(E->getIndexExpr(index), newExpr))
            E->setIndexExpr(index, newExpr);
      };
   };
   return true;
}

bool
ParseXMLInstrumentVisitor::VisitPseudoObjectExpr(PseudoObjectExpr* E) {
   if (isSourceCode(E->getSourceRange())) {
      for (Stmt*& current : E->children()) {
         Expr* newExpr = nullptr;
         auto stmtClass = current->getStmtClass();
         if (stmtClass >= Stmt::firstExprConstant && stmtClass <= Stmt::lastExprConstant
               && hasReplacementExpr(static_cast<Expr*>(current), newExpr))
            current = newExpr;
      };
   };
   return true;
}

bool
ParseXMLInstrumentVisitor::VisitDeclRefExpr(DeclRefExpr* E) {
   // E->getDecl();
   return true;
}

bool
ParseXMLInstrumentVisitor::VisitArraySubscriptExpr(ArraySubscriptExpr* E) {
   if (isSourceCode(E->getSourceRange())) {
      Expr* newLHS = nullptr, *newRHS = nullptr;
      if (hasReplacementExpr(E->getLHS(), newLHS))
         E->setLHS(newLHS);
      if (hasReplacementExpr(E->getRHS(), newRHS))
         E->setRHS(newRHS);
   };
   return true;
}

bool
ParseXMLInstrumentVisitor::VisitChooseExpr(ChooseExpr* E) {
   if (isSourceCode(E->getSourceRange())) {
      Expr* newLHS = nullptr, *newRHS = nullptr, *newCond = nullptr;
      if (hasReplacementExpr(E->getCond(), newCond))
         E->setCond(newCond);
      if (hasReplacementExpr(E->getLHS(), newLHS))
         E->setLHS(newLHS);
      if (hasReplacementExpr(E->getRHS(), newRHS))
         E->setRHS(newRHS);
   };
   return true;
}

bool
ParseXMLInstrumentVisitor::VisitCaseStmt(CaseStmt* stmt) {
   if (isSourceCode(stmt->getSourceRange())) {
      Expr* newLHS = nullptr, *newRHS = nullptr, *newSubStmt = nullptr;
      auto stmtClass = stmt->getSubStmt()->getStmtClass();
      if (stmtClass >= Stmt::firstExprConstant && stmtClass <= Stmt::lastExprConstant
            && hasReplacementExpr(static_cast<Expr*>(stmt->getSubStmt()), newSubStmt))
         stmt->setSubStmt(newSubStmt);
      if (hasReplacementExpr(stmt->getLHS(), newLHS))
         stmt->setLHS(newLHS);
      if (stmt->getRHS() && hasReplacementExpr(stmt->getRHS(), newRHS))
         stmt->setRHS(newRHS);
   };
   return true;
}

bool
ParseXMLInstrumentVisitor::VisitDefaultStmt(DefaultStmt* stmt) {
   if (isSourceCode(stmt->getSourceRange())) {
      Expr* newSubStmt = nullptr;
      auto stmtClass = stmt->getSubStmt()->getStmtClass();
      if (stmtClass >= Stmt::firstExprConstant && stmtClass <= Stmt::lastExprConstant
            && hasReplacementExpr(static_cast<Expr*>(stmt->getSubStmt()), newSubStmt))
         stmt->setSubStmt(newSubStmt);
   };
   return true;
}

bool
ParseXMLInstrumentVisitor::VisitIfStmt(IfStmt* stmt) {
   if (isSourceCode(stmt->getSourceRange())) {
      Expr* newInit = nullptr, *newCond = nullptr, *newThen = nullptr, *newElse = nullptr;
      if (stmt->getInit()) {
         auto stmtClass = stmt->getInit()->getStmtClass();
         if (stmtClass >= Stmt::firstExprConstant && stmtClass <= Stmt::lastExprConstant
               && hasReplacementExpr(static_cast<Expr*>(stmt->getInit()), newInit))
            stmt->setInit(newInit);
      };
      if (hasReplacementExpr(stmt->getCond(), newCond))
         stmt->setCond(newCond);
      auto stmtClass = stmt->getThen()->getStmtClass();
      if (stmtClass >= Stmt::firstExprConstant && stmtClass <= Stmt::lastExprConstant
            && hasReplacementExpr(static_cast<Expr*>(stmt->getThen()), newThen))
         stmt->setThen(newThen);
      if (stmt->getElse()) {
         stmtClass = stmt->getElse()->getStmtClass();
         if (stmtClass >= Stmt::firstExprConstant && stmtClass <= Stmt::lastExprConstant
               && hasReplacementExpr(static_cast<Expr*>(stmt->getElse()), newElse))
            stmt->setElse(newElse);
      };
   };
   return true;
}

bool
ParseXMLInstrumentVisitor::VisitSwitchStmt(SwitchStmt* stmt) {
   if (isSourceCode(stmt->getSourceRange())) {
      Expr* newInit = nullptr, *newCond = nullptr, *newBody = nullptr;
      auto stmtClass = stmt->getInit()->getStmtClass();
      if (stmtClass >= Stmt::firstExprConstant && stmtClass <= Stmt::lastExprConstant
            && hasReplacementExpr(static_cast<Expr*>(stmt->getInit()), newInit))
         stmt->setInit(newInit);
      if (hasReplacementExpr(stmt->getCond(), newCond))
         stmt->setCond(newCond);
      stmtClass = stmt->getBody()->getStmtClass();
      if (stmtClass >= Stmt::firstExprConstant && stmtClass <= Stmt::lastExprConstant
            && hasReplacementExpr(static_cast<Expr*>(stmt->getBody()), newBody))
         stmt->setBody(newBody);
   };
   return true;
}

bool
ParseXMLInstrumentVisitor::VisitWhileStmt(WhileStmt* stmt) {
   if (isSourceCode(stmt->getSourceRange())) {
      Expr* newCond = nullptr, *newBody = nullptr;
      if (hasReplacementExpr(stmt->getCond(), newCond))
         stmt->setCond(newCond);
      auto stmtClass = stmt->getBody()->getStmtClass();
      if (stmtClass >= Stmt::firstExprConstant && stmtClass <= Stmt::lastExprConstant
            && hasReplacementExpr(static_cast<Expr*>(stmt->getBody()), newBody))
         stmt->setBody(newBody);
   };
   return true;
}

bool
ParseXMLInstrumentVisitor::VisitDoStmt(DoStmt* stmt) {
   if (isSourceCode(stmt->getSourceRange())) {
      Expr* newCond = nullptr, *newBody = nullptr;
      if (hasReplacementExpr(stmt->getCond(), newCond))
         stmt->setCond(newCond);
      auto stmtClass = stmt->getBody()->getStmtClass();
      if (stmtClass >= Stmt::firstExprConstant && stmtClass <= Stmt::lastExprConstant
            && hasReplacementExpr(static_cast<Expr*>(stmt->getBody()), newBody))
         stmt->setBody(newBody);
   };
   return true;
}

bool
ParseXMLInstrumentVisitor::VisitForStmt(ForStmt* stmt) {
   if (isSourceCode(stmt->getSourceRange())) {
      Expr* newInit = nullptr, *newCond = nullptr, *newInc = nullptr, *newBody = nullptr;
      auto stmtClass = stmt->getInit()->getStmtClass();
      if (stmtClass >= Stmt::firstExprConstant && stmtClass <= Stmt::lastExprConstant
            && hasReplacementExpr(static_cast<Expr*>(stmt->getInit()), newInit))
         stmt->setInit(newInit);
      if (hasReplacementExpr(stmt->getCond(), newCond))
         stmt->setCond(newCond);
      if (hasReplacementExpr(static_cast<Expr*>(stmt->getInc()), newInc))
         stmt->setInc(newInc);
      stmtClass = stmt->getBody()->getStmtClass();
      if (stmtClass >= Stmt::firstExprConstant && stmtClass <= Stmt::lastExprConstant
            && hasReplacementExpr(static_cast<Expr*>(stmt->getBody()), newBody))
         stmt->setBody(newBody);
   };
   return true;
}

bool
ParseXMLInstrumentVisitor::VisitReturnStmt(ReturnStmt* stmt) {
   if (isSourceCode(stmt->getSourceRange())) {
      Expr* newRetValue = nullptr;
      if (hasReplacementExpr(stmt->getRetValue(), newRetValue))
         stmt->setRetValue(newRetValue);
   };
   return true;
}

bool
ParseXMLInstrumentVisitor::VisitCompoundStmt(CompoundStmt* stmt) {
   if (isSourceCode(stmt->getSourceRange())) {
      for (Stmt*& current : stmt->body()) {
         Expr* newExpr = nullptr;
         auto stmtClass = current->getStmtClass();
         if (stmtClass >= Stmt::firstExprConstant && stmtClass <= Stmt::lastExprConstant
               && hasReplacementExpr(static_cast<Expr*>(current), newExpr))
            current = newExpr;
      };
   };
   return true;
}

} // end of namespace clang

