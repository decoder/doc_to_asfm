/**************************************************************************/
/*                                                                        */
/*  Copyright (C) 2019                                                    */
/*    CEA (Commissariat a l'Energie Atomique et aux Energies              */
/*         Alternatives)                                                  */
/*                                                                        */
/*  you can redistribute it and/or modify it under the terms of the GNU   */
/*  Lesser General Public License as published by the Free Software       */
/*  Foundation, version 2.1.                                              */
/*                                                                        */
/*  It is distributed in the hope that it will be useful,                 */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU Lesser General Public License for more details.                   */
/*                                                                        */
/*  See the GNU Lesser General Public License version 2.1                 */
/*  for more details (enclosed in the file LICENSE).                      */
/*                                                                        */
/**************************************************************************/

/////////////////////////////////
//
// Application : documentation to json converter
// Unit      : main program
// File      : DocxParser.h
// Description :
//   Parser of word documentation
//

#pragma once

#include "TString/SString.h"
#include "XML/XMLParser.h"
#include <zlib.h>

namespace DDocx {

enum Algorithm { AOptimized, AAtomic, AUnstructured };

class FileHeader { // [TODO] correct for big endian
  private:
   enum Size { 
      SFileHeaderSignature = 4, SVersion = 2, SFlags = 2, SCompressionId = 2,
      SLastModificationTime = 2, SLastModificationDate = 2,
      SCrc32 = 4, SCompressedSize = 4, SUncompressedSize = 4,
      SFileNameLength = 2, SExtraFieldLength = 2
   };
   enum Place {
      PFileHeaderSignature = 0,
      PVersion = PFileHeaderSignature + SFileHeaderSignature,
      PFlags = PVersion + SVersion,
      PCompressionId = PFlags + SFlags,
      PLastModificationTime = PCompressionId + SCompressionId,
      PLastModificationDate = PLastModificationTime + SLastModificationTime,
      PCrc32 = PLastModificationDate + SLastModificationDate,
      PCompressedSize = PCrc32 + SCrc32,
      PUncompressedSize = PCompressedSize + SCompressedSize,
      PFileNameLength = PUncompressedSize + SUncompressedSize,
      PExtraFieldLength = PFileNameLength + SFileNameLength
   };
   static const int UHeaderSize = PExtraFieldLength + SExtraFieldLength;
   char content[UHeaderSize];

  public:
   FileHeader() { memset(content, 0, UHeaderSize); }

   void read(STG::IOObject::ISBase& in)
      {  in.readsome(content, UHeaderSize); }
   void readAfterSignature(STG::IOObject::ISBase& in)
      {  in.readsome(content+SFileHeaderSignature, UHeaderSize-SFileHeaderSignature); }
   void clear() { memset(content, 0, UHeaderSize); }
   void setSignature(uint32_t signature)
      {  memcpy(content, &signature, SFileHeaderSignature); }

   bool isValid() const
      {  return getFileHeaderSignature() == 0x04034b50; }

   uint32_t getFileHeaderSignature() const
      {  uint32_t result;
         memcpy(&result, &content[PFileHeaderSignature], SFileHeaderSignature);
         return result;
      }
   uint16_t getVersion() const
      {  uint16_t result;
         memcpy(&result, &content[PVersion], SVersion);
         return result;
      }
   uint16_t getFlags() const
      {  uint16_t result;
         memcpy(&result, &content[PFlags], SFlags);
         return result;
      }
   uint16_t getCompressionId() const
      {  uint16_t result;
         memcpy(&result, &content[PCompressionId], SCompressionId);
         return result;
      }
   uint16_t getLastModificationTime() const
      {  uint16_t result;
         memcpy(&result, &content[PLastModificationTime], SLastModificationTime);
         return result;
      }
   uint16_t getLastModificationDate() const
      {  uint16_t result;
         memcpy(&result, &content[PLastModificationDate], SLastModificationDate);
         return result;
      }
   uint32_t getCrc32() const
      {  uint32_t result;
         memcpy(&result, &content[PCrc32], SCrc32);
         return result;
      }
   uint32_t getCompressedSize() const
      {  uint32_t result;
         memcpy(&result, &content[PCompressedSize], SCompressedSize);
         return result;
      }
   void setCompressedSize(uint32_t newSize)
      {  memcpy(&content[PCompressedSize], &newSize, SCompressedSize); }
   uint32_t getUncompressedSize() const
      {  uint32_t result;
         memcpy(&result, &content[PUncompressedSize], SUncompressedSize);
         return result;
      }
   void setUncompressedSize(uint32_t newSize)
      {  memcpy(&content[PUncompressedSize], &newSize, SUncompressedSize); }
   uint16_t getFileNameLength() const
      {  uint16_t result;
         memcpy(&result, &content[PFileNameLength], SFileNameLength);
         return result;
      }
   uint16_t getExtraFieldLength() const
      {  uint16_t result;
         memcpy(&result, &content[PExtraFieldLength], SExtraFieldLength);
         return result;
      }
   int getHeaderSize() const { return UHeaderSize+getFileNameLength()+getExtraFieldLength(); }
};

class EndOfCentralDirectory { // [TODO] correct for big endian
  private:
   enum Size { 
      SCentralDirectorySignature = 4, SDiskNumber = 2, SInitialDiskNumber = 2, SNumberOfRecords = 2,
      STotalNumberOfRecords = 2, SCentralDirectorySize = 4, SCentralDirectoryOffset = 4,
      SCommentLength = 2
   };
   enum Place {
      PCentralDirectorySignature = 0,
      PDiskNumber = PCentralDirectorySignature + SCentralDirectorySignature,
      PInitialDiskNumber = PDiskNumber + SDiskNumber,
      PNumberOfRecords = PInitialDiskNumber + SInitialDiskNumber,
      PTotalNumberOfRecords = PNumberOfRecords + SNumberOfRecords,
      PCentralDirectorySize = PTotalNumberOfRecords + STotalNumberOfRecords,
      PCentralDirectoryOffset = PCentralDirectorySize + SCentralDirectorySize,
      PCommentLength = PCentralDirectoryOffset + SCentralDirectoryOffset
   };
   static const int UHeaderSize = PCommentLength + SCommentLength;
   char content[UHeaderSize];

  public:
   EndOfCentralDirectory() { memset(content, 0, UHeaderSize); }

   void read(STG::IOObject::ISBase& in)
      {  in.readsome(content, UHeaderSize); }
   void clear() { memset(content, 0, UHeaderSize); }

   bool isValid() const
      {  return getCentralDirectorySignature() == 0x06054b50; }

   uint32_t getCentralDirectorySignature() const
      {  uint32_t result;
         memcpy(&result, &content[PCentralDirectorySignature], SCentralDirectorySignature);
         return result;
      }
   uint16_t getDiskNumber() const
      {  uint16_t result;
         memcpy(&result, &content[PDiskNumber], SDiskNumber);
         return result;
      }
   uint16_t getInitialDiskNumber() const
      {  uint16_t result;
         memcpy(&result, &content[PInitialDiskNumber], SInitialDiskNumber);
         return result;
      }
   uint16_t getNumberOfRecords() const
      {  uint16_t result;
         memcpy(&result, &content[PNumberOfRecords], SNumberOfRecords);
         return result;
      }
   uint16_t getTotalNumberOfRecords() const
      {  uint16_t result;
         memcpy(&result, &content[PTotalNumberOfRecords], STotalNumberOfRecords);
         return result;
      }
   uint32_t getCentralDirectorySize() const
      {  uint32_t result;
         memcpy(&result, &content[PCentralDirectorySize], SCentralDirectorySize);
         return result;
      }
   uint32_t getCentralDirectoryOffset() const
      {  uint32_t result;
         memcpy(&result, &content[PCentralDirectoryOffset], SCentralDirectoryOffset);
         return result;
      }
   uint16_t getCommentLength() const
      {  uint16_t result;
         memcpy(&result, &content[PCommentLength], SCommentLength);
         return result;
      }
};

class CentralDirectoryFileHeader { // [TODO] correct for big endian
  private:
   enum Size { 
      SFileHeaderSignature = 4, SVersionMadeBy = 2, SVersionNeeded = 2, SFlags = 2,
      SCompressionId = 2, SLastModificationTime = 2, SLastModificationDate = 2,
      SCrc32 = 4, SCompressedSize = 4, SUncompressedSize = 4,
      SFileNameLength = 2, SExtraFieldLength = 2, SFileCommentLength = 2,
      SInitialDiskNumber = 2, SInternalFileAttributes = 2, SExternalFileAttributes = 4,
      SHeaderFileOffset = 4
   };
   enum Place {
      PFileHeaderSignature = 0,
      PVersionMadeBy = PFileHeaderSignature + SFileHeaderSignature,
      PVersionNeeded = PVersionMadeBy + SVersionMadeBy,
      PFlags = PVersionNeeded + SVersionNeeded,
      PCompressionId = PFlags + SFlags,
      PLastModificationTime = PCompressionId + SCompressionId,
      PLastModificationDate = PLastModificationTime + SLastModificationTime,
      PCrc32 = PLastModificationDate + SLastModificationDate,
      PCompressedSize = PCrc32 + SCrc32,
      PUncompressedSize = PCompressedSize + SCompressedSize,
      PFileNameLength = PUncompressedSize + SUncompressedSize,
      PExtraFieldLength = PFileNameLength + SFileNameLength,
      PFileCommentLength = PExtraFieldLength + SExtraFieldLength,
      PInitialDiskNumber = PFileCommentLength + SFileCommentLength,
      PInternalFileAttributes = PInitialDiskNumber + SInitialDiskNumber,
      PExternalFileAttributes = PInternalFileAttributes + SInternalFileAttributes,
      PHeaderFileOffset = PExternalFileAttributes + SExternalFileAttributes
   };
   static const int UHeaderSize = PHeaderFileOffset + SHeaderFileOffset;
   char content[UHeaderSize];

  public:
   CentralDirectoryFileHeader() { memset(content, 0, UHeaderSize); }

   void read(STG::IOObject::ISBase& in)
      {  in.readsome(content, UHeaderSize); }
   void clear() { memset(content, 0, UHeaderSize); }
   void setSignature(uint32_t signature)
      {  memcpy(content, &signature, SFileHeaderSignature); }

   bool isValid() const
      {  return getFileHeaderSignature() == 0x02014b50; }

   uint32_t getFileHeaderSignature() const
      {  uint32_t result;
         memcpy(&result, &content[PFileHeaderSignature], SFileHeaderSignature);
         return result;
      }
   uint16_t getVersionMadeBy() const
      {  uint16_t result;
         memcpy(&result, &content[PVersionMadeBy], SVersionMadeBy);
         return result;
      }
   uint16_t getVersionNeeded() const
      {  uint16_t result;
         memcpy(&result, &content[PVersionNeeded], SVersionNeeded);
         return result;
      }
   uint16_t getFlags() const
      {  uint16_t result;
         memcpy(&result, &content[PFlags], SFlags);
         return result;
      }
   uint16_t getCompressionId() const
      {  uint16_t result;
         memcpy(&result, &content[PCompressionId], SCompressionId);
         return result;
      }
   uint16_t getLastModificationTime() const
      {  uint16_t result;
         memcpy(&result, &content[PLastModificationTime], SLastModificationTime);
         return result;
      }
   uint16_t getLastModificationDate() const
      {  uint16_t result;
         memcpy(&result, &content[PLastModificationDate], SLastModificationDate);
         return result;
      }
   uint32_t getCrc32() const
      {  uint32_t result;
         memcpy(&result, &content[PCrc32], SCrc32);
         return result;
      }
   uint32_t getCompressedSize() const
      {  uint32_t result;
         memcpy(&result, &content[PCompressedSize], SCompressedSize);
         return result;
      }
   uint32_t getUncompressedSize() const
      {  uint32_t result;
         memcpy(&result, &content[PUncompressedSize], SUncompressedSize);
         return result;
      }
   uint16_t getFileNameLength() const
      {  uint16_t result;
         memcpy(&result, &content[PFileNameLength], SFileNameLength);
         return result;
      }
   uint16_t getExtraFieldLength() const
      {  uint16_t result;
         memcpy(&result, &content[PExtraFieldLength], SExtraFieldLength);
         return result;
      }
   uint16_t getFileCommentLength() const
      {  uint16_t result;
         memcpy(&result, &content[PFileCommentLength], SFileCommentLength);
         return result;
      }
   uint16_t getInitialDiskNumber() const
      {  uint16_t result;
         memcpy(&result, &content[PInitialDiskNumber], SInitialDiskNumber);
         return result;
      }
   uint16_t getInternalFileAttributes() const
      {  uint16_t result;
         memcpy(&result, &content[PInternalFileAttributes], SInternalFileAttributes);
         return result;
      }
   uint32_t getExternalFileAttributes() const
      {  uint32_t result;
         memcpy(&result, &content[PExternalFileAttributes], SExternalFileAttributes);
         return result;
      }
   uint32_t getHeaderFileOffset() const
      {  uint32_t result;
         memcpy(&result, &content[PHeaderFileOffset], SHeaderFileOffset);
         return result;
      }
};

} // end of namespace DDocx

class DocxParser {
  private:
   STG::DIOObject::IFStream ifFile;
   static const int UDataDescriptorLength = 16;
   STG::XML::CommonParser xpParser;
   bool fIsOpenDocument;
   DDocx::Algorithm aAlgorithm;

   class DecryptionState;
   int inflateBuffer(DecryptionState& state, STG::SubString& decryptedBuffer, bool& isEnd);

  public:
   DocxParser(const char* filename, bool isOpenDocument, DDocx::Algorithm algorithm)
      :  ifFile(filename, std::ios_base::in | std::ios_base::binary),
         fIsOpenDocument(isOpenDocument), aAlgorithm(algorithm)
      {  xpParser.setFlatPrint();
         if (aAlgorithm == DDocx::AOptimized)
            xpParser.setPartialToken();
      }

   bool setToDocument();
   STG::XML::CommonParser& parser() { return xpParser; }
   // template <class TypeObject, class TypeResult>
   // void setParseObject(TypeObject& object, TypeResult* resTemplate)
   //    {  xpParser.state().shift(object, &TypeObject::readXML, resTemplate); }
   void parse();
};
