/**************************************************************************/
/*                                                                        */
/*  Copyright (C) 2014-2019                                               */
/*    CEA (Commissariat a l'Energie Atomique et aux Energies              */
/*         Alternatives)                                                  */
/*                                                                        */
/*  you can redistribute it and/or modify it under the terms of the GNU   */
/*  Lesser General Public License as published by the Free Software       */
/*  Foundation, version 2.1.                                              */
/*                                                                        */
/*  It is distributed in the hope that it will be useful,                 */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU Lesser General Public License for more details.                   */
/*                                                                        */
/*  See the GNU Lesser General Public License version 2.1                 */
/*  for more details (enclosed in the file LICENSE).                      */
/*                                                                        */
/**************************************************************************/

/////////////////////////////////
//
// Library   : XML
// Unit      : Parser
// File      : XMLParser.h
// Description :
//   Definition of a parser of XML document.
//

#ifndef STG_XML_XMLParserH
#define STG_XML_XMLParserH

#include "XML/XMLLexer.h"
#include "Collection/Collection.hpp"
#include "Pointer/DescentParse.h"

namespace STG { namespace XML {

/***************************************/
/* Definition of the class BasicWriter */
/***************************************/

namespace DXML {

class Base : public Lexer::Base {};

}

class BasicWriter : public IOObject, protected ExtendedParameters, public Lexer::Base {
  public:
   class State : public ExtendedParameters {
     public:
      typedef WriteResult (State::*WriteMethod)(SubString&, bool);
     private:
      STG::SubString ssValue;
      STG::SubString ssAttribute;
      int uChar;
      int uDocumentLevel;
      WriteMethod wmEvent;
      WriteMethod wmNextEvent;
      int uWriteSpaces;

      friend class BasicWriter;
      DefineExtendedParameters(4, ExtendedParameters)
      DefineSubExtendedParameters(Point, 3, INHERITED)
      DefineSubExtendedParameters(Follow, 1, Point)

      static WriteMethod getMethod(int code);
      static int getCode(WriteMethod method);
      void setFromCode(int codeParse);
      int getCode() const;

      WriteResult writeOpenStartElement(SubString& out, bool isPretty);
      WriteResult writeCloseStartElement(SubString& out, bool isPretty);
      WriteResult writeCloseStartEndElement(SubString& out, bool isPretty);
      WriteResult writeAddAttribute(SubString& out, bool isPretty);
      WriteResult writeEndElement(SubString& out, bool isPretty);
      WriteResult writeAddText(SubString& out, bool isPretty);
      WriteResult writeAddComment(SubString& out, bool isPretty);
      WriteResult writeAddEP(SubString& out, bool isPretty);
      WriteResult writeAddEntity(SubString& out, bool isPretty);
      WriteResult writeAddChar(SubString& out, bool isPretty);
      WriteResult writeAddData(SubString& out, bool isPretty);

      void enterElement(WriteMethod newEvent);

      WriteResult writeTextSpaces(SubString& out);
      static WriteResult writeText(SubString& text, SubString& out);

     public:
      State() : ssValue(SString()), ssAttribute(SString()), uChar(0), uDocumentLevel(0),
                wmEvent(nullptr), wmNextEvent(nullptr), uWriteSpaces(0) {}
      State(const State& source) = default;
      State& operator=(const State& source) = default;

      void read(ISBase& in, const FormatParameters& params);
      void write(OSBase& out, const FormatParameters& params) const;

      WriteResult openStartElement(const SubString& elementName);
      WriteResult closeStartElement();
      WriteResult addAttribute(const SubString& name, const SubString& value);
      WriteResult endElement(const SubString& elementName);
      WriteResult closeElement();
      WriteResult addText(const SubString& text);
      WriteResult addComment(const SubString& comment);
      WriteResult addEP(const SubString& name);
      WriteResult addEntity(const SubString& name);
      WriteResult addChar(int charValue);
      WriteResult addData(const SubString& text);
      WriteResult start() { return WRNeedEvent; }
      WriteResult end()
         {  if (uDocumentLevel != 0)
               throw STG::EWriteError();
            return WRFinished;
         }
      bool isOpenCloseStartElement() const;
      bool isValid() const;
      const int& getDocumentLevel() const { return uDocumentLevel; }

      WriteResult write(SubString& out, bool isPretty)
         {  return isValid() ? (this->*wmEvent)(out, isPretty) : WRNeedEvent; }
      WriteResult completeWrite(SubString& out, bool isPretty)
         {  WriteResult result = wmEvent ? (this->*wmEvent)(out, isPretty) : WRFinished;
            return (result == WRNeedEvent) ? WRFinished : result;
         }
   };

  private:
   typedef IOObject inherited;
   State sState;

  protected:
   DefineExtendedParameters(1, ExtendedParameters)

   const State& state() const { return sState; }
   State& state() { return sState; }
   bool isPretty() const { return !hasOwnField(); }
   bool isFlatPrint() const { return hasOwnField(); }
   BasicWriter& setPretty() { clearOwnField(); return *this; }
   BasicWriter& setFlatPrint() { mergeOwnField(1); return *this; }

   WriteResult openStartElement(const SubString& elementName);
   WriteResult closeStartElement();
   WriteResult addAttribute(const SubString& name, const SubString& value);
   WriteResult endElement(const SubString& elementName);
   WriteResult closeElement();
   WriteResult addText(const SubString& text);
   WriteResult addComment(const SubString& comment);
   WriteResult addEP(const SubString& name);
   WriteResult addEntity(const SubString& name);
   WriteResult addChar(int charValue);
   WriteResult addData(const SubString& text);

   virtual void _read(ISBase& in, const FormatParameters& params) override
      {  sState.read(in, params); }
   virtual void _write(OSBase& out, const FormatParameters& params) const override
      {  sState.write(out, params); }

  public:
   BasicWriter() {}
   BasicWriter(const BasicWriter& source) = default;
   BasicWriter& operator=(const BasicWriter& source)
      {  inherited::operator=(source);
         sState = source.sState;
         setOwnField(source.queryOwnField());
         return *this;
      }
   DefineCopy(BasicWriter)
   DDefineAssign(BasicWriter)

   virtual WriteResult translateEvent() { AssumeUncalled return WRFinished; }
   virtual WriteResult getNextEvent() { AssumeUncalled return WRFinished; }
   void write(OSBase& out);
   WriteResult delayedEvent() { return WRNeedEvent; }
   WriteResult writeEvent(SubString& out, bool isReentry=false);
   WriteResult writeChunk(SubString& out, WriteResult result = WRNeedEvent);

   void istart()
      {  sState.start(); }
   void iopenStartElement(const SubString& elementName, STG::SubString& buffer, STG::IOObject::OSBase& out);
   void icloseStartElement(STG::SubString& buffer, STG::IOObject::OSBase& out);
   void iaddAttribute(const SubString& name, const SubString& value, STG::SubString& buffer, STG::IOObject::OSBase& out);
   void iendElement(const SubString& elementName, STG::SubString& buffer, STG::IOObject::OSBase& out);
   void icloseElement(STG::SubString& buffer, STG::IOObject::OSBase& out);
   void iaddText(const SubString& text, STG::SubString& buffer, STG::IOObject::OSBase& out);
   void iaddComment(const SubString& comment, STG::SubString& buffer, STG::IOObject::OSBase& out);
   void iaddEP(const SubString& name, STG::SubString& buffer, STG::IOObject::OSBase& out);
   void iaddEntity(const SubString& name, STG::SubString& buffer, STG::IOObject::OSBase& out);
   void iaddChar(int charValue, STG::SubString& buffer, STG::IOObject::OSBase& out);
   void iaddData(const SubString& text, STG::SubString& buffer, STG::IOObject::OSBase& out);
   void iend(STG::SubString& buffer, STG::IOObject::OSBase& out)
      {  sState.end(); out.writeall(buffer.asPersistent()); buffer.clear(); }
};

inline void
BasicWriter::State::enterElement(WriteMethod newEvent) {
   AssumeCondition(!hasPointField())
   if (wmEvent == &State::writeCloseStartElement)
      wmNextEvent = newEvent;
   else
      wmEvent = newEvent;
}

inline BasicWriter::WriteResult
BasicWriter::State::openStartElement(const SubString& elementName) {
   enterElement(&State::writeOpenStartElement);
   ssValue = elementName;
   return WRNeedWrite;
}

inline BasicWriter::WriteResult
BasicWriter::State::closeStartElement() {
   AssumeCondition(!hasPointField());
   wmEvent = &State::writeCloseStartElement;
   return WRNeedEvent;
}

inline BasicWriter::WriteResult
BasicWriter::State::addAttribute(const SubString& name, const SubString& value) {
   AssumeCondition(!hasPointField());
   wmEvent = &State::writeAddAttribute;
   ssAttribute = name;
   ssValue = value;
   return WRNeedWrite;
}

inline BasicWriter::WriteResult
BasicWriter::State::endElement(const SubString& elementName) {
   AssumeCondition(!hasPointField());
   wmEvent = (wmEvent == &State::writeCloseStartElement)
      ? &State::writeCloseStartEndElement : &State::writeEndElement;
   ssValue = elementName;
   return WRNeedWrite;
}

inline BasicWriter::WriteResult
BasicWriter::State::closeElement() {
   AssumeCondition(!hasPointField());
   WriteResult result = WRNeedWrite;
   if (wmEvent == &State::writeCloseStartElement) {
      --uDocumentLevel;
      wmEvent = &State::writeCloseStartEndElement;
   }
   else if (--uDocumentLevel < 0)
      throw STG::EWriteError();
   else
      result = WRNeedEvent;
   return result;
}

inline BasicWriter::WriteResult
BasicWriter::State::addText(const SubString& text) {
   enterElement(&State::writeAddText);
   ssValue = text;
   return WRNeedWrite;
}

inline BasicWriter::WriteResult
BasicWriter::State::addComment(const SubString& comment) {
   enterElement(&State::writeAddComment);
   ssValue = comment;
   return WRNeedWrite;
}

inline BasicWriter::WriteResult
BasicWriter::State::addEP(const SubString& name) {
   enterElement(&State::writeAddEP);
   ssValue = name;
   return WRNeedWrite;
}

inline BasicWriter::WriteResult
BasicWriter::State::addEntity(const SubString& name) {
   enterElement(&State::writeAddEntity);
   ssValue = name;
   return WRNeedWrite;
}

inline BasicWriter::WriteResult
BasicWriter::State::addChar(int charValue) {
   enterElement(&State::writeAddChar);
   uChar = charValue;
   return WRNeedWrite;
}

inline BasicWriter::WriteResult
BasicWriter::State::addData(const SubString& text) {
   enterElement(&State::writeAddData);
   ssValue = text;
   return WRNeedWrite;
}

inline bool
BasicWriter::State::isOpenCloseStartElement() const
   {  return (wmEvent == &State::writeCloseStartElement) && (wmNextEvent == nullptr); }

inline bool
BasicWriter::State::isValid() const {
   return (wmEvent != nullptr)
      && ((wmEvent != &State::writeCloseStartElement) || (wmNextEvent != nullptr));
}

inline BasicWriter::WriteResult
BasicWriter::openStartElement(const SubString& elementName)
   { return sState.openStartElement(elementName); }

inline BasicWriter::WriteResult
BasicWriter::closeStartElement()
   { return sState.closeStartElement(); }

inline BasicWriter::WriteResult
BasicWriter::addAttribute(const SubString& name, const SubString& value)
   { return sState.addAttribute(name, value); }

inline BasicWriter::WriteResult
BasicWriter::endElement(const SubString& elementName)
   { return sState.endElement(elementName); }

inline BasicWriter::WriteResult
BasicWriter::closeElement()
   { return sState.closeElement(); }

inline BasicWriter::WriteResult
BasicWriter::addText(const SubString& text)
   { return sState.addText(text); }

inline BasicWriter::WriteResult
BasicWriter::addComment(const SubString& comment)
   { return sState.addComment(comment); }

inline BasicWriter::WriteResult
BasicWriter::addEP(const SubString& name)
   { return sState.addEP(name); }

inline BasicWriter::WriteResult
BasicWriter::addEntity(const SubString& name)
   { return sState.addEntity(name); }

inline BasicWriter::WriteResult
BasicWriter::addChar(int charValue)
   { return sState.addChar(charValue); }

inline BasicWriter::WriteResult
BasicWriter::addData(const SubString& text)
   { return sState.addData(text); }

inline void
BasicWriter::iopenStartElement(const SubString& elementName, STG::SubString& buffer, STG::IOObject::OSBase& out)
{  openStartElement(elementName);
   while (writeEvent(buffer) == WRNeedPlace)
      {  out.writeall(buffer.asPersistent()); buffer.clear(); }
}

inline void
BasicWriter::icloseStartElement(STG::SubString& buffer, STG::IOObject::OSBase& out)
{  closeStartElement();
   while (writeEvent(buffer) == WRNeedPlace)
      {  out.writeall(buffer.asPersistent()); buffer.clear(); }
}

inline void
BasicWriter::iaddAttribute(const SubString& name, const SubString& value, STG::SubString& buffer, STG::IOObject::OSBase& out)
{  addAttribute(name, value);
   while (writeEvent(buffer) == WRNeedPlace)
      {  out.writeall(buffer.asPersistent()); buffer.clear(); }
}

inline void
BasicWriter::iendElement(const SubString& elementName, STG::SubString& buffer, STG::IOObject::OSBase& out)
{  endElement(elementName);
   while (writeEvent(buffer) == WRNeedPlace)
      {  out.writeall(buffer.asPersistent()); buffer.clear(); }
}

inline void
BasicWriter::icloseElement(STG::SubString& buffer, STG::IOObject::OSBase& out)
{  closeElement();
   while (writeEvent(buffer) == WRNeedPlace)
      {  out.writeall(buffer.asPersistent()); buffer.clear(); }
}

inline void
BasicWriter::iaddText(const SubString& text, STG::SubString& buffer, STG::IOObject::OSBase& out)
{  addText(text);
   while (writeEvent(buffer) == WRNeedPlace)
      {  out.writeall(buffer.asPersistent()); buffer.clear(); }
}

inline void
BasicWriter::iaddComment(const SubString& comment, STG::SubString& buffer, STG::IOObject::OSBase& out)
{  addComment(comment);
   while (writeEvent(buffer) == WRNeedPlace)
      {  out.writeall(buffer.asPersistent()); buffer.clear(); }
}

inline void
BasicWriter::iaddEP(const SubString& name, STG::SubString& buffer, STG::IOObject::OSBase& out)
{  addEP(name);
   while (writeEvent(buffer) == WRNeedPlace)
      {  out.writeall(buffer.asPersistent()); buffer.clear(); }
}

inline void
BasicWriter::iaddEntity(const SubString& name, STG::SubString& buffer, STG::IOObject::OSBase& out)
{  addEntity(name);
   while (writeEvent(buffer) == WRNeedPlace)
      {  out.writeall(buffer.asPersistent()); buffer.clear(); }
}

inline void
BasicWriter::iaddChar(int charValue, STG::SubString& buffer, STG::IOObject::OSBase& out)
{  addChar(charValue);
   while (writeEvent(buffer) == WRNeedPlace)
      {  out.writeall(buffer.asPersistent()); buffer.clear(); }
}

inline void
BasicWriter::iaddData(const SubString& text, STG::SubString& buffer, STG::IOObject::OSBase& out)
{  addData(text);
   while (writeEvent(buffer) == WRNeedPlace)
      {  out.writeall(buffer.asPersistent()); buffer.clear(); }
}

/***************************************/
/* Definition of the class BasicParser */
/***************************************/

class GenericLexer;
class BasicParser : public IOObject, protected ExtendedParameters, public Lexer::Base {
  private:
   class State : protected ExtendedParameters {
     public:
      // result == RRNeedChars or RRContinue or RRFinished
      typedef ReadResult (BasicParser::*ReadPointerMethod)(GenericLexer::Token, SubString& additionalContent);

     public:
      class ParseState {
        private:
         int uPoint;
         ReadPointerMethod rpmReadMethod;

        public:
         ParseState() : uPoint(0), rpmReadMethod(nullptr) {}
         ParseState(const ReadPointerMethod& readMethod) : uPoint(0), rpmReadMethod(readMethod) {}
         // result == RRNeedChars or RRContinue or RRFinished
         ReadResult operator()(BasicParser& parser, const GenericLexer::Token& token, SubString& additionalContent)
            {  return (parser.*rpmReadMethod)(token, additionalContent); }
         int& point() { return uPoint; }
         const int& point() const { return uPoint; }
         void setFromCode(int codeParse, int level, int& needBits);
         int getCode(int level, int& reservedBits) const;
         const ReadPointerMethod& getMethod() const { return rpmReadMethod; }
      };

     private:
      ParseState tpsParseState[2];
      int uDocumentLevel;
      GenericLexer::Token tPreviousToken; // for the attributes
      GenericLexer::Token tNextToken;     // for closeStartElement = "/>"

     protected:
      DefineExtendedParameters(2, ExtendedParameters)
      DefineSubExtendedParameters(ParseLevel, 1, INHERITED)
      DefineSubExtendedParameters(TextContinuation, 1, ParseLevel)

     public:
      State() : uDocumentLevel(0)
         {  tpsParseState[0] = &BasicParser::parseDocument;
            tpsParseState[1] = ParseState();
         }
      State(const State& source) : ExtendedParameters(), uDocumentLevel(source.uDocumentLevel),
            tPreviousToken(source.tPreviousToken), tNextToken(source.tNextToken)
         {  tpsParseState[0] = source.tpsParseState[0];
            tpsParseState[1] = source.tpsParseState[1];
            mergeOwnField(source.queryOwnField());
         }
      State& operator=(const State& sSource)
         {  tpsParseState[0] = sSource.tpsParseState[0];
            tpsParseState[1] = sSource.tpsParseState[1];
            uDocumentLevel = sSource.uDocumentLevel;
            tPreviousToken = sSource.tPreviousToken;
            tNextToken = sSource.tNextToken;
            setOwnField(sSource.queryOwnField());
            return *this;
         }
      void read(ISBase& in, const FormatParameters& params);
      void write(OSBase& out, const FormatParameters& params) const;

      void clear()
         {  clearOwnField(); uDocumentLevel = 0;
            tPreviousToken = GenericLexer::Token(); tNextToken = GenericLexer::Token();
            tpsParseState[0] = &BasicParser::parseDocument; tpsParseState[1] = ParseState();
         }

      // result == RRNeedChars or RRContinue or RRFinished
      ReadResult parse(BasicParser& parser, const GenericLexer::Token& token, SubString& additionalContent)
         {  return tpsParseState[queryParseLevelField()](parser, token, additionalContent); }
      ReadResult parseAgain(BasicParser& parser, const GenericLexer::Token& token, SubString& additionalContent);

      State& descent(ReadPointerMethod parseMethod)
         {  int parseLevel = queryParseLevelField();
            AssumeCondition(parseLevel < 1)
            setParseLevelField(++parseLevel);
            tpsParseState[parseLevel] = ParseState(parseMethod);
            return *this;
         }
      void ascent()
         {  int parseLevel = queryParseLevelField();
            tpsParseState[parseLevel--] = ParseState();
            AssumeCondition(parseLevel >= 0)
            setParseLevelField(parseLevel);
         }

      const int& point() const { return tpsParseState[queryParseLevelField()].point(); }
      int& point() { return tpsParseState[queryParseLevelField()].point(); }
      void increaseElement() { uDocumentLevel++; }
      bool decreaseElement()
         { AssumeCondition(uDocumentLevel > 0) return (--uDocumentLevel) == 0;  }

      int getParseLevel() const { return queryParseLevelField(); }
      void clearTextContinuation() { clearTextContinuationField(); }
      void setTextContinuation() { mergeTextContinuationField(1); }
      bool hasTextContinuation() const { return hasTextContinuationField(); }
      const int& getDocumentLevel() const { return uDocumentLevel; }

      void setLastToken(const GenericLexer::Token& token) { tPreviousToken = token; }
      void clearLastToken() { tPreviousToken = GenericLexer::Token(); }
      const GenericLexer::NameToken& getLastNameToken() const
         {  AssumeCondition(dynamic_cast<const GenericLexer::NameToken*>(&tPreviousToken.getContent()))
            return (const GenericLexer::NameToken&) tPreviousToken.getContent();
         }
      void setNextToken(const GenericLexer::Token& token) { tNextToken = token; }
      bool hasNextToken() const { return tNextToken.isValid(); }
      GenericLexer::Token extractNextToken()
         {  GenericLexer::Token result = tNextToken;
            tNextToken = GenericLexer::Token();
            return result;
         }
   };

   GenericLexer glLexer;
   State        sState;

   bool setToAfterNode();

  protected:
   virtual void _read(ISBase& in, const FormatParameters& params) override;
   virtual void _write(OSBase& out, const FormatParameters& params) const override;
   DefineExtendedParameters(1, ExtendedParameters)

   friend class State;
   friend class State::ParseState;
   GenericLexer& lexer() { return glLexer; }
   const int& getDocumentLevel() const { return sState.getDocumentLevel(); }

   ReadResult parseError(GenericLexer::Token token, SubString& additionalContent);
   ReadResult parseDocument(GenericLexer::Token token, SubString& additionalContent);
      ReadResult parseDeclXml(GenericLexer::Token token, SubString& additionalContent);
      ReadResult parseDoctype(GenericLexer::Token token, SubString& additionalContent);
      ReadResult parseNode(GenericLexer::Token token, SubString& additionalContent);

  public:
   BasicParser() {}
   BasicParser(const BasicParser& source)
      :  IOObject(source), ExtendedParameters(), glLexer(source.glLexer), sState(source.sState)
      {  mergeOwnField(source.queryOwnField()); }
   BasicParser& operator=(const BasicParser& source)
      {  IOObject::operator=(source);
         glLexer = source.glLexer;
         sState = source.sState;
         setOwnField(source.queryOwnField());
         return *this;
      }
   DefineCopy(BasicParser)
   DDefineAssign(BasicParser)

   bool isPretty() const { return !hasOwnField(); }
   bool isFlatPrint() const { return hasOwnField(); }
   BasicParser& setPretty() { clearOwnField(); return *this; }
   BasicParser& setFlatPrint() { mergeOwnField(1); return *this; }

   void setPartialToken() { glLexer.setPartialToken(); }
   void setFullToken() { glLexer.setFullToken(); }
   bool isPartialToken() { return glLexer.isPartialToken(); }
   bool isFullToken() { return glLexer.isFullToken(); }

   // events generated by parseToken, parseChunk and parse
   // setPartialToken => signature = GenericLexer::ContentReader&, SubString&
   // setFullToken    => signature = const SubString&
   virtual ReadResult openStartElement(const GenericLexer::ContentReader& reader, SubString& additionalContent) { return RRContinue; }
   virtual ReadResult continueStartElement(const GenericLexer::ContentReader& reader, SubString& additionalContent) { return RRContinue; }
   virtual ReadResult openStartElement(const SubString& elementName) { return RRContinue; }
   virtual ReadResult closeStartElement() { return RRContinue; }
   virtual ReadResult openAttributeKey(const GenericLexer::ContentReader& reader, SubString& additionalContent) { return RRContinue; }
   virtual ReadResult continueAttributeKey(const GenericLexer::ContentReader& reader, SubString& additionalContent) { return RRContinue; }
   virtual ReadResult setAttributeValue(const GenericLexer::ContentReader& reader, SubString& additionalContent) { return RRContinue; }
   virtual ReadResult continueAttributeValue(const GenericLexer::ContentReader& reader, SubString& additionalContent) { return RRContinue; }
   virtual ReadResult addAttribute(const SubString& name, const SubString& value) { return RRContinue; }
   virtual ReadResult endElement(const GenericLexer::ContentReader& reader, SubString& additionalContent) { return RRContinue; }
   virtual ReadResult continueEndElement(const GenericLexer::ContentReader& reader, SubString& additionalContent) { return RRContinue; }
   virtual ReadResult endElement(const SubString& elementName) { return RRContinue; }
   virtual ReadResult closeElement() { return RRContinue; }
   virtual ReadResult addText(const GenericLexer::ContentReader& reader, SubString& additionalContent) { return RRContinue; }
   virtual ReadResult continueText(const GenericLexer::ContentReader& reader, SubString& additionalContent) { return RRContinue; }
   virtual ReadResult addText(const SubString& text) { return RRContinue; }
   virtual ReadResult addComment(const GenericLexer::ContentReader& reader, SubString& additionalContent) { return RRContinue; }
   virtual ReadResult continueComment(const GenericLexer::ContentReader& reader, SubString& additionalContent) { return RRContinue; }
   virtual ReadResult addComment(const SubString& comment) { return RRContinue; }
   virtual ReadResult addEP(const GenericLexer::ContentReader& reader, SubString& additionalContent) { return RRContinue; }
   virtual ReadResult continueEP(const GenericLexer::ContentReader& reader, SubString& additionalContent) { return RRContinue; }
   virtual ReadResult addEP(const SubString& name) { return RRContinue; }
   virtual ReadResult addEntity(const GenericLexer::ContentReader& reader, SubString& additionalContent) { return RRContinue; }
   virtual ReadResult continueEntity(const GenericLexer::ContentReader& reader, SubString& additionalContent) { return RRContinue; }
   virtual ReadResult addEntity(const SubString& name) { return RRContinue; }
   virtual ReadResult addHexaChar(const GenericLexer::ContentReader& reader, SubString& additionalContent) { return RRContinue; }
   virtual ReadResult continueHexaChar(const GenericLexer::ContentReader& reader, SubString& additionalContent) { return RRContinue; }
   virtual ReadResult addChar(const GenericLexer::ContentReader& reader, SubString& additionalContent) { return RRContinue; }
   virtual ReadResult continueChar(const GenericLexer::ContentReader& reader, SubString& additionalContent) { return RRContinue; }
   virtual ReadResult addChar(int charValue) { return RRContinue; }
   virtual ReadResult addData(const GenericLexer::ContentReader& reader, SubString& additionalContent) { return RRContinue; }
   virtual ReadResult continueData(const GenericLexer::ContentReader& reader, SubString& additionalContent) { return RRContinue; }
   virtual ReadResult addData(const SubString& text) { return RRContinue; }

   virtual void warning(const SubString& warning) {}
   virtual void error(const SubString& error) {}                                       

   virtual void startDocument() {}
   virtual void endDocument() {}

   // result == RRNeedChars or RRContinue or RRFinished
   //    RRNeedChars => need to refill subString
   //    RRContinue  => call our method again
   //    RRFinished  => end of parsing
   ReadResult parseToken(SubString& subString, bool isReentry=false);
   ReadResult parseChunk(SubString& subString);
   void parse(ISBase& in);
   void clear() { glLexer.clear(); sState.clear(); }
};

/************************************/
/* Definition of the class ImplNode */
/************************************/

class ImplNode {
  public:
   class Attribute {
     public:
      class Property : public EnhancedObject {
        private:
         SubString ssName;
         SubString ssValue;

        public:
         Property(const SubString& name) : ssName(name), ssValue(SString()) {}
         Property(const SubString& name, const SubString& value) : ssName(name), ssValue(value) {}
         Property(const Property&) = default;
         DefineCopy(Property)
         DDefineAssign(Property)

         const SubString& getName() const { return ssName; }
         const SubString& getValue() const { return ssValue; }
         void setValue(const SubString& value) { ssValue = value; }
         class Key {
           public:
            Key(const COL::VirtualCollection&) {}
            typedef SubString TypeOfKey;
            typedef const SubString& KeyType;
            typedef SubString ControlKeyType;
            static KeyType key(const Property& source) { return source.getName(); }
            static ComparisonResult compare(KeyType fst, KeyType snd) { return fst.compare(snd); }
         };
      };

     private:
      typedef COL::TCopyCollection<COL::SortedArray<Property, Property::Key> > PropertiesArray;

      PropertiesArray paProperties;

     public:
      Attribute() {}
      Attribute(const Attribute& source) = default;
      Attribute& operator=(const Attribute& source) = default;

      void setValue(const SubString& nameProperty, const SubString& valueProperty)
         {  PropertiesArray::Cursor cursor(paProperties);
            PropertiesArray::LocationResult result;
            if ((bool) (result = paProperties.locateKey(nameProperty, cursor, PropertiesArray::RPUndefined)))
               cursor.elementSAt().setValue(valueProperty);
            else
               paProperties.insertAt(cursor, new Property(nameProperty, valueProperty), result.getInsertionPosition());
         }
      void remove(const SubString& nameProperty)
         {  PropertiesArray::Cursor cursor(paProperties);
            paProperties.locate(nameProperty, cursor);
            paProperties.freeAt(cursor);
         }
      SubString getValue(const SubString& nameProperty) const
         {  PropertiesArray::Cursor cursor(paProperties);
            return (paProperties.locateKey(nameProperty, cursor, PropertiesArray::RPExact))
               ? cursor.elementAt().getValue() : SubString(SString());
         }
   };

  private:
   SubString sName;
   Attribute aProperties;

  public:
   ImplNode(const SubString& name) : sName(name) {}
   ImplNode(const ImplNode& source) = default;
   ImplNode& operator=(const ImplNode& source) = default;

   SubString& getSName() { return sName; }
   const SubString& getName() const { return sName; }
   const Attribute& getProperties() const { return aProperties; }
   Attribute& properties() { return aProperties; }
   void addAttribute(const SubString& nameProperty, const SubString& valueProperty)
      {  aProperties.setValue(nameProperty, valueProperty); }
};

/*****************************************/
/* Definition of the class ContextParser */
/*****************************************/

class ContextParser : public BasicParser {
  protected:
   class NodeAction : public COL::List::Node, public ImplNode {
     private:
      typedef COL::List::Node inherited;

     public:
      NodeAction(const SubString& nodeName) : ImplNode(nodeName) {}
      NodeAction(const NodeAction& source) = default;
      DefineCopy(NodeAction)
      DDefineAssign(NodeAction)
   };

   class NodeActionsStack : public COL::TCopyCollection<COL::TList<NodeAction> > {
     private:
      typedef COL::TCopyCollection<COL::TList<NodeAction> > inherited;
      STG::SubString propertyKey;
      STG::SubString value;
      friend class ContextParser;

     public:
      NodeActionsStack() : propertyKey(STG::SString()), value(STG::SString()) {}
      void pushElement(NodeAction* newAction) { inherited::add(newAction); }
      void popElement() { inherited::freeLast(); }
      NodeAction& currentNode() const { return inherited::getSLast(); }
   };

  private:
   PNT::AutoPointer<NodeActionsStack> apnasStack;

  protected:
   virtual ReadResult openStartElement(const GenericLexer::ContentReader& reader, SubString& additionalContent) override
      {  apnasStack->pushElement(new NodeAction(SString()));
         STG::SubString res = STG::SString();
         ReadResult result = reader.readContentToken(additionalContent, res, true);
         apnasStack->currentNode().getSName().cat(res);
         return result;
      }
   virtual ReadResult continueStartElement(const GenericLexer::ContentReader& reader, SubString& additionalContent) override
      {  STG::SubString res = STG::SString();
         ReadResult result = reader.readContentToken(additionalContent, res, true);
         apnasStack->currentNode().getSName().cat(res);
         return result;
      }
   virtual ReadResult openStartElement(const SubString& nodeName) override
      {  apnasStack->pushElement(new NodeAction(nodeName)); return RRContinue; }

   virtual ReadResult openAttributeKey(const GenericLexer::ContentReader& reader, SubString& additionalContent) override
      {  STG::SubString res = STG::SString();
         ReadResult result = reader.readContentToken(additionalContent, res, true);
         apnasStack->propertyKey.copy(res);
         return result;
      }
   virtual ReadResult continueAttributeKey(const GenericLexer::ContentReader& reader, SubString& additionalContent) override
      {  STG::SubString res = STG::SString();
         ReadResult result = reader.readContentToken(additionalContent, res, true);
         apnasStack->propertyKey.cat(res);
         return result;
      }
   virtual ReadResult setAttributeValue(const GenericLexer::ContentReader& reader, SubString& additionalContent) override
      {  STG::SubString res = STG::SString();
         ReadResult result = reader.readContentToken(additionalContent, res, true);
         apnasStack->value.copy(res);
         if (result == RRContinue) {
            apnasStack->currentNode().addAttribute(apnasStack->propertyKey, apnasStack->value);
            apnasStack->propertyKey = STG::SString();
            apnasStack->value = STG::SString();
         }
         return result;
      }
   virtual ReadResult continueAttributeValue(const GenericLexer::ContentReader& reader, SubString& additionalContent) override
      {  STG::SubString res = STG::SString();
         ReadResult result = reader.readContentToken(additionalContent, res, true);
         apnasStack->value.cat(res);
         if (result == RRContinue) {
            apnasStack->currentNode().addAttribute(apnasStack->propertyKey, apnasStack->value);
            apnasStack->propertyKey = STG::SString();
            apnasStack->value = STG::SString();
         }
         return result;
      }
   virtual ReadResult addAttribute(const SubString& name, const SubString& value) override
      {  apnasStack->currentNode().addAttribute(name, value); return RRContinue; }

   virtual ReadResult endElement(const GenericLexer::ContentReader& reader, SubString& additionalContent) override
      {  if (apnasStack->isEmpty())
            throw EReadError();
         STG::SubString res = STG::SString();
         ReadResult result = reader.readContentToken(additionalContent, res, true);
         apnasStack->value.copy(res);
         if (result == RRContinue) {
            if (apnasStack->value != apnasStack->currentNode().getName())
               throw EReadError();
            apnasStack->value = STG::SString();
         }
         return result;
      }
   virtual ReadResult continueEndElement(const GenericLexer::ContentReader& reader, SubString& additionalContent) override
      {  if (apnasStack->isEmpty())
            throw EReadError();
         STG::SubString res = STG::SString();
         ReadResult result = reader.readContentToken(additionalContent, res, true);
         apnasStack->value.cat(res);
         if (result == RRContinue) {
            if (apnasStack->value != apnasStack->currentNode().getName())
               throw EReadError();
            apnasStack->value = STG::SString();
         }
         return result;
      }
   virtual ReadResult endElement(const SubString& elementName) override
      {  if (apnasStack->isEmpty() || elementName != apnasStack->currentNode().getName())
            throw EReadError();
         return RRContinue;
      }

   virtual ReadResult addText(const GenericLexer::ContentReader& reader, SubString& additionalContent) override
      {  if (apnasStack->isEmpty())
            throw EReadError();
         STG::SubString res = STG::SString();
         ReadResult result = reader.readContentToken(additionalContent, res, true);
         apnasStack->value.copy(res);
         if (result == RRContinue) {
            addText(apnasStack->value);
            apnasStack->value = STG::SString();
         }
         return result;
      }
   virtual ReadResult continueText(const GenericLexer::ContentReader& reader, SubString& additionalContent) override
      {  if (apnasStack->isEmpty())
            throw EReadError();
         STG::SubString res = STG::SString();
         ReadResult result = reader.readContentToken(additionalContent, res, true);
         apnasStack->value.cat(res);
         if (result == RRContinue) {
            addText(apnasStack->value);
            apnasStack->value = STG::SString();
         }
         return result;
      }
   virtual ReadResult addComment(const GenericLexer::ContentReader& reader, SubString& additionalContent) override
      {  if (apnasStack->isEmpty())
            throw EReadError();
         STG::SubString res = STG::SString();
         ReadResult result = reader.readContentToken(additionalContent, res, true);
         apnasStack->value.copy(res);
         if (result == RRContinue) {
            addComment(apnasStack->value);
            apnasStack->value = STG::SString();
         }
         return result;
      }
   virtual ReadResult continueComment(const GenericLexer::ContentReader& reader, SubString& additionalContent) override
      {  if (apnasStack->isEmpty())
            throw EReadError();
         STG::SubString res = STG::SString();
         ReadResult result = reader.readContentToken(additionalContent, res, true);
         apnasStack->value.cat(res);
         if (result == RRContinue) {
            addComment(apnasStack->value);
            apnasStack->value = STG::SString();
         }
         return result;
      }
   virtual ReadResult addEP(const GenericLexer::ContentReader& reader, SubString& additionalContent) override
      {  if (apnasStack->isEmpty())
            throw EReadError();
         STG::SubString res = STG::SString();
         ReadResult result = reader.readContentToken(additionalContent, res, true);
         apnasStack->value.copy(res);
         if (result == RRContinue) {
            addEP(apnasStack->value);
            apnasStack->value = STG::SString();
         }
         return result;
      }
   virtual ReadResult continueEP(const GenericLexer::ContentReader& reader, SubString& additionalContent) override
      {  if (apnasStack->isEmpty())
            throw EReadError();
         STG::SubString res = STG::SString();
         ReadResult result = reader.readContentToken(additionalContent, res, true);
         apnasStack->value.cat(res);
         if (result == RRContinue) {
            addEP(apnasStack->value);
            apnasStack->value = STG::SString();
         }
         return result;
      }
   virtual ReadResult addEntity(const GenericLexer::ContentReader& reader, SubString& additionalContent) override
      {  if (apnasStack->isEmpty())
            throw EReadError();
         STG::SubString res = STG::SString();
         ReadResult result = reader.readContentToken(additionalContent, res, true);
         apnasStack->value.copy(res);
         if (result == RRContinue) {
            addEntity(apnasStack->value);
            apnasStack->value = STG::SString();
         }
         return result;
      }
   virtual ReadResult continueEntity(const GenericLexer::ContentReader& reader, SubString& additionalContent) override
      {  if (apnasStack->isEmpty())
            throw EReadError();
         STG::SubString res = STG::SString();
         ReadResult result = reader.readContentToken(additionalContent, res, true);
         apnasStack->value.cat(res);
         if (result == RRContinue) {
            addEntity(apnasStack->value);
            apnasStack->value = STG::SString();
         }
         return result;
      }
   virtual ReadResult addHexaChar(const GenericLexer::ContentReader& reader, SubString& additionalContent) override
      {  if (apnasStack->isEmpty())
            throw EReadError();
         STG::SubString res = STG::SString();
         ReadResult result = reader.readContentToken(additionalContent, res, true);
         apnasStack->value.copy(res);
         if (result == RRContinue) {
            addChar(apnasStack->value.readHexaInteger());
            apnasStack->value = STG::SString();
         }
         return result;
      }
   virtual ReadResult continueHexaChar(const GenericLexer::ContentReader& reader, SubString& additionalContent) override
      {  if (apnasStack->isEmpty())
            throw EReadError();
         STG::SubString res = STG::SString();
         ReadResult result = reader.readContentToken(additionalContent, res, true);
         apnasStack->value.cat(res);
         if (result == RRContinue) {
            addChar(apnasStack->value.readHexaInteger());
            apnasStack->value = STG::SString();
         }
         return result;
      }
   virtual ReadResult addChar(const GenericLexer::ContentReader& reader, SubString& additionalContent) override
      {  if (apnasStack->isEmpty())
            throw EReadError();
         STG::SubString res = STG::SString();
         ReadResult result = reader.readContentToken(additionalContent, res, true);
         apnasStack->value.copy(res);
         if (result == RRContinue) {
            addChar(apnasStack->value.queryInteger());
            apnasStack->value = STG::SString();
         }
         return result;
      }
   virtual ReadResult continueChar(const GenericLexer::ContentReader& reader, SubString& additionalContent) override
      {  if (apnasStack->isEmpty())
            throw EReadError();
         STG::SubString res = STG::SString();
         ReadResult result = reader.readContentToken(additionalContent, res, true);
         apnasStack->value.cat(res);
         if (result == RRContinue) {
            addChar(apnasStack->value.queryInteger());
            apnasStack->value = STG::SString();
         }
         return result;
      }
   virtual ReadResult addData(const GenericLexer::ContentReader& reader, SubString& additionalContent) override
      {  if (apnasStack->isEmpty())
            throw EReadError();
         STG::SubString res = STG::SString();
         ReadResult result = reader.readContentToken(additionalContent, res, true);
         apnasStack->value.copy(res);
         if (result == RRContinue) {
            addData(apnasStack->value);
            apnasStack->value = STG::SString();
         }
         return result;
      }
   virtual ReadResult continueData(const GenericLexer::ContentReader& reader, SubString& additionalContent) override
      {  if (apnasStack->isEmpty())
            throw EReadError();
         STG::SubString res = STG::SString();
         ReadResult result = reader.readContentToken(additionalContent, res, true);
         apnasStack->value.cat(res);
         if (result == RRContinue) {
            addData(apnasStack->value);
            apnasStack->value = STG::SString();
         }
         return result;
      }

   bool hasNode() const { return apnasStack->isEmpty(); }
   NodeAction& currentNode() const { return apnasStack->currentNode(); }

  public:
   ContextParser() : apnasStack(new NodeActionsStack(), PNT::Pointer::Init()) {}
   ContextParser(NodeActionsStack* stack) : apnasStack(stack, PNT::Pointer::Init()) {}
   ContextParser(const ContextParser& source) = default;
   DefineCopy(ContextParser)
   DDefineAssign(ContextParser)

   virtual ReadResult closeStartElement() override { return RRContinue; }
   virtual ReadResult closeElement() override
      {  apnasStack->popElement(); return apnasStack->isEmpty() ? RRFinished : RRContinue; }
   virtual ReadResult addText(const SubString& text) override { return RRContinue; }
   virtual ReadResult addComment(const SubString& comment) override { return RRContinue; }
   virtual ReadResult addEP(const SubString& name) override { return RRContinue; }
   virtual ReadResult addEntity(const SubString& name) override { return RRContinue; }
   virtual ReadResult addChar(int charValue) override { return RRContinue; }
   virtual ReadResult addData(const SubString& text) override { return RRContinue; }
   virtual void warning(const SubString& warning) override {}
   virtual void error(const SubString& error) override {}

   virtual void startDocument() override {}
   virtual void endDocument() override {}
};

/*****************************************/
/* Definition of the class ContextWriter */
/*****************************************/

class ContextWriter : public BasicWriter {
  public:
   typedef BasicWriter inherited;
   typedef BasicWriter::State InheritedState;
   COL::TCopyCollection<COL::TArray<SubString> > assNameStack;

  protected:
   DefineExtendedParameters(1, BasicWriter)
   DefineSubExtendedParameters(DelayedClose, 1, INHERITED)

   ContextWriter& setPretty() { return (ContextWriter&) inherited::setPretty(); }
   ContextWriter& setFlatPrint() { return (ContextWriter&) inherited::setFlatPrint(); }

   WriteResult openStartElement(const SubString& elementName)
      {  assNameStack.insertCopyAtEnd(elementName);
         return inherited::openStartElement(elementName);
      }
   WriteResult closeStartElement() { return inherited::closeStartElement(); }
   WriteResult addAttribute(const SubString& name, const SubString& value)
      {  return inherited::addAttribute(name, value); }
   WriteResult closeElement()
      {  WriteResult result = WRNeedEvent;
         if (state().isOpenCloseStartElement())
            result = inherited::closeElement();
         else {
            if (assNameStack.isEmpty())
               throw STG::EReadError();
            result = inherited::endElement(assNameStack.getLast());
            mergeDelayedCloseField(1);
         };
         return result;
      }
   WriteResult addText(const SubString& text)         { return inherited::addText(text); }
   WriteResult addComment(const SubString& comment)   { return inherited::addComment(comment); }
   WriteResult addEP(const SubString& name)           { return inherited::addEP(name); }
   WriteResult addEntity(const SubString& name)       { return inherited::addEntity(name); }
   WriteResult addChar(int charValue)                 { return inherited::addChar(charValue); }
   WriteResult addData(const SubString& text)         { return inherited::addData(text); }

  public:
   ContextWriter() {}
   ContextWriter(const ContextWriter& source) = default;
   DefineCopy(ContextWriter)
   DDefineAssign(ContextWriter)

   void write(OSBase& out) { inherited::write(out); }
   WriteResult delayedEvent()
      {  WriteResult result = WRNeedEvent;
         if (hasDelayedCloseField()) {
            assNameStack.freeLast();
            if (inherited::closeElement() != WRNeedEvent)
               throw STG::EReadError();
            clearDelayedCloseField();
            result = WRNeedWrite;
         };
         return result;
      }
   WriteResult writeEvent(SubString& out, bool isReentry=false) { return inherited::writeEvent(out, isReentry); }
   WriteResult writeChunk(SubString& out) { return inherited::writeChunk(out); }
};

/****************************************/
/* Definition of the class CommonParser */
/****************************************/

class CommonParser : public BasicParser {
  public:
   class Arguments {
     private:
      enum Event
         {  EUndefined, EOpenStartElement, ECloseStartElement, EOpenAttribute,
            EAddAttribute, EEndElement, ECloseElement, EText,
            EComment, EEP, EEntity, EChar, EHexaChar, EData
         };
      Event eEvent;
      SubString ssValue;
      SubString ssAttribute;
      SubString ssEntityConverter = STG::SString();
      GenericLexer::ContentReader lcrReader;
      SubString* pssAdditionalContent;
      bool fContinuedToken;
      bool fCompareEqual = false;
      bool fOldToken;

      /* for inlining */
      class ErrorMessage : public COL::List::Node {
        private:
         STG::SubString ssMessage;
         STG::SubString ssFilePos;
         unsigned uLinePos;
         unsigned uColumnPos;

        public:
         ErrorMessage(const STG::SubString& message, const STG::SubString& filePos,
               unsigned linePos, unsigned columnPos)
            :  ssMessage(message), ssFilePos(filePos), uLinePos(linePos), uColumnPos(columnPos) {}
         const STG::SubString& getMessage() const { return ssMessage; }
         const STG::SubString& filepos() const { return ssFilePos; }
         unsigned linepos() const { return uLinePos; }
         unsigned columnpos() const { return uColumnPos; }
      };

      CommonParser* pcpParser;
      int uLocalStackHeight = 0;
      COL::TCopyCollection<COL::TList<ErrorMessage> >* plemErrorMessages = nullptr;
      int uCountErrors = 0;
      bool fDoesStopOnError = false;

     protected:
      bool hasAdditionalContent() const { return pssAdditionalContent; }
      void setAdditionalContent(STG::SubString& content) { pssAdditionalContent = &content; }

     public:
      Arguments()
         :  eEvent(EUndefined), ssValue(SString()), ssAttribute(SString()),
            pssAdditionalContent(nullptr), fContinuedToken(false), fOldToken(false), pcpParser(nullptr) {}
      Arguments(CommonParser& parser)
         :  eEvent(EUndefined), ssValue(SString()), ssAttribute(SString()),
            pssAdditionalContent(nullptr), fContinuedToken(false), fOldToken(false), pcpParser(&parser) {}
      Arguments(const Arguments& source) = default;
      Arguments& operator=(const Arguments& source) = default;

      typedef ReadResult ResultAction;
      bool hasEvent() const            { return eEvent != EUndefined; }
      bool isOpenStartElement() const  { return eEvent == EOpenStartElement; }
      bool isCloseStartElement() const { return eEvent == ECloseStartElement; }
      bool isAttribute() const         { return eEvent == EOpenAttribute || eEvent == EAddAttribute; }
      bool isOpenAttribute() const     { return eEvent == EOpenAttribute; }
      bool isAddAttribute() const      { return eEvent == EAddAttribute; }
      bool isEndElement() const        { return eEvent == EEndElement; }
      bool isCloseElement() const      { return eEvent == ECloseElement; }
      bool isText() const              { return eEvent == EText; }
      bool isEntity() const            { return eEvent == EEntity; }
      bool isExtendedText() const      { return eEvent == EText || eEvent == EEntity; }

      void setNewToken()            { fOldToken = false; }
      bool isContinuedToken() const { return fContinuedToken; }
      bool isNewToken() const       { return !fOldToken; }
      bool isFullToken() const      { AssumeCondition(pcpParser) return pcpParser->isFullToken(); }
      bool hasFullToken() const     { return !lcrReader.isValid(); }
      bool hasPartialToken() const  { return lcrReader.isValid(); }
      SubString& value()            { return ssValue; }
      SubString& attribute()        { return ssAttribute; }
      const GenericLexer::ContentReader& getContentReader() const { return lcrReader; }
      SubString& getAdditionalContent() const { AssumeCondition(pssAdditionalContent) return *pssAdditionalContent; }
      ReadResult convertReaderToString(bool isValue=true)
         {  if (!lcrReader.isValid())
               return RRContinue;
            STG::SubString res = STG::SString();
            auto result = lcrReader.readContentToken(*pssAdditionalContent, res, true);
            if (!fContinuedToken)
               (isValue ? ssValue : ssAttribute).copy(res);
            else
               (isValue ? ssValue : ssAttribute).cat(res);
            if (result != RRNeedChars)
               fContinuedToken = false;
            return result;
         }
      ReadResult convertExtendedReaderToString(bool isValue=true)
         {  if (!lcrReader.isValid())
               return RRContinue;
            STG::SubString res = STG::SString();
            auto result = lcrReader.readContentToken(*pssAdditionalContent, res, true);
            if (eEvent == EText) {
               if (!fContinuedToken)
                  (isValue ? ssValue : ssAttribute).copy(res);
               else
                  (isValue ? ssValue : ssAttribute).cat(res);
            }
            else if (eEvent == EEntity) {
               if (!fContinuedToken) {
                  const STG::SubString* source = &res;
                  if (ssEntityConverter.length() != 0) {
                     ssEntityConverter.cat(res);
                     source = &ssEntityConverter;
                  }
                  if (source->compare("lt") == CREqual)
                     (isValue ? ssValue : ssAttribute).copy('<');
                  else if (source->compare("gt") == CREqual)
                     (isValue ? ssValue : ssAttribute).copy('>');
                  else if (source->compare("amp") == CREqual)
                     (isValue ? ssValue : ssAttribute).copy('&');
                  else {
                     (isValue ? ssValue : ssAttribute).copy('&');
                     (isValue ? ssValue : ssAttribute).cat(*source);
                     (isValue ? ssValue : ssAttribute).cat(';');
                  }
                  if (ssEntityConverter.length() != 0)
                     ssEntityConverter.clear();
               }
               else
                  ssEntityConverter.cat(res);
            }
            if (result != RRNeedChars)
               fContinuedToken = false;
            return result;
         }
      void setValueCompare(const STG::SubString& value) { ssValue = value; fCompareEqual = true; }
      void resetValueCompare(const STG::SubString& value)
         {  if (fCompareEqual)
               fCompareEqual = ssValue.length() == 0;
            if (value.length() == 0)
               ssValue = value;
            else 
               ssValue = SString();
         }
      const STG::SubString& getValueCompare() const { return ssValue; }
      void setAttributeCompare(const STG::SubString& attribute) { ssAttribute = attribute; fCompareEqual = true; }
      ReadResult assumeReaderValue()
         {  return lcrReader.assumeContentToken(*pssAdditionalContent, ssValue, fCompareEqual); }
      ReadResult assumeReaderAttribute()
         {  return lcrReader.assumeContentToken(*pssAdditionalContent, ssAttribute, fCompareEqual); }
      bool isCompareEqual() const { return fCompareEqual && ssValue.length() == 0; }
      ReadResult convertReaderToValue() { return convertReaderToString(true); }
      ReadResult convertExtendedReaderToValue() { return convertExtendedReaderToString(true); }
      ReadResult convertReaderToAttribute() { return convertReaderToString(false); }
      ReadResult skipReaderValue()
         {  return lcrReader.skipContentToken(*pssAdditionalContent); }
      ReadResult skipReaderAttribute()
         {  return lcrReader.skipContentToken(*pssAdditionalContent); }

      void clearValue()
         {  if (ssValue.hasControlOnRepository()) {
               if (ssValue.length() > 0)
                  ssValue.clear();
            }
            else 
               ssValue = SString();
         }
      void clearAttribute() { if (ssAttribute.length() > 0) ssAttribute = SString(); }
      void clearExceptAdditional() { eEvent = EUndefined; clearValue(); clearAttribute(); lcrReader.clear(); }
      void clear() { eEvent = EUndefined; clearValue(); clearAttribute(); lcrReader.clear(); pssAdditionalContent = nullptr; fContinuedToken = false; }
      void setOpenStartElement(const SubString& tag)
         {  AssumeCondition(eEvent == EUndefined)
            eEvent = EOpenStartElement;
            ssValue = tag;
            fOldToken = true;
         }
      void setOpenEvent(Event event, const GenericLexer::ContentReader& reader, SubString& additionalContent)
         {  eEvent = event;
            lcrReader = reader;
            pssAdditionalContent = &additionalContent;
            clearValue(); clearAttribute();
            fContinuedToken = false;
            fOldToken = true;
         }
      void setOpenPartialEvent(Event event, const GenericLexer::ContentReader& reader, SubString& additionalContent)
         {  eEvent = event;
            lcrReader = reader;
            pssAdditionalContent = &additionalContent;
            clearValue();
            fContinuedToken = false;
            fOldToken = true;
         }
      void continueEvent(Event event, const GenericLexer::ContentReader& reader, SubString& additionalContent)
         {  AssumeCondition(eEvent == event)
            pssAdditionalContent = &additionalContent;
            fContinuedToken = true;
            fOldToken = true;
         }
      void setOpenStartElement(const GenericLexer::ContentReader& reader, SubString& additionalContent)
         {  setOpenEvent(EOpenStartElement, reader, additionalContent); }
      void continueStartElement(const GenericLexer::ContentReader& reader, SubString& additionalContent)
         {  continueEvent(EOpenStartElement, reader, additionalContent); }
      void setOpenStartElement(const char* tag) { setOpenStartElement(STG::SString(tag)); }
      void setCloseStartElement() { eEvent = ECloseStartElement; fOldToken = true; }
      void setAddAttribute(const SubString& reference, const SubString& value)
         {  AssumeCondition(eEvent == EUndefined)
            eEvent = EAddAttribute;
            ssAttribute = reference;
            ssValue = value;
            fOldToken = true;
         }
      void setAddAttribute(const char* reference, const char* value)
         {  setAddAttribute(STG::SString(reference), STG::SString(value)); }
      void setAddAttribute(const char* reference, const SubString& value)
         {  setAddAttribute(STG::SString(reference), value); }
      void setOpenAttributeKey(const GenericLexer::ContentReader& reader, SubString& additionalContent)
         {  setOpenEvent(EOpenAttribute, reader, additionalContent); }
      void continueAttributeKey(const GenericLexer::ContentReader& reader, SubString& additionalContent)
         {  continueEvent(EOpenAttribute, reader, additionalContent); }
      void setAttributeValue(const GenericLexer::ContentReader& reader, SubString& additionalContent)
         {  setOpenPartialEvent(EAddAttribute, reader, additionalContent); }
      void continueAttributeValue(const GenericLexer::ContentReader& reader, SubString& additionalContent)
         {  continueEvent(EAddAttribute, reader, additionalContent); }
      void setEndElement(const SubString& tag)
         {  AssumeCondition(eEvent == EUndefined)
            eEvent = EEndElement;
            ssValue = tag;
            fOldToken = true;
         }
      void setEndElement(const char* tag) { setEndElement(STG::SString(tag)); }
      void setEndElement(const GenericLexer::ContentReader& reader, SubString& additionalContent)
         {  setOpenEvent(EEndElement, reader, additionalContent); }
      void continueEndElement(const GenericLexer::ContentReader& reader, SubString& additionalContent)
         {  continueEvent(EEndElement, reader, additionalContent); }
      void setCloseElement() { eEvent = ECloseElement; fOldToken = true; }
      void addText(const SubString& text)
         {  AssumeCondition((eEvent == EUndefined) || (eEvent == EText))
            if (eEvent == EUndefined) {
               eEvent = EText;
               ssValue = text;
            }
            else {
               if (!ssValue.hasControlOnRepository())
                  ssValue = STG::SString(ssValue);
               ssValue.cat(text);
            };
            fOldToken = true;
         }
      void addText(const char* text) { addText(STG::SString(text)); }
      void addText(const GenericLexer::ContentReader& reader, SubString& additionalContent)
         {  setOpenEvent(EText, reader, additionalContent); }
      void continueText(const GenericLexer::ContentReader& reader, SubString& additionalContent)
         {  continueEvent(EText, reader, additionalContent); }
      void setComment(const GenericLexer::ContentReader& reader, SubString& additionalContent)
         {  setOpenEvent(EComment, reader, additionalContent); }
      void continueComment(const GenericLexer::ContentReader& reader, SubString& additionalContent)
         {  continueEvent(EComment, reader, additionalContent); }
      void setComment(const SubString& comment)
         {  AssumeCondition(eEvent == EUndefined)
            eEvent = EComment;
            ssValue = comment;
            fOldToken = true;
         }
      void setEP(const GenericLexer::ContentReader& reader, SubString& additionalContent)
         {  setOpenEvent(EEP, reader, additionalContent); }
      void continueEP(const GenericLexer::ContentReader& reader, SubString& additionalContent)
         {  continueEvent(EEP, reader, additionalContent); }
      void setEP(const SubString& text)
         {  AssumeCondition(eEvent == EUndefined)
            eEvent = EEP;
            ssValue = text;
            fOldToken = true;
         }
      void setEntity(const GenericLexer::ContentReader& reader, SubString& additionalContent)
         {  setOpenEvent(EEntity, reader, additionalContent); }
      void continueEntity(const GenericLexer::ContentReader& reader, SubString& additionalContent)
         {  continueEvent(EEntity, reader, additionalContent); }
      void setChar(const GenericLexer::ContentReader& reader, SubString& additionalContent)
         {  setOpenEvent(EChar, reader, additionalContent); }
      void continueChar(const GenericLexer::ContentReader& reader, SubString& additionalContent)
         {  continueEvent(EChar, reader, additionalContent); }
      void setHexaChar(const GenericLexer::ContentReader& reader, SubString& additionalContent)
         {  setOpenEvent(EHexaChar, reader, additionalContent); }
      void continueHexaChar(const GenericLexer::ContentReader& reader, SubString& additionalContent)
         {  continueEvent(EHexaChar, reader, additionalContent); }
      void setData(const GenericLexer::ContentReader& reader, SubString& additionalContent)
         {  setOpenEvent(EData, reader, additionalContent); }
      void continueData(const GenericLexer::ContentReader& reader, SubString& additionalContent)
         {  continueEvent(EData, reader, additionalContent); }
      void setData(const SubString& comment)
         {  AssumeCondition(eEvent == EUndefined)
            eEvent = EData;
            ssValue = comment;
            fOldToken = true;
         }

      void shift() { ++uLocalStackHeight; }
      void reduce() { AssumeCondition(uLocalStackHeight >= 0) --uLocalStackHeight; }
      void reduceState(Parser::TStateStack<Arguments>& state)
         {  state.reduce();
            reduce();
         }
      template <class TypeObject, typename ReadPointerMethod, class TypeResult>
      void shiftState(Parser::TStateStack<Arguments>& state,
            TypeObject& object, ReadPointerMethod parseMethod, TypeResult* result)
         {  shift();
            state.shift(object, parseMethod, result);
         }
      template <class TypeObject, typename ReadPointerMethod, class TypeResult>
      Parser::TStateStack<Arguments>::TParseState<TypeObject, ReadPointerMethod, TypeResult>&
         shiftResultState(Parser::TStateStack<Arguments>& state, TypeObject* nullObject,
               ReadPointerMethod parseMethod, TypeResult&& result)
         {  shift();
            return state.shiftResult(nullObject, parseMethod, std::move(result));
         }

      bool doesStopAfterTooManyErrors() const
         {  return (fDoesStopOnError || !plemErrorMessages || uCountErrors >= 20); }
      bool addErrorMessage(const STG::SubString& message)
         {  ++uCountErrors;
            if (plemErrorMessages)
               plemErrorMessages->insertNewAtEnd(new ErrorMessage(message, STG::SString(), 0, 0));
            return !doesStopAfterTooManyErrors();
         }
      bool isValidRange() const
         {  return (uLocalStackHeight >= 0 && uLocalStackHeight <= 7); }
      void clearRange() { uLocalStackHeight = 0; }
      bool hasErrors() const
         {  return plemErrorMessages && !plemErrorMessages->isEmpty(); }
      COL::TList<ErrorMessage>& errors() const
         {  AssumeCondition(plemErrorMessages); return *plemErrorMessages; }

      // ReadResult readChars(SubString& buffer) { AssumeCondition(pcpParser) return pcpParser->lexer().readChars(buffer); }
      bool setToNextToken(ReadResult& result)
         {  bool booleanResult = false;
            if (pssAdditionalContent) {
               if (isValidRange()) {
                  bool reentryField = pcpParser->hasReentryField();
                  pcpParser->mergeReentryField(1);
                  while ((result = pcpParser->parseToken(*pssAdditionalContent, true)) == RRContinue) {}
                  booleanResult = (result == RRHasToken);
                  if (!reentryField)
                     pcpParser->clearReentryField();
               }
               else
                  result = RRHasToken;
               if (!booleanResult)
                  clearRange();
            }
            else
               result = RRHasToken;
            return booleanResult;
         }
      bool parseTokens(Parser::TStateStack<Arguments>& state, ReadResult& result)
         {  bool booleanResult = false;
            size_t originalSize = state.getTotalSize();
            int originalPoint = state.hasUpLast() ? state.upLast().point() : -1;
            if (fContinuedToken)
               fContinuedToken = false;
            if (pssAdditionalContent) {
               if (isValidRange()) {
                  result = state.parse(*this);
                  booleanResult = (result == RRHasToken)
                     && state.getTotalSize() < originalSize
                     && !state.isEmpty() && state.point() == originalPoint;
               }
               else
                  result = RRContinue;
               if (!booleanResult)
                  clearRange();
            }
            else
               result = RRContinue;
            return booleanResult;
         }
   };

   typedef Parser::TStateStack<Arguments> State;

  private:
   typedef BasicParser inherited;
   State sState;
   Arguments aArguments;

  protected:
   DefineExtendedParameters(2, inherited)
   DefineSubExtendedParameters(VerificationDisabled, 1, INHERITED)
   DefineSubExtendedParameters(Reentry, 1, VerificationDisabled)
   bool hasVerificationDisabled() const { return hasVerificationDisabledField(); }
   bool isReentry() const { return hasReentryField(); }

   ReadResult parseArgument()
      {  STG::Lexer::Base::ReadResult parseResult;
         do {
            parseResult = sState.parse(aArguments);
         } while (parseResult == RRContinue);
         return (parseResult == RRHasToken) ? RRContinue : parseResult;
      }
   void assumeCleared()
      {  if (aArguments.isText())
            parseArgument();
         aArguments.clear();
      }

  public:
   class Parse {};
   template <class TypeObject, class TypeResult>
   CommonParser(TypeObject& object, TypeResult* resTemplate, Parse) : aArguments(*this) 
      {  sState.shift(object, &TypeObject::readXML, resTemplate); }

   CommonParser() : aArguments(*this) {}
   CommonParser(CommonParser&& source) = default;
   CommonParser(const CommonParser& source) = default;
   CommonParser& operator=(CommonParser&& source) = default;
   CommonParser& operator=(const CommonParser& source) = default;
   DefineCopy(CommonParser)
   DDefineAssign(CommonParser)

   State& state() { return sState; }
   const State& state() const { return sState; }
   CommonParser& setVerificationDisabled() { mergeVerificationDisabledField(1); return *this; }
   ReadResult parseToken(SubString& subString, bool isReentry=false)
      {  ReadResult result;
         if (!lexer().isFullToken() && lexer().hasContentToken()) {
            ReadResult result = lexer().skipContentToken(subString);
            if (result == RRNeedChars)
               return result;
         }
         aArguments.setNewToken();
         result = inherited::parseToken(subString, isReentry); 
         if (isReentry && result == RRContinue) {
            if (!aArguments.isNewToken())
               result = RRHasToken;
            else if (!lexer().isFullToken() && lexer().hasContentToken())
               result = lexer().skipContentToken(subString);
         }
         return result;
      }

   virtual ReadResult openStartElement(const GenericLexer::ContentReader& reader, SubString& additionalContent) override
      {  aArguments.setOpenStartElement(reader, additionalContent);
         if (isReentry())
            return RRContinue;
         return parseArgument();
      }
   virtual ReadResult continueStartElement(const GenericLexer::ContentReader& reader, SubString& additionalContent) override
      {  if (!aArguments.hasEvent())
            return RRContinue;
         aArguments.continueStartElement(reader, additionalContent);
         if (isReentry())
            return RRContinue;
         return parseArgument();
      }
   virtual ReadResult openStartElement(const SubString& elementName) override
      {  assumeCleared();
         aArguments.setOpenStartElement(elementName);
         if (isReentry())
            return RRContinue;
         auto result = parseArgument();
         aArguments.clear();
         return result;
      }
   virtual ReadResult closeStartElement() override
      {  if (isFullToken())
            assumeCleared();
         aArguments.setCloseStartElement();
         if (isReentry())
            return RRContinue;
         auto result = parseArgument();
         if (isFullToken())
            aArguments.clear();
         return result;
      }
   virtual ReadResult openAttributeKey(const GenericLexer::ContentReader& reader, SubString& additionalContent) override
      {  aArguments.setOpenAttributeKey(reader, additionalContent);
         if (isReentry())
            return RRContinue;
         return parseArgument();
      }
   virtual ReadResult continueAttributeKey(const GenericLexer::ContentReader& reader, SubString& additionalContent) override
      {  if (!aArguments.hasEvent())
            return RRContinue;
         aArguments.continueAttributeKey(reader, additionalContent);
         if (isReentry())
            return RRContinue;
         return parseArgument();
      }
   virtual ReadResult setAttributeValue(const GenericLexer::ContentReader& reader, SubString& additionalContent) override
      {  aArguments.setAttributeValue(reader, additionalContent);
         if (isReentry())
            return RRContinue;
         return parseArgument();
      }
   virtual ReadResult continueAttributeValue(const GenericLexer::ContentReader& reader, SubString& additionalContent) override
      {  if (!aArguments.hasEvent())
            return RRContinue;
         aArguments.continueAttributeValue(reader, additionalContent);
         if (isReentry())
            return RRContinue;
         return parseArgument();
      }
   virtual ReadResult addAttribute(const SubString& name, const SubString& value) override
      {  assumeCleared();
         aArguments.setAddAttribute(name, value);
         if (isReentry())
            return RRContinue;
         auto result = parseArgument();
         aArguments.clear();
         return result;
      }
   virtual ReadResult endElement(const GenericLexer::ContentReader& reader, SubString& additionalContent) override
      {  aArguments.setEndElement(reader, additionalContent);
         if (isReentry())
            return RRContinue;
         return parseArgument();
      }
   virtual ReadResult continueEndElement(const GenericLexer::ContentReader& reader, SubString& additionalContent) override
      {  if (!aArguments.hasEvent())
            return RRContinue;
         aArguments.continueEndElement(reader, additionalContent);
         if (isReentry())
            return RRContinue;
         return parseArgument();
      }
   virtual ReadResult endElement(const SubString& elementName) override
      {  assumeCleared();
         aArguments.setEndElement(elementName);
         if (isReentry())
            return RRContinue;
         auto result = parseArgument();
         aArguments.clear();
         return result;
      }
   virtual ReadResult closeElement() override
      {  if (isFullToken())
            assumeCleared();
         aArguments.setCloseElement();
         if (isReentry())
            return RRContinue;
         auto result = parseArgument();
         if (isFullToken())
            aArguments.clear();
         return result;
      }
   virtual ReadResult addText(const GenericLexer::ContentReader& reader, SubString& additionalContent) override
      {  aArguments.addText(reader, additionalContent);
         if (isReentry())
            return RRContinue;
         return parseArgument();
      }
   virtual ReadResult continueText(const GenericLexer::ContentReader& reader, SubString& additionalContent) override
      {  if (!aArguments.hasEvent())
            return RRContinue;
         aArguments.continueText(reader, additionalContent);
         if (isReentry())
            return RRContinue;
         return parseArgument();
      }
   virtual ReadResult addText(const SubString& text) override
      {  aArguments.addText(text); return RRContinue; }
   virtual ReadResult addComment(const GenericLexer::ContentReader& reader, SubString& additionalContent) override
      {  aArguments.setComment(reader, additionalContent);
         if (isReentry())
            return RRContinue;
         return parseArgument();
      }
   virtual ReadResult continueComment(const GenericLexer::ContentReader& reader, SubString& additionalContent) override
      {  if (!aArguments.hasEvent())
            return RRContinue;
         aArguments.continueComment(reader, additionalContent);
         if (isReentry())
            return RRContinue;
         return parseArgument();
      }
   virtual ReadResult addComment(const SubString& comment) override
      {  assumeCleared();
         aArguments.setComment(comment);
         if (isReentry())
            return RRContinue;
         auto result = parseArgument();
         aArguments.clear();
         return result;
      }
   virtual ReadResult addEP(const GenericLexer::ContentReader& reader, SubString& additionalContent) override
      {  aArguments.setEP(reader, additionalContent);
         if (isReentry())
            return RRContinue;
         return parseArgument();
      }
   virtual ReadResult continueEP(const GenericLexer::ContentReader& reader, SubString& additionalContent) override
      {  if (!aArguments.hasEvent())
            return RRContinue;
         aArguments.continueEP(reader, additionalContent);
         if (isReentry())
            return RRContinue;
         return parseArgument();
      }
   virtual ReadResult addEP(const SubString& name) override
      {  assumeCleared();
         aArguments.setEP(name);
         if (isReentry())
            return RRContinue;
         return parseArgument();
      }
   virtual ReadResult addEntity(const GenericLexer::ContentReader& reader, SubString& additionalContent) override
      {  aArguments.setEntity(reader, additionalContent);
         if (isReentry())
            return RRContinue;
         return parseArgument();
      }
   virtual ReadResult continueEntity(const GenericLexer::ContentReader& reader, SubString& additionalContent) override
      {  if (!aArguments.hasEvent())
            return RRContinue;
         aArguments.continueEntity(reader, additionalContent);
         if (isReentry())
            return RRContinue;
         return parseArgument();
      }
   virtual ReadResult addEntity(const SubString& name) override
      {  if (name.compare("amp") == CREqual)
            aArguments.addText(SString().cat('&'));
         else if (name.compare("gt") == CREqual)
            aArguments.addText(SString().cat('>'));
         else if (name.compare("lt") == CREqual)
            aArguments.addText(SString().cat('<'));
         return RRContinue;
      }
   virtual ReadResult addHexaChar(const GenericLexer::ContentReader& reader, SubString& additionalContent) override
      {  aArguments.setHexaChar(reader, additionalContent);
         if (isReentry())
            return RRContinue;
         return parseArgument();
      }
   virtual ReadResult continueHexaChar(const GenericLexer::ContentReader& reader, SubString& additionalContent) override
      {  if (!aArguments.hasEvent())
            return RRContinue;
         aArguments.continueHexaChar(reader, additionalContent);
         if (isReentry())
            return RRContinue;
         return parseArgument();
      }
   virtual ReadResult addChar(const GenericLexer::ContentReader& reader, SubString& additionalContent) override
      {  aArguments.setChar(reader, additionalContent);
         if (isReentry())
            return RRContinue;
         return parseArgument();
      }
   virtual ReadResult continueChar(const GenericLexer::ContentReader& reader, SubString& additionalContent) override
      {  if (!aArguments.hasEvent())
            return RRContinue;
         aArguments.continueChar(reader, additionalContent);
         if (isReentry())
            return RRContinue;
         return parseArgument();
      }
   virtual ReadResult addChar(int charValue) override
      {  aArguments.addText(SString().cat((char) charValue)); return RRContinue; }
   virtual ReadResult addData(const GenericLexer::ContentReader& reader, SubString& additionalContent) override
      {  aArguments.setData(reader, additionalContent);
         if (isReentry())
            return RRContinue;
         return parseArgument();
      }
   virtual ReadResult continueData(const GenericLexer::ContentReader& reader, SubString& additionalContent) override
      {  if (!aArguments.hasEvent())
            return RRContinue;
         aArguments.continueData(reader, additionalContent);
         if (isReentry())
            return RRContinue;
         return parseArgument();
      }
   virtual ReadResult addData(const SubString& text) override
      {  assumeCleared();
         aArguments.setData(text);
         if (isReentry())
            return RRContinue;
         auto result = parseArgument();
         aArguments.clear();
         return result;
      }

   virtual void warning(const SubString& warning) override {}
   virtual void error(const SubString& error) override
      {  if (!aArguments.addErrorMessage(error))
            throw STG::EReadError();
      }

   virtual void startDocument() override {}
   virtual void endDocument() override {}

   class SkipNode : public EnhancedObject {
     private:
      typedef EnhancedObject inherited;

     public:
      SkipNode() {}
      SkipNode(const SkipNode& source) = default;
      DefineCopy(SkipNode)
      ReadResult skip(State& state, Arguments& arguments);
      ReadResult skipInLoop(State& state, Arguments& arguments);
   };
   
   static bool skipNode(State& state, Arguments& argument);

   struct LessCondition {
      int uLevel;
      int uPoint;
      LessCondition(int level, int point) : uLevel(level), uPoint(point) {}
   };
   ReadResult parseChunkUntil(SubString& subString, const LessCondition& condition);
   void parseUntil(ISBase& in, const LessCondition& condition);
};

inline bool
CommonParser::skipNode(State& state, Arguments& argument) {
   bool result = argument.isOpenStartElement();
   if (result) {
      auto& local = state.shiftResult((SkipNode*) nullptr, &SkipNode::skip, SkipNode());
      local.setObject(state.getSResult((SkipNode*) nullptr));
      result = state.parse(argument);
   };
   return result;
}

/****************************************/
/* Definition of the class CommonWriter */
/****************************************/

template <class TypeBase>
class TCommonWriter : public TypeBase, public DXML::Base {
  public:
   typedef CommonParser::FormatParameters FormatParameters;
   class Arguments : public CommonParser::Arguments {
     private:
      TCommonWriter<TypeBase>* pcwWriter = nullptr;

     public:
      typedef WriteResult ResultAction;

      void setAdditionalContent(STG::SubString& out, TCommonWriter<TypeBase>& writer)
         {  CommonParser::Arguments::setAdditionalContent(out);
            pcwWriter = &writer;
         }
      bool writeEvent(WriteResult& result)
         {  bool booleanResult = false;
            if (hasAdditionalContent() && pcwWriter) {
               if (isValidRange()) {
                  bool reentryField = pcwWriter->hasReentryField();
                  pcwWriter->mergeReentryField(1);
                  while ((result = pcwWriter->writeEvent(getAdditionalContent(), true)) == WRNeedWrite) {}
                  booleanResult = (result == WRNeedEvent);
                  if (!reentryField)
                     pcwWriter->clearReentryField();
               }
               else
                  result = WRNeedWrite;
               if (!booleanResult)
                  clearRange();
            }
            else
               result = WRNeedWrite;
            return booleanResult;
         }
      bool writeTokens(Parser::TStateStack<Arguments>& state, WriteResult& result)
         {  bool booleanResult = false;
            size_t originalSize = state.getTotalSize();
            int originalPoint = state.hasUpLast() ? state.upLast().point() : -1;
            if (hasAdditionalContent()) {
               if (isValidRange()) {
                  result = state.parse(*this);
                  booleanResult = (result == WRNeedEvent)
                     && state.getTotalSize() < originalSize
                     && !state.isEmpty() && state.point() == originalPoint;
               }
               else
                  result = WRNeedWrite;
               if (!booleanResult)
                  clearRange();
            }
            else
               result = WRNeedWrite;
            return booleanResult;
         }
      template <class TypeObject, typename WritePointerMethod, class TypeResult>
      void shiftState(Parser::TStateStack<Arguments>& state,
            TypeObject& object, WritePointerMethod writeMethod, TypeResult* resTemplate)
         {  shift();
            state.shift(object, writeMethod, resTemplate);
         }
      template <class TypeObject, typename WritePointerMethod, class TypeResult>
      typename Parser::TStateStack<Arguments>::template TParseState<TypeObject, WritePointerMethod, TypeResult>&
         shiftResultState(Parser::TStateStack<Arguments>& state, TypeObject* nullObject,
               WritePointerMethod writeMethod, TypeResult&& resTemplate)
         {  shift();
            return state.shiftResult(nullObject, writeMethod, std::move(resTemplate));
         }

      void reduceState(Parser::TStateStack<Arguments>& state)
         {  state.reduce();
            reduce();
         }
   };
   typedef Parser::TStateStack<Arguments> State;
   typedef CommonParser::LessCondition LessCondition;

  private:
   typedef TCommonWriter<TypeBase> thisType;
   typedef TypeBase inherited;
   State sState;
   Arguments aArguments;

  protected:
   TemplateDefineExtendedParameters(3, TypeBase)
   DefineSubExtendedParameters(CommonEntityVerification, 1, INHERITED)
   DefineSubExtendedParameters(Reentry, 1, CommonEntityVerification)
   DefineSubExtendedParameters(VerificationDisabled, 1, Reentry)

   BasicWriter::WriteResult writeText(STG::SubString& content);

  public:
   class Write {};
   template <class TypeObject, class TypeResult>
   TCommonWriter(const TypeObject& object, TypeResult* resTemplate, Write)
      { sState.shift(const_cast<TypeObject&>(object), &TypeObject::writeXML, resTemplate); }

   TCommonWriter() {}
   TCommonWriter(thisType&& source) = default;
   TCommonWriter(const thisType& source) = default;
   thisType& operator=(thisType&& source) = default;
   thisType& operator=(const thisType& source) = default;
   TemplateDefineCopy(TCommonWriter, TypeBase)
   DTemplateDefineAssign(TCommonWriter, TypeBase)
   virtual BasicWriter::WriteResult translateEvent() override;
   virtual BasicWriter::WriteResult getNextEvent() override;

   State& state() { return sState; }
   const State& state() const { return sState; }

   const int& getDocumentLevel() const { return inherited::state().getDocumentLevel(); }
   bool isCommonEntityVerification() const { return hasCommonEntityVerificationField(); }
   bool hasVerificationDisabled() const { return hasVerificationDisabledField(); }

   thisType& setNoEntityVerification() { clearCommonEntityVerificationField(); return *this; }
   thisType& setCommonEntityVerification() { mergeCommonEntityVerificationField(1); return *this; }
   thisType& setVerificationDisabled() { mergeVerificationDisabledField(1); return *this; }
   thisType& setPretty() { return (thisType&) inherited::setPretty(); }
   thisType& setFlatPrint() { return (thisType&) inherited::setFlatPrint(); }
};

class CommonWriter : public TCommonWriter<BasicWriter> {
  private:
   typedef TCommonWriter<BasicWriter> inherited;
   
  public:
   template <class TypeObject>
   CommonWriter(const TypeObject& object, Write)
      { state().shift(const_cast<TypeObject&>(object), &TypeObject::writeXML); }

   CommonWriter() {}
   CommonWriter(const CommonWriter& source) = default;
   
   WriteResult writeChunkUntil(SubString& out, const LessCondition& condition, WriteResult result = WRNeedEvent);
   void writeUntil(OSBase& out, const LessCondition& condition);
};

}} // end of namespace STG::XML

#endif // STG_XML_XMLParserH

