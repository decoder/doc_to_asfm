/**************************************************************************/
/*                                                                        */
/*  Copyright (C) 2014-2019                                               */
/*    CEA (Commissariat a l'Energie Atomique et aux Energies              */
/*         Alternatives)                                                  */
/*                                                                        */
/*  you can redistribute it and/or modify it under the terms of the GNU   */
/*  Lesser General Public License as published by the Free Software       */
/*  Foundation, version 2.1.                                              */
/*                                                                        */
/*  It is distributed in the hope that it will be useful,                 */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU Lesser General Public License for more details.                   */
/*                                                                        */
/*  See the GNU Lesser General Public License version 2.1                 */
/*  for more details (enclosed in the file LICENSE).                      */
/*                                                                        */
/**************************************************************************/

/////////////////////////////////
//
// Library   : XML
// Unit      : Lexer
// File      : XMLLexer.cpp
// Description :
//   Implementation of a lexer of XML document.
//

#include "XML/XMLLexer.h"
// #include "XML/XMLParser.h"

namespace STG { namespace XML {

/***************************************************/
/* Implementation of the class GenericLexer::Token */
/***************************************************/

void
GenericLexer::AbstractToken::read(Type& type, IOObject::ISBase& in, const IOObject::FormatParameters& params) {
   if (params.isRaw()) {
      char typeValue;
      in.read(typeValue, true);
      if (typeValue < 0 || typeValue >= TypesNB)
         throw EReadError();
      type = (Type) typeValue;
   }
   else {
      int typeValue;
      in.read(typeValue, false);
      if (typeValue < 0 || typeValue >= TypesNB)
         throw EReadError();
      type = (Type) typeValue;
   };
}

void
GenericLexer::AbstractToken::write(Type type, IOObject::OSBase& out, const IOObject::FormatParameters& params) {
   if (params.isRaw())
      out.write((int) type, true);
   else
      out.write((char) type, false);
}

GenericLexer::AbstractToken*
GenericLexer::AbstractToken::createToken(Type type) {
   switch (type) {
      case TUndefined: case TSeparator: case TDeclxml: case TEnddeclxml: case TDeclelement:
      case TDoctype: case TGreater: case TSlashgreater: case TOpsqbrace: case TClsqbrace:
      case TEqual: case TVersion: case TPublic: case TSystem: case TEncoding: case TStandalone:
      case TDecllistattribute: case TDeclentity: case TDeclnotation:
         return nullptr;
      case TComment: return new CommentToken();
      case TNodeBeginName: case TNodeBeginEndName: case TName: case TCallep: case TCallentity:
         return new NameToken(type);
      case TTextualData: case TFinalTextualData: return new TextualDataToken();
      case TCData: return new CDataToken();
      case TLitteral: return new LitteralToken();
      case TCallhexachar: return new CallCharToken();
      case TCallchar: return new CallCharToken();
      default: break;
   };
   return nullptr;
}

bool
GenericLexer::AbstractToken::needExtension(GenericLexer::AbstractToken::Type type) {
   switch (type) {
      case TComment: case TNodeBeginName: case TNodeBeginEndName: case TName: case TCallep:
      case TCallentity: case TTextualData: case TFinalTextualData: case TCData: case TLitteral:
      case TCallhexachar: case TCallchar:
         return true;
      default:
         return false;
   };
}

void
GenericLexer::Token::read(IOObject::ISBase& in, const IOObject::FormatParameters& params) {
   AbstractToken::read(tType, in, params);
   if (!AbstractToken::needExtension(tType))
      return;
   bool isRaw = params.isRaw();
   if (!isRaw) in.assume(' ');
   if (((char) in.get()) - '0') {
      if (!isRaw) in.assume(' ');
      ppatContent.absorbElement(AbstractToken::createToken(tType));
      if (tType == GenericLexer::AbstractToken::TCallchar || tType == GenericLexer::AbstractToken::TCallhexachar)
         in.read(((CallCharToken&) *ppatContent).call(), isRaw);
      else
         in.read(((StringToken&) *ppatContent).string().asPersistent(), isRaw);
   };
}

void
GenericLexer::Token::write(IOObject::OSBase& out, const IOObject::FormatParameters& params) const {
   AbstractToken::write(tType, out, params);
   if (!AbstractToken::needExtension(tType))
      return;

   bool isRaw = params.isRaw();
   if (!isRaw) out.put(' ');
   out.put(ppatContent.isValid() ? '1' : '0');
   if (ppatContent.isValid()) {
      if (!isRaw) out.put(' ');
      if (tType == GenericLexer::AbstractToken::TCallchar || tType == GenericLexer::AbstractToken::TCallhexachar)
         out.write(((CallCharToken&) *ppatContent).getCall(), isRaw);
      else
         out.write(((StringToken&) *ppatContent).getString().asPersistent(), isRaw);
   };
}

/********************************************/
/* Implementation of the class GenericLexer */
/********************************************/

GenericLexer::ReadPointerMethodTable GenericLexer::rpmtGlobalReadMethods;
GenericLexer::ReadPointerMethodTable::ReadPointerMethodTable() {
   clear();
   elementAt(0).setMethod(&GenericLexer::readCharsInitial);
   elementAt(1).setMethod(&GenericLexer::readCharsDeclxml);
   elementAt(2).setMethod(&GenericLexer::readCharsDoctype);
   elementAt(3).setMethod(&GenericLexer::readCharsElementHeader);
   elementAt(4).setMethod(&GenericLexer::readCharsElementFoot);
   elementAt(5).setMethod(&GenericLexer::readCharsBody);
}

GenericLexer::ReadPointerMethodFstTable GenericLexer::rpmftFstReadMethods;
GenericLexer::ReadPointerMethodFstTable::ReadPointerMethodFstTable() {
   clear();
   elementAt(0).setMethod(&GenericLexer::readCharsEndComment);
   elementAt(1).setMethod(&GenericLexer::readCharsEndName);
   elementAt(2).setMethod(&GenericLexer::readCharsEndSeparator);
   elementAt(3).setMethod(&GenericLexer::readCharsEndLitteral);
   elementAt(4).setMethod(&GenericLexer::readCharsEndDeclaration);
   elementAt(5).setMethod(&GenericLexer::readCharsEndEntity);
   elementAt(6).setMethod(&GenericLexer::readCharsEndTextualData);
   elementAt(7).setMethod(&GenericLexer::readCharsEndCdata);
   elementAt(8).setMethod(&GenericLexer::readCharsEndCallChar);
   elementAt(9).setMethod(&GenericLexer::readCharsEndCallHexaChar);
}

void
GenericLexer::_read(ISBase& in, const FormatParameters& params) {
   bool isRaw = params.isRaw();
   tToken.read(in, params);
   if (!isRaw)
      in.assume('\n');
   tbCurrentToken.read(in, params);
   if (!isRaw)
      in.assume('\n');
   int state = 0;
   in.read(state, isRaw);
   if ((state < 0) || (state >= (rpmtGlobalReadMethods.Size*(rpmftFstReadMethods.Size+1))))
      throw EReadError();
   rpmState = rpmtGlobalReadMethods[state/(rpmftFstReadMethods.Size+1)].getMethod();
   rpmFstState = (state % (rpmftFstReadMethods.Size+1))
      ? rpmftFstReadMethods[(state%(rpmftFstReadMethods.Size+1))-1].getMethod() : nullptr;
   if (!isRaw)
      in.assume(' ');
   in.read(chContext, isRaw);
}

void
GenericLexer::_write(OSBase& out, const FormatParameters& params) const {
   bool isRaw = params.isRaw();
   tToken.write(out, params);
   if (!isRaw)
      out.put('\n');
   tbCurrentToken.write(out, params);
   if (!isRaw)
      out.put('\n');
   int state = 0;
   for (int global=0; rpmState != rpmtGlobalReadMethods[global].getMethod(); global++, state += 9);
   if (rpmFstState)
      for (int local=0; rpmFstState != rpmftFstReadMethods[local].getMethod(); local++, state++);
   out.write(state, isRaw);
   if (!isRaw)
      out.put(' ');
   out.write(chContext, isRaw);
}

GenericLexer::ReadResult
GenericLexer::readCharsEndComment(SubString& buffer, SubStringContainer* container) {
   int endComment = buffer.scanPos("-->");
   SubString comment(buffer);
   comment.setUpperClosed();
   if (endComment >= 0) {
      comment.setLength(endComment);
      if (!container || !container->fImmediateUse) {
         ((!container || !container->ptbBuffer) ? tbCurrentToken : *container->ptbBuffer) << comment;
         if (container && container->pssSubString && !container->fImmediateUse)
            tbCurrentToken >> *container->pssSubString;
      }
      else if (container && container->pssSubString) {
         if (container->fCompare) {
            container->fDifferent = container->pssSubString->length() != comment.length()
                  || *container->pssSubString != comment;
            if (!container->fDifferent)
               container->pssSubString->setToEnd();
         }
         else if (container->fImmediateUse)
            *container->pssSubString = comment;
      }

      buffer.advance(endComment+3);
      if (!container) {
         CommentToken* token = nullptr;
         tToken = Token(token = new CommentToken());
         tbCurrentToken >> token->comment();
         rpmFstState = nullptr;
         return RRHasToken;
      };
      rpmFstState = nullptr;
      return RRContinue;
   };

   comment.restrictToLength(2);
   if (!container || !container->fImmediateUse)
      ((!container || !container->ptbBuffer) ? tbCurrentToken : *container->ptbBuffer) << comment;
   else if (container->pssSubString) {
      if (container->fCompare) {
         int referenceLength = container->pssSubString->length(), commentLength = comment.length();
         if (referenceLength < commentLength)
            container->fDifferent = true;
         else {
            container->fDifferent = container->pssSubString->compare(comment, commentLength) != CREqual;
            if (!container->fDifferent)
               container->pssSubString->advance(commentLength);
         };
      }
      else if (container->fImmediateUse)
         *container->pssSubString = comment;
   }
   buffer.setToEnd(comment);
   return RRNeedChars;
}

GenericLexer::ReadResult
GenericLexer::readCharsEndName(SubString& buffer, SubStringContainer* container) {
   int ch;
   if (!container || !container->fImmediateUse) {
      auto& textBuffer = (!container || !container->ptbBuffer) ? tbCurrentToken : *container->ptbBuffer;
      while (isalnum(ch = buffer.getChar().getInternal()) || ch == '.' || ch == '-' || ch == '_' || ch == ':')
         textBuffer << (char) buffer.extractChar();
      if (container && container->pssSubString && !container->fImmediateUse)
         tbCurrentToken >> *container->pssSubString;
   }
   else {
      if (container->fCompare)
         while (isalnum(ch = buffer.getChar().getInternal()) || ch == '.' || ch == '-' || ch == '_' || ch == ':') {
            char extractedChar = (char) buffer.extractChar();
            if (!container->fDifferent)
               container->fDifferent = container->pssSubString->length() == 0
                     || (char) container->pssSubString->extractChar() != extractedChar;
         }
      else if (container->pssSubString) {
         *container->pssSubString = buffer;
         container->pssSubString->setUpperClosed();
         int length = 0;
         while (isalnum(ch = buffer.getChar().getInternal()) || ch == '.' || ch == '-' || ch == '_' || ch == ':') {
            ++length;
            buffer.advance();
         }
         container->pssSubString->setLength(length);
      }
      else
         while (isalnum(ch = buffer.getChar().getInternal()) || ch == '.' || ch == '-' || ch == '_' || ch == ':')
            buffer.advance();
   }
   if (ch != EOF) {
      if (!container) {
         NameToken* token = nullptr;
         tToken = Token(token = new NameToken((AbstractToken::Type) chContext));
         tbCurrentToken >> token->name();
         rpmFstState = nullptr;
         return RRHasToken;
      };
      rpmFstState = nullptr;
      return RRContinue;
   };
   return RRNeedChars;
}

GenericLexer::ReadResult
GenericLexer::readCharsEndSeparator(SubString& buffer, SubStringContainer* container) {
   int ch;
   if (buffer.length() == 0)
      return RRNeedChars;
   while (isspace(ch = buffer.extractChar().getInternal()));
   if (ch != EOF) {
      buffer.advance(-1);
      if (!container)
         tToken = Token(AbstractToken::TSeparator);
      rpmFstState = nullptr;
      return container ? RRContinue : RRHasToken;
   };
   return RRNeedChars;
}

GenericLexer::ReadResult
GenericLexer::readCharsInitial(SubString& buffer) {
   int ch;
   if ((ch = buffer.getChar().getInternal()) == EOF)
      return RRNeedChars;
   else if (ch == '<') {
      ComparisonResult result;
      if ((result = buffer.compareSub("<?xml")) != CRNonComparable) {
         if (result == CRLess)
            return RRNeedChars;
         buffer.advance(sizeof("<?xml")-1);
         tToken = Token(AbstractToken::TDeclxml);
         rpmState = &GenericLexer::readCharsDeclxml;
         return RRHasToken;
      };
      if ((result = buffer.compareSub("<!DOCTYPE")) != CRNonComparable) {
         if (result == CRLess)
            return RRNeedChars;
         buffer.advance(sizeof("<!DOCTYPE")-1);
         tToken = Token(AbstractToken::TDoctype);
         rpmState = &GenericLexer::readCharsDoctype;
         return RRHasToken;
      };
      if ((result = buffer.compareSub("<!--")) != CRNonComparable) {
         if (result == CRLess)
            return RRNeedChars;
         buffer.advance(sizeof("<!--")-1);
         rpmFstState = &GenericLexer::readCharsEndComment;
         if (oOptions == OPartialToken) {
            tToken = Token(AbstractToken::TComment);
            return RRHasToken;
         }
         return readCharsEndComment(buffer);
      };
      if ((ch = buffer.getChar(1).getInternal()) == EOF)
         return RRNeedChars;
      if (isalpha(ch) || (ch == '_') || (ch == ':')) {
         if (oOptions == OFullToken) {
            tbCurrentToken << (char) ch;
            buffer.advance(2);
         }
         else
            buffer.advance();
         chContext = AbstractToken::TNodeBeginName;
         rpmState = &GenericLexer::readCharsElementHeader;
         rpmFstState = &GenericLexer::readCharsEndName;
         if (oOptions == OPartialToken) {
            tToken = Token(AbstractToken::TNodeBeginName);
            return RRHasToken;
         }
         return readCharsEndName(buffer);
      };
      throw EReadError();
   }
   else if (isspace(ch)) {
      rpmFstState = &GenericLexer::readCharsEndSeparator;
      if (oOptions == OPartialToken) {
         tToken = Token(AbstractToken::TSeparator);
         return RRHasToken;
      }
      return readCharsEndSeparator(buffer);
   };
   throw EReadError();
}

GenericLexer::ReadResult
GenericLexer::readCharsEndLitteral(SubString& buffer, SubStringContainer* container) {
   int ch;
   if (!container || !container->fImmediateUse) {
      auto& textBuffer = (!container || !container->ptbBuffer) ? tbCurrentToken : *container->ptbBuffer;
      while (((ch = buffer.getChar().getInternal()) != chContext) && (ch != EOF))
         textBuffer << (char) buffer.extractChar();
      if (container && container->pssSubString && !container->fImmediateUse)
         tbCurrentToken >> *container->pssSubString;
   }
   else {
      if (container->fCompare)
         while (((ch = buffer.getChar().getInternal()) != chContext) && (ch != EOF)) {
            char extractedChar = (char) buffer.extractChar();
            if (!container->fDifferent)
               container->fDifferent = container->pssSubString->length() == 0
                     || (char) container->pssSubString->extractChar() != extractedChar;
         }
      else if (container->pssSubString) {
         *container->pssSubString = buffer;
         container->pssSubString->setUpperClosed();
         int length = 0;
         while (((ch = buffer.getChar().getInternal()) != chContext) && (ch != EOF)) {
            ++length;
            buffer.advance();
         }
         container->pssSubString->setLength(length);
      }
      else
         while (((ch = buffer.getChar().getInternal()) != chContext) && (ch != EOF))
            buffer.advance();
   }
   if (ch != EOF) {
      buffer.advance();
      if (!container) {
         LitteralToken* token = nullptr;
         tToken = Token(token = new LitteralToken());
         tbCurrentToken >> token->litteral();
         rpmFstState = nullptr;
         return RRHasToken;
      };
      rpmFstState = nullptr;
      return RRContinue;
   };
   return RRNeedChars;
}

GenericLexer::ReadResult
GenericLexer::readCharsDeclxml(SubString& buffer) {
   int ch;
   if ((ch = buffer.getChar().getInternal()) == EOF) return RRNeedChars;
   else if (isalpha(ch)) {
      ComparisonResult result;
      if ((result = buffer.compareSub("version")) != CRNonComparable) {
         if (result == CRLess)
            return RRNeedChars;
         buffer.advance(sizeof("version")-1);
         tToken = Token(AbstractToken::TVersion);
         return RRHasToken;
      };
      if ((result = buffer.compareSub("encoding")) != CRNonComparable) {
         if (result == CRLess)
            return RRNeedChars;
         buffer.advance(sizeof("encoding")-1);
         tToken = Token(AbstractToken::TEncoding);
         return RRHasToken;
      };
      if ((result = buffer.compareSub("standalone")) != CRNonComparable) {
         if (result == CRLess)
            return RRNeedChars;
         buffer.advance(sizeof("standalone")-1);
         tToken = Token(AbstractToken::TStandalone);
         return RRHasToken;
      };
   }
   else if (isspace(ch)) {
      buffer.advance();
      rpmFstState = &GenericLexer::readCharsEndSeparator;
      if (oOptions == OPartialToken) {
         tToken = Token(AbstractToken::TSeparator);
         return RRHasToken;
      }
      return readCharsEndSeparator(buffer);
   }
   else if (ch == '=') {
      buffer.advance();
      tToken = Token(AbstractToken::TEqual);
      return RRHasToken;
   }
   else if (ch =='?') {
      if ((ch = buffer.getChar(1).getInternal()) == EOF)
         return RRNeedChars;
      if (ch == '>') {
         buffer.advance(2);
         tToken = Token(AbstractToken::TEnddeclxml);
         rpmState = &GenericLexer::readCharsInitial;
         return RRHasToken;
      };
   }
   else if ((ch == '\"') || (ch == '\'')) {
      chContext = (char) ch;
      buffer.advance();
      rpmFstState = &GenericLexer::readCharsEndLitteral;
      if (oOptions == OPartialToken) {
         tToken = Token(AbstractToken::TLitteral);
         return RRHasToken;
      }
      return readCharsEndLitteral(buffer);
   };
   throw EReadError();
}

GenericLexer::ReadResult
GenericLexer::readCharsEndDeclaration(SubString& buffer, SubStringContainer* container) {
   int endDeclaration = buffer.scanPos('>');
   if (endDeclaration >= 0) {
      buffer.advance(endDeclaration+1);
      tToken = Token((AbstractToken::Type) chContext);
      rpmFstState = nullptr;
      return container ? RRContinue : RRHasToken;
   };
   buffer.setToEnd();
   return RRNeedChars;
}

GenericLexer::ReadResult
GenericLexer::readCharsEndEntity(SubString& buffer, SubStringContainer* container) {
   int ch;
   if (!container || !container->fImmediateUse) {
      auto& textBuffer = (!container || !container->ptbBuffer) ? tbCurrentToken : *container->ptbBuffer;
      while (isalnum(ch = buffer.getChar().getInternal()) || ch == '.' || ch == '-' || ch == '_' || ch == ':')
         textBuffer << (char) buffer.extractChar();
      if (container && container->pssSubString && !container->fImmediateUse)
         tbCurrentToken >> *container->pssSubString;
   }
   else {
      if (container->fCompare)
         while (isalnum(ch = buffer.getChar().getInternal()) || ch == '.' || ch == '-' || ch == '_' || ch == ':') {
            char extractedChar = (char) buffer.extractChar();
            if (!container->fDifferent)
               container->fDifferent = container->pssSubString->length() == 0
                     || (char) container->pssSubString->extractChar() != extractedChar;
         }
      else if (container->pssSubString) {
         *container->pssSubString = buffer;
         container->pssSubString->setUpperClosed();
         int length = 0;
         while (isalnum(ch = buffer.getChar().getInternal()) || ch == '.' || ch == '-' || ch == '_' || ch == ':') {
            ++length;
            buffer.advance();
         }
         container->pssSubString->setLength(length);
      }
      else
         while (isalnum(ch = buffer.getChar().getInternal()) || ch == '.' || ch == '-' || ch == '_' || ch == ':')
            buffer.advance();
   }
   if (ch == ';') {
      buffer.advance();
      if (!container) {
         NameToken* token = nullptr;
         tToken = Token(token = new NameToken((AbstractToken::Type) chContext));
         tbCurrentToken >> token->name();
         rpmFstState = nullptr;
         return RRHasToken;
      };
      rpmFstState = nullptr;
      return RRContinue;
   };
   if (ch == EOF)
      return RRNeedChars;
   throw EReadError();
}

GenericLexer::ReadResult
GenericLexer::readCharsEndCallChar(SubString& buffer, SubStringContainer* container) {
   int ch;
   if (!container || !container->fImmediateUse) {
      auto& textBuffer = (!container || !container->ptbBuffer) ? tbCurrentToken : *container->ptbBuffer;
      while (isdigit(ch = buffer.getChar().getInternal()))
         textBuffer << (char) buffer.extractChar();
      if (container && container->pssSubString && !container->fImmediateUse)
         tbCurrentToken >> *container->pssSubString;
   }
   else {
      if (container->fCompare)
         while (isdigit(ch = buffer.getChar().getInternal())) {
            char extractedChar = (char) buffer.extractChar();
            if (!container->fDifferent)
               container->fDifferent = container->pssSubString->length() == 0
                     || (char) container->pssSubString->extractChar() != extractedChar;
         }
      else if (container->pssSubString) {
         *container->pssSubString = buffer;
         container->pssSubString->setUpperClosed();
         int length = 0;
         while (isdigit(ch = buffer.getChar().getInternal())) {
            ++length;
            buffer.advance();
         }
         container->pssSubString->setLength(length);
      }
      else
         while (isdigit(ch = buffer.getChar().getInternal()))
            buffer.advance();
   }
   if (ch == ';') {
      buffer.advance();
      if (!container) {
         SString integer;
         tbCurrentToken >> integer;
         tToken = Token(new CallCharToken(integer.queryInteger()));
         rpmFstState = nullptr;
         return RRHasToken;
      };
      rpmFstState = nullptr;
      return RRContinue;
   };
   if (ch == EOF)
      return RRNeedChars;
   throw EReadError();
}

GenericLexer::ReadResult
GenericLexer::readCharsEndCallHexaChar(SubString& buffer, SubStringContainer* container) {
   int ch;
   if (!container || !container->fImmediateUse) {
      auto& textBuffer = (!container || !container->ptbBuffer) ? tbCurrentToken : *container->ptbBuffer;
      while (isdigit(ch = buffer.getChar().getInternal())
            || (('a' <= ch) && (ch <= 'f')) || (('A' <= ch) && (ch <= 'F')))
         textBuffer << (char) buffer.extractChar();
      if (container && container->pssSubString && !container->fImmediateUse)
         tbCurrentToken >> *container->pssSubString;
   }
   else {
      if (container->fCompare)
         while (isdigit(ch = buffer.getChar().getInternal())
               || (('a' <= ch) && (ch <= 'f')) || (('A' <= ch) && (ch <= 'F'))) {
            char extractedChar = (char) buffer.extractChar();
            if (!container->fDifferent)
               container->fDifferent = container->pssSubString->length() == 0
                     || (char) container->pssSubString->extractChar() != extractedChar;
         }
      else if (container->pssSubString) {
         *container->pssSubString = buffer;
         container->pssSubString->setUpperClosed();
         int length = 0;
         while (isdigit(ch = buffer.getChar().getInternal())
               || (('a' <= ch) && (ch <= 'f')) || (('A' <= ch) && (ch <= 'F'))) {
            ++length;
            buffer.advance();
         }
         container->pssSubString->setLength(length);
      }
      else
         while (isdigit(ch = buffer.getChar().getInternal())
               || (('a' <= ch) && (ch <= 'f')) || (('A' <= ch) && (ch <= 'F')))
            buffer.advance();
   }
   if (ch == ';') {
      buffer.advance();
      if (!container) {
         SString integer;
         tbCurrentToken >> integer;
         tToken = Token(new CallCharToken(integer.readHexaInteger()));
         rpmFstState = nullptr;
         return RRHasToken;
      };
      rpmFstState = nullptr;
      return RRContinue;
   };
   if (ch == EOF)
      return RRNeedChars;
   throw EReadError();
}

GenericLexer::ReadResult
GenericLexer::readCharsDoctype(SubString& buffer) {
   int ch;
   if ((ch = buffer.getChar().getInternal()) == EOF)
      return RRNeedChars;
   else if (isalpha(ch) || (ch == '_') || (ch == ':')) {
      ComparisonResult result;
      if ((result = buffer.compareSub("SYSTEM")) != CRNonComparable) {
         if (result == CRLess)
            return RRNeedChars;
         buffer.advance(sizeof("SYSTEM")-1);
         tToken = Token(AbstractToken::TSystem);
         return RRHasToken;
      };
      if ((result = buffer.compareSub("PUBLIC")) != CRNonComparable) {
         if (result == CRLess)
            return RRNeedChars;
         buffer.advance(sizeof("PUBLIC")-1);
         tToken = Token(AbstractToken::TPublic);
         return RRHasToken;
      };
      if (oOptions == OFullToken) {
         tbCurrentToken << (char) ch;
         buffer.advance();
      }
      chContext = (char) AbstractToken::TName;
      rpmFstState = &GenericLexer::readCharsEndName;
      if (oOptions == OPartialToken) {
         tToken = Token(AbstractToken::TName);
         return RRHasToken;
      }
      return readCharsEndName(buffer);
   }
   else if (ch == '<') {
      if ((ch = buffer.getChar(1).getInternal()) == EOF)
         return RRNeedChars;
      if (ch != '!')
         throw EReadError();
      ComparisonResult result;
      if ((((result = buffer.compareSub("<!ELEMENT")) != CRNonComparable) && ((chContext = AbstractToken::TDeclelement) != 0))
          || (((result = buffer.compareSub("<!ATTLIST")) != CRNonComparable) && ((chContext = AbstractToken::TDecllistattribute) != 0))
          || (((result = buffer.compareSub("<!ENTITY")) != CRNonComparable) && ((chContext = AbstractToken::TDeclentity) != 0))
          || (((result = buffer.compareSub("<!NOTATION")) != CRNonComparable) && ((chContext = AbstractToken::TDeclnotation) != 0))) {
         if (result == CRLess)
            return RRNeedChars;
         buffer.advance((chContext == AbstractToken::TDeclelement)
               ? (sizeof("<!ELEMENT")-1) : ((chContext == AbstractToken::TDecllistattribute)
               ? (sizeof("<!ATTLIST")-1) : ((chContext == AbstractToken::TDeclentity)
               ? (sizeof("<!ENTITY")-1)
               : (sizeof("<!NOTATION")-1))));
         rpmFstState = &GenericLexer::readCharsEndDeclaration;
         if (oOptions == OPartialToken) {
            tToken = Token((AbstractToken::Type) chContext);
            return RRHasToken;
         }
         return readCharsEndDeclaration(buffer);
      }
      else if ((result = buffer.compareSub("<!--")) != CRNonComparable) {
         if (result == CRLess)
            return RRNeedChars;
         buffer.advance(sizeof("<!--")-1);
         rpmFstState = &GenericLexer::readCharsEndComment;
         if (oOptions == OPartialToken) {
            tToken = Token(AbstractToken::TComment);
            return RRHasToken;
         }
         return readCharsEndComment(buffer);
      }
      else
         throw EReadError();
   }
   else if (isspace(ch)) {
      rpmFstState = &GenericLexer::readCharsEndSeparator;
      if (oOptions == OPartialToken) {
         tToken = Token(AbstractToken::TSeparator);
         return RRHasToken;
      }
      return readCharsEndSeparator(buffer);
   }
   else if ((ch == ']') || (ch == '[')) {
      buffer.advance();
      tToken = Token((ch == '[') ? AbstractToken::TOpsqbrace : AbstractToken::TClsqbrace);
      return RRHasToken;
   }
   else if ((ch == '\"') || (ch == '\'')) {
      chContext = (char) buffer.extractChar();
      rpmFstState = &GenericLexer::readCharsEndLitteral;
      if (oOptions == OPartialToken) {
         tToken = Token(AbstractToken::TLitteral);
         return RRHasToken;
      }
      return readCharsEndLitteral(buffer);
   }
   else if (ch == '%') {
      buffer.advance();
      chContext = AbstractToken::TCallep;
      rpmFstState = &GenericLexer::readCharsEndEntity;
      if (oOptions == OPartialToken) {
         tToken = Token(AbstractToken::TCallep);
         return RRHasToken;
      }
      return readCharsEndEntity(buffer);
   }
   else if (ch == '>') {
      buffer.advance();
      tToken = Token(AbstractToken::TGreater);
      rpmState = &GenericLexer::readCharsInitial;
      return RRHasToken;
   };
   throw EReadError();
}

GenericLexer::ReadResult
GenericLexer::readCharsElementHeader(SubString& buffer) {
   int ch;
   if ((ch = buffer.getChar().getInternal()) == EOF)
      return RRNeedChars;
   else if (isalpha(ch) || (ch == '_') || (ch == ':')) {
      if (oOptions == OFullToken)
         tbCurrentToken << (char) buffer.extractChar();
      chContext = (char) AbstractToken::TName;
      rpmFstState = &GenericLexer::readCharsEndName;
      if (oOptions == OPartialToken) {
         tToken = Token(AbstractToken::TName);
         return RRHasToken;
      }
      return readCharsEndName(buffer);
   }
   else if (isspace(ch)) {
      buffer.advance();
      rpmFstState = &GenericLexer::readCharsEndSeparator;
      if (oOptions == OPartialToken) {
         tToken = Token(AbstractToken::TSeparator);
         return RRHasToken;
      }
      return readCharsEndSeparator(buffer);
   }
   else if (ch == '=') {
      buffer.advance();
      tToken = Token(AbstractToken::TEqual);
      return RRHasToken;
   }
   else if ((ch == '\"') || (ch == '\'')) {
      buffer.advance();
      chContext = (char) ch;
      rpmFstState = &GenericLexer::readCharsEndLitteral;
      if (oOptions == OPartialToken) {
         tToken = Token(AbstractToken::TLitteral);
         return RRHasToken;
      }
      return readCharsEndLitteral(buffer);
   }
   else if (ch == '/') {
      if ((ch = buffer.getChar(1).getInternal()) == EOF)
         return RRNeedChars;
      if (ch == '>') {
         buffer.advance(2);
         tToken = Token(AbstractToken::TSlashgreater);
         rpmState = &GenericLexer::readCharsBody;
         return RRHasToken;
      };
      throw EReadError();
   }
   else if (ch == '>') {
      buffer.advance();
      tToken = Token(AbstractToken::TGreater);
      rpmState = &GenericLexer::readCharsBody;
      return RRHasToken;
   };
   throw EReadError();
}

GenericLexer::ReadResult
GenericLexer::readCharsElementFoot(SubString& buffer) {
   int ch;
   if ((ch = buffer.getChar().getInternal()) == EOF)
      return RRNeedChars;
   else if (isspace(ch)) {
      buffer.advance();
      rpmFstState = &GenericLexer::readCharsEndSeparator;
      if (oOptions == OPartialToken) {
         tToken = Token(AbstractToken::TSeparator);
         return RRHasToken;
      }
      return readCharsEndSeparator(buffer);
   }
   else if (ch == '>') {
      buffer.advance();
      tToken = Token(AbstractToken::TGreater);
      rpmState = &GenericLexer::readCharsBody;
      return RRHasToken;
   };
   throw EReadError();
}

GenericLexer::ReadResult
GenericLexer::readCharsEndTextualData(SubString& buffer, SubStringContainer* container) {
   SetOfChars endRead;
   endRead.add('&').add('<');
   int endTextualData = buffer.scanPos(endRead);
   if (endTextualData >= 0) {
      STG::SubString textualData = STG::SubString(buffer);
      textualData.setUpperClosed().setLength(endTextualData);
      if (!container || !container->fImmediateUse) {
         ((!container || !container->ptbBuffer) ? tbCurrentToken : *container->ptbBuffer) << textualData;
         if (container && container->pssSubString && !container->fImmediateUse)
            tbCurrentToken >> *container->pssSubString;
      }
      else if (container && container->pssSubString) {
         if (container->fCompare) {
            container->fDifferent = container->pssSubString->length() != textualData.length()
                  || *container->pssSubString != textualData;
            if (!container->fDifferent)
               container->pssSubString->setToEnd();
         }
         else if (container->pssSubString)
            *container->pssSubString = textualData;
      }

      buffer.advance(endTextualData);
      if (!container) {
         TextualDataToken* token = nullptr;
         tToken = Token(token = new TextualDataToken());
         tbCurrentToken >> token->text();
         if (buffer.getChar() == '<')
            token->setTextFinal();
         rpmFstState = nullptr;
         return RRHasToken;
      };

      if (buffer.getChar() == '<')
         tToken = Token(AbstractToken::TFinalTextualData);
      rpmFstState = nullptr;
      return RRContinue;
   };
   if (!container || !container->fImmediateUse)
      ((!container || !container->ptbBuffer) ? tbCurrentToken : *container->ptbBuffer) << buffer;
   else if (container && container->pssSubString) {
      if (container->fCompare) {
         int referenceLength = container->pssSubString->length(), bufferLength = buffer.length();
         if (referenceLength < bufferLength)
            container->fDifferent = true;
         else {
            container->fDifferent = container->pssSubString->compare(buffer, bufferLength) != CREqual;
            if (!container->fDifferent)
               container->pssSubString->advance(bufferLength);
         };
      }
      else if (container->pssSubString) {
         *container->pssSubString = buffer;
         container->pssSubString->setUpperClosed();
      }
      buffer.setToEnd();
   }
   return RRNeedChars;
}

GenericLexer::ReadResult
GenericLexer::readCharsEndCdata(SubString& buffer, SubStringContainer* container) {
   int endCData = buffer.scanPos("]]>");
   SubString cdata(buffer);
   cdata.setUpperClosed();
   if (endCData >= 0) {
      cdata.setLength(endCData);
      if (!container || !container->fImmediateUse) {
         ((!container || !container->ptbBuffer) ? tbCurrentToken : *container->ptbBuffer) << cdata;
         if (container && container->pssSubString && !container->fImmediateUse)
            tbCurrentToken >> *container->pssSubString;
      }
      else if (container && container->pssSubString) {
         if (container->fCompare) {
            container->fDifferent = container->pssSubString->length() != cdata.length()
                  || *container->pssSubString != cdata;
            if (!container->fDifferent)
               container->pssSubString->setToEnd();
         }
         else if (container->pssSubString)
            *container->pssSubString = cdata;
      }

      buffer.advance(endCData+3);
      if (!container) {
         CDataToken* token = nullptr;
         tToken = Token(token = new CDataToken());
         tbCurrentToken >> token->data();
         rpmFstState = nullptr;
         return RRHasToken;
      };
      rpmFstState = nullptr;
      return RRContinue;
   };

   cdata.restrictToLength(2);
   if (!container || !container->fImmediateUse)
      ((!container || !container->ptbBuffer) ? tbCurrentToken : *container->ptbBuffer) << cdata;
   else if (container && container->pssSubString) {
      if (container->fCompare) {
         int referenceLength = container->pssSubString->length(), cdataLength = cdata.length();
         if (referenceLength < cdataLength)
            container->fDifferent = true;
         else {
            container->fDifferent = container->pssSubString->compare(cdata, cdataLength) != CREqual;
            if (!container->fDifferent)
               container->pssSubString->advance(cdataLength);
         };
      }
      else
         *container->pssSubString = cdata;
   }
   buffer.setToEnd(cdata);
   return RRNeedChars;
}

GenericLexer::ReadResult
GenericLexer::readCharsBody(SubString& buffer) {
   int ch;
   if ((ch = buffer.getChar().getInternal()) == EOF)
      return RRNeedChars;
   else if ((ch != '&') && (ch != '<')) {
      if (oOptions == OFullToken)
         tbCurrentToken << (char) buffer.extractChar();
      rpmFstState = &GenericLexer::readCharsEndTextualData;
      if (oOptions == OPartialToken) {
         tToken = Token(AbstractToken::TTextualData);
         return RRHasToken;
      }
      return readCharsEndTextualData(buffer);
   }
   else if (ch == '<') {
      if ((ch = buffer.getChar(1).getInternal()) == EOF)
         return RRNeedChars;
      ComparisonResult result;
      if ((result = buffer.compareSub("<![CDATA[")) != CRNonComparable) {
         if (result == CRLess)
            return RRNeedChars;
         buffer.advance(sizeof("<![CDATA[")-1);
         rpmFstState = &GenericLexer::readCharsEndCdata;
         if (oOptions == OPartialToken) {
            tToken = Token(AbstractToken::TCData);
            return RRHasToken;
         }
         return readCharsEndCdata(buffer);
      };
      if ((result = buffer.compareSub("<!--")) != CRNonComparable) {
         if (result == CRLess)
            return RRNeedChars;
         buffer.advance(sizeof("<!--")-1);
         rpmFstState = &GenericLexer::readCharsEndComment;
         if (oOptions == OPartialToken) {
            tToken = Token(AbstractToken::TComment);
            return RRHasToken;
         }
         return readCharsEndComment(buffer);
      };
      if (ch == '/') {
         if ((ch = buffer.getChar(2).getInternal()) == EOF)
            return RRNeedChars;
         if (oOptions == OFullToken) {
            tbCurrentToken << (char) ch;
            buffer.advance(3);
         }
         else
            buffer.advance(2);
         chContext = AbstractToken::TNodeBeginEndName;
         rpmState = &GenericLexer::readCharsElementFoot;
         rpmFstState = &GenericLexer::readCharsEndName;
         if (oOptions == OPartialToken) {
            tToken = Token(AbstractToken::TNodeBeginEndName);
            return RRHasToken;
         }
         return readCharsEndName(buffer);
      };
      if (isalpha(ch) || (ch == '_') || (ch == ':')) {
         if (oOptions == OFullToken) {
            buffer.advance(2);
            tbCurrentToken << (char) ch;
         }
         else
            buffer.advance();
         chContext = AbstractToken::TNodeBeginName;
         rpmState = &GenericLexer::readCharsElementHeader;
         rpmFstState = &GenericLexer::readCharsEndName;
         if (oOptions == OPartialToken) {
            tToken = Token(AbstractToken::TNodeBeginName);
            return RRHasToken;
         }
         return readCharsEndName(buffer);
      };
      throw EReadError();
   }
   else if (ch == '&') {
      if ((ch = buffer.getChar(1).getInternal()) == EOF)
         return RRNeedChars;
      if (ch == '#') {
         if ((ch = buffer.getChar(2).getInternal()) == EOF)
            return RRNeedChars;
         if (ch == 'x') {
            if ((ch = buffer.getChar(3).getInternal()) == EOF)
               return RRNeedChars;
            if (isdigit(ch) || (('a' <= ch) && (ch <= 'f')) || (('A' <= ch) && (ch <= 'F'))) {
               if (oOptions == OFullToken) {
                  tbCurrentToken << (char) ch;
                  buffer.advance(4);
               }
               else
                  buffer.advance(3);
               rpmFstState = &GenericLexer::readCharsEndCallHexaChar;
               if (oOptions == OPartialToken) {
                  tToken = Token(AbstractToken::TCallhexachar);
                  return RRHasToken;
               }
               return readCharsEndCallHexaChar(buffer);
            };
            throw EReadError();
         }
         else if (isdigit(ch)) {
            if (oOptions == OFullToken) {
               tbCurrentToken << (char) ch;
               buffer.advance(3);
            }
            else
               buffer.advance(2);
            rpmFstState = &GenericLexer::readCharsEndCallChar;
            if (oOptions == OPartialToken) {
               tToken = Token(AbstractToken::TCallchar);
               return RRHasToken;
            }
            return readCharsEndCallChar(buffer);
         };
         throw EReadError();
      };
      if (isalpha(ch) || (ch == '_') || (ch == ':')) {
         buffer.advance();
         if (oOptions == OFullToken)
            tbCurrentToken << (char) buffer.extractChar();
         chContext = AbstractToken::TCallentity;
         rpmFstState = &GenericLexer::readCharsEndEntity;
         if (oOptions == OPartialToken) {
            tToken = Token(AbstractToken::TCallentity);
            return RRHasToken;
         }
         return readCharsEndEntity(buffer);
      };
      throw EReadError();
   };
   throw EReadError();
}

void
GenericLexer::setToXMLDeclaration()
   {  AssumeCondition(rpmState == &GenericLexer::readCharsDeclxml) }

void
GenericLexer::setToDoctype()
   {  AssumeCondition(rpmState == &GenericLexer::readCharsDoctype) }

void
GenericLexer::setToElementHeader()
   {  AssumeCondition(rpmState == &GenericLexer::readCharsElementHeader) }

void
GenericLexer::setToInitial()
   {  AssumeCondition((rpmState == &GenericLexer::readCharsInitial)
         || (rpmState == &GenericLexer::readCharsBody))
      rpmState = &GenericLexer::readCharsInitial;
   }

void
GenericLexer::setToElementFoot()
   {  AssumeCondition(rpmState == &GenericLexer::readCharsElementFoot) }

void
GenericLexer::setToBody()
   {  AssumeCondition(rpmState == &GenericLexer::readCharsBody) }

}} // end of namespace STG::XML

