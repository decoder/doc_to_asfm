/**************************************************************************/
/*                                                                        */
/*  Copyright (C) 2014-2019                                               */
/*    CEA (Commissariat a l'Energie Atomique et aux Energies              */
/*         Alternatives)                                                  */
/*                                                                        */
/*  you can redistribute it and/or modify it under the terms of the GNU   */
/*  Lesser General Public License as published by the Free Software       */
/*  Foundation, version 2.1.                                              */
/*                                                                        */
/*  It is distributed in the hope that it will be useful,                 */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU Lesser General Public License for more details.                   */
/*                                                                        */
/*  See the GNU Lesser General Public License version 2.1                 */
/*  for more details (enclosed in the file LICENSE).                      */
/*                                                                        */
/**************************************************************************/

/////////////////////////////////
//
// Library   : XML
// Unit      : Parser
// File      : XMLParser.cpp
// Description :
//   Implementation of a parser of XML document.
//

#include "XML/XMLParser.h"

namespace STG {}

#include "Collection/Collection.template"
#include "TString/SetOfChars.template"
#include "TString/StringRep.template"
#include "TString/SubString.template"
#include "TString/SString.template"

namespace STG { namespace XML {

/*******************************************/
/* Implementation of the class BasicWriter */
/*******************************************/

BasicWriter::State::WriteMethod
BasicWriter::State::getMethod(int code) {
   switch(code) {
      case 0: return nullptr;
      case 1: return &State::writeOpenStartElement;
      case 2: return &State::writeCloseStartElement;
      case 3: return &State::writeCloseStartEndElement;
      case 4: return &State::writeAddAttribute;
      case 5: return &State::writeEndElement;
      case 6: return &State::writeAddText;
      case 7: return &State::writeAddComment;
      case 8: return &State::writeAddEP;
      case 9: return &State::writeAddEntity;
      case 10: return &State::writeAddChar;
      case 11: return &State::writeAddData;
      default: AssumeUncalled
   };
   return nullptr;
}

int
BasicWriter::State::getCode(WriteMethod method) {
   if (method == nullptr)
      return 0;
   if (method == &State::writeOpenStartElement)
      return 1;
   if (method == &State::writeCloseStartElement)
      return 2;
   if (method == &State::writeCloseStartEndElement)
      return 3;
   if (method == &State::writeAddAttribute)
      return 4;
   if (method == &State::writeEndElement)
      return 5;
   if (method == &State::writeAddText)
      return 6;
   if (method == &State::writeAddComment)
      return 7;
   if (method == &State::writeAddEP)
      return 8;
   if (method == &State::writeAddEntity)
      return 9;
   if (method == &State::writeAddChar)
      return 10;
   if (method == &State::writeAddData)
      return 11;
   AssumeUncalled
   return 0;
}

void
BasicWriter::State::setFromCode(int codeParse) {
   wmEvent = getMethod(codeParse & ((1 << 4)-1));
   codeParse >>= 4;
   wmNextEvent = getMethod(codeParse & ((1 << 4)-1));
   codeParse >>= 4;
   setPointField(codeParse & 7);
   codeParse >>= 3;
   setFollowField(codeParse & 1);
}

int
BasicWriter::State::getCode() const {
   return getCode(wmEvent) | (getCode(wmNextEvent) << 4)
      | (queryPointField() << 8) | (queryFollowField() << 11);
}

void
BasicWriter::State::read(ISBase& in, const FormatParameters& params) {
   bool isRaw = params.isRaw();
   int codeParse;
   in.read(codeParse, isRaw);
   setFromCode(codeParse);
   
   WriteMethod data = wmEvent;
   if (wmEvent == &State::writeCloseStartElement)
      data = wmNextEvent;
   else if (wmNextEvent != nullptr)
      throw STG::EReadError();

   if (!hasFollowField() && isValid() && ((data == &State::writeOpenStartElement)
         || (data == &State::writeEndElement) || (data == &State::writeAddText)
         || (data == &State::writeAddComment) || (data == &State::writeAddEP)
         || (data == &State::writeAddEntity) || (data == &State::writeAddData)
         || (data == &State::writeAddChar))) {
      if (!isRaw)
         in.assume(' ');
      in.read(uWriteSpaces, isRaw);
   };
   if ((data == &State::writeOpenStartElement) || (data == &State::writeEndElement)
         || (data == &State::writeAddText) || (data == &State::writeAddComment)
         || (data == &State::writeAddEP) || (data == &State::writeAddEntity)
         || (data == &State::writeAddData)) {
      if (!isRaw)
         in.assume(' ');
      in.read(ssValue.asPersistent(), isRaw);
   }
   else if (data == &State::writeAddAttribute) {
      if (!isRaw)
         in.assume(' ');
      in.read(ssAttribute.asPersistent(), isRaw);
      if (!isRaw)
         in.assume(' ');
      in.read(ssValue.asPersistent(), isRaw);
   }
   else if (data == &State::writeAddChar) {
      if (!isRaw)
         in.assume(' ');
      in.read(uChar, isRaw);
   };
}

void
BasicWriter::State::write(OSBase& out, const FormatParameters& params) const {
   bool isRaw = params.isRaw();
   out.write(getCode(), isRaw);

   WriteMethod data = wmEvent;
   if (wmEvent == &State::writeCloseStartElement)
      data = wmNextEvent;
   else if (wmNextEvent != nullptr)
      throw STG::EWriteError();

   if (!hasFollowField() && isValid() && ((data == &State::writeOpenStartElement)
         || (data == &State::writeEndElement) || (data == &State::writeAddText)
         || (data == &State::writeAddComment) || (data == &State::writeAddEP)
         || (data == &State::writeAddEntity) || (data == &State::writeAddData)
         || (data == &State::writeAddChar))) {
      if (!isRaw)
         out.put(' ');
      out.write(uWriteSpaces, isRaw);
   };
   if ((data == &State::writeOpenStartElement) || (data == &State::writeEndElement)
         || (data == &State::writeAddText) || (data == &State::writeAddComment)
         || (data == &State::writeAddEP) || (data == &State::writeAddEntity)
         || (data == &State::writeAddData)) {
      if (!isRaw)
         out.put(' ');
      out.write(ssValue.asPersistent(), isRaw);
   }
   else if (data == &State::writeAddAttribute) {
      if (!isRaw)
         out.put(' ');
      out.write(ssAttribute.asPersistent(), isRaw);
      if (!isRaw)
         out.put(' ');
      out.write(ssValue.asPersistent(), isRaw);
   }
   else if (data == &State::writeAddChar) {
      if (!isRaw)
         out.put(' ');
      out.write(uChar, isRaw);
   };
}

BasicWriter::WriteResult
BasicWriter::State::writeTextSpaces(SubString& out) {
   if (!hasFollowField()) {
      while (uWriteSpaces > 0) {
         if (out.getPlace() < 2)
            return WRNeedPlace;
         out.cat("  ");
         --uWriteSpaces;
      };
      // mergeFollowField(1);
   };
   return WRNeedWrite;
}

BasicWriter::WriteResult
BasicWriter::State::writeText(SubString& text, SubString& out) {
   WriteResult result = WRNeedWrite;
   if (out.getPlace() < text.length()) {
      SubString first(text, 0, out.getPlace()-1);
      out.cat(first);
      text.setToEnd(first);
      result = WRNeedPlace;
   }
   else
      out.cat(text);
   return result;
}

BasicWriter::WriteResult
BasicWriter::State::writeOpenStartElement(SubString& out, bool isPretty) {
   AssumeCondition((queryPointField() >= 0) && (queryPointField() <= 3) && (wmNextEvent == nullptr))
   if (queryPointField() == 0) {
      if (!isPretty)
         setPointField(2);
      else {
         if (hasFollowField()) {
            if (out.getPlace() < 1)
               return WRNeedPlace;
            clearFollowField();
            out.cat('\n');
         };
         uWriteSpaces = uDocumentLevel;
         setPointField(1);
      };
   };
   if (queryPointField() == 1) {
      if (writeTextSpaces(out) == WRNeedPlace)
         return WRNeedPlace;
      setPointField(2);
   };
   if (queryPointField() == 2) {
      if (out.getPlace() < 1)
         return WRNeedPlace;
      out.cat('<');
      setPointField(3);
   };
   if (queryPointField() == 3) {
      if (writeText(ssValue, out) == WRNeedPlace)
         return WRNeedPlace;
   };
   clearPointField();
   wmEvent = nullptr;
   return WRNeedEvent;
}

BasicWriter::WriteResult
BasicWriter::State::writeCloseStartElement(SubString& out, bool isPretty) {
   AssumeCondition(queryPointField() == 0)
   if (isPretty) {
      if (out.getPlace() < 2)
         return WRNeedPlace;
      out.cat(">\n");
   }
   else {
      if (out.getPlace() < 1)
         return WRNeedPlace;
      out.cat('>');
   };
   ++uDocumentLevel;
   if (wmNextEvent != nullptr) {
      wmEvent = wmNextEvent;
      wmNextEvent = nullptr;
      return WRNeedWrite;
   };
   wmEvent = nullptr;
   return WRNeedEvent;
}

BasicWriter::WriteResult
BasicWriter::State::writeCloseStartEndElement(SubString& out, bool isPretty) {
   AssumeCondition((queryPointField() == 0) && (wmNextEvent == nullptr))
   if (isPretty) {
      if (out.getPlace() < 3)
         return WRNeedPlace;
      out.cat("/>\n");
   }
   else {
      if (out.getPlace() < 2)
         return WRNeedPlace;
      out.cat("/>");
   };
   wmEvent = nullptr;
   return WRNeedEvent;
}

BasicWriter::WriteResult
BasicWriter::State::writeAddAttribute(SubString& out, bool) {
   AssumeCondition((queryPointField() <= 4) && (wmNextEvent == nullptr))
   if (queryPointField() == 0) {
      if (out.getPlace() < 1)
         return WRNeedPlace;
      out.cat(' ');
      setPointField(1);
   };
   if (queryPointField() == 1) {
      if (writeText(ssAttribute, out) == WRNeedPlace)
         return WRNeedPlace;
      setPointField(2);
   };
   if (queryPointField() == 2) {
      if (out.getPlace() < 2)
         return WRNeedPlace;
      out.cat("=\"");
      setPointField(3);
   };
   if (queryPointField() == 3) {
      if (writeText(ssValue, out) == WRNeedPlace)
         return WRNeedPlace;
      setPointField(4);
   };
   if (queryPointField() == 4) {
      if (out.getPlace() < 1)
         return WRNeedPlace;
      out.cat('\"');
   };
   clearPointField();
   wmEvent = nullptr;
   return WRNeedEvent;
}

BasicWriter::WriteResult
BasicWriter::State::writeEndElement(SubString& out, bool isPretty) {
   AssumeCondition((queryPointField() <= 4) && (wmNextEvent == nullptr))
   if (queryPointField() == 0) {
      if (!isPretty)
         setPointField(2);
      else {
         if (hasFollowField()) {
            if (out.getPlace() < 1)
               return WRNeedPlace;
            clearFollowField();
            out.cat('\n');
         };
         uWriteSpaces = uDocumentLevel-1;
         setPointField(1);
      };
   };
   if (queryPointField() == 1) {
      if (writeTextSpaces(out) == WRNeedPlace)
         return WRNeedPlace;
      setPointField(2);
   };
   if (queryPointField() == 2) {
      if (out.getPlace() < 2)
         return WRNeedPlace;
      out.cat("</");
      setPointField(3);
   };
   if (queryPointField() == 3) {
      if (writeText(ssValue, out) == WRNeedPlace)
         return WRNeedPlace;
      setPointField(4);
   };
   if (queryPointField() == 4) {
      if (isPretty) {
         if (out.getPlace() < 2)
            return WRNeedPlace;
         out.cat(">\n");
      }
      else {
         if (out.getPlace() < 1)
            return WRNeedPlace;
         out.cat('>');
      };
      // --uDocumentLevel; // done by closeElement
   };
   clearPointField();
   wmEvent = nullptr;
   return WRNeedEvent;
}

BasicWriter::WriteResult
BasicWriter::State::writeAddText(SubString& out, bool isPretty) {
   AssumeCondition((queryPointField() <= 2) && (wmNextEvent == nullptr))
   if (queryPointField() == 0) {
      if (isPretty && !hasFollowField()) {
         uWriteSpaces = uDocumentLevel;
         setPointField(1);
      }
      else
         setPointField(2);
   };
   if (queryPointField() == 1) {
      if (writeTextSpaces(out) == WRNeedPlace)
         return WRNeedPlace;
      setPointField(2);
   };
   if (queryPointField() == 2) {
      if (writeText(ssValue, out) == WRNeedPlace)
         return WRNeedPlace;
   };
   mergeFollowField(1);
   clearPointField();
   wmEvent = nullptr;
   return WRNeedEvent;
}

BasicWriter::WriteResult
BasicWriter::State::writeAddComment(SubString& out, bool isPretty) {
   AssumeCondition((queryPointField() <= 4) && (wmNextEvent == nullptr))
   if (queryPointField() == 0) {
      if (isPretty) {
         if (hasFollowField()) {
            if (out.getPlace() < 1)
               return WRNeedPlace;
            out.cat('\n');
            clearFollowField();
         };
         uWriteSpaces = uDocumentLevel;
         setPointField(1);
      }
      else
         setPointField(2);
   };
   if (queryPointField() == 1) {
      if (writeTextSpaces(out) == WRNeedPlace)
         return WRNeedPlace;
      setPointField(2);
   };
   if (queryPointField() == 2) {
      if (out.getPlace() < 5)
         return WRNeedPlace;
      out.cat("<!-- ");
      setPointField(3);
   };
   if (queryPointField() == 3) {
      if (writeText(ssValue, out) == WRNeedPlace)
         return WRNeedPlace;
      setPointField(4);
   };
   if (queryPointField() == 4) {
      if (isPretty) {
         if (out.getPlace() < 5)
            return WRNeedPlace;
         out.cat(" -->\n");
      }
      else {
         if (out.getPlace() < 4)
            return WRNeedPlace;
         out.cat(" -->");
      };
   };
   clearPointField();
   wmEvent = nullptr;
   return WRNeedEvent;
}

BasicWriter::WriteResult
BasicWriter::State::writeAddEP(SubString& out, bool isPretty) {
   AssumeCondition((queryPointField() <= 4) && (wmNextEvent == nullptr))
   if (queryPointField() == 0) {
      if (isPretty && !hasFollowField()) {
         uWriteSpaces = uDocumentLevel;
         setPointField(1);
      }
      else
         setPointField(2);
   };
   if (queryPointField() == 1) {
      if (writeTextSpaces(out) == WRNeedPlace)
         return WRNeedPlace;
      setPointField(2);
   };
   if (queryPointField() == 2) {
      if (out.getPlace() < 1)
         return WRNeedPlace;
      out.cat('%');
      setPointField(3);
   };
   if (queryPointField() == 3) {
      if (writeText(ssValue, out) == WRNeedPlace)
         return WRNeedPlace;
      setPointField(4);
   };
   if (queryPointField() == 4) {
      if (out.getPlace() < 1)
         return WRNeedPlace;
      out.cat(';');
   };
   mergeFollowField(1);
   clearPointField();
   wmEvent = nullptr;
   return WRNeedEvent;
}

BasicWriter::WriteResult
BasicWriter::State::writeAddEntity(SubString& out, bool isPretty) {
   AssumeCondition((queryPointField() <= 4) && (wmNextEvent == nullptr))
   if (queryPointField() == 0) {
      if (isPretty && !hasFollowField()) {
         uWriteSpaces = uDocumentLevel;
         setPointField(1);
      }
      else
         setPointField(2);
   };
   if (queryPointField() == 1) {
      if (writeTextSpaces(out) == WRNeedPlace)
         return WRNeedPlace;
      setPointField(2);
   };
   if (queryPointField() == 2) {
      if (out.getPlace() < 1)
         return WRNeedPlace;
      out.cat('&');
      setPointField(3);
   };
   if (queryPointField() == 3) {
      if (writeText(ssValue, out) == WRNeedPlace)
         return WRNeedPlace;
      setPointField(4);
   };
   if (queryPointField() == 4) {
      if (out.getPlace() < 1)
         return WRNeedPlace;
      out.cat(';');
   };
   mergeFollowField(1);
   clearPointField();
   wmEvent = nullptr;
   return WRNeedEvent;
}

BasicWriter::WriteResult
BasicWriter::State::writeAddChar(SubString& out, bool isPretty) {
   AssumeCondition((queryPointField() <= 4) && (wmNextEvent == nullptr))
   if (queryPointField() == 0) {
      if (isPretty && !hasFollowField()) {
         uWriteSpaces = uDocumentLevel;
         setPointField(1);
      }
      else
         setPointField(2);
   };
   if (queryPointField() == 1) {
      if (writeTextSpaces(out) == WRNeedPlace)
         return WRNeedPlace;
      setPointField(2);
   };
   if (queryPointField() == 2) {
      if (out.getPlace() < 1)
         return WRNeedPlace;
      out.cat('#');
      setPointField(3);
   };
   if (queryPointField() == 3) {
      SString convert = SString();
      convert.setInteger(uChar);
      if (out.getPlace() < convert.length())
         return WRNeedPlace;
      out.catInteger(convert);
      setPointField(4);
   };
   if (queryPointField() == 4) {
      if (out.getPlace() < 1)
         return WRNeedPlace;
      out.cat(' ');
   };
   mergeFollowField(1);
   clearPointField();
   wmEvent = nullptr;
   return WRNeedEvent;
}

BasicWriter::WriteResult
BasicWriter::State::writeAddData(SubString& out, bool isPretty) {
   AssumeCondition((queryPointField() <= 4) && (wmNextEvent == nullptr))
   if (queryPointField() == 0) {
      if (isPretty && !hasFollowField()) {
         uWriteSpaces = uDocumentLevel;
         setPointField(1);
      }
      else
         setPointField(2);
   };
   if (queryPointField() == 1) {
      if (writeTextSpaces(out) == WRNeedPlace)
         return WRNeedPlace;
      setPointField(2);
   };
   if (queryPointField() == 2) {
      if (out.getPlace() < 9)
         return WRNeedPlace;
      out.cat("<![CDATA[");
      setPointField(3);
   };
   if (queryPointField() == 3) {
      if (writeText(ssValue, out) == WRNeedPlace)
         return WRNeedPlace;
      setPointField(4);
   };
   if (queryPointField() == 4) {
      if (out.getPlace() < 3)
         return WRNeedPlace;
      out.cat("]]>");
   };
   mergeFollowField(1);
   clearPointField();
   wmEvent = nullptr;
   return WRNeedEvent;
}


/*******************************************/
/* Implementation of the class BasicWriter */
/*******************************************/

void
BasicWriter::write(OSBase& out) {
   SString buffer(5000);
   WriteResult result = WRNeedEvent;
   while (result != WRFinished) {
      result = writeChunk(buffer, (result == WRNeedPlace) ? WRNeedWrite : result);
      out.writeall(buffer.asPersistent());
      buffer.clear();
   };
}

BasicWriter::WriteResult
BasicWriter::writeEvent(SubString& out, bool isReentry) {
   WriteResult result = WRNeedWrite;
   if (!isReentry && !sState.isValid())
      while ((result = getNextEvent()) == WRNeedEvent);
   if (result == WRNeedWrite)
      result = sState.write(out, isPretty());
   else if (result == WRFinished)
      result = sState.completeWrite(out, isPretty());
   return result;
}

BasicWriter::WriteResult
BasicWriter::writeChunk(SubString& out, WriteResult result) {
   while ((result != WRFinished) && (result != WRNeedPlace)) {
      if (result == WRNeedWrite)
         result = sState.write(out, isPretty());
      if (result == WRNeedEvent)
         result = getNextEvent();
   };
   if (result == WRFinished) {
      result = sState.completeWrite(out, isPretty());
      while ((result != WRFinished) && (result != WRNeedPlace)) {
         if (result == WRNeedEvent)
            result = getNextEvent();
         if (result == WRNeedWrite)
            result = sState.completeWrite(out, isPretty());
      };
   };
   return result;
}

/*�************************************************/
/* Implementation of the class BasicParser::State */
/**************************************************/

void
BasicParser::State::ParseState::setFromCode(int codeParse, int level, int& needBits) {
   if (level == 0) {
      rpmReadMethod = (codeParse & 1) ? &BasicParser::parseDocument : &BasicParser::parseError;
      uPoint = (codeParse >> 1) & 7;
      needBits = 4;
   }
   else if (level == 1) {
      int codeMethod = codeParse & 3;
      rpmReadMethod = (codeMethod == 0) ? &BasicParser::parseDeclXml
         : ((codeMethod == 1) ? &BasicParser::parseDoctype : &BasicParser::parseNode);
      uPoint = (codeMethod >> 2) & 15;
      needBits = 6;
   };
}

int
BasicParser::State::ParseState::getCode(int level, int& reservedBits) const {
   if (level == 0) {
      reservedBits = 4;
      return ((rpmReadMethod == &BasicParser::parseDocument) ? 1 : 0)
         + (uPoint << 1);
   }
   else if (level == 1) {
      reservedBits = 6;
      return ((rpmReadMethod == &BasicParser::parseDeclXml)
         ? 0 : ((rpmReadMethod == &BasicParser::parseDoctype)
         ? 1 : 2)) + (uPoint << 2);
   };
   throw EReadError();
}

void
BasicParser::State::read(ISBase& in, const FormatParameters& params) {
   bool isRaw = params.isRaw();

   // Read the current state of parsing
   int codeParse;
   in.read(codeParse, isRaw);
   setParseLevelField(codeParse & 1);
   setTextContinuationField((codeParse >>= 1) & 1);
   int needBits = 0;
   tpsParseState[0].setFromCode(codeParse >>= 1, 0, needBits);
   if (queryParseLevelField() == 1)
      tpsParseState[1].setFromCode(codeParse >>= needBits, 1, needBits);
   if (codeParse >>= needBits)
      throw EReadError();
   if (!isRaw) in.assume(' ');

   // Read the additional parameters
   in.read(uDocumentLevel, isRaw);
   if (uDocumentLevel < 0)
      throw EReadError();
   if (!isRaw) in.assume('\t');
   tPreviousToken.read(in, params);
   if (!isRaw) in.assume(' ');
   tNextToken.read(in, params);
}

void
BasicParser::State::write(OSBase& out, const FormatParameters& params) const {
   bool isRaw = params.isRaw();
   int codeParse = queryParseLevelField() + (queryTextContinuationField() << 1);
   int oldReservedBits = 2;
   int reservedBits;
   codeParse |= (tpsParseState[0].getCode(0, reservedBits)) << oldReservedBits;
   oldReservedBits += reservedBits;
   if (queryParseLevelField() >= 1)
      codeParse |= (tpsParseState[1].getCode(1, reservedBits)) << oldReservedBits;
   out.write(codeParse, isRaw);
   if (!isRaw) out.put(' ');
   out.write(uDocumentLevel, isRaw);
   if (!isRaw) out.put('\t');
   tPreviousToken.write(out, params);
   if (!isRaw) out.put(' ');
   tNextToken.write(out, params);
}

BasicParser::ReadResult
BasicParser::State::parseAgain(BasicParser& parser, const GenericLexer::Token& token, SubString& additionalContent) {
   switch (token.getType()) {
      case GenericLexer::AbstractToken::TComment:
         return parser.continueComment(parser.glLexer.getContentReader(), additionalContent);
      case GenericLexer::AbstractToken::TNodeBeginName:
         return parser.continueStartElement(parser.glLexer.getContentReader(), additionalContent);
      case GenericLexer::AbstractToken::TNodeBeginEndName:
         return parser.continueEndElement(parser.glLexer.getContentReader(), additionalContent);
      case GenericLexer::AbstractToken::TDeclentity:
         return parser.continueEntity(parser.glLexer.getContentReader(), additionalContent);
      case GenericLexer::AbstractToken::TName:
         if (tpsParseState[1].getMethod() == &BasicParser::parseNode)
            return parser.continueAttributeKey(parser.glLexer.getContentReader(), additionalContent);
         return RRContinue;
      case GenericLexer::AbstractToken::TTextualData:
      case GenericLexer::AbstractToken::TFinalTextualData:
         return parser.continueText(parser.glLexer.getContentReader(), additionalContent);
      case GenericLexer::AbstractToken::TCData:
         return parser.continueData(parser.glLexer.getContentReader(), additionalContent);
      case GenericLexer::AbstractToken::TLitteral:
         return parser.continueAttributeValue(parser.glLexer.getContentReader(), additionalContent);
      case GenericLexer::AbstractToken::TCallep:
         return parser.continueEP(parser.glLexer.getContentReader(), additionalContent);
      case GenericLexer::AbstractToken::TCallentity:
         return parser.continueEntity(parser.glLexer.getContentReader(), additionalContent);
      case GenericLexer::AbstractToken::TCallhexachar:
         return parser.continueHexaChar(parser.glLexer.getContentReader(), additionalContent);
      case GenericLexer::AbstractToken::TCallchar:
         return parser.continueChar(parser.glLexer.getContentReader(), additionalContent);
      default:
         return RRContinue;
   };
}

/*******************************************/
/* Implementation of the class BasicParser */
/*******************************************/

void
BasicParser::_read(ISBase& in, const FormatParameters& params) {
   glLexer.read(in, params);
   if (!params.isRaw()) in.assume('\n');
   sState.read(in, params);
}

void
BasicParser::_write(OSBase& out, const FormatParameters& params) const {
   glLexer.write(out, params);
   if (!params.isRaw()) out.put('\n');
   sState.write(out, params);
}

/* Implementation of the high level parsing methods */

// result == RRNeedChars or RRContinue or RRFinished
//    RRNeedChars => need to refill subString
//    RRContinue  => call our method again
//    RRFinished  => end of parsing
BasicParser::ReadResult
BasicParser::parseToken(SubString& subString, bool isReentry) {
   GenericLexer::ReadResult result;
   if (sState.hasNextToken()) {
      result = sState.parse(*this, sState.extractNextToken(), subString);
      return result;
   }
   if (!glLexer.isFullToken() && glLexer.hasContentToken()) {
      result = sState.parseAgain(*this, glLexer.getToken(), subString);
      if (result != RRNeedChars && glLexer.hasContentToken())
         result = glLexer.skipContentToken(subString); // empty old unused token
      if (result == RRNeedChars || result == RRFinished)
         return result;
   };
   while ((result = glLexer.readChars(subString)) == RRContinue);
   if (result == RRHasToken) {
      result = sState.parse(*this, glLexer.getToken(), subString);
      if (!isReentry && glLexer.isPartialToken() && glLexer.hasContentToken()) {
         result = sState.parseAgain(*this, glLexer.getToken(), subString);
         if (result != RRNeedChars && glLexer.hasContentToken())
            result = glLexer.skipContentToken(subString);
      };
   }
   return result;
}

// result == RRNeedChars or RRContinue or RRFinished
//    RRNeedChars => need to refill subString
//    RRFinished  => end of parsing
BasicParser::ReadResult
BasicParser::parseChunk(SubString& subString) {
   GenericLexer::ReadResult result;
   BasicParser::ReadResult parseResult;
   if (glLexer.isFullToken()) {
      do { // parseToken is inlined
         while ((result = glLexer.readChars(subString)) == RRContinue);
         parseResult = (result == RRHasToken) ? sState.parse(*this, glLexer.getToken(), subString) : result;
      } while (parseResult == RRContinue);
   }
   else {
      do { // parseToken is inlined
         if (sState.hasNextToken()) {
            parseResult = sState.parse(*this, sState.extractNextToken(), subString);
            continue;
         }
         if (glLexer.hasContentToken()) {
            result = sState.parseAgain(*this, glLexer.getToken(), subString);
            if (result != RRNeedChars && glLexer.hasContentToken())
               result = glLexer.skipContentToken(subString);
            if (result == RRNeedChars || result == RRFinished)
               return result;
         };
         while ((result = glLexer.readChars(subString)) == RRContinue);
         parseResult = result;
         if (result == RRHasToken) {
            parseResult = sState.parse(*this, glLexer.getToken(), subString);
            if (parseResult != RRNeedChars && glLexer.hasContentToken()) { // glLexer.isPartialToken()
               parseResult = sState.parseAgain(*this, glLexer.getToken(), subString);
               if (parseResult != RRNeedChars && glLexer.hasContentToken())
                  result = glLexer.skipContentToken(subString);
            };
         }
      } while (parseResult == RRContinue);
   }
   return parseResult;
}

void
BasicParser::parse(ISBase& in) {
   SubString buffer(SString(5000));
   ReadResult parseResult = RRNeedChars;
   if (!in.good())
      return;
   startDocument();
   while (in.good() && (parseResult != RRFinished)) {
      if (buffer.length() > 0) {
         int length = buffer.length();
         STG::SubString text(buffer);
         buffer.seek(0,0);
         buffer.copy(text).setToEnd();
         in.readsome(buffer.asPersistent(), 5000-length);
         buffer.seek(0,0);
      }
      else {
         buffer.seek(0,0);
         in.readsome(buffer.asPersistent(), 5000);
      };
      // in.read(buffer, buffer.getPlace());
      parseResult = parseChunk(buffer);
   };
   if ((parseResult != RRFinished) && (sState.getDocumentLevel() > 0))
      error(SString("Unexpected end of XML stream"));
   else
      endDocument();
}

/* Implementation of the low level parsing methods, event methods triggered by tokens */

BasicParser::ReadResult
BasicParser::parseError(GenericLexer::Token token, SubString& additionalContent) {
   AssumeCondition(sState.getParseLevel() >= 1)
   if ((token.getType() == GenericLexer::AbstractToken::TGreater)
      || (token.getType() == GenericLexer::AbstractToken::TSlashgreater)
      || (token.getType() == GenericLexer::AbstractToken::TEnddeclxml))
      sState.ascent();
   return RRContinue;
}

BasicParser::ReadResult
BasicParser::parseDocument(GenericLexer::Token token, SubString& additionalContent) {
   // document: [DBegin] separators [DAfterSeparators] opt_declxml [DAfterDeclXML]
   //                    opt_decltypedoc [DAfterDeclTypeDoc] element [DAfterElement]
   enum Delimiters { DBegin, DAfterSeparators, DAfterDeclXML, DAfterDeclTypeDoc, DAfterElement };
   
   AssumeCondition(sState.getParseLevel() == 0)
   ReadResult result = RRContinue;
   switch (token.getType()) {
      case GenericLexer::AbstractToken::TDeclxml:
         {  int point = sState.point();
            if (point >= DAfterDeclXML) {
               error(SString("Unexpected XML declaration"));
               sState.descent(&BasicParser::parseError);
            }
            else {
               if (point == DAfterSeparators)
                  warning(SString("Unexpected XML declaration"));
               sState.point() = DAfterDeclXML;
               sState.descent(&BasicParser::parseDeclXml);
               glLexer.setToXMLDeclaration();
            };
         };
         break;
      case GenericLexer::AbstractToken::TComment:
         {  if (sState.point() <= DAfterSeparators)
               sState.point() = DAfterDeclXML;
            if (token.hasContent())
               addComment(((const GenericLexer::CommentToken&) *token.extractContent()).getComment());
            else
               result = addComment(glLexer.getContentReader(), additionalContent);
         };
         break;
      case GenericLexer::AbstractToken::TSeparator:
         {  if (sState.point() == DBegin) sState.point() = DAfterSeparators; };
         break;
      case GenericLexer::AbstractToken::TDoctype:
         {  if (sState.point() >= DAfterDeclTypeDoc) {
               error(SString("Unexpected document type definition"));
               sState.descent(&BasicParser::parseError);
            }
            else {
               sState.point() = DAfterDeclTypeDoc;
               sState.descent(&BasicParser::parseDoctype);
               glLexer.setToDoctype();
            };
         };
         break;
      case GenericLexer::AbstractToken::TNodeBeginName:
         {  if (sState.point() >= DAfterElement) {
               error(SString("Unexpected XML node element"));
               sState.descent(&BasicParser::parseError);
            }
            else {
               sState.increaseElement();
               sState.point() = DAfterElement;
               sState.descent(&BasicParser::parseNode);
               glLexer.setToElementHeader();
               if (token.hasContent())
                  openStartElement(((GenericLexer::NameToken&) *token.extractContent()).getName());
               else
                  result = openStartElement(glLexer.getContentReader(), additionalContent);
            };
         };
         break;
      default:
         error(SString("Unexpected token in XML document"));
   };
   // if the state has decreased, it is likely to be equal to 0
   return (sState.getParseLevel() == 0 && sState.point() == DAfterElement) ? RRFinished : result;
}

BasicParser::ReadResult
BasicParser::parseDeclXml(GenericLexer::Token token, SubString& additionalContent) {
   // declxml: DECLXML [DAfterDeclXML] SEPARATOR [DAfterDeclXMLSeparator] infoversion
   //          [DAfterInfoVersion] opt_declcodage [DAfterDeclCodage] opt_decldocauto
   //          [DAfterDeclDocAuto] ENDDECLXML
   // infoversion: VERSION [DAfterVersion] equal [DAfterEqualVersion] LITTERAL
   // declcodage:  SEPARATOR [DAfterDeclCodageSeparator] ENCODING [DAfterDeclCodageEncoding]
   //              equal [DAfterDeclCodageEqual] LITTERAL
   // decldocauto: SEPARATOR [DAfterDeclDocAutoSeparator] STANDALONE [DAfterDeclDocAutoStandAlone]
   //              equal [DAfterDeclDocAutoEqual] LITTERAL
   enum Delimiters
      {  DAfterDeclXML, DAfterDeclXMLSeparator, DAfterVersion, DAfterEqualVersion,
         DAfterInfoVersion, DAfterDeclCodageSeparator, DAfterDeclCodageEncoding,
         DAfterDeclCodageEqual, DAfterDeclCodage, DAfterDeclDocAutoSeparator,
         DAfterDeclDocAutoStandAlone, DAfterDeclDocAutoEqual, DAfterDeclDocAuto
      };

   AssumeCondition(sState.getParseLevel() == 1)
   int point = sState.point();
   switch (token.getType()) {
      case GenericLexer::AbstractToken::TSeparator:
         {  if ((point == DAfterDeclXML) || (point == DAfterInfoVersion) || (point == DAfterDeclDocAuto))
               ++sState.point();
         };
         break;
      case GenericLexer::AbstractToken::TVersion:
         {  if ((point == DAfterDeclXMLSeparator) || (point == DAfterDeclCodageSeparator)
                  || (point == DAfterDeclDocAutoSeparator) || (point == DAfterDeclDocAuto)) {
               if (point != DAfterDeclXMLSeparator)
                  warning(SString("Unexpected token in XML declaration"));
               sState.point() = DAfterVersion;
            }
            else
               error(SString("Unexpected token in XML declaration"));
         };
         break;
      case GenericLexer::AbstractToken::TEncoding:
         {  if ((point == DAfterDeclXMLSeparator) || (point == DAfterDeclCodageSeparator)
                  || (point == DAfterDeclDocAutoSeparator) || (point == DAfterDeclDocAuto)) {
               if (point != DAfterDeclCodageSeparator)
                  warning(SString("Unexpected token in XML declaration"));
               sState.point() = DAfterDeclCodageEncoding;
            }
            else
               error(SString("Unexpected token in XML declaration"));
         };
         break;
      case GenericLexer::AbstractToken::TStandalone:
         {  if ((point == DAfterDeclXMLSeparator) || (point == DAfterDeclCodageSeparator)
                     || (point == DAfterDeclDocAutoSeparator) || (point == DAfterDeclDocAuto) || (point == DAfterDeclCodage)) {
               if ((point == DAfterDeclDocAuto) || (point == DAfterDeclXMLSeparator))
                  warning(SString("Unexpected token in XML declaration"));
               sState.point() = DAfterDeclDocAutoStandAlone;
            }
            else
               error(SString("Unexpected token in XML declaration"));
         };
         break;
      case GenericLexer::AbstractToken::TEqual:
         {  if ((point == DAfterVersion) || (point == DAfterDeclCodageEncoding)
                  || (point == DAfterDeclDocAutoStandAlone))
               ++sState.point();
            else
               error(SString("Unexpected token in XML declaration"));
         };
         break;
      case GenericLexer::AbstractToken::TLitteral:
         {  if ((point == DAfterEqualVersion) || (point == DAfterDeclCodageEqual)
                  || (point == DAfterDeclDocAutoEqual))
               ++sState.point();
            else
               error(SString("Unexpected token in XML declaration"));
         };
         break;
      case GenericLexer::AbstractToken::TEnddeclxml:
         {  if ((point == DAfterDeclXML) || (point == DAfterDeclXMLSeparator) || (point == DAfterDeclCodage) || (point == DAfterDeclCodageSeparator)
                  || (point == DAfterDeclDocAuto) || (point == DAfterDeclDocAutoSeparator)) {
               if (point == DAfterDeclXML || point == DAfterDeclXMLSeparator)
                  warning(SString("Unexpected end XML declaration"));
            }
            else
               error(SString("Unexpected end XML declaration"));
            sState.ascent();
            glLexer.setToInitial();
         };
         break;
      default:
         error(SString("Unexpected token in XML declaration"));
   };
   return RRContinue;
}

BasicParser::ReadResult
BasicParser::parseDoctype(GenericLexer::Token token, SubString& additionalContent) {
// decltypedoc: DOCTYPE [DAfterDocType] SEPARATOR [DAfterDocTypeSeparator] NOM [DAfterDocTypeName]
//                idexterne [DAfterExternId] opt_balisage [DAfterBalise] GREATER
// idexterne:   SEPARATOR [DAfterExternIdSeparator] SYSTEM [DAfterExternIdSystem] SEPARATOR
//                [DAfterExternIdSndSeparator] LITTERAL
//            | SEPARATOR [DAfterExternIdSeparator] PUBLIC [DAfterExternIdPublic] SEPARATOR
//                [DAfterExternIdPublicSeparator] LITTERAL [DAfterExternIdPublicLitteral] SEPARATOR
//                [DAfterExternIdPublicSndSeparator] LITTERAL
// balisage:    OPSQBRACE [DAfterBaliseOpenSquareBrace] CLSQBRACE
//            | OPSQBRACE [DAfterBaliseOpenSquareBrace] list_declbalisage CLSQBRACE
// list_declbalisage:
//    (DECLELEMENT | DECLLISTATTRIBUTE | DECLENTITY | DECLNOTATION | COMMENT | APPELEP)*

   enum Delimiters
      {  DAfterDocType, DAfterDocTypeSeparator, DAfterDocTypeName,
         DAfterExternIdSeparator, DAfterExternIdSystem, DAfterExternIdSndSeparator,
            DAfterExternIdPublic, DAfterExternIdPublicSeparator, DAfterExternIdPublicLitteral,
            DAfterExternIdPublicSndSeparator, DAfterExternId,
         DAfterBaliseOpenSquareBrace, DAfterBalise
      };
   AssumeCondition(sState.getParseLevel() == 1)
   int point = sState.point();
   switch (token.getType()) {
      case GenericLexer::AbstractToken::TSeparator:
         {  if ((point == DAfterDocType) || (point == DAfterDocTypeName)
                  || (point == DAfterExternIdSystem) || (point == DAfterExternIdPublic)
                  || (point == DAfterExternIdPublicLitteral))
               ++sState.point();
         };
         break;
      case GenericLexer::AbstractToken::TName:
         {  if (point == DAfterDocTypeSeparator)
               ++sState.point();
            else
               error(SString("Unexpected token in document type definition"));
         };
         break;
      case GenericLexer::AbstractToken::TSystem:
         {  if (point == DAfterExternIdSeparator)
               ++sState.point();
            else
               error(SString("Unexpected token in document type definition"));
         };
         break;
      case GenericLexer::AbstractToken::TPublic:
         {  if (point == DAfterExternIdSeparator)
               sState.point() = DAfterExternIdPublic;
            else
               error(SString("Unexpected token in document type definition"));
         };
         break;
      case GenericLexer::AbstractToken::TLitteral:
         {  if (point == DAfterExternIdSndSeparator)
               sState.point() = DAfterExternId;
            else if ((point == DAfterExternIdPublicSeparator) || (point == DAfterExternIdPublicSndSeparator))
               ++sState.point();
            else
               error(SString("Unexpected token in document type definition"));
         };
         break;
      case GenericLexer::AbstractToken::TOpsqbrace:
         {  if (point == DAfterExternId)
               ++sState.point();
            else
               error(SString("Unexpected token in document type definition"));
         };
         break;
      case GenericLexer::AbstractToken::TDeclelement: case GenericLexer::AbstractToken::TDecllistattribute:
      case GenericLexer::AbstractToken::TDeclentity: case GenericLexer::AbstractToken::TDeclnotation:
      case GenericLexer::AbstractToken::TComment: case GenericLexer::AbstractToken::TCallep:
         {  if (point != DAfterBalise) // comments do no generate any action here
               error(SString("Unexpected token in document type definition"));
         };
         break;
      case GenericLexer::AbstractToken::TClsqbrace:
         {  if (point == DAfterBalise)
               ++sState.point();
            else
               error(SString("Unexpected token in document type definition"));
         };
         break;
      case GenericLexer::AbstractToken::TGreater:
         {  if (!((point == DAfterDocTypeName) || (point == DAfterExternIdSeparator)
                  || (point == DAfterExternId) || (point == DAfterBalise)))
               error(SString("Unexpected end in document type definition"));
            sState.ascent();
            glLexer.setToInitial();
         };
         break;
      default:
         error(SString("Unexpected token in document type definition"));
   };
   return RRContinue;
}

bool
BasicParser::setToAfterNode() {
   bool result;
   if ((result = sState.decreaseElement()) != false) {
      sState.ascent();
      glLexer.setToInitial();
   }
   else
      glLexer.setToBody();
   return result;
}

BasicParser::ReadResult
BasicParser::parseNode(GenericLexer::Token token, SubString& additionalContent) {
// element:   TNODEBEGINNAME [DAfterNodeStart] (SEPARATOR [DAfterStartSeparator] NOM [DAfterStartName]
//                EQUAL [DAfterStartEqual] LITTERAL)* TSLASHGREATER [DAfterNode]
//          | TNODEBEGINNAME [DAfterNodeStart] (SEPARATOR [DAfterStartSeparator] 1 NOM [DAfterStartName]
//                EQUAL [DAfterStartEqual] LITTERAL)* TGREATER [DAfterNodeHeader]
//               (element | TTEXTUALDATA | TCALLEP | TCALLENTITY | TCALLCAR | TCDATA | TCOMMENT)*
//            TNODEBEGINENDNAME [DAfterEndStart] TGREATER [DAfterNode]

   enum Delimiters
      {  DAfterNodeStart, DAfterStartSeparator, DAfterStartName, DAfterStartEqual,
         DAfterNodeHeader, DAfterEndStart, DAfterNode
      };

   AssumeCondition(sState.getParseLevel() == 1)
   int point = sState.point();
   ReadResult result = RRContinue;
   if (point == DAfterNodeHeader) {
      switch (token.getType()) {
         case GenericLexer::AbstractToken::TNodeBeginName:
            {  sState.increaseElement();
               sState.point() = DAfterNodeStart;
               sState.clearTextContinuation();
               glLexer.setToElementHeader();
               if (token.hasContent())
                  result = openStartElement(((GenericLexer::NameToken&) *token.extractContent()).getName());
               else
                  result = openStartElement(glLexer.getContentReader(), additionalContent);
            };
            return result;
         case GenericLexer::AbstractToken::TTextualData:
            if (!token.hasContent())
               result = addText(glLexer.getContentReader(), additionalContent);
            else if (!sState.hasTextContinuation() && isPretty()) {
               STG::SubString text = ((GenericLexer::TextualDataToken&) *token.extractContent()).text();
               sState.setTextContinuation();
               if (text.leftTrim().length() > 0)
                  result = addText(text);
            }
            else
               result = addText(((const GenericLexer::TextualDataToken&) *token.extractContent()).getText());
            return result;
         case GenericLexer::AbstractToken::TFinalTextualData:
            if (!token.hasContent())
               result = addText(glLexer.getContentReader(), additionalContent);
            else if (isPretty()) {
               STG::SubString text = ((GenericLexer::TextualDataToken&) *token.extractContent()).text();
               if (!sState.hasTextContinuation()) {
                  if (text.trim().length() > 0)
                     result = addText(text);
               }
               else {
                  sState.clearTextContinuation();
                  if (text.rightTrim().length() > 0)
                     result = addText(text);
               };
            }
            else
               result = addText(((const GenericLexer::TextualDataToken&) *token.extractContent()).getText());
            return result;
         case GenericLexer::AbstractToken::TCallep:
            {  sState.setTextContinuation();
               if (token.hasContent())
                  result = addEP(((const GenericLexer::NameToken&) *token.extractContent()).getName());
               else
                  result = addEP(glLexer.getContentReader(), additionalContent);
            }
            return result;
         case GenericLexer::AbstractToken::TCallentity:
            {  sState.setTextContinuation();
               if (token.hasContent())
                  result = addEntity(((const GenericLexer::NameToken&) *token.extractContent()).getName());
               else
                  result = addEntity(glLexer.getContentReader(), additionalContent);
            }
            return result;
         case GenericLexer::AbstractToken::TCallhexachar:
         case GenericLexer::AbstractToken::TCallchar:
            {  sState.setTextContinuation();
               if (token.hasContent())
                  result = addChar(((const GenericLexer::CallCharToken&) *token.extractContent()).getCall());
               else if (token.getType() == GenericLexer::AbstractToken::TCallhexachar)
                  result = addHexaChar(glLexer.getContentReader(), additionalContent);
               else
                  result = addChar(glLexer.getContentReader(), additionalContent);
            }
            return result;
         case GenericLexer::AbstractToken::TCData:
            {  sState.setTextContinuation();
               if (token.hasContent())
                  result = addData(((const GenericLexer::CDataToken&) *token.extractContent()).getData());
               else
                  result = addData(glLexer.getContentReader(), additionalContent);
            }
            return result;
         case GenericLexer::AbstractToken::TComment:
            if (token.hasContent())
               result = addComment(((const GenericLexer::CommentToken&) *token.extractContent()).getComment());
            else
               result = addComment(glLexer.getContentReader(), additionalContent);
            return (result == RRNeedChars || result == RRFinished) ? result : RRContinue;
         case GenericLexer::AbstractToken::TNodeBeginEndName:
            {  sState.point() = DAfterEndStart;
               sState.clearTextContinuation();
               glLexer.setToElementFoot();
               if (token.hasContent())
                  result = endElement(((const GenericLexer::NameToken&) *token.extractContent()).getName());
               else
                  result = endElement(glLexer.getContentReader(), additionalContent);
            }
            return result;
         default:
            break;
      };
   };

   switch (token.getType()) {
      case GenericLexer::AbstractToken::TSeparator:
         {  if (point == DAfterNodeStart)
               ++sState.point();
         };
         break;
      case GenericLexer::AbstractToken::TName:
         {  if (point == DAfterStartSeparator) {
               ++sState.point();
               if (token.hasContent())
                  sState.setLastToken(token);
               else
                  result = openAttributeKey(glLexer.getContentReader(), additionalContent);
            }
            else
               error(SString("Unexpected token in XML node"));
         };
         break;
      case GenericLexer::AbstractToken::TEqual:
         {  if (point == DAfterStartName)
               ++sState.point();
            else
               error(SString("Unexpected token in XML node"));
         };
         break;
      case GenericLexer::AbstractToken::TLitteral:
         {  if (point == DAfterStartEqual) {
               sState.point() = DAfterNodeStart;
               if (token.hasContent()) {
                  STG::SubString attribute = sState.getLastNameToken().getName();
                  sState.clearLastToken();
                  result = addAttribute(attribute,
                        ((const GenericLexer::LitteralToken&) *token.extractContent()).getLitteral());
               }
               else
                  result = setAttributeValue(glLexer.getContentReader(), additionalContent);
            }
            else
               error(SString("Unexpected token in XML node"));
         };
         break;
      case GenericLexer::AbstractToken::TSlashgreater:
         {  if (!((point == DAfterNodeStart) || (point == DAfterStartSeparator)))
               error(SString("Unexpected end name in XML node definition"));
            if (isFullToken()) {
               sState.point() = DAfterNode;
               if (!setToAfterNode())
                  sState.point() = DAfterNodeHeader;
               result = closeStartElement();
               if (result != RRFinished)
                  result = closeElement();
            }
            else {
               sState.point() = DAfterEndStart;
               sState.setNextToken(GenericLexer::AbstractToken::TGreater);
               result = closeStartElement();
            }
         };
         break;
      case GenericLexer::AbstractToken::TGreater:
         {  if (point < DAfterNodeHeader) {
               if (!((point == DAfterNodeStart) || (point == DAfterStartSeparator)))
                  error(SString("Unexpected end name in XML node definition"));
               sState.point() = DAfterNodeHeader;
               glLexer.setToBody();
               result = closeStartElement();
            }
            else if (point == DAfterEndStart) {
               sState.point() = DAfterNode;
               if (!setToAfterNode())
                  sState.point() = DAfterNodeHeader;
               result = closeElement();
            }
            else
               error(SString("Unexpected end name in XML node definition"));
         };
         break;
      case GenericLexer::AbstractToken::TNodeBeginName:
      case GenericLexer::AbstractToken::TTextualData:
      case GenericLexer::AbstractToken::TFinalTextualData:
      case GenericLexer::AbstractToken::TCallep:
      case GenericLexer::AbstractToken::TCallentity:
      case GenericLexer::AbstractToken::TCallhexachar:
      case GenericLexer::AbstractToken::TCallchar:
      case GenericLexer::AbstractToken::TCData:
      case GenericLexer::AbstractToken::TComment:
         error(SString("Unexpected token in XML node"));
         break;
      case GenericLexer::AbstractToken::TNodeBeginEndName:
         {  error(SString("Unexpected end name in XML node definition"));
            sState.point() = DAfterEndStart;
            if (token.hasContent())
               result = endElement(((GenericLexer::NameToken&) *token.extractContent()).getName());
            else
               result = endElement(glLexer.getContentReader(), additionalContent);
         };
         break;
      default:
         error(SString("Unexpected token in XML node"));
   };
   return result;
}

/********************************************/
/* Implementation of the class CommonParser */
/********************************************/

CommonParser::ReadResult
CommonParser::parseChunkUntil(SubString& subString, const LessCondition& condition) {
   BasicParser::ReadResult parseResult;
   do {
      if (!sState.isLessThan(condition.uLevel, condition.uPoint))
         return RRFinished;
      parseResult = parseToken(subString);
   } while (parseResult == RRContinue);
   return parseResult;
}

void
CommonParser::parseUntil(ISBase& in, const LessCondition& condition) {
   SubString buffer(SString(5000));
   ReadResult parseResult = RRNeedChars;
   if (!in.good())
      return;
   startDocument();
   while (in.good() && (parseResult != RRFinished)) {
      if (buffer.length() > 0) {
         int length = buffer.length();
         STG::SubString text(buffer);
         buffer.seek(0,0);
         buffer.copy(text).setToEnd();
         in.readsome(buffer.asPersistent(), 5000-length);
         buffer.seek(0,0);
      }
      else {
         buffer.seek(0,0);
         in.readsome(buffer.asPersistent(), 5000);
      };
      // in.read(buffer, buffer.getPlace());
      parseResult = parseChunkUntil(buffer, condition);
   };
   if (parseResult != RRFinished)
      error(SString("Unexpected end of XML stream"));
   else
      endDocument();
}

CommonParser::ReadResult
CommonParser::SkipNode::skip(State& state, Arguments& argument) {
   if (argument.isOpenStartElement() && !argument.isContinuedToken())
      ++state.point();
   else if (argument.isCloseElement()) {
      --state.point();
      if (state.point() == 0)
         argument.reduceState(state); // potential destruction of this
   };
   return RRHasToken;
}

CommonParser::ReadResult
CommonParser::SkipNode::skipInLoop(State& state, Arguments& argument) {
   CommonParser::ReadResult result;
   while (true) {
      if (argument.isOpenStartElement() && !argument.isContinuedToken())
         ++state.point();
      else if (argument.isCloseElement()) {
         --state.point();
         if (state.point() == 0) {
            argument.reduceState(state); // potential destruction of this
            return RRHasToken;
         }
      }
      if (!argument.setToNextToken(result)) return result;
   };
}

/********************************************/
/* Implementation of the class CommonWriter */
/********************************************/

template <class TypeBase>
BasicWriter::WriteResult
TCommonWriter<TypeBase>::writeText(STG::SubString& content) {
   BasicWriter::WriteResult result = WRNeedEvent;
   if (!isCommonEntityVerification()) {
      if ((result = inherited::addText(content)) != WRNeedPlace)
         content.setToEnd();
   }
   else {
      SetOfChars endRead;
      endRead.add('&').add('<').add('>');
      if (endRead.accept(content.getChar())) {
         char ch = content.getChar();
         if (ch == '&')
            result = inherited::addEntity(STG::SString("amp"));
         else if (ch == '<')
            result = inherited::addEntity(STG::SString("lt"));
         else // ch == '>'
            result = inherited::addEntity(STG::SString("gt"));
         content.advance();
      }
      else {
         PNT::PassPointer<SubString> beforeEntity = content.scanB(endRead);
         if (beforeEntity.isValid()) {
            beforeEntity->setLength(0).setToBegin(content);
            content.setToEnd(*beforeEntity);
            if ((result = inherited::addText(*beforeEntity)) == WRNeedPlace)
               content.setToBegin(*beforeEntity);
         }
         else if ((result = inherited::addText(content)) != WRNeedPlace)
            content.setToEnd();
      };
   };
   return result;
}

template <class TypeBase>
BasicWriter::WriteResult
TCommonWriter<TypeBase>::translateEvent() {
   BasicWriter::WriteResult result = inherited::delayedEvent();
   if (result != WRNeedEvent)
      return result;

   if (aArguments.isOpenStartElement())
      result = inherited::openStartElement(aArguments.value());
   else if (aArguments.isCloseStartElement())
      result = inherited::closeStartElement();
   else if (aArguments.isAddAttribute())
      result = inherited::addAttribute(aArguments.attribute(), aArguments.value());
   else if (aArguments.isEndElement())
      result = inherited::endElement(aArguments.value());
   else if (aArguments.isCloseElement())
      result = inherited::closeElement();
   else if (aArguments.isText()) {
      if (aArguments.value().length() > 0)
         result = writeText(aArguments.value());
      if ((result != WRNeedPlace) && (aArguments.value().length() == 0))
         aArguments.clearExceptAdditional();
      return result;
   };
   if (result != WRNeedPlace)
      aArguments.clearExceptAdditional();
   return result;
}

template <class TypeBase>
BasicWriter::WriteResult
TCommonWriter<TypeBase>::getNextEvent() {
   BasicWriter::WriteResult result = inherited::delayedEvent();
   if (result != WRNeedEvent)
      return result;

   if (sState.isEmpty())
      result = WRFinished;
   else if (aArguments.isText() && (aArguments.value().length() > 0)) {
      result = writeText(aArguments.value());
      if (result != WRNeedPlace)
         aArguments.clearExceptAdditional();
   }
   else if (sState.parse(aArguments))
      TCommonWriter<TypeBase>::translateEvent();
   else if (!hasVerificationDisabled())
      throw STG::EWriteError();
   else
      result = WRFinished;
   return result;
}

CommonWriter::WriteResult
CommonWriter::writeChunkUntil(SubString& out, const LessCondition& condition, WriteResult result) {
   while ((result != WRFinished) && (result != WRNeedPlace)) {
      if (result == WRNeedWrite)
         result = BasicWriter::state().write(out, isPretty());
      if (result == WRNeedEvent) {
         if (!state().isLessThan(condition.uLevel, condition.uPoint))
            return WRFinished;
         result = getNextEvent();
      };
   };
   if (result == WRFinished) {
      result = BasicWriter::state().completeWrite(out, isPretty());
      while ((result != WRFinished) && (result != WRNeedPlace)) {
         if (result == WRNeedEvent) {
            if (!state().isLessThan(condition.uLevel, condition.uPoint))
               return WRFinished;
            result = getNextEvent();
         };
         if (result == WRNeedWrite)
            result = BasicWriter::state().completeWrite(out, isPretty());
      };
   };
   return result;
}

void
CommonWriter::writeUntil(OSBase& out, const LessCondition& condition) {
   SString buffer(5000);
   WriteResult result = WRNeedEvent;
   while (result != WRFinished) {
      result = writeChunkUntil(buffer, condition, (result == WRNeedPlace) ? WRNeedWrite : result);
      out.writeall(buffer.asPersistent());
      buffer.clear();
   };
}

}} // end of namespace STG::XML

typedef TemplateElementKeyCastParameters<STG::XML::ImplNode::Attribute::Property,
   STG::XML::ImplNode::Attribute::Property::Key, SimpleCast> PropertyKeyCastParameters;

template class COL::TSortedArray<STG::XML::ImplNode::Attribute::Property,STG::XML::ImplNode::Attribute::Property::Key, SimpleCast>;
template int COL::ImplArray::localize<PropertyKeyCastParameters>
   (const STG::SubString&, const PropertyKeyCastParameters&, int, int) const;
template int COL::ImplArray::merge<PropertyKeyCastParameters>
   (const PropertyKeyCastParameters&, const COL::ImplArray&, int, int, bool, const VirtualCast*);

template class STG::XML::TCommonWriter<STG::XML::BasicWriter>;
template class STG::XML::TCommonWriter<STG::XML::ContextWriter>;
