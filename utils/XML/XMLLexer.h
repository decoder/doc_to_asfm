/**************************************************************************/
/*                                                                        */
/*  Copyright (C) 2014-2019                                               */
/*    CEA (Commissariat a l'Energie Atomique et aux Energies              */
/*         Alternatives)                                                  */
/*                                                                        */
/*  you can redistribute it and/or modify it under the terms of the GNU   */
/*  Lesser General Public License as published by the Free Software       */
/*  Foundation, version 2.1.                                              */
/*                                                                        */
/*  It is distributed in the hope that it will be useful,                 */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU Lesser General Public License for more details.                   */
/*                                                                        */
/*  See the GNU Lesser General Public License version 2.1                 */
/*  for more details (enclosed in the file LICENSE).                      */
/*                                                                        */
/**************************************************************************/

/////////////////////////////////
//
// Library   : XML
// Unit      : Lexer
// File      : XMLLexer.h
// Description :
//   Definition of a lexer of XML document.
//

#ifndef STG_XML_XMLLexerH

#include "Pointer/TreeMethods.h"
#include "TString/SString.h"

namespace STG { namespace XML {

/****************************************/
/* Definition of the class GenericLexer */
/****************************************/

class GenericLexer : public IOObject, public STG::Lexer::Base {
  public:
   class AbstractToken : public COL::List::Node {
     public:
      enum Type { TUndefined, TComment, TSeparator, TDeclxml, TEnddeclxml, TDeclelement, TDoctype,
         TNodeBeginName, TNodeBeginEndName, TGreater, TSlashgreater, TOpsqbrace, TClsqbrace, TEqual,
         TVersion, TPublic, TSystem, TEncoding, TStandalone, TDecllistattribute, TDeclentity,
         TDeclnotation, TName, TTextualData, TFinalTextualData, TCData, TLitteral, TCallep,
         TCallentity, TCallhexachar, TCallchar, TypesNB
      };

     private:
      typedef COL::List::Node inherited;
      Type tType;

     protected:
      Type& type() { return tType; }
      
     public:
      AbstractToken(Type type) : tType(type) {}
      AbstractToken(const AbstractToken& source) = default;
      DefineCopy(AbstractToken)
      DDefineAssign(AbstractToken)

      const Type& getType() const { return tType; }
      static void read(Type& type, IOObject::ISBase& in, const IOObject::FormatParameters& params);
      static void write(Type type, IOObject::OSBase& out, const IOObject::FormatParameters& params);
      static AbstractToken* createToken(Type type);
      static bool needExtension(Type type);
   };

   class StringToken : public AbstractToken {
     private:
      SString sString;

     public:
      StringToken(Type type) : AbstractToken(type) {}
      StringToken(const StringToken& source) = default;
      DefineCopy(StringToken)
      DDefineAssign(StringToken)

      SString& string() { return sString; }
      const SString& getString() const { return sString; }
   };

   class CallCharToken : public AbstractToken {
     private:
      int uCall;

     public:
      CallCharToken() : AbstractToken(TCallchar), uCall(0) {}
      CallCharToken(int call) : AbstractToken(TCallchar), uCall(call) {}
      CallCharToken(const CallCharToken& source) = default;
      DefineCopy(CallCharToken)
      DDefineAssign(CallCharToken)

      int& call() { return uCall; }
      const int& getCall() const { return uCall; }
   };

   class CommentToken : public StringToken {
     public:
      CommentToken() : StringToken(TComment) {}
      CommentToken(const CommentToken& source) = default;
      DefineCopy(CommentToken)

      SString& comment() { return string(); }
      const SString& getComment() const { return getString(); }
   };

   class NameToken : public StringToken {
     public:
      NameToken(Type tType) : StringToken(tType) {}
      NameToken(const NameToken& source) = default;
      DefineCopy(NameToken)

      SString& name() { return string(); }
      const SString& getName() const { return getString(); }
   };

   class LitteralToken : public StringToken {
     public:
      LitteralToken() : StringToken(TLitteral) {}
      LitteralToken(const LitteralToken& source) = default;
      DefineCopy(LitteralToken)

      SString& litteral() { return string(); }
      const SString& getLitteral() const { return getString(); }
   };

   class TextualDataToken : public StringToken {
     public:
      TextualDataToken() : StringToken(TTextualData) {}
      TextualDataToken(const TextualDataToken& source) = default;
      DefineCopy(TextualDataToken)

      void setTextFinal() { type() = TFinalTextualData; }
         
      SString& text() { return string(); }
      const SString& getText() const { return getString(); }
   };

   class CDataToken : public StringToken {
     public:
      CDataToken() : StringToken(TCData) {}
      CDataToken(const CDataToken& source) = default;
      DefineCopy(CDataToken)

      SString& data() { return string(); }
      const SString& getData() const { return getString(); }
   };

   class Token {
     private:
      AbstractToken::Type tType;
      PNT::PassPointer<AbstractToken> ppatContent;

     public:
      Token() : tType(AbstractToken::TUndefined) {}
      Token(AbstractToken::Type type) : tType(type) {}
      Token(AbstractToken* token)
         {  AssumeAllocation(token)
            ppatContent.absorbElement(token);
            tType = token->getType();
         }
      Token(const Token& source)
         :  tType(source.tType), ppatContent(source.ppatContent)
         {  if (ppatContent.isValid()) // for setPartialToken mode
               const_cast<Token&>(source).tType = AbstractToken::TUndefined;
         }
      Token& operator=(const Token& source)
         {  tType = source.tType; ppatContent = source.ppatContent;
            if (ppatContent.isValid()) // for setPartialToken mode
               const_cast<Token&>(source).tType = AbstractToken::TUndefined;
            return *this;
         }

      bool isValid() const { return tType != AbstractToken::TUndefined; }
      const AbstractToken::Type& getType() const
         {  AssumeCondition(tType != AbstractToken::TUndefined) return tType; }
      void assumeContent()
         {  AssumeCondition(tType != AbstractToken::TUndefined)
            if (!ppatContent.isValid())
               ppatContent.absorbElement(new AbstractToken(tType));
         }
      bool hasContent() const { return ppatContent.isValid(); }
      AbstractToken* createContent() { return ppatContent.extractElement(); }
      PNT::PassPointer<AbstractToken> extractContent() { return ppatContent; }
      const AbstractToken& getContent() const { return *ppatContent; }

      void read(IOObject::ISBase& in, const IOObject::FormatParameters& params);
      void write(IOObject::OSBase& out, const IOObject::FormatParameters& params) const;
   };

  private:
   class SubStringContainer {
     private:
      const bool fImmediateUse : 1;
      const bool fCompare : 1;
      bool fDifferent : 1;
      SubString* const pssSubString;
      Lexer::TextBuffer* const ptbBuffer;
      friend class GenericLexer;

     public:
      SubStringContainer()
         :  fImmediateUse(true), fCompare(false), fDifferent(false), pssSubString(nullptr), ptbBuffer(nullptr) {}
      SubStringContainer(SubString& subString)
         :  fImmediateUse(true), fCompare(true), fDifferent(false), pssSubString(&subString), ptbBuffer(nullptr) {}
      SubStringContainer(SubString& subString, bool immediateUse)
         :  fImmediateUse(immediateUse), fCompare(false), fDifferent(false), pssSubString(&subString), ptbBuffer(nullptr) {}
      SubStringContainer(Lexer::TextBuffer& buffer)
         :  fImmediateUse(false), fCompare(false), fDifferent(false), pssSubString(nullptr), ptbBuffer(&buffer) {}

      bool isDifferent() const { AssumeCondition(fCompare) return fDifferent; }
   };

   typedef ReadResult (GenericLexer::*ReadPointerMethod)(SubString&);
   typedef ReadResult (GenericLexer::*ReadPointerMethodFst)(SubString&, SubStringContainer*);
   typedef IOObject inherited;
   enum Options { OFullToken, OPartialToken };

   Lexer::TextBuffer tbCurrentToken;
   ReadPointerMethod rpmState;
   ReadPointerMethodFst rpmFstState;
   char              chContext;
   Token             tToken;
   Options           oOptions;

   /* Definition of the table of read methods */
#ifdef _MSC_VER
  public:
#endif
   class ReadPointerMethodTable;
   class ReadPointerMethodFstTable;
   class ReadPointerMethodDecisionNode : public PNT::DecisionNode<ReadPointerMethod> {
     protected:
      friend class ReadPointerMethodTable;
      void setMethod(ReadPointerMethod method) { PNT::DecisionNode<ReadPointerMethod>::setMethod(method); }
     public:
      ReadPointerMethodDecisionNode() {}
      const ReadPointerMethod& getMethod() const { return PNT::DecisionNode<ReadPointerMethod>::getMethod(); }
   };
   class ReadPointerMethodFstDecisionNode : public PNT::DecisionNode<ReadPointerMethodFst> {
     protected:
      friend class ReadPointerMethodFstTable;
      void setMethod(ReadPointerMethodFst method) { PNT::DecisionNode<ReadPointerMethodFst>::setMethod(method); }
     public:
      ReadPointerMethodFstDecisionNode() {}
      const ReadPointerMethodFst& getMethod() const { return PNT::DecisionNode<ReadPointerMethodFst>::getMethod(); }
   };
   class ReadPointerMethodTable : public PNT::ConstMethodTable<ReadPointerMethodDecisionNode, 6> {
     public:
      ReadPointerMethodTable();
   };
   class ReadPointerMethodFstTable : public PNT::ConstMethodTable<ReadPointerMethodFstDecisionNode, 10> {
     public:
      ReadPointerMethodFstTable();
   };
   friend class ReadPointerMethodTable;
   friend class ReadPointerMethodFstTable;

   static ReadPointerMethodTable rpmtGlobalReadMethods;
   static ReadPointerMethodFstTable rpmftFstReadMethods;

  protected:
   virtual void _read(ISBase& in, const FormatParameters& params) override;
   virtual void _write(OSBase& out, const FormatParameters& params) const override;

   ReadResult readCharsInitial(SubString& buffer);
      ReadResult readCharsEndComment(SubString& buffer, SubStringContainer* container = nullptr);
      ReadResult readCharsEndName(SubString& buffer, SubStringContainer* container = nullptr);
      ReadResult readCharsEndSeparator(SubString& buffer, SubStringContainer* container = nullptr);
   ReadResult readCharsDeclxml(SubString& buffer);
      ReadResult readCharsEndLitteral(SubString& buffer, SubStringContainer* container = nullptr);
   ReadResult readCharsDoctype(SubString& buffer);
      ReadResult readCharsEndDeclaration(SubString& buffer, SubStringContainer* container = nullptr);
      ReadResult readCharsEndEntity(SubString& buffer, SubStringContainer* container = nullptr);
   ReadResult readCharsElementHeader(SubString& buffer);
   ReadResult readCharsElementFoot(SubString& buffer);
   ReadResult readCharsBody(SubString& buffer);
      ReadResult readCharsEndTextualData(SubString& buffer, SubStringContainer* container = nullptr);
      ReadResult readCharsEndCdata(SubString& buffer, SubStringContainer* container = nullptr);
      ReadResult readCharsEndCallChar(SubString& buffer, SubStringContainer* container = nullptr);
      ReadResult readCharsEndCallHexaChar(SubString& buffer, SubStringContainer* container = nullptr);

  public:
   GenericLexer()
      :  rpmState(&GenericLexer::readCharsInitial), rpmFstState(nullptr), chContext('\0'), oOptions(OFullToken) {}
   GenericLexer(const GenericLexer& source) = default;
   DefineCopy(GenericLexer)
   DDefineAssign(GenericLexer)

   void setPartialToken() { oOptions = OPartialToken; }
   void setFullToken() { oOptions = OFullToken; }
   bool isPartialToken() const { return oOptions == OPartialToken; }
   bool isFullToken() const { return oOptions == OFullToken; }

   // setFullToken:
   //    result = RRNeedChars => buffer to be refilled
   //    result = RRContinue  => to be called again
   //    result = RRHasToken  => getToken is valid and complete
   //
   // setPartialToken:
   //    before RRHasToken: !hasContentToken()
   //       result = RRNeedChars => buffer to be refilled
   //       result = RRContinue  => to be called again
   //       result = RRHasToken  => getToken is valid (it has a type) but it has no content
   //          method ...ContentToken should be called to access to the beginning of the content
   //    after RRHasToken: hasContentToken()
   //       result = RRNeedChars => buffer to be refilled
   //          method ...ContentToken should be called to access to the middle of the content
   //       result = RRContinue  => getToken is still valid
   //          method ...ContentToken should be called to access to the end of the content
   ReadResult readChars(SubString& buffer)
      {  return rpmFstState ? (this->*rpmFstState)(buffer, nullptr) : (this->*rpmState)(buffer); }
   const Token& getToken() { return tToken; }
   bool hasContentToken() const { return rpmFstState; }
   ReadResult readContentToken(SubString& buffer, SubString& result, bool immediateUse=false)
      {  AssumeCondition(rpmFstState)
         SubStringContainer container(result, immediateUse);
         return (this->*rpmFstState)(buffer, &container);
      }
   ReadResult readContentToken(SubString& buffer, Lexer::TextBuffer& result)
      {  AssumeCondition(rpmFstState)
         SubStringContainer container(result);
         return (this->*rpmFstState)(buffer, &container);
      }
   ReadResult skipContentToken(SubString& buffer)
      {  AssumeCondition(rpmFstState)
         SubStringContainer container;
         return (this->*rpmFstState)(buffer, &container);
      }
   ReadResult assumeContentToken(SubString& buffer, SubString& comparisonContent, bool& isEqual)
      {  AssumeCondition(rpmFstState)
         if (!isEqual) {
            SubStringContainer container;
            return (this->*rpmFstState)(buffer, &container);
         };
         SubStringContainer container(comparisonContent);
         ReadResult result = (this->*rpmFstState)(buffer, &container);
         isEqual = !container.isDifferent();
         return result;
      }
   class ContentReader {
     private:
      GenericLexer* plLexer;

     public:
      ContentReader() : plLexer(nullptr) {}
      ContentReader(GenericLexer& lexer) : plLexer(&lexer) {}
      ContentReader(const ContentReader&) = default;
      ContentReader& operator=(const ContentReader&) = default;

      bool isValid() const { return plLexer && plLexer->hasContentToken(); }
      void clear() { plLexer = nullptr; }
      ReadResult readContentToken(SubString& buffer, SubString& result, bool immediateUse=false) const
         {  AssumeCondition(plLexer) return plLexer->readContentToken(buffer, result, immediateUse); }
      ReadResult readContentToken(SubString& buffer, Lexer::TextBuffer& result) const
         {  AssumeCondition(plLexer) return plLexer->readContentToken(buffer, result); }
      ReadResult skipContentToken(SubString& buffer) const
         {  AssumeCondition(plLexer) return plLexer->skipContentToken(buffer); }
      ReadResult assumeContentToken(SubString& buffer, SubString& comparisonContent, bool& isEqual) const
         {  AssumeCondition(plLexer) return plLexer->assumeContentToken(buffer, comparisonContent, isEqual); }
   };
   ContentReader getContentReader() { return ContentReader(*this); }

   void clear()
      {  tbCurrentToken.clear();
         rpmState = &GenericLexer::readCharsInitial;
         rpmFstState = nullptr;
         chContext = '\0';
         tToken = Token();
      }

   void setToXMLDeclaration();
   void setToDoctype();
   void setToElementHeader();
   void setToInitial();
   void setToElementFoot();
   void setToBody();
};

}} // end of namespace STG::XML

#endif // STG_XML_XMLLexerH
