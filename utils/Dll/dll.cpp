/**************************************************************************/
/*                                                                        */
/*  Copyright (C) 2013-2019                                               */
/*    CEA (Commissariat a l'Energie Atomique et aux Energies              */
/*         Alternatives)                                                  */
/*                                                                        */
/*  you can redistribute it and/or modify it under the terms of the GNU   */
/*  Lesser General Public License as published by the Free Software       */
/*  Foundation, version 2.1.                                              */
/*                                                                        */
/*  It is distributed in the hope that it will be useful,                 */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU Lesser General Public License for more details.                   */
/*                                                                        */
/*  See the GNU Lesser General Public License version 2.1                 */
/*  for more details (enclosed in the file LICENSE).                      */
/*                                                                        */
/**************************************************************************/

/////////////////////////////////
//
// Library   : Standard classes
// Unit      : Basic object
// File      : dll.cpp
// Description :
//   Implementation of basic material for dynamic library management.
//

#include "Dll/dll.h"

#ifdef __unix__
# include <dlfcn.h>
#elif _WIN32
# include <windows.h>
#else
# error \
  Unrecognized platform. Please implement functions of this file for your platform.
#endif

#include "TString/String.hpp"
#ifdef __unix__

namespace DLL
{

Library::Library() : _lib(nullptr) {}

Library::Library(const char* libName)
  : _lib(nullptr)
  { _lib = dlopen(libName, RTLD_LAZY);}

void
Library::setFromFile(const char* libName)
  { AssumeCondition(!_lib)
    _lib = dlopen(libName, RTLD_LAZY);
  }

Library::~Library()
  { if (_lib) dlclose(_lib); }

bool Library::isOpen(const char** message) const
  { if (_lib)
      return true;
    if (message)
      *message = dlerror();
    return false;
  }

DLL::Library::operator bool() const { return _lib; }

void Library::loadSymbol(const char* symbol, void** fun)
{
  AssumeCondition(_lib)
  *fun = dlsym(_lib, symbol);
  char* err = dlerror();
  if (err) {
     static STG::SString errorMessage;
     errorMessage.clear();
     STG::DIOObject::OSRSubString out(errorMessage);
     out << "Unable to load symbol '" << symbol << "': " << err;
     throw STG::EReadError(errorMessage.getChunk().string);
  }
}

} // DLL

#elif _WIN32

namespace DLL
{

Library::Library(const char* libName)
  { _lib = LoadLibrary(libName); }

void
Library::setFromFile(const char* libName)
  { if (_lib)
     throw STG::EReadError();
    _lib = LoadLibrary(libName);
  }

Library::~Library() { FreeLibrary(_lib); }

bool Library::isOpen(const char** message) const
  { if (_lib)
      return true;
    DWORD errorMessageID = GetLastError();
    if(errorMessageID == 0)
      return false; //No error message has been recorded

    if (message) {
      LPSTR messageBuffer = nullptr;
      size_t size = FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
                                   NULL, errorMessageID, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&messageBuffer, 0, NULL);
      static STG::SubString currentMessage = STG::SString();
      currentMessage.copy(currentMessageBuffer, size);
      LocalFree(currentMessageBuffer);
      *message = currentMessage.getChunk().string;
    }
    return false;
  }

void Library::loadSymbol(const char* symbol, void** fun)
{
  *fun = reinterpret_cast<void*>(GetProcAddress(_lib, symbol));
  if (*fun)
     return;
  static STG::SString errorMessage;
  errorMessage.clear();
  STG::DIOObject::TOSRSubString out(errorMessage);
  out << "Unable to load symbol '" << symbol << "': Error "
      << (long unsigned int) GetLastError();
  throw STG::EReadError(errorMessage.queryChunk().string);
}

} // DLL

#endif
