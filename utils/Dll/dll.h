/**************************************************************************/
/*                                                                        */
/*  Copyright (C) 2013-2019                                               */
/*    CEA (Commissariat a l'Energie Atomique et aux Energies              */
/*         Alternatives)                                                  */
/*                                                                        */
/*  you can redistribute it and/or modify it under the terms of the GNU   */
/*  Lesser General Public License as published by the Free Software       */
/*  Foundation, version 2.1.                                              */
/*                                                                        */
/*  It is distributed in the hope that it will be useful,                 */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU Lesser General Public License for more details.                   */
/*                                                                        */
/*  See the GNU Lesser General Public License version 2.1                 */
/*  for more details (enclosed in the file LICENSE).                      */
/*                                                                        */
/**************************************************************************/

/////////////////////////////////
//
// Library   : Standard classes
// Unit      : Basic object
// File      : dll.h
// Description :
//   Definition of basic material for dynamic library management.
//

#pragma once

namespace DLL
{

class Library
{
public:
#ifdef __unix__
  typedef void* type;
#elif _WIN32
  typedef HMODULE type;
#endif

  Library();
  Library(Library&& source)
    : _lib(source._lib) { source._lib = 0; }
  Library(const char* libName);
  ~Library();

  bool isOpen(const char** message=nullptr) const;
  operator bool() const;
  void setFromFile(const char* libName);

  template<typename _Func>
  inline void loadSymbol(const char* symbol, _Func* fun)
    { loadSymbol(symbol, reinterpret_cast<void**>(fun));}

private:
  void loadSymbol(const char* symbol, void** fun);
  type _lib;
};

}

