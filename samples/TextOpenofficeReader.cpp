#include "doc2json/header-doc.hpp"
#include <cassert>
#include <cstdlib>
#include <climits>
#include <cstring>
#include <vector>

inline bool
str2int (int &i, const std::string& str) {
   const char* s = str.c_str();
   char *end;
   long  l;
   errno = 0;
   l = strtol(s, &end, 0);
   if ((errno == ERANGE && l == LONG_MAX) || l > INT_MAX)
      return false;
   if ((errno == ERANGE && l == LONG_MIN) || l < INT_MIN)
      return false;
   if (*s == '\0' || *end != '\0')
      return false;
   i = l;
   return true;
}

void
DocumentConfiguration::load(XMLParserArguments& arguments, XMLParserStateStack& state) {
   while (!arguments.isOpenStartElement() || arguments.value() != "doc_to_json") {
      if (arguments.isOpenStartElement())
         state.skipNode(arguments);
      else if (arguments.isCloseElement()) {
         arguments.setToNextToken();
         return;
      }
      else
         arguments.setToNextToken();
   };
   // arguments.isOpenStartElement() && arguments.value() == "doc_to_json"
   arguments.setToNextToken();
   while (!arguments.isCloseStartElement())
      arguments.setToNextToken();
   arguments.setToNextToken();

   int sectionNumber = 0;
   while (!arguments.isCloseElement()) {
      if (arguments.isOpenStartElement()) {
         std::string title = arguments.value();
         if (title == "title")
            sectionNumber = 1;
         else if (title.compare(0, strlen("header"), "header") == 0) {
            if (str2int(sectionNumber, title.substr(strlen("header"))))
               ++sectionNumber;
            else
               sectionNumber = 0;
         }
         arguments.setToNextToken();

         if (sectionNumber > 0) {
            while (arguments.isAttribute()) {
               if (arguments.attribute() == "styleSection")
                  setKeyValue((std::string("header") + std::to_string(sectionNumber)).c_str(), arguments.value());
               else if (arguments.attribute() == "jsonText")
                  setKeyValue((std::string("json") + std::to_string(sectionNumber)).c_str(), arguments.value());
               arguments.setToNextToken();
            }
         }
         while (!arguments.isCloseStartElement())
            arguments.setToNextToken();
         arguments.setToNextToken();

         while (!arguments.isCloseElement()) {
            if (arguments.isOpenStartElement())
               state.skipNode(arguments);
            else
               arguments.setToNextToken();
         };
      };
      arguments.setToNextToken();
   };
}

bool
closeToHeader(JSonWriter& jsonWriter, int& currentHeader, int header)
{  if (currentHeader < header)
      return false;

   while (currentHeader > header) {
      jsonWriter.closeObject();
      if (currentHeader > 1) {
         jsonWriter.closeObject();
         jsonWriter.closeArray();
      }
      --currentHeader;
   };
   currentHeader = header;
   return true;
}

bool
closeAllHeaders(JSonWriter& jsonWriter, int& currentHeader)
{  if (currentHeader == 0)
      return false;
   while (currentHeader > 1) {
      jsonWriter.closeObject();
      jsonWriter.closeObject();
      jsonWriter.closeArray();
      --currentHeader;
   };
   if (currentHeader == 1)
      jsonWriter.closeObject();
   jsonWriter.end();
   currentHeader = 0;
   return true;
}

void
Document::load(XMLParserArguments& arguments, XMLParserStateStack& state) {
   int currentHeader = 0;
   bool isInHeader = false;
   bool isFirstInHeader = false;
   std::string textContent;
   bool isContinueText = false;
   std::vector<ExtString> styles;

   while (not arguments.isOpenStartElement() && not arguments.isCloseElement())
      arguments.setToNextToken();
   if (arguments.isCloseElement()) {
      arguments.setToNextToken();
      return;
   }
   // arguments.isOpenStartElement()
   while (arguments.value() != "office:document-content") {
      state.skipNode(arguments);
      while (not arguments.isOpenStartElement() && not arguments.isCloseElement())
         arguments.setToNextToken();
      if (arguments.isCloseElement())
         return;
   }
   arguments.setToNextToken();

   // skip xml attributes for header "office:document-content"
   while (not arguments.isCloseStartElement())
      arguments.setToNextToken();
   arguments.setToNextToken();

   while (not arguments.isCloseElement()) {
      if (arguments.isOpenStartElement()) {
         if (arguments.value() != "office:body") {
            state.skipNode(arguments);
            continue;
         }
         arguments.setToNextToken();
         while (not arguments.isCloseElement()) {
            if (arguments.isOpenStartElement()) {
               if (arguments.value() != "office:text") {
                  state.skipNode(arguments);
                  continue;
               }
               arguments.setToNextToken();

               // skip xml attributes for header "office:text"
               while (not arguments.isCloseStartElement())
                  arguments.setToNextToken();
               arguments.setToNextToken();

               while (not arguments.isCloseElement()) { // office:text
                  int newHeader = 0;
                  if (arguments.isOpenStartElement()) {
                     if (arguments.value() != "text:p" && arguments.value() != "text:h") {
                        state.skipNode(arguments);
                        continue;
                     }
                     arguments.setToNextToken();

                     // skip xml attributes for "text:p"
                     while (arguments.isAttribute()) {
                        if (arguments.attribute() == "text:style-name") {
                           int iterHeader = currentHeader+1;
                           int iterHeaderSave = iterHeader;
                           bool hasFound = false;
                           while (!hasFound && iterHeader > 0) {
                              if (iterHeader-1 == (int) (styles.size())) {
                                 styles.push_back(ExtString());
                                 styles.back() = configuration.getValue(
                                       std::string("header") + std::to_string(iterHeader));
                              }
                              if (arguments.value() == styles[iterHeader-1]) {
                                 newHeader = iterHeader;
                                 hasFound = true;
                              }
                              --iterHeader;
                           };
                           isInHeader = hasFound;
                           if (isInHeader && textContent.length() > 0) {
                              jsonWriter.addKey("text");
                              if (textContent.length() > 2)
                                 textContent.erase(textContent.length()-2, 2); // remove last '\n'
                              jsonWriter.addString(textContent);
                              textContent = std::string();
                              isContinueText = false;
                           }
                        };
                        arguments.setToNextToken();
                     }
                     while (not arguments.isCloseStartElement())
                        arguments.setToNextToken();
                     arguments.setToNextToken();

                     while (not arguments.isCloseElement()) { // text:p, text:h
                        if (arguments.isOpenStartElement()) {
                           if (arguments.value() != "text:span" || (isInHeader && newHeader == 0)) {
                              state.skipNode(arguments);
                              continue;
                           }
                           arguments.setToNextToken();
                           while (not arguments.isCloseStartElement())
                              arguments.setToNextToken();
                           arguments.setToNextToken();

                           while (not arguments.isCloseElement()) { // text:span
                              if (arguments.isExtendedText()) {
                                 bool isStart = isInHeader && !isContinueText && (newHeader == currentHeader + 1),
                                      isRestart = isInHeader && !isContinueText && (newHeader <= currentHeader);
                                 const char* idContent = !isInHeader ? nullptr
                                    :  configuration.getValue(
                                       std::string("json") + std::to_string(newHeader));
                                 if (isStart || isRestart) {
                                    if (isRestart) {
                                       closeToHeader(jsonWriter, currentHeader, newHeader);
                                       jsonWriter.closeObject();
                                       jsonWriter.closeObject();
                                    }
                                    if (newHeader == 1)
                                       jsonWriter.start();
                                    else { // newHeader > 1
                                       if (isStart) {
                                          jsonWriter.addKey("content");
                                          jsonWriter.openArray();
                                       }
                                       jsonWriter.openObject();
                                       jsonWriter.addKey("id");
                                       jsonWriter.addString(idContent);
                                       jsonWriter.addKey("content");
                                    };
                                    currentHeader = newHeader;
                                    isFirstInHeader = true;
                                 }
                                 else if (isInHeader && !isFirstInHeader && !isContinueText) {
                                    closeToHeader(jsonWriter, currentHeader, newHeader);
                                    // jsonWriter.closeObject();
                                 }
                                 isContinueText = true;
                                 textContent += arguments.extendedValue();
                              }
                              arguments.setToNextToken();
                           } // end of text:span
                        }
                        if (arguments.isExtendedText()) {
                           bool isStart = isInHeader && !isContinueText && (newHeader == currentHeader + 1),
                                isRestart = isInHeader && !isContinueText && (newHeader <= currentHeader);
                           const char* idContent = !isInHeader ? nullptr
                              :  configuration.getValue(
                                 std::string("json") + std::to_string(newHeader));
                           if (isStart || isRestart) {
                              if (isRestart) {
                                 closeToHeader(jsonWriter, currentHeader, newHeader);
                                 jsonWriter.closeObject();
                                 jsonWriter.closeObject();
                              }
                              if (newHeader == 1)
                                 jsonWriter.start();
                              else { // newHeader > 1
                                 if (isStart) {
                                    jsonWriter.addKey("content");
                                    jsonWriter.openArray();
                                 }
                                 jsonWriter.openObject();
                                 jsonWriter.addKey("id");
                                 jsonWriter.addString(idContent);
                                 jsonWriter.addKey("content");
                              };
                              currentHeader = newHeader;
                              isFirstInHeader = true;
                           }
                           else if (isInHeader && !isFirstInHeader && !isContinueText) {
                              closeToHeader(jsonWriter, currentHeader, newHeader);
                              // jsonWriter.closeObject();
                           }
                           isContinueText = true;
                           textContent += arguments.extendedValue();
                        }
                        arguments.setToNextToken();
                     } // end of text:p
                     if (isInHeader) {
                        isInHeader = false;
                        isFirstInHeader = false;
                        jsonWriter.openObject();
                        jsonWriter.addKey("title");
                        jsonWriter.addString(textContent);
                        textContent = std::string();
                        isContinueText = false;
                     }
                     else
                        textContent += "\\n";
                  }
                  arguments.setToNextToken();
               } // end of office:text
            }
            arguments.setToNextToken();
         } // end of office:body
         if (textContent.length() > 0) {
            jsonWriter.addKey("text");
            if (textContent.length() > 2)
               textContent.erase(textContent.length()-2, 2); // remove last '\n'
            jsonWriter.addString(textContent);
            textContent = std::string();
            isContinueText = false;
         }
      }
      arguments.setToNextToken();
   } // end of office:document-content
   closeAllHeaders(jsonWriter, currentHeader);
}

