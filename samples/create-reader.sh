#!/bin/bash
INPUT_FILE=$1
if [ $1 = "-I" ]
then
  DOC2JSON_INSTALL_DIR=$2
  INPUT_FILE=$3
fi

if [ -z ${DOC2JSON_INSTALL_DIR} ]
then
  LOCATE_BIN=$(which create-parse-algorithm) ;
  if [ $? -eq 0 ]
  then
    DOC2JSON_INSTALL_DIR=$(dirname $(dirname $LOCATE_BIN)) ;
  else
    echo "you should set the environment variable DOC2JSON_INSTALL_DIR with the directory containing bin/doc2json"
    exit 1
  fi
fi

INSTR_FILE=${INPUT_FILE}
TARGET_DLL_FILE=${INPUT_FILE}
if [[ ${INPUT_FILE} =~ ^(.*).cpp$ ]];
then 
  INSTR_FILE=${BASH_REMATCH[1]}Instr.cpp ; 
  TARGET_DLL_FILE=$(dirname ${INPUT_FILE})/lib$(basename ${BASH_REMATCH[1]}).so ; 
elif [[ $1 =~ ^(.*).cc$ ]];
then 
  INSTR_FILE=${BASH_REMATCH[1]}Instr.cc ; 
  TARGET_DLL_FILE=$(dirname ${INPUT_FILE})/lib$(basename ${BASH_REMATCH[1]}).so ; 
else 
  echo "the parsing algorithm should have .cpp or .cc extension"
  exit 1
fi

${DOC2JSON_INSTALL_DIR}/bin/create-parse-algorithm -extra-arg="-I${DOC2JSON_INSTALL_DIR}/include" ${INPUT_FILE} -- > ${INSTR_FILE}
/usr/bin/c++ -g -O2 -fPIC -shared -Wl,-soname,${TARGET_DLL_FILE} -std=c++17 -o ${TARGET_DLL_FILE} \
  -DCodeReader_EXPORTS -DINSTR -I${DOC2JSON_INSTALL_DIR}/include \
  ${DOC2JSON_INSTALL_DIR}/include/doc2json/header-doc.cpp ${INSTR_FILE}

