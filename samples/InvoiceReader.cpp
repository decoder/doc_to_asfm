#include "doc2json/header-doc.hpp"
#include <cassert>
#include <cstdlib>
#include <climits>
#include <iostream>

inline bool
str2int (int &i, const std::string& str) {
   const char* s = str.c_str();
   char *end;
   long  l;
   errno = 0;
   l = strtol(s, &end, 0);
   if ((errno == ERANGE && l == LONG_MAX) || l > INT_MAX)
      return false;
   if ((errno == ERANGE && l == LONG_MIN) || l < INT_MIN)
      return false;
   if (*s == '\0' || *end != '\0')
      return false;
   i = l;
   return true;
}

inline bool
str2double (double &val, const std::string& str) {
   const char* s = str.c_str();
   char *end;
   errno = 0;
   val = strtod(s, &end);
   if (errno == ERANGE)
      return false;
   if (*s == '\0' || *end != '\0')
      return false;
   return true;
}

inline bool
str2double_with_procent (double &val, const std::string& str) {
   const char* s = str.c_str();
   char *end;
   errno = 0;
   val = strtod(s, &end);
   if (errno == ERANGE)
      return false;
   if (*s == '\0')
      return false;
   while (*end != '\0' && isspace(*end)) ++end;
   if (*end == '%') {
      ++end;
      val /= 100.0;
      while (*end != '\0' && isspace(*end)) ++end;
   }
   if (*end != '\0')
      return false;
   return true;
}

inline bool
str2range(int& min, int& max, const std::string& str, bool* isSingle=nullptr) {
   const char* s = str.c_str();
   char *end;
   long  l;
   errno = 0;
   l = strtol(s, &end, 0);
   if ((errno == ERANGE && l == LONG_MAX) || l > INT_MAX)
      return false;
   if ((errno == ERANGE && l == LONG_MIN) || l < INT_MIN)
      return false;
   if (*s == '\0' || (*end != '\0' && *end != '-'))
      return false;
   // *s != 0 && 
   min = l;
   if (*end == '\0') {
      if (isSingle)
         *isSingle = true;
      max = min;
      return true;
   }
   char* end_max;
   l = strtol(end+1, &end_max, 0);
   if ((errno == ERANGE && l == LONG_MAX) || l > INT_MAX)
      return false;
   if ((errno == ERANGE && l == LONG_MIN) || l < INT_MIN)
      return false;
   if (*(end+1) == '\0' || (*end_max != '\0'))
      return false;
   max = l;
   if (isSingle)
      *isSingle = false;
   return true;
}

enum TypeSection
   {  TSInvalid, TSAddress, TSAddress2, TSDate, TSPhone, TSInvoiceId, TSBillTo, TSServiceTo,
      TSProducts, TSDiscount, TSTotal, TSPaid, TSEnd
   };

const char* dict[TSEnd]
   =  {  "invalid", "address", "address2", "date", "phone", "id", "bill to", "service to",
         "products", "discount", "total", "paid"
      };

enum TypeProductSection
   {  TPSInvalid, TPSProductId, TPSDescription, TPSQuantity, TPSUnitPrice, TPSTotal, TPSEnd
   };

const char* productDict[TPSEnd]
   =  {  "invalid", "id", "description", "quantity", "unit price", "total" };

struct Position {
   int firstStaticLine=-1, lastStaticLine=-1;
   int firstStaticColumn=-1, lastStaticColumn=-1;
   std::string triggerText;
   mutable int firstTriggerLine=-1, lastTriggerLine=-1;
   mutable int firstTriggerColumn=-1, lastTriggerColumn=-1;
   enum LocalPosition { LPUndefined, LPAfter, LPBelow } localPosition = LPUndefined;
   std::string endTriggerText;
   int firstEndTriggerLine=-1, lastEndTriggerLine=-1;
   int firstEndTriggerColumn=-1, lastEndTriggerColumn=-1;

   bool isValid() const { return firstStaticLine >= 0 || triggerText.length() > 0; }
   bool doesAccept(int line, int col, int& nextLine, int& nextCol, bool& isNoMoreActive,
         bool& doesRequireText, const char* text) const;
};

bool
Position::doesAccept(int line, int col, int& nextLine, int& nextCol, bool& isNoMoreActive,
      bool& doesRequireText, const char* text) const {
   if (!isValid())
      return false;
   if (firstStaticLine >= 0 && firstTriggerLine < 0) {
      if (!(firstStaticLine <= line && line <= lastStaticLine)) {
         if (firstStaticLine < nextLine || nextLine <= 0) {
            nextLine = firstStaticLine;
            nextCol = 0;
         }
         if (lastStaticLine >= 0 && line > lastStaticLine)
            isNoMoreActive = true;
         return false;
      }
   }
   if (triggerText.length() == 0 && firstStaticColumn >= 0 && firstTriggerColumn < 0) {
      if (!(firstStaticColumn <= col && col <= lastStaticColumn)) {
         if (lastStaticColumn < nextCol || nextCol <= 0)
            nextCol = lastStaticColumn;
         return false;
      }
   }
   if (firstTriggerLine < 0 && triggerText.length() > 0 && localPosition) {
      if (text && triggerText != text)
         return false;
      if (localPosition == LPAfter) {
         if (firstTriggerLine < 0) {
            if (!text)
               return doesRequireText = true;
            firstTriggerLine = line;
            firstTriggerColumn = col+1;
            if (lastStaticLine >= 0)
               lastTriggerLine += firstTriggerLine;
            if (lastTriggerColumn >= 0)
               lastTriggerColumn += firstTriggerColumn;
         }
      }
      else { // localPosition = LPBelow
         if (firstTriggerLine < 0) {
            if (!text)
               return doesRequireText = true;
            firstTriggerLine = line+1;
            firstTriggerColumn = col;
            if (lastTriggerLine >= 0)
               lastTriggerLine += firstTriggerLine;
            if (lastTriggerColumn >= 0)
               lastTriggerColumn += firstTriggerColumn;
         }
      }
      if (!(firstTriggerLine <= line && (line <= lastTriggerLine || lastTriggerLine < 0))) {
         if (firstTriggerLine < nextLine || nextLine <= 0) {
            nextLine = firstTriggerLine;
            nextCol = 0;
         }
         if (lastTriggerLine >= 0 && line > lastTriggerLine)
            isNoMoreActive = true;
         return false;
      }
      if (firstTriggerColumn >= 0) {
         if (!(firstTriggerColumn <= col && col <= lastTriggerColumn)) {
            if (lastTriggerColumn < nextCol || nextCol <= 0)
               nextCol = lastTriggerColumn;
            return false;
         }
      }
   }
   if (firstTriggerLine >= 0) {
      if (line < firstTriggerLine)
         return false;
      if (endTriggerText.length() > 0 && localPosition) {
         nextLine = line;
         nextCol = col;
         if (text && endTriggerText == text) {
            isNoMoreActive = true;
            return false;
         }
         if (!text)
            return doesRequireText = true;
      }
   }
   return true;
}

struct ProductColumn {
   int firstStaticColumn=-1, lastStaticColumn=-1;

   bool doesAccept(int col) const
      {  return col >= firstStaticColumn && col <= lastStaticColumn; }
};

// [TODO] move inside DocumentConfiguration
struct ConfigInfo {
   int numberOfColumns=0;
   Position positions[TSEnd];
   ProductColumn productPositions[TPSEnd];

   void updateActiveSection(uint32_t& possibleActiveSections, uint32_t& inactiveSections,
         uint32_t& sureActiveSections, uint32_t& futureActiveSections, int& nextLine, int& nextCol, bool& doesRequireText,
         const char* text, uint32_t& sureActiveProductSections)
      {  int line = nextLine, col = nextCol;
         nextLine = nextCol = 0;
         for (TypeSection section = (TypeSection) (TSInvalid+1); section < TSEnd;
               section = (TypeSection) (section+1)) {
            if (possibleActiveSections & (1U << section)) {
               bool isNoMoreActive = false;
               if (positions[section].doesAccept(line, col, nextLine, nextCol, isNoMoreActive,
                        doesRequireText, text)) {
                  if (!doesRequireText || text) {
                     sureActiveSections |= (1U << section);
                     if (section == TSProducts)
                        futureActiveSections |= (1U << section);
                  }
                  if (section == TSProducts && !doesRequireText) {
                     for (TypeProductSection productSection=(TypeProductSection) (TPSInvalid+1);
                           productSection < TPSEnd;
                           productSection = (TypeProductSection) (productSection+1)) {
                        if (productPositions[productSection].doesAccept(col))
                           sureActiveProductSections |= (1U << productSection);
                     }
                  }
               }
               else {
                  if (isNoMoreActive)
                     inactiveSections |= (1U << section);
                  else
                     futureActiveSections |= (1U << section);
                  possibleActiveSections &= ~(1U << section);
               }
            }
         }
      }
} configInfo;


void
DocumentConfiguration::load(XMLParserArguments& arguments, XMLParserStateStack& state) {
   while (!arguments.isOpenStartElement() || arguments.value() != "doc_to_json") {
      if (arguments.isOpenStartElement())
         state.skipNode(arguments);
      else if (arguments.isCloseElement()) {
         arguments.setToNextToken();
         return;
      }
      else
         arguments.setToNextToken();
   };
   // arguments.isOpenStartElement() && arguments.value() == "doc_to_json"
   arguments.setToNextToken();
   while (!arguments.isCloseStartElement())
      arguments.setToNextToken();
   arguments.setToNextToken();

   TypeSection typeSection = TSInvalid;
   while (!arguments.isCloseElement()) {
      if (arguments.isOpenStartElement()) {
         if (arguments.value() != "table") {
            state.skipNode(arguments);
            continue;
         }
         arguments.setToNextToken();
         while (arguments.isAttribute()) {
            if (arguments.attribute() == "number_of_columns") {
               if (!str2int(configInfo.numberOfColumns, arguments.value()))
                  std::cerr << "wrong number of columns in configuration file!" << std::endl;
            }
            arguments.setToNextToken();
         }
         while (!arguments.isCloseStartElement())
            arguments.setToNextToken();
         arguments.setToNextToken();

         while (!arguments.isCloseElement()) {
            if (arguments.isOpenStartElement()) {
               if (arguments.value() == "address")
                  typeSection = TSAddress;
               else if (arguments.value() == "address2")
                  typeSection = TSAddress2;
               else if (arguments.value() == "date")
                  typeSection = TSDate;
               else if (arguments.value() == "phone")
                  typeSection = TSPhone;
               else if (arguments.value() == "invoice_id")
                  typeSection = TSInvoiceId;
               else if (arguments.value() == "bill_to")
                  typeSection = TSBillTo;
               else if (arguments.value() == "service_to")
                  typeSection = TSServiceTo;
               else if (arguments.value() == "products")
                  typeSection = TSProducts;
               else if (arguments.value() == "discount")
                  typeSection = TSDiscount;
               else if (arguments.value() == "total")
                  typeSection = TSTotal;
               else if (arguments.value() == "paid")
                  typeSection = TSPaid;
               else
                  typeSection = TSInvalid;
               arguments.setToNextToken();

               while (arguments.isAttribute()) {
                  if (arguments.attribute() == "lines") {
                     bool isOk;
                     if (!configInfo.positions[typeSection].localPosition)
                        isOk = str2range(configInfo.positions[typeSection].firstStaticLine,
                              configInfo.positions[typeSection].lastStaticLine, arguments.value());
                     else
                        isOk = str2range(configInfo.positions[typeSection].firstTriggerLine,
                              configInfo.positions[typeSection].lastTriggerLine, arguments.value());
                     if (!isOk)
                        std::cerr << "wrong lines in configuration file!" << std::endl;
                  }
                  else if (arguments.attribute() == "columns") {
                     bool isOk;
                     if (!configInfo.positions[typeSection].localPosition)
                        isOk = str2range(configInfo.positions[typeSection].firstStaticColumn,
                           configInfo.positions[typeSection].lastStaticColumn, arguments.value());
                     else
                        isOk = str2range(configInfo.positions[typeSection].firstTriggerColumn,
                           configInfo.positions[typeSection].lastTriggerColumn, arguments.value());
                     if (!isOk)
                        std::cerr << "wrong columns in configuration file!" << std::endl;
                  }
                  else if (arguments.attribute() == "position") {
                     if (arguments.value() == "after")
                        configInfo.positions[typeSection].localPosition = Position::LPAfter;
                     else if (arguments.value() == "below")
                        configInfo.positions[typeSection].localPosition = Position::LPBelow;
                     else
                        std::cerr << "wrong position in configuration file!" << std::endl;
                  }
                  else if (arguments.attribute() == "lines_at_text")
                     configInfo.positions[typeSection].triggerText = arguments.value();
                  else if (arguments.attribute() == "lines_at") {
                     if (!str2range(configInfo.positions[typeSection].firstStaticLine,
                              configInfo.positions[typeSection].lastStaticLine, arguments.value()))
                        std::cerr << "wrong lines in configuration file!" << std::endl;
                  }
                  else if (arguments.attribute() == "columns_at") {
                     if (!str2range(configInfo.positions[typeSection].firstStaticColumn,
                              configInfo.positions[typeSection].lastStaticColumn, arguments.value()))
                        std::cerr << "wrong lines in configuration file!" << std::endl;
                  }
                  else if (arguments.attribute() == "end_text")
                     configInfo.positions[typeSection].endTriggerText = arguments.value();
                  else if (arguments.attribute() == "end_lines_at") {
                     if (!str2range(configInfo.positions[typeSection].firstEndTriggerLine,
                              configInfo.positions[typeSection].lastEndTriggerLine, arguments.value()))
                        std::cerr << "wrong lines in configuration file!" << std::endl;
                  }
                  else if (arguments.attribute() == "end_columns_at") {
                     if (!str2range(configInfo.positions[typeSection].firstEndTriggerColumn,
                              configInfo.positions[typeSection].lastEndTriggerColumn, arguments.value()))
                        std::cerr << "wrong lines in configuration file!" << std::endl;
                  }
                  arguments.setToNextToken();
               }
               while (!arguments.isCloseStartElement())
                  arguments.setToNextToken();
               arguments.setToNextToken();

               while (!arguments.isCloseElement()) {
                  if (arguments.isOpenStartElement()) {
                     TypeProductSection productSection = TPSInvalid;
                     if (typeSection == TSProducts && arguments.value() == "id")
                        productSection = TPSProductId;
                     else if (typeSection == TSProducts && arguments.value() == "description")
                        productSection = TPSDescription;
                     else if (typeSection == TSProducts && arguments.value() == "quantity")
                        productSection = TPSQuantity;
                     else if (typeSection == TSProducts && arguments.value() == "unit_price")
                        productSection = TPSUnitPrice;
                     else if (typeSection == TSProducts && arguments.value() == "total")
                        productSection = TPSTotal;
                     else {
                        state.skipNode(arguments);
                        continue;
                     }
                     arguments.setToNextToken();
                     while (arguments.isAttribute()) {
                        if (arguments.attribute() == "columns") {
                           if (!str2range(configInfo.productPositions[productSection].firstStaticColumn,
                                 configInfo.productPositions[productSection].lastStaticColumn, arguments.value()))
                              std::cerr << "wrong columns in configuration file!" << std::endl;
                        }
                        arguments.setToNextToken();
                     }
                     while (!arguments.isCloseStartElement())
                        arguments.setToNextToken();
                     state.skipNodeContent(arguments);
                  }
                  else
                     arguments.setToNextToken();
               }
            }
            arguments.setToNextToken();
         }
      }
      arguments.setToNextToken();
   };
}

void
Document::load(XMLParserArguments& arguments, XMLParserStateStack& state) {
   while (not arguments.isOpenStartElement() && not arguments.isCloseElement())
      arguments.setToNextToken();
   if (arguments.isCloseElement()) {
      arguments.setToNextToken();
      return;
   }
   // arguments.isOpenStartElement()
   while (arguments.value() != "office:document-content") {
      state.skipNode(arguments);
      while (not arguments.isOpenStartElement() && not arguments.isCloseElement())
         arguments.setToNextToken();
      if (arguments.isCloseElement())
         return;
   }
   arguments.setToNextToken();

   jsonWriter.start();
   jsonWriter.openObject();
   jsonWriter.openArray();
   // skip xml attributes for header "office:document-content"
   while (not arguments.isCloseStartElement())
      arguments.setToNextToken();
   arguments.setToNextToken();

   while (not arguments.isCloseElement()) { // office:document-content
      if (arguments.isOpenStartElement()) {
         if (arguments.value() != "office:body") {
            state.skipNode(arguments);
            continue;
         }
         arguments.setToNextToken();

         while (not arguments.isCloseStartElement())
            arguments.setToNextToken();
         arguments.setToNextToken();

         while (not arguments.isCloseElement()) { // office:body
            if (arguments.isOpenStartElement()) {
               if (arguments.value() != "office:spreadsheet") {
                  state.skipNode(arguments);
                  continue;
               }
               arguments.setToNextToken();
               while (not arguments.isCloseStartElement())
                  arguments.setToNextToken();
               arguments.setToNextToken();

               while (not arguments.isCloseElement()) { // office:spreadsheet
                  if (arguments.isOpenStartElement()) {
                     if (arguments.value() != "table:table") {
                        state.skipNode(arguments);
                        continue;
                     }
                     arguments.setToNextToken();
                     while (not arguments.isCloseStartElement())
                        arguments.setToNextToken();
                     arguments.setToNextToken();

                     int line = 0, nextLine = 0, nextColumn = 0;
                     uint32_t possibleActiveSections = ~((~0U << TSEnd) | 1U);
                     uint32_t sureActiveSections = 0, inactiveSections = 0, futureActiveSections = 0;
                     bool doesAccept = false;
                     bool isProduct = false;
                     bool hasValidProductId = false;
                     uint32_t sureActiveProductSections = 0;
                     bool doesOpenIfContent = true;
                     jsonWriter.openObject();
                     while (not arguments.isCloseElement()) { // table:table
                        if (arguments.isOpenStartElement()) {
                           if (arguments.value() != "table:table-row") {
                              state.skipNode(arguments);
                              continue;
                           }
                           arguments.setToNextToken();
                           while (not arguments.isCloseStartElement())
                              arguments.setToNextToken();
                           arguments.setToNextToken();
                           ++line;

                           int column = 1;
                           while (not arguments.isCloseElement()) { // table:row
                              if (arguments.isOpenStartElement()) {
                                 if (arguments.value() != "table:table-cell") {
                                    state.skipNode(arguments);
                                    continue;
                                 }
                                 arguments.setToNextToken();
                                 int columnIncrement = 1;
                                 bool doesRequireText = false;
                                 doesAccept = (nextLine <= 0 || nextColumn <= 0
                                       || ((line > nextLine
                                          || (line == nextLine && column >= nextColumn))));
                                 if (doesAccept) {
                                    nextLine = line; nextColumn = column;
                                    configInfo.updateActiveSection(possibleActiveSections,
                                          inactiveSections, sureActiveSections, futureActiveSections,
                                          nextLine, nextColumn, doesRequireText, nullptr, sureActiveProductSections);
                                    doesAccept = sureActiveSections | possibleActiveSections;
                                 };
                                 if (sureActiveSections & (1U << TSProducts)) {
                                    if (!isProduct) {
                                       jsonWriter.openObject();
                                       jsonWriter.addKey(dict[TSProducts]);
                                       jsonWriter.openArray();
                                       doesOpenIfContent = true;
                                       isProduct = true;
                                    }
                                    else if (column == 1) {
                                       if (!doesOpenIfContent) {
                                          jsonWriter.closeObject();
                                          doesOpenIfContent = true;
                                       }
                                    }
                                 }
                                 else if (!(possibleActiveSections & (1U << TSProducts)) && isProduct) {
                                    if (!doesOpenIfContent)
                                       jsonWriter.closeObject();
                                    jsonWriter.closeArray();
                                    isProduct = false;
                                 }

                                 while (arguments.isAttribute()) {
                                    if (arguments.attribute() == "table:number-columns-repeated")
                                       str2int(columnIncrement, arguments.value());
                                    else if (arguments.attribute() == "table:number-columns-spanned")
                                       str2int(columnIncrement, arguments.value());
                                    arguments.setToNextToken();
                                 }
                                 while (not arguments.isCloseStartElement())
                                    arguments.setToNextToken();
                                 arguments.setToNextToken();
                                 std::string textCell;
                                 while (not arguments.isCloseElement()) { // table:table-cell
                                    if (arguments.isOpenStartElement()) {
                                       if (!doesAccept || arguments.value() != "text:p") {
                                          state.skipNode(arguments);
                                          continue;
                                       }
                                       arguments.setToNextToken();
                                       while (not arguments.isCloseStartElement())
                                          arguments.setToNextToken();
                                       arguments.setToNextToken();
                                       while (not arguments.isCloseElement()) { // text:p
                                          if (arguments.isOpenStartElement()) {
                                             state.skipNode(arguments);
                                             continue;
                                          }
                                          if (arguments.isExtendedText())
                                             textCell += arguments.extendedValue();
                                          arguments.setToNextToken();
                                       }
                                    }
                                    arguments.setToNextToken();
                                 } // end of table:table-cell

                                 if (possibleActiveSections && doesRequireText) { // doesAccept
                                    int newNextLine = line, newNextColumn = column;
                                    doesRequireText = false;
                                    configInfo.updateActiveSection(possibleActiveSections,
                                          inactiveSections, sureActiveSections, futureActiveSections,
                                          newNextLine, newNextColumn, doesRequireText, textCell.c_str(), sureActiveProductSections);
                                    if (newNextLine < nextLine) {
                                       nextLine = newNextLine;
                                       nextColumn = newNextColumn;
                                    }
                                    else if (newNextLine == nextLine && newNextColumn < nextColumn)
                                       nextColumn = newNextColumn;
                                    doesAccept = sureActiveSections;
                                    if (sureActiveSections & (1U << TSProducts)) {
                                       if (!isProduct) {
                                          jsonWriter.addKey(dict[TSProducts]);
                                          jsonWriter.openArray();
                                          doesOpenIfContent = true;
                                          isProduct = true;
                                       }
                                       else if (column == 1) {
                                          if (hasValidProductId) {
                                             if (!doesOpenIfContent) {
                                                jsonWriter.closeObject();
                                                doesOpenIfContent = true;
                                             }
                                             hasValidProductId = false;
                                          }
                                       }
                                    }
                                    else if (isProduct) {
                                       if (!doesOpenIfContent)
                                          jsonWriter.closeObject();
                                       jsonWriter.closeArray();
                                       isProduct = false;
                                    }
                                 }
                                 if (doesAccept) {
                                    for (TypeSection section = (TypeSection) (TSInvalid+1);
                                          section < TSEnd; section = (TypeSection) (section+1)) {
                                       if (sureActiveSections & (1U << section)) {
                                          if (section == TSProducts) {
                                             for (TypeProductSection productSection
                                                      = (TypeProductSection) (TPSInvalid+1);
                                                   productSection < TPSEnd;
                                                   productSection = (TypeProductSection) (productSection+1)) {
                                                if (sureActiveProductSections & (1U << productSection)) {
                                                   if (column == 1)
                                                      hasValidProductId = textCell != "";
                                                   if (hasValidProductId) {
                                                      if (doesOpenIfContent) {
                                                         jsonWriter.openObject();
                                                         doesOpenIfContent = false;
                                                      }
                                                      jsonWriter.addKey(productDict[productSection]);
                                                      if (productSection == TPSProductId) {
                                                         int val = 0;
                                                         if (!str2int(val, textCell))
                                                            std::cerr << "invalid product id!" << std::endl;
                                                         jsonWriter.addIValue(val);
                                                      }
                                                      else if (productSection == TPSQuantity
                                                            || productSection == TPSUnitPrice
                                                            || productSection == TPSTotal) {
                                                         double val = 0;
                                                         if (!str2double(val, textCell))
                                                            std::cerr << ((productSection == TPSQuantity) ? "invalid quantity!" : (productSection == TPSUnitPrice ? "invalid unit price!" : "invalid total!")) << std::endl;
                                                         jsonWriter.addFloat(val);
                                                      }
                                                      else
                                                         jsonWriter.addString(textCell);
                                                   }
                                                }
                                             }
                                          }
                                          else {
                                             jsonWriter.addKey(dict[section]);
                                             if (section == TSInvoiceId) {
                                                int val = 0;
                                                if (!str2int(val, textCell))
                                                   std::cerr << "invalid invoice id!" << std::endl;
                                                jsonWriter.addIValue(val);
                                             }
                                             else if (section == TSDiscount) {
                                                double val = 0;
                                                if (!str2double_with_procent(val, textCell))
                                                   std::cerr << "invalid discount!";
                                                jsonWriter.addFloat(val);
                                             }
                                             else if (section == TSTotal || section == TSPaid) {
                                                double val = 0;
                                                if (!str2double(val, textCell))
                                                   std::cerr << ((section == TSTotal) ? "invalid total!" : "invalid paid!") << std::endl;
                                                jsonWriter.addFloat(val);
                                             }
                                             else
                                                jsonWriter.addString(textCell);
                                          }
                                       }
                                    }
                                 }
                                 textCell.clear();
                                 possibleActiveSections = futureActiveSections;
                                 inactiveSections |= (sureActiveSections & ~(1U << TSProducts));
                                 futureActiveSections = sureActiveSections = sureActiveProductSections = 0U;
                                 column += columnIncrement;
                              }
                              arguments.setToNextToken();
                           } // end of table:row
                        }
                        arguments.setToNextToken();
                     } // end of table:table
                     if (isProduct) {
                        if (doesOpenIfContent)
                           jsonWriter.closeObject();
                        jsonWriter.closeArray();
                        isProduct = false;
                     }
                     jsonWriter.closeObject();
                  }
                  arguments.setToNextToken();
               } // end of office:spreadsheet
            }
            arguments.setToNextToken();
         } // end of office:body
      }
      arguments.setToNextToken();
   } // end of office:document-content
   jsonWriter.closeArray();
   jsonWriter.closeObject();
   jsonWriter.end();
}

