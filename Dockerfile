ARG PUID=1000
ARG GUID=1000
FROM ubuntu:20.04

RUN apt update -y && DEBIAN_FRONTEND="noninteractive" apt install -y \
    cmake make g++ zlib1g-dev clang libclang-dev bash

ARG PUID
ARG GUID

# create a group and a user
RUN groupadd -g ${GUID} doc && \
    useradd -r -u ${PUID} -g doc -d /doc2json doc && \
    mkdir -m 700 /doc2json && \
    chown doc:doc /doc2json

USER doc
WORKDIR /doc2json

# set the environment variables
RUN mkdir /doc2json/bin
ENV PATH="/doc2json/bin:${PATH}"
ENV DOC2JSON_INSTALL_DIR="/doc2json"

# install doc2json
COPY --chown=doc:doc . /doc2json/src
RUN cd /doc2json/src && \
    rm -rf build && \
    mkdir build && \
    cd build && \
    cmake -DCMAKE_INSTALL_PREFIX:PATH=$PWD/../.. .. && \
    make -j 4 && \
    make install

CMD [ "bash" ]

