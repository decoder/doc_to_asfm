The application doc2json
========================

This application is a product of the H2020 DECODER project.

It translates a structured documentation that follows a given template into a
`json` document.

It is applied in the DECODER project to extract information from a developer's
documentation written in word. This `json` document contains information that the
other DECODER tools can understand. In this context, the `json` format follows
the Abstract Semi Formal Modeling language and can be sent directly to the
Persistent Knowledge Monitor of DECODER.

It may have other applications in a general context. With appropriate parsing
algorithms supplied by the user, it can extracts data from any structured
documentation. The [samples](samples) directory contains algorithms to extract
the text of a word/openoffice document into a `json` format that nests the
sections of the document. It also contains algorithms to extract data from
invoices following the same openoffice template.

# Installation of the application

The applications requires [zlib](https://zlib.net/) to retrieve the content
of the documents and [Clang Tools](https://clang.llvm.org/docs/ClangTools.html)
to convert the user's parsing algorithm into an interruptible reading algorithm.

To install these libraries, you can type the following commands:

```sh
> sudo apt-get install zlib1g-dev clang libclang-dev
> apt list --installed "libclang*-dev"
```

If the clang version is less than clang-10 (for instance clang-6), the next cmake
build process may fail and you need to update to clang-10 with the following commands

```sh
> sudo apt-get install clang-10 libclang-10-dev
> sudo apt-get purge --auto-remove libclang-common-6.0-dev
> sudo ln -s /usr/bin/clang-10 /usr/bin/clang
```

Note that if after having tested doc2json, you need to revert to your original clang 6 version,
just type

```sh
# do no use these commands before having built the project and the algorithms libraries
> sudo rm /usr/bin/clang
> sudo apt-get install clang
```

Then you can download and build the project

Here are the compilation commands
```sh
> git clone git@gitlab.ow2.org:decoder/doc_to_asfm.git doc_to_asfm
> cd doc_to_asfm
> mkdir build
> cd build
> cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=$PWD ..
> make -j 4
# optional
# > make test
# > make install
```

`make test` checks that the extraction works on the developer's documentation
of the project `doc2json` itself.

`make install` installs the parsing algorithms of the directory [samples](samples)
with their associated configuration files. They can be tested with the following
commands (in the `build` directory)

```sh
> ./bin/doc2json ./share/doc2json/libInvoiceReader.so -c ./share/doc2json/config-invoice.xml ../test/invoice.ods -o test/invoice.json
> ./bin/doc2json ./share/doc2json/libTextWordReader.so -c ./share/doc2json/config-text-word.xml ../test/StructuredDocument_french.docx -o test/StructuredDocument_french_word.json
> ./bin/doc2json ./share/doc2json/libTextOpenofficeReader.so -c ./share/doc2json/config-text-openoffice.xml ../test/StructuredDocument_french.odt -o test/StructuredDocument_french_openoffice.json
```

to extract the content of the documents in the `json` format.

Then

```sh
diff test/StructuredDocument_french_openoffice.json test/StructuredDocument_french_word.json
```

should show no differences between the two `json` extracted files even if the origin format
(word versus openoffice/opendocument) is very different.

A utility `create-reader.sh` is provided to generate a parsing library from a custom
user's parsing algorithm. Hence the command

```sh
./bin/create-reader.sh -I . share/doc2json/InvoiceReader.cpp
```

regenerate the parsing library `share/doc2json/libInvoiceReader.so` from the parsing algorithm
`share/doc2json/InvoiceReader.cpp`.


# Current capabilities

The current test translates a developer's documentation written in word format
with the following structuration for the documentation

* application description
* unit description
* class description
    * types inside the class
    * macros inside the class
    * methods inside the class

The command (one of the test)

```sh
> ./build/bin/doc_to_asfm ./build/samples/libCodeReader.so -c test/config.xml utils/StandardClasses/doc/Standard.docx -o test/standard.json
```

parses the word file `Standard.docx`, retrieves the documentation structure
and persists it into the `standard.json` file.

# Usage

The basic usage consists in comparing the content of the documentation with
the content of the code. It should provide a way to start the review of an
existing project

* start the application verification with the leaf units
* start the unit verification with the leaf classes
* start the class verification with
    * the leaf methods
    * the constructors, the destructors
    * more and more arbitrary combination of methods inside the class

The review consists in verifying that the documentation is consistent with the
code and that the application progressively provides more and more high-level
concepts.




